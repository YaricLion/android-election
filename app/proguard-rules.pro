# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in d:\work\androidstudio\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-verbose
-dontoptimize
-dontpreverify

-keepattributes *Annotation*
-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgent
-keep public class * extends android.preference.Preference
-keep public class * extends android.support.v4.app.Fragment
-keep public class * extends android.app.Fragment
# For native methods, see http://proguard.sourceforge.net/manual/examples.html#native
-keepclasseswithmembernames class * {
    native <methods>;
}
-keep public class * extends android.view.View {
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
    public void set*(...);
}
-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}
-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}
-keepclassmembers class * extends android.app.Activity {
   public void *(android.view.View);
}

# For enumeration classes, see http://proguard.sourceforge.net/manual/examples.html#enumerations
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}
-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}
-keepclassmembers class **.R$* {
    public static <fields>;
}

#-keep public class * {
#    public protected *;
#}


-assumenosideeffects class android.util.Log {
    public static *** d(...);
    public static *** v(...);
}


# Preserve all native method names and the names of their classes.
-keepclasseswithmembernames class * {
    native <methods>;
}

-keepclasseswithmembernames class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembernames class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}


-keep class sun.misc.Unsafe { *; }
#/////////////////////////////////////////////////////////////////////

#com.android.support:appcompat-v7
-keep class android.support.v7.** { *; }
-keep interface android.support.v7.** { *; }

#com.android.support:support-v4
-keep class android.support.v4.** { *; }
-keep interface android.support.v4.** { *; }

#android.support.design
-keep class android.support.design.** { *; }
-keep interface android.support.design.** { *; }

#com.squareup:otto
    #-keepattributes *Annotation*
-keepclassmembers class ** {
    @com.squareup.otto.Subscribe public *;
    @com.squareup.otto.Produce public *;
}

# LeakCanary
-keep class org.eclipse.mat.** { *; }
-keep class com.squareup.leakcanary.** { *; }

#timber
-keep class com.jakewharton.timber.** {*; }
-keep interface com.jakewharton.timber.** {*; }

#com.jakewharton:butterknife
-keep class butterknife.** { *; }
-dontwarn butterknife.internal.**
-keep class **$$ViewBinder { *; }

-keepclasseswithmembernames class * {
    @butterknife.* <fields>;
}

-keepclasseswithmembernames class * {
    @butterknife.* <methods>;
}

#CrashLytics
-keepattributes SourceFile,LineNumberTable

# For Guava:
-keep class com.google.** { *; }
-keep interface com.google.** { *; }
-dontwarn javax.annotation.**
-dontwarn javax.inject.**
-dontwarn sun.misc.Unsafe

# For RxJava:
-keep class rx.** {*; }
-keep interface rx.** {*; }


-dontwarn org.mockito.**
-dontwarn org.junit.**
-dontwarn org.robolectric.**

#com.google.code.gson:gson

##---------------Begin: proguard configuration for Gson  ----------
# Adapted from https://code.google.com/p/google-gson/source/browse/trunk/examples/android-proguard-example/proguard.cfg
#
# Gson uses generic clazz information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# We use Gson's @SerializedName annotation which won't work without this:
-keepattributes *Annotation*

# Gson specific classes
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.stream.** { *; }

# Classes that will be serialized/deserialized over Gson
# http://stackoverflow.com/a/7112371/56285
-keep class com.itdoors.elections.data.api.model.** { *; }
-keepclassmembernames interface * {
    @com.google.gson.annotations.* <methods>;
}
##---------------End: proguard configuration for Gson  ----------


#Dagger 2 ?


#    commons-io:commons-io:2.4
#    commons-codec:commons-codec:1.10
#    org.apache.commons:commons-lang3:3.3.2

-keep class org.apache.** { *; }
-keep interface org.apache.** { *; }
-keep interface javax.** { *; }
-keep class javax.** { *; }

#OkHttp
-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }

#Picasso
-keep class com.squareup.picasso.** { *; }
-keep interface com.squareup.picasso.** { *; }

#Okio
-dontwarn okio.**
-keep class okio.** { *; }
-keep interface okio.** { *; }

#Retrofit
-dontwarn retrofit.**
-keep class retrofit.** { *; }
-keep interface retrofit.** { *; }

-keepclassmembernames interface * {
    @retrofit.http.* <methods>;
}

#Joda net.danlew.android.joda
-dontwarn org.joda.time.**
-keep class net.danlew.android.joda.** { *; }
-keep interface net.danlew.android.joda.** { *; }


# uk.co.chrisjenx:calligraphy
-keep class uk.co.chrisjenx.** { *; }
-keep interface uk.co.chrisjenx.** { *; }

#com.nispok:snackbar
-keep class com.nispok.snackbar.** { *; }
-keep interface com.nispok.snackbar.** { *; }

#com.rengwuxian.materialedittext:library
-keep class com.rengwuxian.materialedittext.** { *; }
-keep interface com.rengwuxian.materialedittext.** { *; }

#com.afollestad:material-dialogs
-keep class com.afollestad.materialdialogs.** { *; }
-keep interface com.afollestad.materialdialogs.** { *; }

#frankiesardo:icepick
-dontwarn icepick.**
-keep class **$$Icepick { *; }
-keepnames class * { @icepick.State *; }
-keepclasseswithmembernames class * {
    @icepick.* <fields>;
}

#zxing
-keep class com.google.zxing.**  { *; }
-keep interface com.google.zxing.**  { *; }

#com.journeyapps
-keep class journeyapps.**  { *; }
-keep interface journeyapps.**  { *; }

#pl.charmas.android:android-reactive-location
-keep class pl.charmas.android.reactivelocation.**  { *; }
-keep interface pl.charmas.android.reactivelocation.**  { *; }

#play services ?
-dontwarn org.apache.**
-dontwarn android.net.http.AndroidHttpClient
-dontwarn com.google.android.gms.**
-dontwarn com.android.volley.toolbox.**