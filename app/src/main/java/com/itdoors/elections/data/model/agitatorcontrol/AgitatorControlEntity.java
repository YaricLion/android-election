package com.itdoors.elections.data.model.agitatorcontrol;

import com.google.common.collect.ImmutableList;
import com.google.gson.annotations.SerializedName;
import com.itdoors.elections.data.api.model.CodeValue;

import java.util.List;

public class AgitatorControlEntity {

    @SerializedName("guid")
    private final String agitatorId;
    @SerializedName("codes")
    private final List<CodeValue> codeValues;
    @SerializedName("comment")
    private final String comment;
    @SerializedName("date")
    private final String timestamp;
    @SerializedName("lat")
    private final String latitude;
    @SerializedName("long")
    String longitude;

    private AgitatorControlEntity(Builder builder) {
        agitatorId = builder.agitatorId;
        codeValues = builder.codeValues;
        comment = builder.comment;
        timestamp = builder.dateTimestamp;
        latitude = builder.latitude;
        longitude = builder.longitude;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(AgitatorControlEntity copy) {
        Builder builder = new Builder();
        builder.agitatorId = copy.agitatorId;
        builder.codeValues = copy.codeValues;
        builder.comment = copy.comment;
        builder.dateTimestamp = copy.timestamp;
        builder.latitude = copy.latitude;
        builder.longitude = copy.longitude;
        return builder;
    }

    @Override
    public String toString() {
        return "AgitatorControlEntity{" +
                "uid='" + agitatorId + '\'' +
                ", codeValues=" + codeValues +
                ", comment='" + comment + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                '}';
    }

    public AgitatorControlEntity(String agitatorId, List<CodeValue> codeValues, String comment, String dateTimestamp, String latitude) {
        this.agitatorId = agitatorId;
        this.codeValues = ImmutableList.copyOf(codeValues);
        this.comment = comment;
        this.timestamp = dateTimestamp;
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getComment() {
        return comment;
    }

    public List<CodeValue> getCodeValues() {
        return codeValues;
    }

    public String getUid() {
        return agitatorId;
    }


    public static final class Builder {
        private String agitatorId;
        private List<CodeValue> codeValues;
        private String comment;
        private String dateTimestamp;
        private String latitude;
        private String longitude;

        public Builder() {
        }

        public Builder uid(String val) {
            agitatorId = val;
            return this;
        }

        public Builder codeValues(List<CodeValue> val) {
            codeValues = val;
            return this;
        }

        public Builder comment(String val) {
            comment = val;
            return this;
        }

        public Builder timestamp(String val) {
            dateTimestamp = val;
            return this;
        }

        public Builder latitude(String val) {
            latitude = val;
            return this;
        }

        public Builder longitude(String val) {
            longitude = val;
            return this;
        }

        public AgitatorControlEntity build() {
            return new AgitatorControlEntity(this);
        }
    }
}
