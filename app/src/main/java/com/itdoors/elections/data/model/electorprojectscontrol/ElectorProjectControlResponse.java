package com.itdoors.elections.data.model.electorprojectscontrol;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.itdoors.elections.data.api.model.Project;
import com.itdoors.elections.data.api.model.ProjectControlState;
import com.itdoors.elections.data.model.Elector;

/**
 * Created by yariclion on 25.06.16.
 */
public final class ElectorProjectControlResponse implements Parcelable {

    public static final int ALREADY_SCANNED_HTTP_CODE = 209;

    @SerializedName("guid")
    private final String controlGuid;
    @SerializedName("state")
    private final ProjectControlState controlState;
    @SerializedName("scan_date_time")
    private final String scanDateTime;
    @SerializedName("create_date_time")
    private final String createDateTime;
    @SerializedName("project")
    private final Project project;
    @SerializedName("supporter")
    private final Elector elector;

    public ElectorProjectControlResponse(String controlGuid, ProjectControlState controlState, String scanDateTime, String createDateTime, Project project, Elector elector) {
        this.controlGuid = controlGuid;
        this.controlState = controlState;
        this.scanDateTime = scanDateTime;
        this.createDateTime = createDateTime;
        this.project = project;
        this.elector = elector;
    }

    public String getGuid() {
        return controlGuid;
    }

    public ProjectControlState getControlState() {
        return controlState;
    }

    public String getCreateDateTime() {
        return createDateTime;
    }

    public String getScanDateTime() {
        return scanDateTime;
    }

    public Project getProject() {
        return project;
    }

    public Elector getElector() {
        return elector;
    }

    @Override
    public String toString() {
        return "ElectorProjectControlResponse{" +
                "controlGuid='" + controlGuid + '\'' +
                ", controlState=" + controlState +
                ", scanDateTime='" + scanDateTime + '\'' +
                ", createDateTime='" + createDateTime + '\'' +
                ", project=" + project +
                ", elector=" + elector +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.controlGuid);
        dest.writeInt(this.controlState == null ? -1 : this.controlState.ordinal());
        dest.writeString(this.scanDateTime);
        dest.writeString(this.createDateTime);
        dest.writeParcelable(this.project, 0);
        dest.writeParcelable(this.elector, 0);
    }

    protected ElectorProjectControlResponse(Parcel in) {
        this.controlGuid = in.readString();
        int tmpControlState = in.readInt();
        this.controlState = tmpControlState == -1 ? null : ProjectControlState.values()[tmpControlState];
        this.scanDateTime = in.readString();
        this.createDateTime = in.readString();
        this.project = in.readParcelable(Project.class.getClassLoader());
        this.elector = in.readParcelable(Elector.class.getClassLoader());
    }

    public static final Parcelable.Creator<ElectorProjectControlResponse> CREATOR = new Parcelable.Creator<ElectorProjectControlResponse>() {
        public ElectorProjectControlResponse createFromParcel(Parcel source) {
            return new ElectorProjectControlResponse(source);
        }

        public ElectorProjectControlResponse[] newArray(int size) {
            return new ElectorProjectControlResponse[size];
        }
    };
}
