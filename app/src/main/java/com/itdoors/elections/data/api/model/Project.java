package com.itdoors.elections.data.api.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by v014nd on 22.06.2016.
 */
public final class Project implements Parcelable {

    @NonNull
    @SerializedName("guid")
    private final String guid;

    @NonNull
    @SerializedName("name")
    private final String name;

    public Project(String guid, String name) {
        this.guid = guid;
        this.name = name;
    }

    @NonNull
    public String getUid() {
        return guid;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Project{" +
                "guid='" + guid + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.guid);
        dest.writeString(this.name);
    }

    protected Project(Parcel in) {
        this.guid = in.readString();
        this.name = in.readString();
    }

    public static final Parcelable.Creator<Project> CREATOR = new Parcelable.Creator<Project>() {
        @Override
        public Project createFromParcel(Parcel source) {
            return new Project(source);
        }

        @Override
        public Project[] newArray(int size) {
            return new Project[size];
        }
    };

}
