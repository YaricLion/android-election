package com.itdoors.elections.data.model.agitatorprojectscontrol;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.itdoors.elections.data.api.model.Project;
import com.itdoors.elections.data.api.model.ProjectControlState;
import com.itdoors.elections.data.model.Agitator;

public final class AgitatorProjectControlViewModel implements Parcelable {

    @SerializedName("guid")
    private final String controlGuid;
    @SerializedName("state")
    private final ProjectControlState controlState;
    @SerializedName("scan_date_time")
    private final String scanDateTime;
    @SerializedName("create_date_time")
    private final String createDateTime;
    @SerializedName("project")
    private final Project project;
    @SerializedName("agitator")
    private final Agitator agitator;

    public AgitatorProjectControlViewModel(String controlGuid, ProjectControlState controlState, String scanDateTime, String createDateTime, Project project, Agitator agitator) {
        this.controlGuid = controlGuid;
        this.controlState = controlState;
        this.scanDateTime = scanDateTime;
        this.createDateTime = createDateTime;
        this.project = project;
        this.agitator = agitator;
    }

    private AgitatorProjectControlViewModel(Builder builder) {
        this.controlGuid = builder.controlGuid;
        this.controlState = builder.controlState;
        this.scanDateTime = builder.scanDateTime;
        this.createDateTime = builder.createDateTime;
        this.project = builder.project;
        this.agitator = builder.agitator;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(AgitatorProjectControlViewModel copy) {
        Builder builder = new Builder();
        builder.controlGuid = copy.controlGuid;
        builder.controlState = copy.controlState;
        builder.scanDateTime = copy.scanDateTime;
        builder.createDateTime = copy.createDateTime;
        builder.project = copy.project;
        builder.agitator = copy.agitator;
        return builder;
    }

    public String getScanDateTime() {
        return scanDateTime;
    }

    public Agitator getAgitator() {
        return agitator;
    }

    public Project getProject() {
        return project;
    }

    public ProjectControlState getControlState() {
        return controlState;
    }

    public String getGuid() {
        return controlGuid;
    }

    public String getCreateDateTime() {
        return createDateTime;
    }

    @Override
    public String toString() {
        return "AgitatorProjectControlViewModel{" +
                "controlGuid='" + controlGuid + '\'' +
                ", controlState=" + controlState +
                ", scanDateTime='" + scanDateTime + '\'' +
                ", createDateTime='" + createDateTime + '\'' +
                ", project=" + project +
                ", agitator=" + agitator +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.controlGuid);
        dest.writeInt(this.controlState == null ? -1 : this.controlState.ordinal());
        dest.writeString(this.scanDateTime);
        dest.writeString(this.createDateTime);
        dest.writeParcelable(this.project, 0);
        dest.writeParcelable(this.agitator, 0);
    }

    protected AgitatorProjectControlViewModel(Parcel in) {
        this.controlGuid = in.readString();
        int tmpControlState = in.readInt();
        this.controlState = tmpControlState == -1 ? null : ProjectControlState.values()[tmpControlState];
        this.scanDateTime = in.readString();
        this.createDateTime = in.readString();
        this.project = in.readParcelable(Project.class.getClassLoader());
        this.agitator = in.readParcelable(Agitator.class.getClassLoader());
    }

    public static final Parcelable.Creator<AgitatorProjectControlViewModel> CREATOR = new Parcelable.Creator<AgitatorProjectControlViewModel>() {
        public AgitatorProjectControlViewModel createFromParcel(Parcel source) {
            return new AgitatorProjectControlViewModel(source);
        }

        public AgitatorProjectControlViewModel[] newArray(int size) {
            return new AgitatorProjectControlViewModel[size];
        }
    };

    public static final class Builder {
        private String controlGuid;
        private ProjectControlState controlState;
        private String scanDateTime;
        private String createDateTime;
        private Project project;
        private Agitator agitator;

        public Builder() {
        }

        public Builder controlGuid(String val) {
            controlGuid = val;
            return this;
        }

        public Builder controlState(ProjectControlState val) {
            controlState = val;
            return this;
        }

        public Builder scanDateTime(String val) {
            scanDateTime = val;
            return this;
        }

        public Builder createDateTime(String val) {
            createDateTime = val;
            return this;
        }

        public Builder project(Project val) {
            project = val;
            return this;
        }

        public Builder agitator(Agitator val) {
            agitator = val;
            return this;
        }

        public AgitatorProjectControlViewModel build() {
            return new AgitatorProjectControlViewModel(this);
        }
    }

}
