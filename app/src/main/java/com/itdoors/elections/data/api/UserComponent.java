package com.itdoors.elections.data.api;

import android.app.Application;

import com.google.gson.Gson;
import com.itdoors.elections.AppComponent;
import com.itdoors.elections.data.db.DataService;
import com.itdoors.elections.data.oauth.OauthTokenHelper;
import com.itdoors.elections.ui.ActivityContainer;
import com.itdoors.elections.ui.activity.AgitatorControlActivity;
import com.itdoors.elections.ui.activity.AgitatorProjectControlActivity;
import com.itdoors.elections.ui.activity.ElectorControlActivity;
import com.itdoors.elections.ui.activity.ElectorProjectControlActivity;
import com.itdoors.elections.ui.activity.HomeActivity;
import com.itdoors.elections.ui.activity.MainActivity;
import com.itdoors.elections.ui.activity.ProjectsActivity;
import com.itdoors.elections.ui.activity.ScanActivity;
import com.itdoors.elections.ui.fragment.dialog.ProjectControlResponseDialog;
import com.itdoors.elections.ui.fragment.dialog.AreYouSureDialog;
import com.itdoors.elections.ui.fragment.dialog.ControlAgitatorResponseDialogFragment;
import com.itdoors.elections.ui.fragment.dialog.EnableGpsDialogFragment;
import com.itdoors.elections.ui.fragment.dialog.GetCoordinatesDialogFragment;
import com.itdoors.elections.ui.mvp.model.prefs.ApiUrlRxPrefs;
import com.itdoors.elections.ui.mvp.model.prefs.TokenRxPrefs;
import com.itdoors.elections.ui.mvp.model.prefs.UserRxPrefs;
import com.itdoors.elections.ui.service.SendService;
import com.itdoors.elections.ui.view.projects.ProjectsLayoutView;
import com.itdoors.elections.ui.view.storage.StorageControlLayoutView;
import com.itdoors.elections.util.Device;
import com.itdoors.elections.util.GuidQrParser;
import com.itdoors.elections.util.SchedulerProvider;
import com.squareup.otto.Bus;
import com.squareup.picasso.Picasso;

import javax.inject.Named;

import dagger.Component;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;


@UserScope
@Component(dependencies = AppComponent.class, modules = {UserModule.class, ApiModule.class})

public interface UserComponent {

    void inject(HomeActivity activity);

    void inject(ProjectsActivity activity);

    void inject(MainActivity activity);

    void inject(ScanActivity activity);

    void inject(AgitatorControlActivity activity);

    void inject(AgitatorProjectControlActivity activity);

    void inject(ElectorControlActivity activity);

    void inject(ElectorProjectControlActivity activity);

    void inject(StorageControlLayoutView view);

    void inject(ProjectsLayoutView view);

    void inject(SendService view);

    void inject(EnableGpsDialogFragment dialogFragment);

    void inject(ControlAgitatorResponseDialogFragment dialogFragment);

    void inject(GetCoordinatesDialogFragment dialogFragment);

    void inject(AreYouSureDialog dialogFragment);

    void inject(ProjectControlResponseDialog dialogFragment);

    OauthTokenHelper oauthTokenHelper();

    OauthService oauthService();

    AppService appService();

    Application app();

    TokenRxPrefs tokenHelper();

    UserRxPrefs userHelper();

    ApiUrlRxPrefs apiUrlRxPrefs();

    Bus bus();

    Picasso picasso();

    Gson gson();

    Device device();

    SchedulerProvider sheSchedulerProvider();

    ReactiveLocationProvider locationProvider();

    ActivityContainer activityContainer();

    DataService dataService();

    GuidQrParser guidQrParser();

    @Named("UserApiUrl")
    String apiUrl();

    @Named("UserApiEmail")
    String email();

}
