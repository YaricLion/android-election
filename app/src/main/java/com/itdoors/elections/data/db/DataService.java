package com.itdoors.elections.data.db;

import com.itdoors.elections.data.api.model.Code;
import com.itdoors.elections.data.api.model.CodeValue;
import com.itdoors.elections.data.api.model.TimeRange;
import com.itdoors.elections.data.model.ElectionDay;
import com.itdoors.elections.data.model.agitatorcontrol.AgitatorControlEntity;
import com.itdoors.elections.data.model.agitatorprojectscontrol.AgitatorProjectControlSendEntity;
import com.itdoors.elections.data.model.electorcontrol.ElectorControlEntity;
import com.itdoors.elections.data.model.electorprojectscontrol.ElectorProjectControlSendEntity;
import com.itdoors.elections.data.model.project.ProjectEntity;

import java.util.List;

import dagger.Provides;
import rx.Observable;

public interface DataService extends ConnectableDataService {

    Observable<List<ElectorControlEntity>> getElectorControlInfo();

    Observable<List<AgitatorControlEntity>> getAgitatorControlInfo();

    Observable<Void> addElectorControl(ElectorControlEntity entity);
    Observable<Void> addAgitatorControl(AgitatorControlEntity entity);

    Observable<Void> clearElectorControlInfo();
    Observable<Void> clearAgitatorControlInfo();

    Observable<List<Code>> getCodes();

    Observable<Void> rewriteCodes(List<Code> codeValues);

    Observable<Boolean> isProjectsAvailable();

    Observable<Void> writeProjects(List<ProjectEntity> entities);

    Observable<List<ProjectEntity>> getProjects();

    Observable<Void> addAgitatorProjectControl(AgitatorProjectControlSendEntity entity);

    Observable<Void> addElectorProjectControl(ElectorProjectControlSendEntity entity);


    Observable<List<AgitatorProjectControlSendEntity>> getAgitatorProjectControlInfo();

    Observable<List<ElectorProjectControlSendEntity>> getElectorProjectControlInfo();

    Observable<Void> clearAgitatorProjectControlInfo();

    Observable<Void> clearElectorProjectControlInfo();

    Observable<Long> getAgitatorProjectControlCount();

    Observable<Long> getElectorProjectControlCount();
    Observable<Long> getElectorControlCount();

    Observable<Long> getCodesCount();

    Observable<Void> rewriteElectionDay(ElectionDay electionDay);

    Observable<Long> getElectionDayCount();

    Observable<ElectionDay> getElectionDay();
}
