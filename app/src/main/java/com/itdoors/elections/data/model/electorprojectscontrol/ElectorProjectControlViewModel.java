package com.itdoors.elections.data.model.electorprojectscontrol;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.itdoors.elections.data.api.model.Project;
import com.itdoors.elections.data.api.model.ProjectControlState;
import com.itdoors.elections.data.model.Elector;

public final class ElectorProjectControlViewModel implements Parcelable {
    @SerializedName("guid")
    private final String controlGuid;
    @SerializedName("state")
    private final ProjectControlState controlState;
    @SerializedName("scan_date_time")
    private final String scanDateTime;
    @SerializedName("create_date_time")
    private final String createDateTime;
    @SerializedName("project")
    private final Project project;
    @SerializedName("supporter")
    private final Elector elector;

    public ElectorProjectControlViewModel(String controlGuid, ProjectControlState controlState, String scanDateTime, String createDateTime, Project project, Elector elector) {
        this.controlGuid = controlGuid;
        this.controlState = controlState;
        this.scanDateTime = scanDateTime;
        this.createDateTime = createDateTime;
        this.project = project;
        this.elector = elector;
    }


    private ElectorProjectControlViewModel(Builder builder) {
        this.controlGuid = builder.controlGuid;
        this.controlState = builder.controlState;
        this.scanDateTime = builder.scanDateTime;
        this.createDateTime = builder.createDateTime;
        this.project = builder.project;
        this.elector = builder.elector;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(ElectorProjectControlViewModel copy) {
        Builder builder = new Builder();
        builder.controlGuid = copy.controlGuid;
        builder.controlState = copy.controlState;
        builder.scanDateTime = copy.scanDateTime;
        builder.createDateTime = copy.createDateTime;
        builder.project = copy.project;
        builder.elector = copy.elector;
        return builder;
    }

    public String getGuid() {
        return controlGuid;
    }

    public ProjectControlState getControlState() {
        return controlState;
    }

    public String getCreateDateTime() {
        return createDateTime;
    }

    public String getScanDateTime() {
        return scanDateTime;
    }

    public Project getProject() {
        return project;
    }

    public Elector getElector() {
        return elector;
    }

    @Override
    public String toString() {
        return "ElectorProjectControlViewModel{" +
                "controlGuid='" + controlGuid + '\'' +
                ", controlState=" + controlState +
                ", scanDateTime='" + scanDateTime + '\'' +
                ", createDateTime='" + createDateTime + '\'' +
                ", project=" + project +
                ", elector=" + elector +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.controlGuid);
        dest.writeInt(this.controlState == null ? -1 : this.controlState.ordinal());
        dest.writeString(this.scanDateTime);
        dest.writeString(this.createDateTime);
        dest.writeParcelable(this.project, 0);
        dest.writeParcelable(this.elector, 0);
    }

    protected ElectorProjectControlViewModel(Parcel in) {
        this.controlGuid = in.readString();
        int tmpControlState = in.readInt();
        this.controlState = tmpControlState == -1 ? null : ProjectControlState.values()[tmpControlState];
        this.scanDateTime = in.readString();
        this.createDateTime = in.readString();
        this.project = in.readParcelable(Project.class.getClassLoader());
        this.elector = in.readParcelable(Elector.class.getClassLoader());
    }

    public static final Parcelable.Creator<ElectorProjectControlViewModel> CREATOR = new Parcelable.Creator<ElectorProjectControlViewModel>() {
        public ElectorProjectControlViewModel createFromParcel(Parcel source) {
            return new ElectorProjectControlViewModel(source);
        }

        public ElectorProjectControlViewModel[] newArray(int size) {
            return new ElectorProjectControlViewModel[size];
        }
    };

    public static final class Builder {
        private String controlGuid;
        private ProjectControlState controlState;
        private String scanDateTime;
        private String createDateTime;
        private Project project;
        private Elector elector;

        public Builder() {
        }

        public Builder controlGuid(String val) {
            controlGuid = val;
            return this;
        }

        public Builder controlState(ProjectControlState val) {
            controlState = val;
            return this;
        }

        public Builder scanDateTime(String val) {
            scanDateTime = val;
            return this;
        }

        public Builder createDateTime(String val) {
            createDateTime = val;
            return this;
        }

        public Builder project(Project val) {
            project = val;
            return this;
        }

        public Builder elector(Elector val) {
            elector = val;
            return this;
        }


        public ElectorProjectControlViewModel build() {
            return new ElectorProjectControlViewModel(this);
        }
    }

}
