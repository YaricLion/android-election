package com.itdoors.elections.data.api.deserializer;

import com.google.common.base.Optional;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.itdoors.elections.data.api.model.Project;
import com.itdoors.elections.data.api.model.ProjectControlState;
import com.itdoors.elections.data.model.Elector;
import com.itdoors.elections.data.model.electorprojectscontrol.ElectorProjectControlResponse;

import java.lang.reflect.Type;

public final class ElectorProjectControlDeserializer implements JsonDeserializer<ElectorProjectControlResponse> {

    public static ElectorProjectControlResponse deserialize(JsonElement json) {

        JsonObject jObj = (JsonObject) json;

        Optional<JsonElement> guid = Optional.fromNullable(jObj.get("guid"));
        if (!guid.isPresent())
            throw new JsonParseException("Failed to parse control with no guid");

        Optional<JsonElement> state = Optional.fromNullable(jObj.get("state"));
        Optional<JsonElement> scanDateTime = Optional.fromNullable(jObj.get("scan_date_time"));
        Optional<JsonElement> createDateTime = Optional.fromNullable(jObj.get("create_date_time"));
        Optional<JsonObject> project = Optional.fromNullable(jObj.getAsJsonObject("project"));
        Optional<JsonObject> elector = Optional.fromNullable(jObj.getAsJsonObject("supporter"));

        ProjectControlState controlState = ProjectControlState.fromCode(state.isPresent() ? state.get().getAsString() : null);
        if (controlState == null)
            throw new JsonParseException("Couldn't project control state : " + (state.isPresent() ? state.get().getAsString() : "null"));

        Project projectParsed = null;
        if (project.isPresent()) {
            Optional<JsonElement> projectGuid = Optional.fromNullable(project.get().get("guid"));
            if (!projectGuid.isPresent())
                throw new JsonParseException("Failed to parse project with no guid");

            Optional<JsonElement> projectName = Optional.fromNullable(project.get().get("name"));
            projectParsed = new Project(
                    projectGuid.get().getAsString(),
                    projectName.isPresent() ? projectName.get().getAsString() : null);
        }

        Elector electorParsed = null;
        if (elector.isPresent()) {
            Optional<JsonElement> electorGuid = Optional.fromNullable(elector.get().get("guid"));
            if (!electorGuid.isPresent())
                throw new JsonParseException("Failed to parse elector with no guid");

            Optional<JsonElement> electorName = Optional.fromNullable(elector.get().get("first_name"));
            Optional<JsonElement> electorSurname = Optional.fromNullable(elector.get().get("last_name"));
            Optional<JsonElement> electorSecondName = Optional.fromNullable(elector.get().get("second_name"));
            Optional<JsonElement> electorBirthDate = Optional.fromNullable(elector.get().get("birth_date"));

            electorParsed = new Elector(
                    electorGuid.get().getAsString(),
                    electorName.isPresent() ? electorName.get().getAsString() : null,
                    electorSurname.isPresent() ? electorSurname.get().getAsString() : null,
                    electorSecondName.isPresent() ? electorSecondName.get().getAsString() : null,
                    electorBirthDate.isPresent() ? electorBirthDate.get().getAsString() : null
            );

        }

        return new ElectorProjectControlResponse(
                guid.get().getAsString(),
                controlState,
                scanDateTime.isPresent() ? scanDateTime.get().getAsString() : null,
                createDateTime.isPresent() ? createDateTime.get().getAsString() : null,
                projectParsed,
                electorParsed
        );
    }

    @Override
    public ElectorProjectControlResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        return deserialize(json);
    }

}
