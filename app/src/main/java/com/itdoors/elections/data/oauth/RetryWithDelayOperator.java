package com.itdoors.elections.data.oauth;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by v014nd on 25.09.2015.
 */
public final class RetryWithDelayOperator implements Func1<Observable<? extends Throwable>, Observable<?>> {

    private final int maxRetries;
    private final int retryDelay;
    private final TimeUnit timeUnit;
    private int retryCount;

    public RetryWithDelayOperator(final int maxRetries, final int retryDelay, TimeUnit timeUnit) {
        this.maxRetries = maxRetries;
        this.retryDelay = retryDelay;
        this.timeUnit = timeUnit;
        this.retryCount = 0;
    }

    @Override public Observable<?> call(Observable<? extends Throwable> attempts) {
        return attempts
                .flatMap(throwable -> {
                    if (++retryCount < maxRetries) {
                        return Observable.timer(retryDelay,timeUnit);
                    }
                    return Observable.error(throwable);
                });
    }
}
