package com.itdoors.elections.data.oauth;

import com.itdoors.elections.ui.event.login.NeedToLoginAgainEvent;
import com.itdoors.elections.ui.event.login.NeedToLoginEvent;
import com.squareup.otto.Bus;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.functions.Func1;
import timber.log.Timber;

import static com.itdoors.elections.util.MVPUtils.isUnauthorizedError;

public final class RetryWithTokenOperator implements Func1<Observable<? extends Throwable>, Observable<?>> {

    private final OauthTokenHelper oauthTokenHelper;
    private final Bus bus;

    public RetryWithTokenOperator(OauthTokenHelper oauthTokenHelper, Bus bus) {
        this.oauthTokenHelper = oauthTokenHelper;
        this.bus = bus;
    }

    @Override public Observable<?> call(Observable<? extends Throwable> attempts) {
        return attempts
               .flatMap(throwable -> {
                    if (isUnauthorizedError(throwable)) {
                        try {
                            oauthTokenHelper.refresh();
                            return Observable.timer(1, TimeUnit.SECONDS);
                        } catch (OauthTokenHelper.AccessTokenNotExistedException exception) {
                            Timber.d("RetryWithTokenOperator:  Need to login event");
                            bus.post(new NeedToLoginEvent());
                        } catch (OauthTokenHelper.NeedToReloginException exception) {
                            Timber.d("RetryWithTokenOperator:  Need to relogin event");
                            bus.post(new NeedToLoginAgainEvent());
                        } catch (IOException exception) {
                            Timber.e(exception, "Refresh token failed");
                        }
                    }
                    return Observable.error(throwable);
                });
    }

}
