package com.itdoors.elections.data.api.deserializer;

import com.google.common.base.Optional;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.itdoors.elections.data.model.electorcontrol.ElectorProjectControlListErrorResponse;
import com.itdoors.elections.data.model.electorcontrol.ElectorProjectControlListResponse;
import com.itdoors.elections.data.model.electorcontrol.ElectorProjectControlListSuccessResponse;
import com.itdoors.elections.data.model.electorprojectscontrol.ElectorProjectControlResponse;

import java.lang.reflect.Type;

import static com.itdoors.elections.data.api.deserializer.IsResponseSuccessfulFunc.DEFAULT;

/**
 * Created by yariclion on 27.06.16.
 */
public final class ElectorProjectControlListResponseDeserializer implements JsonDeserializer<ElectorProjectControlListResponse> {

    @Override
    public ElectorProjectControlListResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jObj = (JsonObject) json;
        Optional<JsonElement> code = Optional.fromNullable(jObj.get("code"));
        if (!code.isPresent())
            throw new JsonParseException("Failed to parse agitator project control response without any code inside");

        int codeResponse = code.get().getAsInt();
        if (DEFAULT.isSuccessful(codeResponse)) {

            Optional<JsonObject> projectControl = Optional.fromNullable(jObj.getAsJsonObject("project_control"));
            if (!projectControl.isPresent())
                throw new JsonParseException("Failed to parse agitator project control response without any control info inside");

            ElectorProjectControlResponse projectControlResponse = ElectorProjectControlDeserializer.deserialize(projectControl.get());
            return new ElectorProjectControlListSuccessResponse(codeResponse, projectControlResponse);
        } else {

            Optional<JsonElement> msg = Optional.fromNullable(jObj.get("message"));
            Optional<JsonElement> projectGuid = Optional.fromNullable(jObj.get("project_guid"));
            Optional<JsonElement> agitatorGuid = Optional.fromNullable(jObj.get("agitator_guid"));
            Optional<JsonElement> scanDateTime = Optional.fromNullable(jObj.get("scan_date_time"));

            return new ElectorProjectControlListErrorResponse(
                    codeResponse,
                    msg.isPresent() ? msg.get().getAsString() : null,
                    projectGuid.isPresent() ? projectGuid.get().getAsString() : null,
                    agitatorGuid.isPresent() ? agitatorGuid.get().getAsString() : null,
                    scanDateTime.isPresent() ? scanDateTime.get().getAsString() : null
            );
        }

    }

}

