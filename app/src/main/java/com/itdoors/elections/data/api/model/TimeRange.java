package com.itdoors.elections.data.api.model;

import android.os.Parcel;
import android.os.Parcelable;

public final class TimeRange implements Parcelable {

    private final String startTime;
    private final String endTime;

    public TimeRange(String start, String end) {
        this.startTime = start;
        this.endTime = end;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public static TimeRange from(String start, String end) {
        return new TimeRange(start, end);
    }

    @Override
    public String toString() {
        return "TimeRange{" +
                "startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.startTime);
        dest.writeString(this.endTime);
    }

    protected TimeRange(Parcel in) {
        this.startTime = in.readString();
        this.endTime = in.readString();
    }

    public static final Parcelable.Creator<TimeRange> CREATOR = new Parcelable.Creator<TimeRange>() {

        @Override
        public TimeRange createFromParcel(Parcel source) {
            return new TimeRange(source);
        }

        @Override
        public TimeRange[] newArray(int size) {
            return new TimeRange[size];
        }

    };
}
