package com.itdoors.elections.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yariclion on 13.07.16.
 */
public final class ElectionDay implements Parcelable {

    @SerializedName("startedAt")
    private final String start;
    @SerializedName("finishedAt")
    private final String finish;

    public ElectionDay(String start, String finish) {
        this.start = start;
        this.finish = finish;
    }

    public String getStart() {
        return start;
    }

    public String getFinish() {
        return finish;
    }

    @Override public String toString() {
        return "ElectionDay{" +
                "start='" + start + '\'' +
                ", finish='" + finish + '\'' +
                '}';
    }

    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.start);
        dest.writeString(this.finish);
    }

    protected ElectionDay(Parcel in) {
        this.start = in.readString();
        this.finish = in.readString();
    }

    public static final Parcelable.Creator<ElectionDay> CREATOR = new Parcelable.Creator<ElectionDay>() {
        public ElectionDay createFromParcel(Parcel source) {
            return new ElectionDay(source);
        }

        public ElectionDay[] newArray(int size) {
            return new ElectionDay[size];
        }
    };
}
