package com.itdoors.elections.data.api;

import android.app.Application;

import com.google.gson.Gson;
import com.itdoors.elections.BuildConfig;
import com.itdoors.elections.PerApp;
import com.itdoors.elections.data.api.model.oauth.AccessToken;
import com.itdoors.elections.data.oauth.OauthTokenHelper;
import com.jakewharton.retrofit.Ok3Client;
import com.itdoors.elections.util.Prefs;
import com.squareup.otto.Bus;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit.Endpoint;
import retrofit.Endpoints;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

@Module
public final class ApiModule {

    public static final String PRODUCTION_API_URL = "http://192.168.4.60:8001/app.php";//https://iq-shtab.com.ua";

    public static final String CHARSET = "UTF-8";

    //public static final String PRODUCTION_API_URL = "";

/*
    @Provides @PerApp Endpoint provideEndpoint(@Named("apiURL") String url) {
        return Endpoints.newFixedEndpoint(url);
    }
*/

/*
    @Provides @PerApp @Named("apiURL") String provideApiUrl(){
        return PRODUCTION_API_URL;
    }
*/


 /*   @Provides @PerApp @Named("charset") String provideCharset() {
        return CHARSET;
    }

    @Provides @PerApp GsonConverter provideGsonConverter(Gson gson, @Named("charset") String charset){
        return new GsonConverter(gson, charset);
    }

    @Provides @PerApp RestAdapter provideApiRestAdapter(Endpoint endpoint, OkHttpClient client,
                                   GsonConverter converter) {
        return new RestAdapter.Builder() //
                .setClient(new Ok3Client(client)) //
                .setEndpoint(endpoint) //
                .setConverter(converter) //
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                .build();
    }

    @Provides @PerApp AppService provideAppService(RestAdapter restAdapter, OauthTokenHelper oauthTokenHelper, Bus bus, Application app, Gson gson) {
        AppService appService = restAdapter.create(AppService.class);
        if(BuildConfig.DEBUG){
            appService = new StubOfflineAppService(appService, app, gson);
        }
        return new AppServiceWithOauth(appService, oauthTokenHelper, bus);
    }

    @Provides @PerApp OauthService provideOauthService(RestAdapter restAdapter) {
        return restAdapter.create(OauthService.class);
    }*/

    @Provides
    @UserScope
    @Named("charset")
    String provideCharset() {
        return CHARSET;
    }

    @Provides
    @UserScope
    Endpoint provideEndpoint(@Named("UserApiUrl") String url) {
        return Endpoints.newFixedEndpoint(url);
    }

    @Provides
    @UserScope
    GsonConverter provideGsonConverter(Gson gson, @Named("charset") String charset) {
        return new GsonConverter(gson, charset);
    }

    @Provides
    @UserScope
    RestAdapter provideApiRestAdapter(Endpoint endpoint, OkHttpClient client,
                                      GsonConverter converter) {
        return new RestAdapter.Builder() //
                .setClient(new Ok3Client(client)) //
                .setEndpoint(endpoint) //
                .setConverter(converter) //
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                .build();
    }

    @Provides
    @UserScope
    AppService provideAppService(RestAdapter restAdapter, OauthTokenHelper oauthTokenHelper, Bus bus, Application app, Gson gson) {
        AppService appService = restAdapter.create(AppService.class);
        return new AppServiceWithOauth(appService, oauthTokenHelper, bus);
    }

    @Provides
    @UserScope
    OauthService provideOauthService(RestAdapter restAdapter) {
        return restAdapter.create(OauthService.class);
    }

    @Provides
    @UserScope
    OauthTokenHelper provideOauthTokenHelper(@Named("tokenPrefs") Prefs<AccessToken> tokenHelper, OkHttpClient client, Endpoint endpoint, Gson gson) {
        return new OauthTokenHelper(tokenHelper, client, endpoint, gson);
    }

}
