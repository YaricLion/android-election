package com.itdoors.elections.data.model.electorcontrol;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by v014nd on 07.06.2016.
 */
public final class ElectorControlResponse {

    @SerializedName("guid")
    private final String electorControlGuid;
    @SerializedName("status")
    private final int status;
    @SerializedName("message")
    private final String message;

    public ElectorControlResponse(String electorControlGuid, int status, String message) {
        this.electorControlGuid = electorControlGuid;
        this.status = status;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public String getElectorControlGuid() {
        return electorControlGuid;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "ElectorControlResponse{" +
                "electorControlGuid='" + electorControlGuid + '\'' +
                ", status=" + status +
                ", message='" + message + '\'' +
                '}';
    }

}
