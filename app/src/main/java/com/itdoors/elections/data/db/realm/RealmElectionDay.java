package com.itdoors.elections.data.db.realm;

import io.realm.RealmObject;

/**
 * Created by yariclion on 13.07.16.
 */
public class RealmElectionDay extends RealmObject{
    private String start;
    private String finish;

    public void setFinish(String finish) {
        this.finish = finish;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getStart() {
        return start;
    }

    public String getFinish() {
        return finish;
    }

}
