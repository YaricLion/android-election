package com.itdoors.elections.data.model.electorprojectscontrol;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public final class ElectorProjectControlSendEntity implements Parcelable {

    @SerializedName("project_guid")
    private final String projectGui;
    @SerializedName("supporter_guid")
    private final String supporterGuid;
    @SerializedName("lat")
    private final String latitude;
    @SerializedName("long")
    private final String longitude;
    @SerializedName("scan_date_time")
    private final String scanDateTime;

    public ElectorProjectControlSendEntity(String projectGui, String supporterGuid, String latitude, String longitude, String scanDateTime) {
        this.projectGui = projectGui;
        this.supporterGuid = supporterGuid;
        this.latitude = latitude;
        this.longitude = longitude;
        this.scanDateTime = scanDateTime;
    }

    private ElectorProjectControlSendEntity(Builder builder) {
        this.projectGui = builder.projectGui;
        this.supporterGuid = builder.supporterGuid;
        this.latitude = builder.latitude;
        this.longitude = builder.longitude;
        this.scanDateTime = builder.scanDateTime;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(ElectorProjectControlSendEntity copy) {
        Builder builder = new Builder();
        builder.projectGui = copy.projectGui;
        builder.supporterGuid = copy.supporterGuid;
        builder.latitude = copy.latitude;
        builder.longitude = copy.longitude;
        builder.scanDateTime = copy.scanDateTime;
        return builder;
    }

    public String getSupporterGuid() {
        return supporterGuid;
    }

    public String getScanDateTime() {
        return scanDateTime;
    }

    public String getProjectGui() {
        return projectGui;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    @Override
    public String toString() {
        return "ElectorProjectControlSendEntity{" +
                "projectGui='" + projectGui + '\'' +
                ", supporterGuid='" + supporterGuid + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", scanDateTime='" + scanDateTime + '\'' +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.projectGui);
        dest.writeString(this.supporterGuid);
        dest.writeString(this.latitude);
        dest.writeString(this.longitude);
        dest.writeString(this.scanDateTime);
    }

    protected ElectorProjectControlSendEntity(Parcel in) {
        this.projectGui = in.readString();
        this.supporterGuid = in.readString();
        this.latitude = in.readString();
        this.longitude = in.readString();
        this.scanDateTime = in.readString();
    }

    public static final Parcelable.Creator<ElectorProjectControlSendEntity> CREATOR = new Parcelable.Creator<ElectorProjectControlSendEntity>() {
        public ElectorProjectControlSendEntity createFromParcel(Parcel source) {
            return new ElectorProjectControlSendEntity(source);
        }

        public ElectorProjectControlSendEntity[] newArray(int size) {
            return new ElectorProjectControlSendEntity[size];
        }
    };

    public static final class Builder {
        private String projectGui;
        private String supporterGuid;
        private String latitude;
        private String longitude;
        private String scanDateTime;

        public Builder() {
        }

        public Builder projectGui(String val) {
            projectGui = val;
            return this;
        }

        public Builder supporterGuid(String val) {
            supporterGuid = val;
            return this;
        }

        public Builder latitude(String val) {
            latitude = val;
            return this;
        }

        public Builder longitude(String val) {
            longitude = val;
            return this;
        }

        public Builder scanDateTime(String val) {
            scanDateTime = val;
            return this;
        }


        public ElectorProjectControlSendEntity build() {
            return new ElectorProjectControlSendEntity(this);
        }
    }

}
