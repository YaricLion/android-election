package com.itdoors.elections.data.api;

import com.itdoors.elections.data.api.model.Project;
import com.itdoors.elections.data.model.Agitator;
import com.itdoors.elections.data.model.ElectionDay;
import com.itdoors.elections.data.model.agitatorcontrol.AgitatorControlResponseOld;
import com.itdoors.elections.data.model.agitatorcontrol.AgitatorControlSend;
import com.itdoors.elections.data.model.agitatorcontrol.AgitatorControlResponse;
import com.itdoors.elections.data.api.model.Code;
import com.itdoors.elections.data.model.Elector;
import com.itdoors.elections.data.model.agitatorprojectscontrol.AgitatorProjectControlListResponse;
import com.itdoors.elections.data.model.agitatorprojectscontrol.AgitatorProjectControlResponse;
import com.itdoors.elections.data.model.agitatorprojectscontrol.AgitatorProjectControlSend;
import com.itdoors.elections.data.model.electorcontrol.ElectorControlResponseOld;
import com.itdoors.elections.data.model.electorcontrol.ElectorControlResponse;
import com.itdoors.elections.data.model.electorcontrol.ElectorControlSend;
import com.itdoors.elections.data.api.model.User;
import com.itdoors.elections.data.model.electorcontrol.ElectorProjectControlListResponse;
import com.itdoors.elections.data.model.electorprojectscontrol.ElectorProjectControlResponse;
import com.itdoors.elections.data.model.electorprojectscontrol.ElectorProjectControlSend;

import java.util.List;

import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.PATCH;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

public interface AppService {

    @Headers({"Accept: application/json"})
    @GET("/api/v1/agitator/{guid}")
    Observable<Agitator> getAgitator(@Path("guid") String guid);

    @Headers({"Accept: application/json"})
    @GET("/api/v1/elector/{guid}")
    Observable<Elector> verifyElector(@Path("guid") String guid);

    @Headers({"Accept: application/json"})
    @GET("/api/v1/user")
    Observable<User> getUser(@Query(OauthInterceptor.access_token) String token);

    @Headers({"Accept: application/json"})
    @GET("/api/v1/control/agitator/code")
    Observable<List<Code>> getCodes();

    @Headers({"Accept: application/json"})
    @PUT("/api/v1/control/supporter") @FormUrlEncoded
    Observable<ElectorControlResponseOld> controlElectors(
            @Field("guid") String electorId,
            @Field("date") String dateTimestamp,
            @Field("lat") String latitude,
            @Field("long") String longitude
    );

    @Headers({"Accept: application/json"})
    @PUT("/api/v1/control/agitator") @FormUrlEncoded
    Observable<AgitatorControlResponseOld> controlAgitators(
            @Field("guid") String agitatorId,
            @Field("codes") String jsonCodeValues,
            @Field("comment") String comment,
            @Field("date") String dateTimestamp,
            @Field("lat") String latitude,
            @Field("long") String longitude
    );

    @Headers({"Accept: application/json"})
    @PUT("/api/v1/control/elector")
    Observable<Response> controlElectors(@Body List<ElectorControlSend> electorControlItems);

    @Headers({"Accept: application/json"})
    @PUT("/api/v1/control/agitator")
    Observable<Response> controlAgitators(@Body List<AgitatorControlSend> agitatorControlItems);

    @Headers({"Accept: application/json"})
    @GET("/api/v1/projects?filter=mobile")
    Observable<List<Project>> getProjects();

    @Headers({"Accept: application/json"})
    @POST("/api/v1/projects_control/supporter")
    Observable<ElectorProjectControlResponse> controlElectorProject(@Body ElectorProjectControlSend item);

    @Headers({"Accept: application/json"})
    @POST("/api/v1/projects_control/agitator")
    Observable<AgitatorProjectControlResponse> controlAgitatorProject(@Body AgitatorProjectControlSend item);

    @Headers({"Accept: application/json"})
    @POST("/api/v1/projects_control/supporter")
    Observable<Response> controlElectorProject(@Body List<ElectorProjectControlSend> list);

    @Headers({"Accept: application/json"})
    @POST("/api/v1/projects_control/agitator")
    Observable<Response> controlAgitatorProject(@Body List<AgitatorProjectControlSend> list);

    @Headers({"Accept: application/json"})
    @PATCH("/api/v1/projects_control/agitator/{guid}")
    @FormUrlEncoded
    Observable<AgitatorProjectControlResponse> changeAgitatorStatus(@Path("guid") String guid, @Field("state") String state);


    @Headers({"Accept: application/json"})
    @PATCH("/api/v1/projects_control/supporter/{guid}")
    @FormUrlEncoded
    Observable<ElectorProjectControlResponse> changeElectorStatus(@Path("guid") String guid, @Field("state") String state);

    @Headers({"Accept: application/json"})
    @GET("/api/v1/dayx")
    Observable<ElectionDay> getElectionDayTimes();

}
