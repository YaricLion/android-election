package com.itdoors.elections.data.api.deserializer;

public interface IsResponseSuccessfulFunc {

    boolean isSuccessful(int code);

    IsResponseSuccessfulFunc DEFAULT = code -> {
        return (code >= 200) && (code < 300);
    };

}
