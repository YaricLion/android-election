package com.itdoors.elections.data.model.project;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.itdoors.elections.data.model.ViewModel;
import com.itdoors.elections.ui.mvp.model.Model;

public final class ProjectViewModel implements ViewModel, Parcelable {

    @NonNull
    @SerializedName("guid")
    private final String guid;
    @NonNull
    @SerializedName("name")
    private final String name;

    public ProjectViewModel(@NonNull String guid, @NonNull String name) {
        this.guid = guid;
        this.name = name;
    }

    public ProjectViewModel(Builder builder) {
        guid = builder.guid;
        name = builder.name;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(ProjectViewModel copy) {
        Builder builder = new Builder();
        builder.guid = copy.guid;
        builder.name = copy.name;
        return builder;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public String getGuid() {
        return guid;
    }

    @Override
    public String toString() {
        return "ProjectViewModel{" +
                "guid='" + guid + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    public static final class Builder {
        private String guid;
        private String name;

        public Builder() {
        }

        public Builder uid(String val) {
            guid = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public ProjectViewModel build() {
            return new ProjectViewModel(this);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.guid);
        dest.writeString(this.name);
    }

    protected ProjectViewModel(Parcel in) {
        this.guid = in.readString();
        this.name = in.readString();
    }

    public static final Parcelable.Creator<ProjectViewModel> CREATOR = new Parcelable.Creator<ProjectViewModel>() {
        public ProjectViewModel createFromParcel(Parcel source) {
            return new ProjectViewModel(source);
        }

        public ProjectViewModel[] newArray(int size) {
            return new ProjectViewModel[size];
        }
    };
}
