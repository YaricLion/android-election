package com.itdoors.elections.data.model.agitatorcontrol;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by v014nd on 07.06.2016.
 */
public final class AgitatorControlSend {

    @SerializedName("guid")
    private final String agitatorId;
    @SerializedName("codes")
    private final String jsonCodeValues;
    @SerializedName("comment")
    private final String comment;
    @SerializedName("date")
    private final String dateTimestamp;
    @SerializedName("lat")
    private final String latitude;
    @SerializedName("long")
    private final String longitude;

    public AgitatorControlSend(String agitatorId, String jsonCodeValues, String comment, String dateTimestamp, String latitude, String longitude) {
        this.agitatorId = agitatorId;
        this.jsonCodeValues = jsonCodeValues;
        this.comment = comment;
        this.dateTimestamp = dateTimestamp;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public AgitatorControlSend(Builder builder) {
        this.agitatorId = builder.agitatorId;
        this.jsonCodeValues = builder.jsonCodeValues;
        this.comment = builder.comment;
        this.dateTimestamp = builder.dateTimestamp;
        this.latitude = builder.latitude;
        this.longitude = builder.longitude;
    }

    public String getAgitatorId() {
        return agitatorId;
    }

    public String getComment() {
        return comment;
    }

    public String getDateTimestamp() {
        return dateTimestamp;
    }

    public String getCodeValues() {
        return jsonCodeValues;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }


    public static Builder newBuilder() {
        return new Builder();
    }

    public static final class Builder {

        private String agitatorId;
        private String jsonCodeValues;
        private String comment;
        private String dateTimestamp;
        private String latitude;
        private String longitude;

        public Builder() {
        }

        public Builder uid(String val) {
            agitatorId = val;
            return this;
        }

        public Builder jsonCodeValues(String val) {
            jsonCodeValues = val;
            return this;
        }

        public Builder comment(String val) {
            comment = val;
            return this;
        }

        public Builder dateTimestamp(String val) {
            dateTimestamp = val;
            return this;
        }

        public Builder latitude(String val) {
            latitude = val;
            return this;
        }

        public Builder longitude(String val) {
            longitude = val;
            return this;
        }

        public AgitatorControlSend build() {
            return new AgitatorControlSend(this);
        }

    }

}
