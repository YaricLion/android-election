package com.itdoors.elections.data.model.agitatorprojectscontrol;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yariclion on 27.06.16.
 */
public final class AgitatorProjectControlListSuccessResponse implements AgitatorProjectControlListResponse, Parcelable {

    @SerializedName("code")
    private final int code;
    @SerializedName("project_control")
    private final AgitatorProjectControlResponse projectControl;

    public AgitatorProjectControlListSuccessResponse(int code, AgitatorProjectControlResponse projectControl) {
        this.code = code;
        this.projectControl = projectControl;
    }

    @Override
    public int getCode() {
        return code;
    }

    public AgitatorProjectControlResponse getProjectControl() {
        return projectControl;
    }

    @Override
    public String toString() {
        return "AgitatorProjectControlListSuccessResponse{" +
                "code=" + code +
                ", projectControl=" + projectControl +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.code);
        dest.writeParcelable(this.projectControl, 0);
    }

    protected AgitatorProjectControlListSuccessResponse(Parcel in) {
        this.code = in.readInt();
        this.projectControl = in.readParcelable(AgitatorProjectControlResponse.class.getClassLoader());
    }

    public static final Creator<AgitatorProjectControlListSuccessResponse> CREATOR = new Creator<AgitatorProjectControlListSuccessResponse>() {
        public AgitatorProjectControlListSuccessResponse createFromParcel(Parcel source) {
            return new AgitatorProjectControlListSuccessResponse(source);
        }

        public AgitatorProjectControlListSuccessResponse[] newArray(int size) {
            return new AgitatorProjectControlListSuccessResponse[size];
        }
    };
}
