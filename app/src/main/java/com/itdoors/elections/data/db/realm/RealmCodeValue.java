package com.itdoors.elections.data.db.realm;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by v014nd on 16.06.2016.
 */
public class RealmCodeValue extends RealmObject {
    @PrimaryKey
    @Required
    private String code;
    private String value;

    public void setCode(String code) {
        this.code = code;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
}
