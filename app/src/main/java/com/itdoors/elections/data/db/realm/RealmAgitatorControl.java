package com.itdoors.elections.data.db.realm;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by v014nd on 08.06.2016.
 */
public class RealmAgitatorControl extends RealmObject {

    @PrimaryKey
    @Required
    private String uid;
    private RealmList<RealmCodeValue> codeValues;
    private String comment;
    private String timestamp;
    private String latitude;
    private String longitude;

    public String getUid() {
        return uid;
    }

    public RealmList<RealmCodeValue> getCodeValues() {
        return codeValues;
    }

    public void setCodeValues(RealmList<RealmCodeValue> codeValues) {
        this.codeValues = codeValues;
    }

    public String getComment() {
        return comment;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


}
