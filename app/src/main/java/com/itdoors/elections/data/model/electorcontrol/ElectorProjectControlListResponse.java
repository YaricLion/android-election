package com.itdoors.elections.data.model.electorcontrol;

import com.itdoors.elections.data.model.ProjectControlResponseListItem;

public interface ElectorProjectControlListResponse extends ProjectControlResponseListItem {
}
