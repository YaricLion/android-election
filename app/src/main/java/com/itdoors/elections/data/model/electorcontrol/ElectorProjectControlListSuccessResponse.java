package com.itdoors.elections.data.model.electorcontrol;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.itdoors.elections.data.model.electorprojectscontrol.ElectorProjectControlResponse;

public final class ElectorProjectControlListSuccessResponse implements ElectorProjectControlListResponse, Parcelable {

    @SerializedName("code")
    private final int code;
    @SerializedName("project_control")
    private final ElectorProjectControlResponse projectControl;

    public ElectorProjectControlListSuccessResponse(int code, ElectorProjectControlResponse projectControl) {
        this.code = code;
        this.projectControl = projectControl;
    }

    @Override
    public int getCode() {
        return code;
    }

    public ElectorProjectControlResponse getProjectControl() {
        return projectControl;
    }

    @Override
    public String toString() {
        return "ElectorProjectControlListSuccessResponse{" +
                "code=" + code +
                ", projectControl=" + projectControl +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.code);
        dest.writeParcelable(this.projectControl, 0);
    }

    protected ElectorProjectControlListSuccessResponse(Parcel in) {
        this.code = in.readInt();
        this.projectControl = in.readParcelable(ElectorProjectControlResponse.class.getClassLoader());
    }

    public static final Creator<ElectorProjectControlListSuccessResponse> CREATOR = new Creator<ElectorProjectControlListSuccessResponse>() {
        public ElectorProjectControlListSuccessResponse createFromParcel(Parcel source) {
            return new ElectorProjectControlListSuccessResponse(source);
        }

        public ElectorProjectControlListSuccessResponse[] newArray(int size) {
            return new ElectorProjectControlListSuccessResponse[size];
        }
    };
}
