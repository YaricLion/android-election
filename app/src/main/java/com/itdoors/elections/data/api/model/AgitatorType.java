package com.itdoors.elections.data.api.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.itdoors.elections.R;

import timber.log.Timber;

/**
 * Created by v014nd on 07.10.2015.
 */
public enum AgitatorType implements Parcelable{

    TENT    ("tent",   R.string.agitator_type_tent),
    JUNIOR  ("junior", R.string.agitator_type_junior),
    MIDDLE  ("middle", R.string.agitator_type_middle),
    SENIOR  ("senior", R.string.agitator_type_seniour),
    CEO     ("ceo",    R.string.agitator_type_ceo),
    UNKNOWN ("?",      R.string.agitator_type_unknown);

    private final String code;
    private final int resourceNameId;

    AgitatorType(String code, int resourceNameId) {
        this.code = code;
        this.resourceNameId = resourceNameId;
    }

    public static AgitatorType fromCode(String code){

        if("tent".equalsIgnoreCase(code))            return TENT;
        else if("junior".equalsIgnoreCase(code))     return JUNIOR;
        else if("middle".equalsIgnoreCase(code))     return MIDDLE;
        else if("senior".equalsIgnoreCase(code))     return SENIOR;
        else if("ceo".equalsIgnoreCase(code))        return CEO;
        else if(code != null && !code.isEmpty()) {
            Timber.e("Unknown agitator type : " + code);
            return UNKNOWN;
        }
        else {
            Timber.e("Unknown agitator type : null");
            return null;
        }
    }

    @Override public String toString() {
        return code;
    }

    public int getResourceNameId() {
        return resourceNameId;
    }

    public static final Parcelable.Creator<AgitatorType> CREATOR = new Parcelable.Creator<AgitatorType>() {

        public AgitatorType createFromParcel(Parcel in) {
            return AgitatorType.values()[in.readInt()];
        }
        public AgitatorType[] newArray(int size) {
            return new AgitatorType[size];
        }
    };

    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel out, int flags) {
        out.writeInt(ordinal());
    }
}
