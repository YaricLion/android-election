package com.itdoors.elections.data.api.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.common.base.MoreObjects;
import com.google.gson.annotations.SerializedName;

public final class Code implements Parcelable {

    @NonNull @SerializedName("name")
    private final String name;
    @NonNull @SerializedName("code")
    private final String code;

    public Code(String name, String code) {
        this.name = name;
        this.code = code;
    }

    @NonNull public String getName() {
        return name;
    }

    @NonNull public String getCode() {
        return code;
    }

    @Override public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .add("code", code)
                .toString();
    }

    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Code code = (Code) o;

        if (!this.name.equals(code.name)) return false;
        return this.code.equals(code.code);

    }

    @Override public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + code.hashCode();
        return result;
    }

    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.code);
    }

    protected Code(Parcel in) {
        this.name = in.readString();
        this.code = in.readString();
    }

    public static final Parcelable.Creator<Code> CREATOR = new Parcelable.Creator<Code>() {
        public Code createFromParcel(Parcel source) {
            return new Code(source);
        }

        public Code[] newArray(int size) {
            return new Code[size];
        }
    };
}
