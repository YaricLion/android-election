package com.itdoors.elections.data.db.realm.rx;

import com.itdoors.elections.data.db.RealmDataService;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.exceptions.RealmException;
import rx.Observable;
import rx.Subscriber;

public class RemoveAllOnSubscribe<E extends RealmObject> implements Observable.OnSubscribe<Void> {

    private final Class<E> clazz;

    public RemoveAllOnSubscribe(Class<E> clazz) {
        this.clazz = clazz;
    }

    @Override
    public void call(Subscriber<? super Void> subscriber) {
        Realm bgRealm = null;
        boolean withError = false;
        try {
            bgRealm = Realm.getDefaultInstance();
            if (bgRealm == null)
                throw new RealmDataService.RealmNotInitException("Failed to init realm");

            bgRealm.beginTransaction();
            bgRealm.delete(clazz);
            bgRealm.commitTransaction();
        } catch (Throwable throwable) {
            withError = true;
            if (bgRealm != null)
                bgRealm.cancelTransaction();
            subscriber.onError(new RealmException("Error when try to add clazz : " + clazz.toString(), throwable));
        } finally {
            if (bgRealm != null)
                bgRealm.close();
        }
        if (!withError)
            subscriber.onNext((Void) null);
        subscriber.onCompleted();
    }

}
