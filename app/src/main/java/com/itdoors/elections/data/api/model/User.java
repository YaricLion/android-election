package com.itdoors.elections.data.api.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.gson.annotations.SerializedName;

/**
 * Created by v014nd on 09.09.2015.
 */
public final class User implements Parcelable {

    @NonNull @SerializedName("user_id")
    private final int id;
    @SerializedName("user_name")
    private final String name;
    @SerializedName("user_surname")
    private final String surname;
    @SerializedName("user_email")
    private final String email;

    @SerializedName("user_photo")
    private final String photo_link;

    public User(@NonNull int id, String name, String surname, String email, String photo_link) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.photo_link = photo_link;
    }

    public String getName() {
        return name;
    }

    @NonNull
    public int getId() {
        return id;
    }

    public String getSurname() {
        return surname;
    }

    public String getPhotoLink() {
        return photo_link;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("surname", surname)
                .add("email", email)
                .add("photo_link", photo_link)
                .toString();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.surname);
        dest.writeString(this.email);
        dest.writeString(this.photo_link);
    }

    protected User(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.surname = in.readString();
        this.email = in.readString();
        this.photo_link = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
