package com.itdoors.elections.data.model.electorcontrol;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public final class ElectorControlSend {

    @SerializedName("guid")
    private final String electorId;
    @SerializedName("date")
    private final String dateTimestamp;
    @SerializedName("lat")
    private final String latitude;
    @SerializedName("long")
    private final String longitude;

    public ElectorControlSend(String electorId, String dateTimestamp, String latitude, String longitude) {
        this.electorId = electorId;
        this.dateTimestamp = dateTimestamp;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    private ElectorControlSend(Builder builder) {
        electorId = builder.electorId;
        dateTimestamp = builder.dateTimestamp;
        latitude = builder.latitude;
        longitude = builder.longitude;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(ElectorControlSend copy) {
        Builder builder = new Builder();
        builder.electorId = copy.electorId;
        builder.dateTimestamp = copy.dateTimestamp;
        builder.latitude = copy.latitude;
        builder.longitude = copy.longitude;
        return builder;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getDateTimestamp() {
        return dateTimestamp;
    }

    public String getElectorId() {
        return electorId;
    }


    public static final class Builder {
        private String electorId;
        private String dateTimestamp;
        private String latitude;
        private String longitude;

        private Builder() {
        }

        public Builder uid(String val) {
            electorId = val;
            return this;
        }

        public Builder dateTimestamp(String val) {
            dateTimestamp = val;
            return this;
        }

        public Builder latitude(String val) {
            latitude = val;
            return this;
        }

        public Builder longitude(String val) {
            longitude = val;
            return this;
        }

        public ElectorControlSend build() {
            return new ElectorControlSend(this);
        }
    }
}
