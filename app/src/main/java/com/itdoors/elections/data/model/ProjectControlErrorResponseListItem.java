package com.itdoors.elections.data.model;

/**
 * Created by yariclion on 27.06.16.
 */
public interface ProjectControlErrorResponseListItem extends ProjectControlResponseListItem {
    String getMessage();
}
