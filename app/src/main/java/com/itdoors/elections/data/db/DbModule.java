package com.itdoors.elections.data.db;

import android.app.Application;

import com.itdoors.elections.BuildConfig;
import com.itdoors.elections.PerApp;

import dagger.Module;
import dagger.Provides;
//import io.realm.RealmConfiguration;

@Module
public final class DbModule {

    /*  private final RealmConfiguration realmConfiguration;

      public DbModule(RealmConfiguration realmConfiguration) {
          this.realmConfiguration = realmConfiguration;
      }
  */
    /*@Provides @PerApp RealmConfiguration provideRealmConfiguration(){
        return realmConfiguration;
    }
*/
    @Provides
    @PerApp
    DataService provideDataService() {
        return new RealmDataService();
    }

}
