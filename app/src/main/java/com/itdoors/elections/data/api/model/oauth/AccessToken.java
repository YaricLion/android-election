package com.itdoors.elections.data.api.model.oauth;

import com.google.gson.annotations.SerializedName;

public final class AccessToken {

    @SerializedName("access_token")
    private final String token;

    @SerializedName("expires_in")
    private final int expiresIn;

    @SerializedName("token_type")
    private final String tokenType;

    @SerializedName("scope")
    private final String scope;

    @SerializedName("refresh_token")
    private final String refreshToken;

    public AccessToken(String token, int expiresIn, String tokenType, String scope,
                       String refreshToken) {
        this.token = token;
        this.expiresIn = expiresIn;
        this.tokenType = tokenType;
        this.scope = scope;
        this.refreshToken = refreshToken;

    }

    public String getToken() {
        return token;
    }

    public int getExpiresIn() {
        return expiresIn;
    }

    public String getTokenType() {
        return tokenType;
    }

    public String getScope() {
        return scope;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    @Override
    public String toString() {
        return "AccessToken{" +
                "accessToken='" + token + '\'' +
                ", tokenType='" + tokenType + '\'' +
                ", expiresIn=" + expiresIn +
                ", refreshToken='" + refreshToken + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccessToken that = (AccessToken) o;

        if (expiresIn != that.expiresIn) return false;
        if (!refreshToken.equals(that.refreshToken)) return false;
        if (!scope.equals(that.scope)) return false;
        if (!token.equals(that.token)) return false;
        if (!tokenType.equals(that.tokenType)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = token.hashCode();
        result = 31 * result + expiresIn;
        result = 31 * result + tokenType.hashCode();
        result = 31 * result + scope.hashCode();
        result = 31 * result + refreshToken.hashCode();
        return result;
    }
}
