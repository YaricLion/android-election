package com.itdoors.elections.data.api;

import com.itdoors.elections.BuildConfig;
import com.itdoors.elections.data.api.model.oauth.AccessToken;

import retrofit.http.Field;
import retrofit.http.Headers;
import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

public interface OauthService {

    class GrandType {
        public static final String PASSWORD = "password";
        public static final String REFRESH_TOKEN = "refresh_token";
    }

    @Headers({
            "Accept: application/json",
    })
    @GET("/oauth/v2/token")
    Observable<AccessToken> getAccessToken(
            @Query("client_id")
            String clientId,
            @Query("client_secret")
            String clientSecret,
            @Query("grant_type")
            String grantType,
            @Query("username")
            String username,
            @Query("password")
            String password
    );

}
