package com.itdoors.elections.data;


import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.itdoors.elections.BuildConfig;
import com.itdoors.elections.PerApp;
import com.itdoors.elections.R;
import com.itdoors.elections.data.api.OauthInterceptor;
import com.itdoors.elections.data.api.deserializer.AgitatorDeserializer;
import com.itdoors.elections.data.api.deserializer.AgitatorProjectControlDeserializer;
import com.itdoors.elections.data.api.deserializer.AgitatorProjectControlListResponseDeserializer;
import com.itdoors.elections.data.api.deserializer.DateTimeConverter;
import com.itdoors.elections.data.api.deserializer.ElectorProjectControlDeserializer;
import com.itdoors.elections.data.api.deserializer.ElectorProjectControlListResponseDeserializer;
import com.itdoors.elections.data.model.Agitator;
import com.itdoors.elections.data.api.model.User;
import com.itdoors.elections.data.api.model.oauth.AccessToken;
import com.itdoors.elections.data.model.agitatorprojectscontrol.AgitatorProjectControlListResponse;
import com.itdoors.elections.data.model.agitatorprojectscontrol.AgitatorProjectControlResponse;
import com.itdoors.elections.data.model.electorcontrol.ElectorProjectControlListResponse;
import com.itdoors.elections.data.model.electorprojectscontrol.ElectorProjectControlResponse;
import com.itdoors.elections.ui.mvp.model.prefs.ApiUrlRxPrefs;
import com.itdoors.elections.ui.mvp.model.prefs.TokenRxPrefs;
import com.itdoors.elections.ui.mvp.model.prefs.UserRxPrefs;
import com.itdoors.elections.util.GuidQrParser;
import com.itdoors.elections.util.Prefs;
import com.itdoors.elections.util.SSLCertificateUtils;
import com.itdoors.elections.util.StringPreference;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.LruCache;
import com.squareup.picasso.Picasso;

import org.joda.time.DateTime;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import timber.log.Timber;

import static android.content.Context.MODE_PRIVATE;

@Module
public final class DataModule {

    static final int DISK_CACHE_SIZE = 50 * 1024 * 1024; // 50MB

    @Provides @PerApp  SharedPreferences provideSharedPreferences(Application app) {
        return app.getSharedPreferences("elections", MODE_PRIVATE);
    }

    @Provides @PerApp
    Gson provideGson() {
        GsonBuilder gson = new GsonBuilder();
        gson.registerTypeAdapter(DateTime.class, new DateTimeConverter());
        gson.registerTypeAdapter(Agitator.class, new AgitatorDeserializer());
        gson.registerTypeAdapter(ElectorProjectControlResponse.class, new ElectorProjectControlDeserializer());
        gson.registerTypeAdapter(AgitatorProjectControlResponse.class, new AgitatorProjectControlDeserializer());

        gson.registerTypeAdapter(ElectorProjectControlListResponse.class, new ElectorProjectControlListResponseDeserializer());
        gson.registerTypeAdapter(AgitatorProjectControlListResponse.class, new AgitatorProjectControlListResponseDeserializer());

        return gson.create();
    }

    @Provides @PerApp
    OkHttpClient provideOkHttpClient(Application app, OauthInterceptor oauthInterceptor) {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(message -> Timber.tag("OkHttp").d(message));
        loggingInterceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        return createOkHttpClient(app, oauthInterceptor, loggingInterceptor);
    }

    @Provides @PerApp
    LruCache provideMemoryLruCache(Application app){
        return new LruCache(app);
    }

    @Provides @PerApp
    Picasso providePicasso(Application app, OkHttpClient client, LruCache lruMemoryCache) {
        return new Picasso.Builder(app)
                .downloader(new OkHttp3Downloader(client))
                .memoryCache(lruMemoryCache)
                .listener((picasso, uri, e) -> Timber.e(e, "Failed to load image: %s", uri))
                .build();
    }

    static OkHttpClient createOkHttpClient(Application app, Interceptor... interceptors) {

        File cacheDir = new File(app.getCacheDir(), "http");
        Cache cache = new Cache(cacheDir, DISK_CACHE_SIZE);

        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        for(Interceptor interceptor : interceptors)
            builder.addInterceptor(interceptor);

        X509TrustManager trustManager;
        SSLSocketFactory sslSocketFactory;
        try {
            trustManager = SSLCertificateUtils.trustManagerForCertificates(trustedCertificatesInputStream(app));
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, new TrustManager[]{trustManager}, null);
            sslSocketFactory = sslContext.getSocketFactory();
        } catch (GeneralSecurityException | IOException e) {
            throw new RuntimeException(e);
        }

        //client.setSslSocketFactory(sslSocketFactory);
        //client.setHostnameVerifier((hostname, session) -> true);

        OkHttpClient client = builder.cache(cache)
                .writeTimeout(60, TimeUnit.SECONDS)
                .sslSocketFactory(sslSocketFactory)
                .hostnameVerifier((hostname, session) -> true)
                .build();


        return client;
    }

    private static InputStream trustedCertificatesInputStream(Context context) throws IOException {
        //return context.getResources().openRawResource(R.raw.election_ch);
        //return context.getResources().openRawResource(R.raw.election_ch);

        return context.getAssets().open("cert/" + BuildConfig.CERTIFICATE);
    }


    @Provides
    @PerApp
    GuidQrParser provideGuiParser() {
        return GuidQrParser.getInstance();
    }

    @Provides
    @PerApp
    ReactiveLocationProvider provideReactiveLocationProvider(Application application) {
        return new ReactiveLocationProvider(application);
    }
    @Provides @PerApp @Named("userInfo") StringPreference provideUserPrefs(SharedPreferences prefs){
        return new StringPreference(prefs, "user_info");
    }

    @Provides @PerApp @Named("tokenInfo") StringPreference provideTokenPrefs(SharedPreferences prefs){
        return new StringPreference(prefs, "token_info");
    }

    @Provides
    @PerApp
    @Named("apiUrlPrefs")
    StringPreference provideApiUrlPrefs(SharedPreferences prefs) {
        return new StringPreference(prefs, "api_url");
    }

    @Provides
    @PerApp
    @Named("tokenPrefs")
    Prefs<AccessToken> provideAccessTokenPrefsHelper(@Named("tokenInfo") StringPreference prefs, Gson gson) {
        return new Prefs.PrefsImpl<>(prefs, gson, AccessToken.class);
    }

    @Provides
    @PerApp
    @Named("userPrefs")
    Prefs<User> provideUserPrefsHelper(@Named("userInfo") StringPreference prefs, Gson gson) {
        return new Prefs.PrefsImpl<>(prefs, gson, User.class);
    }


    @Provides
    @PerApp
    TokenRxPrefs provideTokenRxPrefs(@Named("tokenPrefs") Prefs<AccessToken> prefs) {
        return new TokenRxPrefs(prefs);
    }

    @Provides
    @PerApp
    UserRxPrefs provideUserRxPrefs(@Named("userPrefs") Prefs<User> prefs) {
        return new UserRxPrefs(prefs);
    }

    @Provides
    @PerApp
    ApiUrlRxPrefs provideApiUrlRxPrefs(@Named("apiUrlPrefs") StringPreference prefs) {
        return new ApiUrlRxPrefs(prefs);
    }

}
