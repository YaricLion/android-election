package com.itdoors.elections.data.model;

/**
 * Created by v014nd on 07.06.2016.
 */
public interface ControlStorageViewModel extends ViewModel {
    String uid();

    int timestamp();
}
