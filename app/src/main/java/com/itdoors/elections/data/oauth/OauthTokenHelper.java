package com.itdoors.elections.data.oauth;

import com.google.gson.Gson;
import com.itdoors.elections.BuildConfig;
import com.itdoors.elections.data.api.OauthService;
import com.itdoors.elections.data.api.model.oauth.AccessToken;
import com.itdoors.elections.util.Prefs;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit.Endpoint;
import timber.log.Timber;


/**
 * Helper class to refresh token
 * @author YaricLion
 */
public final class OauthTokenHelper {

    private final Prefs<AccessToken> tokenHelper;
    private final OkHttpClient client;
    private final Endpoint endpoint;
    private final Gson gson;

    private final Object lock = new Object();
    private final AtomicBoolean isRefresh = new AtomicBoolean(false);

    public static class AccessTokenNotExistedException extends IOException{
        public AccessTokenNotExistedException(String msg){
            super(msg);
        }
    }

    public static class NeedToReloginException extends IOException{
        public NeedToReloginException(String msg){
            super(msg);
        }
    }

    public static class UnknownRefreshException extends IOException{
        public UnknownRefreshException(String msg){
            super(msg);
        }
    }

    public OauthTokenHelper(Prefs<AccessToken> tokenHelper, OkHttpClient client, Endpoint endpoint, Gson gson) {

        this.tokenHelper = tokenHelper;
        this.client = client;
        this.endpoint = endpoint;
        this.gson = gson;
    }

    /**
     * Refresh token
     *
     * If many threads trying to refresh token simultaneously, only one will start request to refresh the token.
     * This thread will set the state to refreshing, while other threads will be blocked and wil wait for refreshing end.
     * If thread see the state @isRefreshing == false he will start refresh anyway even if the token was refreshed
     * previously. This may happen if to much requests is running in thread pool (more that pool size),
     * so simultaneously running will be only few ot them while others waiting in blocking queue.
     */

    public void refresh() throws IOException {
        if (isRefresh.compareAndSet(false, true)) {
            synchronized (lock) {
                try {
                    makeActualRefreshRequestAndSave();
                } finally {
                    isRefresh.set(false);
                    lock.notifyAll();
                }
            }
        }
        else {
            synchronized (lock) {
                while (isRefresh.get()) {
                    try {
                        lock.wait();
                    } catch (InterruptedException ignored) {
                    }
                }
            }
        }
    }

    /**
     * Make refresh token request
     */

    private void makeActualRefreshRequestAndSave() throws IOException{

        Timber.d("Start to refresh token");
        validateTokenExisting();

        AccessToken token = tokenHelper.get();
        try {

            Request request = new Request.Builder()
                    .url(endpoint.getUrl() + "/oauth/v2/token")
                    .header("Accept", "application/json")
                    .post(new FormBody.Builder()
                            .add("client_id", BuildConfig.CLIENT_ID)
                            .add("client_secret", BuildConfig.CLIENT_SECRET)
                            .add("grant_type", OauthService.GrandType.REFRESH_TOKEN)
                            .add("refresh_token", token.getRefreshToken())
                            .build()) //
                    .build();

            IOException e = null;

            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                AccessToken newAccessToken = gson.fromJson(response.body().string(), AccessToken.class);
                if (newAccessToken != null ) {
                    Timber.d("Saving the new token : " + newAccessToken.toString());
                    tokenHelper.set(newAccessToken);
                }
                else{
                    e = new UnknownRefreshException("Error when try to refresh token");
                }
            }
            else {
                e = (response.code() == 401 || response.code() == 400) ?
                   new NeedToReloginException("User token has expired or rejected") :
                   new UnknownRefreshException("Error when try to refresh token");
            }
            if(e != null){
                throw e;
            }
        }
        catch (IOException ex){
            Timber.e(ex, "Failed to refresh token");
            throw ex;
        }

    }

    private void validateTokenExisting() throws IOException{
        if(!tokenHelper.isSet()){
            IOException e = new AccessTokenNotExistedException("Try to refresh token that is not existing");
            Timber.e(e, "Token is not existing");
            throw e;
        }
    }
}
