package com.itdoors.elections.data.api;

import android.net.Uri;

import com.itdoors.elections.PerApp;
import com.itdoors.elections.data.api.model.oauth.AccessToken;
import com.itdoors.elections.util.Prefs;


import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Named;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import timber.log.Timber;

/**
 * Created by v014nd on 10.09.2015.
 */

@PerApp
public final class OauthInterceptor implements Interceptor{

    public static final String access_token = "access_token";

    private static final String token_path = "/oauth/v2/token";
    private static final String IMAGE_LINK_REGEX = "(http(s?):\\/\\/.*\\.)(jpe?g|png|gif)(\\?.*)?$";

    private final Prefs<AccessToken> tokenHelper;

    @Inject
    public OauthInterceptor(@Named("tokenPrefs") Prefs<AccessToken> tokenHelper) {
        this.tokenHelper = tokenHelper;
    }

    private Request signRequest(Request request, String token){

        String method = request.method();

        Timber.d("Retrofit Sign request with token: " + method  + ", " + token);
        Uri uri = Uri.parse(request.url().toString()).buildUpon()
             .appendQueryParameter(access_token, token)
             .build();

        Request newRequest = new Request.Builder()
            .url(uri.toString())
            .headers(request.headers())
            .method(request.method(), request.body())
            .tag(request.tag())
            .build();
        return newRequest;

    }


    private static boolean isOAuthRequest(Request request){
        String path = request.url().encodedPath();
        return path !=null && path.contains(token_path);
    }

    /**
     * For getUser request with token request
     */
    private static boolean hasTokenInside(Request request) {
        String query = request.url().query();
        return  query != null && query.contains(access_token);
    }


    private static boolean isImageRequest(Request request) {
        return request.url().toString().matches(IMAGE_LINK_REGEX);
    }

    @Override  public Response intercept(Chain chain) throws IOException {

        Timber.d("Intercept request");

        final Request request = chain.request();

        if(isOAuthRequest(request)) {
            Timber.d("Catch oAuth token request : %s. Proceed...", request.url());
            return chain.proceed(request);
        }
        if(hasTokenInside(request)) {
            Timber.d("Catch token inside request : %s. Proceed...", request.url());
            return chain.proceed(request);
        }
        if(isImageRequest(request)) {
            Timber.d("Catch image request : %s. Proceed...", request.url());
            return chain.proceed(request);
        }

        Timber.d("token : " + tokenHelper.get());

        if(tokenHelper.isSet()) {
            Request signedRequest = signRequest(request, tokenHelper.get().getToken());
            return chain.proceed(signedRequest);
        }

        return chain.proceed(request);

    }




}
