package com.itdoors.elections.data.api.model.oauth;

/**
 * @link https://tools.ietf.org/html/rfc6749#page-45
 */


public enum OAuthErrorType {

    INVALID_REQUEST, INVALID_CLIENT, INVALID_GRANT, UNAUTHORIZED_CLIENT, UNSUPPORTED_GRANT_TYPE, INVALID_SCOPE,  UNKNOWN;

    public static OAuthErrorType fromString(String code) {

        for(OAuthErrorType type: OAuthErrorType.values()){
            if(type.toString().equalsIgnoreCase(code))
                return type;
        }
        return UNKNOWN;
    }

    @Override
    public String toString() {
        switch (this){
            case INVALID_REQUEST:
                return "invalid_request";
            case INVALID_CLIENT:
                return "invalid_client";
            case INVALID_GRANT:
                return "invalid_grant";
            case UNAUTHORIZED_CLIENT:
                return "unauthorized_client";
            case UNSUPPORTED_GRANT_TYPE:
                return "unsupported_grant_type";
            case INVALID_SCOPE:
                return "invalid_scope";
            default:
                return "unknown";
        }
    }
}
