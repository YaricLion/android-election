package com.itdoors.elections.data.db.realm.rx;

import com.itdoors.elections.data.db.RealmDataService;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.exceptions.RealmException;
import rx.Observable;
import rx.Subscriber;


public abstract class AddOnSubscribe<E> implements Observable.OnSubscribe<Void> {

    private final E entity;

    public AddOnSubscribe(E entity) {
        this.entity = entity;
    }

    @Override
    public void call(Subscriber<? super Void> subscriber) {

        Realm bgRealm = null;
        boolean withError = false;

        try {
            bgRealm = Realm.getDefaultInstance();
            if (bgRealm == null)
                throw new RealmDataService.RealmNotInitException("Failed to init realm");

            bgRealm.beginTransaction();
            RealmObject realmObject = convert(entity);
            bgRealm.copyToRealm(realmObject);
            bgRealm.commitTransaction();
        } catch (Throwable throwable) {
            withError = true;
            if (bgRealm != null)
                bgRealm.cancelTransaction();
            subscriber.onError(new RealmException("Error when try to add entity : " + entity.toString(), throwable));
        } finally {
            if (bgRealm != null)
                bgRealm.close();
        }
        if (!withError)
            subscriber.onNext((Void) null);

        subscriber.onCompleted();
    }

    abstract RealmObject convert(E entity);

}
