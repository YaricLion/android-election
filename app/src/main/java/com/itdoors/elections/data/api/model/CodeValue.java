package com.itdoors.elections.data.api.model;


import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public final class CodeValue {

    @NonNull @SerializedName("code")
    private final String code;
    @NonNull @SerializedName("value")
    private final String value;

    public CodeValue(@NonNull String code, @NonNull String value) {
        this.code = code;
        this.value = value;
    }

    @NonNull
    public String getCode() {
        return code;
    }

    @NonNull
    public String getValue() {
        return value;
    }

}
