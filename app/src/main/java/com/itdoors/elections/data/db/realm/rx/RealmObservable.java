package com.itdoors.elections.data.db.realm.rx;

import android.content.Context;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import rx.Observable;
import rx.functions.Func1;

public final class RealmObservable {

    private RealmObservable() {
        throw new AssertionError("No instances!");
    }

    public static <E> Observable<Void> add(final E entity, final Func1<E, RealmObject> convertFunc) {
        return Observable.create(new AddOnSubscribe<E>(entity) {
            @Override RealmObject convert(E entity) {
                return convertFunc.call(entity);
            }
        });
    }

    public static <E> Observable<Void> listAdd(final List<E> entities, final Func1<E, RealmObject> convertFunc) {
        return Observable.create(new AddListOnSubscribe<E>(entities) {
            @Override RealmObject convert(E entity) {
                return convertFunc.call(entity);
            }
        });
    }

    public static <E> Observable<Void> listAddOrUpdate(final List<E> entities, final Func1<E, RealmObject> convertFunc) {
        return Observable.create(new AddOrUpdateListOnSubscribe<E>(entities) {
            @Override RealmObject convert(E entity) {
                return convertFunc.call(entity);
            }
        });
    }

    public static <E> Observable<Void> addOrUpdateWithoutPK(final E entity, final Func1<E, RealmObject> convertFunc) {
        return Observable.create(new AddOrUpdateWithoutPKOnSubscribe<E>(entity) {
            @Override RealmObject convert(E entity) {
                return convertFunc.call(entity);
            }
        });
    }

    public static <E extends RealmObject> Observable<Void> removeAll(final Class<E> clazz) {
        return Observable.create(new RemoveAllOnSubscribe<E>(clazz));
    }

    public static <T extends RealmObject> Observable<RealmResults<T>> results(final Func1<Realm, RealmResults<T>> function) {
        return Observable.create(new OnSubscribeRealm<RealmResults<T>>() {
            @Override public RealmResults<T> get(Realm realm) {
                return function.call(realm);
            }
        });
    }


}
