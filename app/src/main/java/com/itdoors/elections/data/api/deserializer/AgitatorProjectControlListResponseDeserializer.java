package com.itdoors.elections.data.api.deserializer;

import com.google.common.base.Optional;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.itdoors.elections.data.model.agitatorprojectscontrol.AgitatorProjectControlListErrorResponse;
import com.itdoors.elections.data.model.agitatorprojectscontrol.AgitatorProjectControlListResponse;
import com.itdoors.elections.data.model.agitatorprojectscontrol.AgitatorProjectControlListSuccessResponse;
import com.itdoors.elections.data.model.agitatorprojectscontrol.AgitatorProjectControlResponse;

import java.lang.reflect.Type;

import static com.itdoors.elections.data.api.deserializer.IsResponseSuccessfulFunc.DEFAULT;

/**
 * Created by yariclion on 27.06.16.
 */
public final class AgitatorProjectControlListResponseDeserializer implements JsonDeserializer<AgitatorProjectControlListResponse> {

    @Override
    public AgitatorProjectControlListResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jObj = (JsonObject) json;
        Optional<JsonElement> code = Optional.fromNullable(jObj.get("code"));
        if (!code.isPresent())
            throw new JsonParseException("Failed to parse agitator project control response without any code inside");

        int codeResponse = code.get().getAsInt();
        if (DEFAULT.isSuccessful(codeResponse)) {

            Optional<JsonObject> projectControl = Optional.fromNullable(jObj.getAsJsonObject("project_control"));
            if (!projectControl.isPresent())
                throw new JsonParseException("Failed to parse agitator project control response without any control info inside");

            AgitatorProjectControlResponse projectControlResponse = AgitatorProjectControlDeserializer.deserialize(projectControl.get());
            return new AgitatorProjectControlListSuccessResponse(codeResponse, projectControlResponse);
        } else {

            Optional<JsonElement> msg = Optional.fromNullable(jObj.get("message"));
            Optional<JsonElement> projectGuid = Optional.fromNullable(jObj.get("project_guid"));
            Optional<JsonElement> supporterGuid = Optional.fromNullable(jObj.get("supporter_guid"));
            Optional<JsonElement> scanDateTime = Optional.fromNullable(jObj.get("scan_date_time"));

            return new AgitatorProjectControlListErrorResponse(
                    codeResponse,
                    msg.isPresent() ? msg.get().getAsString() : null,
                    projectGuid.isPresent() ? projectGuid.get().getAsString() : null,
                    supporterGuid.isPresent() ? supporterGuid.get().getAsString() : null,
                    scanDateTime.isPresent() ? scanDateTime.get().getAsString() : null
            );
        }

    }

}
