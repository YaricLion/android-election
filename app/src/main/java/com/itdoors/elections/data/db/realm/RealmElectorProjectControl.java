package com.itdoors.elections.data.db.realm;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by yariclion on 27.06.16.
 */
public class RealmElectorProjectControl extends RealmObject {
    private String projectGui;
    private String supporterGuid;
    private String latitude;
    private String longitude;
    private String scanDateTime;

    public void setScanDateTime(String scanDateTime) {
        this.scanDateTime = scanDateTime;
    }

    public void setProjectGui(String projectGui) {
        this.projectGui = projectGui;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setSupporterGuid(String supporterGuid) {
        this.supporterGuid = supporterGuid;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getProjectGui() {
        return projectGui;
    }

    public String getScanDateTime() {
        return scanDateTime;
    }

    public String getSupporterGuid() {
        return supporterGuid;
    }
}
