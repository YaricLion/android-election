package com.itdoors.elections.data.model.agitatorcontrol;

import com.google.gson.annotations.SerializedName;
import com.itdoors.elections.data.api.model.CodeValue;
import com.itdoors.elections.data.model.ControlStorageViewModel;

import java.util.List;

/**
 * Created by v014nd on 07.06.2016.
 */
public class AgitatorControlViewModel implements ControlStorageViewModel {

    private final String uid;
    private final List<CodeValue> codeValues;
    private final String comment;
    private final String timestamp;

    public AgitatorControlViewModel(String uid, List<CodeValue> codeValues, String comment, String timestamp) {
        this.uid = uid;
        this.codeValues = codeValues;
        this.comment = comment;
        this.timestamp = timestamp;
    }

    private AgitatorControlViewModel(Builder builder) {
        uid = builder.uid;
        codeValues = builder.codeValues;
        comment = builder.comment;
        timestamp = builder.timestamp;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(AgitatorControlViewModel copy) {
        Builder builder = new Builder();
        builder.uid = copy.uid;
        builder.codeValues = copy.codeValues;
        builder.comment = copy.comment;
        builder.timestamp = copy.timestamp;
        return builder;
    }

    @Override
    public String uid() {
        return uid;
    }

    @Override
    public int timestamp() {
        return Integer.parseInt(timestamp);
    }


    public String getComment() {
        return comment;
    }

    @Override
    public String toString() {
        return "AgitatorControlViewModel{" +
                "uid='" + uid + '\'' +
                ", codeValues=" + codeValues +
                ", comment='" + comment + '\'' +
                ", timestamp='" + timestamp + '\'' +
                '}';
    }

    public static final class Builder {
        private String uid;
        private List<CodeValue> codeValues;
        private String comment;
        private String timestamp;

        public Builder() {
        }

        public Builder uid(String val) {
            uid = val;
            return this;
        }

        public Builder codeValues(List<CodeValue> val) {
            codeValues = val;
            return this;
        }

        public Builder comment(String val) {
            comment = val;
            return this;
        }

        public Builder timestamp(String val) {
            timestamp = val;
            return this;
        }

        public AgitatorControlViewModel build() {
            return new AgitatorControlViewModel(this);
        }
    }
}
