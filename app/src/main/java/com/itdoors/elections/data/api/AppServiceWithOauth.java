package com.itdoors.elections.data.api;

import com.itdoors.elections.data.api.model.Project;
import com.itdoors.elections.data.model.Agitator;
import com.itdoors.elections.data.model.ElectionDay;
import com.itdoors.elections.data.model.agitatorcontrol.AgitatorControlResponseOld;
import com.itdoors.elections.data.model.agitatorcontrol.AgitatorControlSend;
import com.itdoors.elections.data.model.agitatorcontrol.AgitatorControlResponse;
import com.itdoors.elections.data.api.model.Code;
import com.itdoors.elections.data.model.Elector;
import com.itdoors.elections.data.model.agitatorprojectscontrol.AgitatorProjectControlListResponse;
import com.itdoors.elections.data.model.agitatorprojectscontrol.AgitatorProjectControlResponse;
import com.itdoors.elections.data.model.agitatorprojectscontrol.AgitatorProjectControlSend;
import com.itdoors.elections.data.model.electorcontrol.ElectorControlResponseOld;
import com.itdoors.elections.data.model.electorcontrol.ElectorControlSend;
import com.itdoors.elections.data.model.electorcontrol.ElectorControlResponse;
import com.itdoors.elections.data.api.model.User;
import com.itdoors.elections.data.model.electorcontrol.ElectorProjectControlListResponse;
import com.itdoors.elections.data.model.electorprojectscontrol.ElectorProjectControlResponse;
import com.itdoors.elections.data.model.electorprojectscontrol.ElectorProjectControlSend;
import com.itdoors.elections.data.oauth.OauthTokenHelper;
import com.itdoors.elections.data.oauth.RetryWithTokenOperator;
import com.squareup.otto.Bus;

import java.util.List;

import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.Path;
import rx.Observable;

public class AppServiceWithOauth implements AppService{

    private final AppService delegate;

    private final OauthTokenHelper ouaOauthTokenHelper;
    private final Bus bus;

    public AppServiceWithOauth(AppService delegate, OauthTokenHelper ouaOauthTokenHelper, Bus bus) {
        this.delegate = delegate;
        this.ouaOauthTokenHelper = ouaOauthTokenHelper;
        this.bus = bus;
    }

    @Override public Observable<Agitator> getAgitator(@Path("uid") String guid) {
        return delegate.getAgitator(guid)
                .retryWhen(new RetryWithTokenOperator(ouaOauthTokenHelper, bus));
    }

    @Override public Observable<Elector> verifyElector(@Path("uid") String guid) {
        return delegate.verifyElector(guid)
                .retryWhen(new RetryWithTokenOperator(ouaOauthTokenHelper, bus));
    }

    @Override public Observable<ElectorControlResponseOld> controlElectors(@Field("uid") String electorId, @Field("date") String dateTimestamp, @Field("lat") String latitude, @Field("long") String longitude) {
        return delegate.controlElectors(electorId, dateTimestamp, latitude, longitude)
                .retryWhen(new RetryWithTokenOperator(ouaOauthTokenHelper, bus));
    }

    @Override public Observable<User> getUser(@Field(OauthInterceptor.access_token) String token) {
        return delegate.getUser(token)
                .retryWhen(new RetryWithTokenOperator(ouaOauthTokenHelper, bus));
    }

    @Override public Observable<List<Code>> getCodes() {
        return delegate.getCodes()
                .retryWhen(new RetryWithTokenOperator(ouaOauthTokenHelper, bus));
    }

    @Override public Observable<AgitatorControlResponseOld> controlAgitators(@Field("uid") String agitatorId, @Field("codes") String jsonCodeValues, @Field("comment") String comment, @Field("date") String dateTimestamp, @Field("lat") String latitude, @Field("long") String longitude) {
        return delegate.controlAgitators(agitatorId, jsonCodeValues, comment, dateTimestamp, latitude, longitude)
                .retryWhen(new RetryWithTokenOperator(ouaOauthTokenHelper, bus));
    }

    @Override public Observable<Response> controlElectors(@Body List<ElectorControlSend> electorControlItems) {
        return delegate.controlElectors(electorControlItems).retryWhen(new RetryWithTokenOperator(ouaOauthTokenHelper, bus));
    }

    @Override public Observable<Response> controlAgitators(@Body List<AgitatorControlSend> agitatorControlItems) {
        return delegate.controlAgitators(agitatorControlItems).retryWhen(new RetryWithTokenOperator(ouaOauthTokenHelper, bus));
    }

    @Override public Observable<List<Project>> getProjects() {
        return delegate.getProjects()
                .retryWhen(new RetryWithTokenOperator(ouaOauthTokenHelper, bus));
    }

    @Override public Observable<ElectorProjectControlResponse> controlElectorProject(@Body ElectorProjectControlSend item) {
        return delegate.controlElectorProject(item)
                .retryWhen(new RetryWithTokenOperator(ouaOauthTokenHelper, bus));
    }

    @Override public Observable<AgitatorProjectControlResponse> controlAgitatorProject(@Body AgitatorProjectControlSend item) {
        return delegate.controlAgitatorProject(item)
                .retryWhen(new RetryWithTokenOperator(ouaOauthTokenHelper, bus));
    }

    @Override public Observable<Response> controlElectorProject(@Body List<ElectorProjectControlSend> list) {
        return delegate.controlElectorProject(list)
                .retryWhen(new RetryWithTokenOperator(ouaOauthTokenHelper, bus));
    }

    @Override public Observable<Response> controlAgitatorProject(@Body List<AgitatorProjectControlSend> list) {
        return delegate.controlAgitatorProject(list)
                .retryWhen(new RetryWithTokenOperator(ouaOauthTokenHelper, bus));
    }

    @Override public Observable<AgitatorProjectControlResponse> changeAgitatorStatus(@Path("guid") String guid, @Field("state") String state) {
        return delegate.changeAgitatorStatus(guid, state)
                .retryWhen(new RetryWithTokenOperator(ouaOauthTokenHelper, bus));
    }

    @Override public Observable<ElectorProjectControlResponse> changeElectorStatus(@Path("guid") String guid, @Field("state") String state) {
        return delegate.changeElectorStatus(guid, state)
                .retryWhen(new RetryWithTokenOperator(ouaOauthTokenHelper, bus));
    }

    @Override public Observable<ElectionDay> getElectionDayTimes() {
        return delegate.getElectionDayTimes()
                .retryWhen(new RetryWithTokenOperator(ouaOauthTokenHelper, bus));
    }

}
