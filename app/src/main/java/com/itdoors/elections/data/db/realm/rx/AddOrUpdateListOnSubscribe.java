package com.itdoors.elections.data.db.realm.rx;

import com.google.common.collect.ImmutableList;
import com.itdoors.elections.data.db.RealmDataService;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.exceptions.RealmException;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by yariclion on 25.06.16.
 */
public abstract class AddOrUpdateListOnSubscribe<E> implements Observable.OnSubscribe<Void> {

    private final List<E> entities;

    public AddOrUpdateListOnSubscribe(List<E> entities) {
        this.entities = ImmutableList.copyOf(entities);
    }

    @Override
    public void call(Subscriber<? super Void> subscriber) {

        Realm bgRealm = null;
        boolean withError = false;

        try {
            bgRealm = Realm.getDefaultInstance();
            if (bgRealm == null)
                throw new RealmDataService.RealmNotInitException("Failed to init realm");

            bgRealm.beginTransaction();
            for (E entity : entities) {
                RealmObject realmObject = convert(entity);
                bgRealm.copyToRealmOrUpdate(realmObject);
            }
            bgRealm.commitTransaction();
        } catch (Throwable throwable) {
            withError = true;
            if (bgRealm != null)
                bgRealm.cancelTransaction();
            subscriber.onError(new RealmException("Error when try to add entities : " + entities.toString(), throwable));
        } finally {
            if (bgRealm != null)
                bgRealm.close();
        }
        if (!withError)
            subscriber.onNext((Void) null);

        subscriber.onCompleted();
    }

    abstract RealmObject convert(E entity);

}
