package com.itdoors.elections.data.api.deserializer;

import com.google.common.base.Optional;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.itdoors.elections.data.model.Agitator;
import com.itdoors.elections.data.api.model.AgitatorType;

import java.lang.reflect.Type;

/**
 * Created by v014nd on 07.10.2015.
 */
public class AgitatorDeserializer implements JsonDeserializer<Agitator> {

    @Override public Agitator deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonObject jObj = (JsonObject)json;

        Optional<JsonElement> id = Optional.fromNullable(jObj.get("id"));

        if(!id.isPresent())
            throw new JsonParseException("Failed to parse agitator with no id");

        Optional<JsonElement> name        = Optional.fromNullable(jObj.get("name"));
        Optional<JsonElement> surname     = Optional.fromNullable(jObj.get("surname"));
        Optional<JsonElement> second_name = Optional.fromNullable(jObj.get("second_name"));
        Optional<JsonElement> photo       = Optional.fromNullable(jObj.get("photo"));
        Optional<JsonElement> type        = Optional.fromNullable(jObj.get("type"));

        AgitatorType agitatorType = AgitatorType.TENT; //AgitatorType.fromCode( type.isPresent() ? type.get().getAsString() : null);

        if(agitatorType == null)
            throw new JsonParseException("Couldn't parse agitator type : " + ( type.isPresent() ?  type.get().getAsString() : "null") );

        return new Agitator(
            id.get().getAsString(),
            name.isPresent()        ? name.get().getAsString()        : null,
            surname.isPresent()     ? surname.get().getAsString()     : null,
            second_name.isPresent() ? second_name.get().getAsString() : null,
            photo.isPresent()       ? photo.get().getAsString()       : null,
            agitatorType
        );

    }
}
