package com.itdoors.elections.data.db;

import java.io.IOException;

/**
 * Created by v014nd on 08.06.2016.
 */
public interface ConnectableDataService {
    void open() throws IOException;
    void close() throws IOException;
}
