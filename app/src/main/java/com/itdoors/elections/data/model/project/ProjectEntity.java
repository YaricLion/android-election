package com.itdoors.elections.data.model.project;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class ProjectEntity {

    @NonNull
    @SerializedName("guid")
    private final String guid;
    @NonNull
    @SerializedName("name")
    private final String name;

    public ProjectEntity(@NonNull String guid, @NonNull String name) {
        this.guid = guid;
        this.name = name;
    }

    public ProjectEntity(Builder builder) {
        guid = builder.guid;
        name = builder.name;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(ProjectEntity copy) {
        Builder builder = new Builder();
        builder.guid = copy.guid;
        builder.name = copy.name;
        return builder;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public String getUid() {
        return guid;
    }

    @Override
    public String toString() {
        return "ProjectEntity{" +
                "guid='" + guid + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    public static final class Builder {
        private String guid;
        private String name;

        public Builder() {
        }

        public Builder uid(String val) {
            guid = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public ProjectEntity build() {
            return new ProjectEntity(this);
        }
    }


}
