package com.itdoors.elections.data.model.electorcontrol;

import android.support.annotation.NonNull;

import com.google.common.base.MoreObjects;
import com.google.gson.annotations.SerializedName;

public final class ElectorControlResponseOld {

    @NonNull @SerializedName("control_id")
    private final int id;
    @NonNull @SerializedName("elector_id")
    private final int elector_id;

    public ElectorControlResponseOld(@NonNull int id, @NonNull int elector_id) {
        this.id = id;
        this.elector_id = elector_id;
    }

    @NonNull public int getId() {
        return id;
    }

    @NonNull public int getElectorId() {
        return elector_id;
    }

    @Override public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("elector_id", elector_id)
                .toString();
    }

}
