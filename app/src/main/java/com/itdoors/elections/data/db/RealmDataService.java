package com.itdoors.elections.data.db;

import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.itdoors.elections.data.api.model.Code;
import com.itdoors.elections.data.api.model.CodeValue;
import com.itdoors.elections.data.db.realm.RealmAgitatorControl;
import com.itdoors.elections.data.db.realm.RealmAgitatorProjectControl;
import com.itdoors.elections.data.db.realm.RealmCode;
import com.itdoors.elections.data.db.realm.RealmCodeValue;
import com.itdoors.elections.data.db.realm.RealmElectionDay;
import com.itdoors.elections.data.db.realm.RealmElectorProjectControl;
import com.itdoors.elections.data.db.realm.RealmProject;
import com.itdoors.elections.data.db.realm.rx.RealmObservable;
import com.itdoors.elections.data.model.ElectionDay;
import com.itdoors.elections.data.model.agitatorcontrol.AgitatorControlEntity;
import com.itdoors.elections.data.model.agitatorprojectscontrol.AgitatorProjectControlSendEntity;
import com.itdoors.elections.data.model.electorcontrol.ElectorControlEntity;
import com.itdoors.elections.data.db.realm.RealmElectorControl;
import com.itdoors.elections.data.model.electorprojectscontrol.ElectorProjectControlSendEntity;
import com.itdoors.elections.data.model.project.ProjectEntity;
import com.itdoors.elections.util.Device;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
//import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import rx.Observable;
import timber.log.Timber;

public class RealmDataService implements DataService {

    private Realm realm;

    public RealmDataService() {
    }

    public static class RealmNotInitException extends IOException {
        public RealmNotInitException(String detailMessage) {
            super(detailMessage);
        }
    }

    @Override
    public void open() throws IOException {
        try {
            realm = Realm.getDefaultInstance();
        } catch (Throwable throwable) {
            Timber.tag("STORAGE").e(throwable, "Fail to open Realm!");
            throw new IOException(throwable);
        }
    }

    @Override
    public void close() throws IOException {
        try {

            if (realm != null) {
                realm.close();
            }

        } catch (Throwable throwable) {
            Timber.tag("STORAGE").e(throwable, "Fail to close Realm!");
            throw new IOException(throwable);
        }

    }

 /*   @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    @Override
    public Observable<List<ElectorControlEntity>> getElectorControlInfo() {
        Device.throwIfNotOnMainThread();
        return realm.where(RealmElectorControl.class)
                .findAllAsync()
                .sort("timestamp", Sort.DESCENDING)
                .asObservable()
                .filter(RealmResults::isLoaded)
                .map(RealmDataService::electors); // Realm object very bad with all timing Rx operation like toList()
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    @Override
    public Observable<List<AgitatorControlEntity>> getAgitatorControlInfo() {
        Device.throwIfNotOnMainThread();
        return realm.where(RealmAgitatorControl.class)
                .findAllAsync()
                .sort("timestamp", Sort.DESCENDING)
                .asObservable()
                .filter(RealmResults::isLoaded)
                .map(RealmDataService::agitators);
    }
*/





    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    @Override
    public Observable<Void> addElectorControl(ElectorControlEntity entity) {
        return RealmObservable.add(entity, electorControlEntity -> {
            RealmElectorControl realmControl = new RealmElectorControl();
            realmControl.setUid(entity.getUid());
            realmControl.setTimestamp(entity.getTimestamp());
            realmControl.setLatitude(entity.getLatitude());
            realmControl.setLongitude(entity.getLongitude());
            return realmControl;
        });
    }


    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    @Override
    public Observable<Void> addAgitatorControl(AgitatorControlEntity entity) {
        return RealmObservable.add(entity, agitatorControlEntity -> {

            List<CodeValue> codeValues = entity.getCodeValues();
            RealmList<RealmCodeValue> realmCodeValues = new RealmList<>();
            for (CodeValue codeValue : codeValues) {
                RealmCodeValue realmCodeValue = new RealmCodeValue();
                realmCodeValue.setCode(codeValue.getCode());
                realmCodeValue.setValue(codeValue.getValue());
                realmCodeValues.add(realmCodeValue);
            }

            RealmAgitatorControl realmControl = new RealmAgitatorControl();
            realmControl.setUid(entity.getUid());
            realmControl.setTimestamp(entity.getTimestamp());
            realmControl.setComment(entity.getComment());
            realmControl.setCodeValues(realmCodeValues);
            realmControl.setLatitude(entity.getLatitude());
            realmControl.setLongitude(entity.getLongitude());

            return realmControl;

        });
    }

    /*@RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    @Override
    public Observable<Void> addElectorControl(List<ElectorControlEntity> entities) {
        return RealmObservable.listAdd(entities, entity -> {
            RealmElectorControl realmControl = new RealmElectorControl();
            realmControl.setUid(entity.getUid());
            realmControl.setTimestamp(entity.getTimestamp());
            realmControl.setLatitude(entity.getLatitude());
            realmControl.setLongitude(entity.getLongitude());
            return realmControl;
        });
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    @Override
    public Observable<Void> addAgitatorControl(List<AgitatorControlEntity> entities) {
        return RealmObservable.listAdd(entities, entity -> {

            List<CodeValue> codeValues = entity.getCodeValues();
            RealmList<RealmCodeValue> realmCodeValues = new RealmList<>();
            for (CodeValue codeValue : codeValues) {
                RealmCodeValue realmCodeValue = new RealmCodeValue();
                realmCodeValue.setCode(codeValue.getCode());
                realmCodeValue.setValue(codeValue.getValue());
                realmCodeValues.add(realmCodeValue);
            }

            RealmAgitatorControl realmControl = new RealmAgitatorControl();
            realmControl.setUid(entity.getUid());
            realmControl.setTimestamp(entity.getTimestamp());
            realmControl.setComment(entity.getComment());
            realmControl.setCodeValues(realmCodeValues);
            realmControl.setLatitude(entity.getLatitude());
            realmControl.setLongitude(entity.getLongitude());

            return realmControl;
        });
    }*/

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    @Override
    public Observable<Void> addAgitatorProjectControl(AgitatorProjectControlSendEntity send) {

        Timber.tag("STORAGE").d("addAgitatorProjectControl : " + send.toString());

        return RealmObservable.add(send, entity -> {
            RealmAgitatorProjectControl realmControl = new RealmAgitatorProjectControl();
            realmControl.setAgitatorGuid(entity.getAgitatorGuid());
            realmControl.setProjectGui(entity.getProjectGui());
            realmControl.setScanDateTime(entity.getScanDateTime());
            realmControl.setLatitude(entity.getLatitude());
            realmControl.setLongitude(entity.getLongitude());
            return realmControl;
        });
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    @Override
    public Observable<Void> addElectorProjectControl(ElectorProjectControlSendEntity send) {
        Timber.tag("STORAGE").d("Storage add elector entity : " + send.toString());

        return RealmObservable.add(send, entity -> {
            RealmElectorProjectControl realmControl = new RealmElectorProjectControl();
            realmControl.setProjectGui(entity.getProjectGui());
            realmControl.setSupporterGuid(entity.getSupporterGuid());
            realmControl.setScanDateTime(entity.getScanDateTime());
            realmControl.setLatitude(entity.getLatitude());
            realmControl.setLongitude(entity.getLongitude());
            return realmControl;
        });
    }

/*
    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    @Override public Observable<List<ElectorProjectControlSendEntity>> getElectorProjectControlInfo() {
        Device.throwIfNotOnMainThread();
        return realm.where(RealmElectorProjectControl.class)
                .findAllAsync()
                .asObservable()
                .filter(RealmResults::isLoaded)
                .map(RealmDataService::electorProjectControls);
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    @Override public Observable<List<AgitatorProjectControlSendEntity>> getAgitatorProjectControlInfo() {
        Device.throwIfNotOnMainThread();
        return realm.where(RealmAgitatorProjectControl.class)
                .findAllAsync()
                .asObservable()
                .filter(RealmResults::isLoaded)
                .map(RealmDataService::agitatorProjectControls);
    }*/

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    @Override
    public Observable<List<ElectorControlEntity>> getElectorControlInfo() {
        return RealmObservable.results(bgRealm -> bgRealm.where(RealmElectorControl.class).findAll())
                .map(RealmDataService::electorControls);
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    @Override
    public Observable<List<AgitatorControlEntity>> getAgitatorControlInfo() {
        return RealmObservable.results(bgRealm -> bgRealm.where(RealmAgitatorControl.class).findAll())
                .map(RealmDataService::agitatorControls);
    }


    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    @Override
    public Observable<List<ElectorProjectControlSendEntity>> getElectorProjectControlInfo() {
        return RealmObservable.results(bgRealm -> bgRealm.where(RealmElectorProjectControl.class).findAll())
                .map(RealmDataService::electorProjectControls);
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    @Override
    public Observable<List<AgitatorProjectControlSendEntity>> getAgitatorProjectControlInfo() {
        return RealmObservable.results(bgRealm -> bgRealm.where(RealmAgitatorProjectControl.class).findAll())
                .map(RealmDataService::agitatorProjectControls);
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    @Override
    public Observable<Void> clearAgitatorProjectControlInfo() {
        return RealmObservable.removeAll(RealmAgitatorProjectControl.class);
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    @Override
    public Observable<Void> clearElectorProjectControlInfo() {
        return RealmObservable.removeAll(RealmElectorProjectControl.class);
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    @Override
    public Observable<Void> clearElectorControlInfo() {
        return RealmObservable.removeAll(RealmElectorControl.class);
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    @Override
    public Observable<Void> clearAgitatorControlInfo() {
        return RealmObservable.removeAll(RealmAgitatorControl.class);
    }

    @Override
    public Observable<Long> getAgitatorProjectControlCount() {
        Device.throwIfNotOnMainThread();
        return Observable.just(realm.where(RealmAgitatorProjectControl.class).count());
    }

    @Override
    public Observable<Long> getElectorProjectControlCount() {
        Device.throwIfNotOnMainThread();
        return Observable.just(realm.where(RealmElectorProjectControl.class).count());
    }

    @Override public Observable<Long> getElectorControlCount() {
        Device.throwIfNotOnMainThread();
        return Observable.just(realm.where(RealmElectorControl.class).count());
    }

    @Override
    public Observable<Long> getCodesCount(){
        Device.throwIfNotOnMainThread();
        return Observable.just(realm.where(RealmCode.class).count());
    }


    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    @Override
    public Observable<List<Code>> getCodes() {
        Device.throwIfNotOnMainThread();
        return realm.where(RealmCode.class)
                .findAllAsync()
                .asObservable()
                .filter(RealmResults::isLoaded)
                .map(RealmDataService::codes); // Realm object very bad with all timing Rx operation like toList()
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    @Override
    public Observable<Void> rewriteCodes(List<Code> codeValues) {
        return RealmObservable.listAddOrUpdate(codeValues, code -> {
            RealmCode realmObject = new RealmCode();
            realmObject.setName(code.getName());
            realmObject.setCode(code.getCode());
            return realmObject;
        });
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    @Override
    public Observable<Void> rewriteElectionDay(ElectionDay electionDay) {
        return RealmObservable.addOrUpdateWithoutPK(electionDay, ed -> {
            RealmElectionDay realmObject = new RealmElectionDay();
            realmObject.setStart(ed.getStart());
            realmObject.setFinish(ed.getFinish());
            return realmObject;
        });
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    @Override
    public Observable<Long> getElectionDayCount() {
        Device.throwIfNotOnMainThread();
        return Observable.just(realm.where(RealmElectionDay.class).count());
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    @Override
    public Observable<ElectionDay> getElectionDay() {
        Device.throwIfNotOnMainThread();
        return realm.where(RealmElectionDay.class)
                .findFirstAsync()
                .asObservable()
                .filter(realmObject -> RealmObject.isLoaded(realmObject))
                .map(realmObject -> RealmDataService.electionDay((RealmElectionDay) realmObject));
    }

    @Override
    public Observable<Boolean> isProjectsAvailable() {
        return Observable.just(false);
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    @Override
    public Observable<Void> writeProjects(List<ProjectEntity> entities) {
        return RealmObservable.listAddOrUpdate(entities, entity -> {
            RealmProject realmObject = new RealmProject();
            realmObject.setGuid(entity.getUid());
            realmObject.setName(entity.getName());
            return realmObject;
        });
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    @Override
    public Observable<List<ProjectEntity>> getProjects() {
        Device.throwIfNotOnMainThread();
        return realm.where(RealmProject.class)
                .findAllAsync()
                .asObservable()
                .filter(RealmResults::isLoaded)
                .map(RealmDataService::projects); // Realm object very bad with all timing Rx operation like toList()
    }


    private static ElectionDay electionDay(RealmElectionDay realmElectionDay){
        return new ElectionDay(realmElectionDay.getStart(), realmElectionDay.getFinish());
    }

    private static List<Code> codes(RealmResults<RealmCode> realmCodes) {
        List<Code> codes = new ArrayList<>(realmCodes.size());
        for (RealmCode realmCode : realmCodes) {
            Code code = new Code(realmCode.getName(), realmCode.getCode());
            codes.add(code);
        }
        return codes;
    }

    private static List<ProjectEntity> projects(RealmResults<RealmProject> realmProjects) {
        List<ProjectEntity> projectEntities = new ArrayList<>(realmProjects.size());
        for (RealmProject realmProject : realmProjects) {
            ProjectEntity projectEntity = ProjectEntity.newBuilder()
                    .uid(realmProject.getGuid())
                    .name(realmProject.getName())
                    .build();
            projectEntities.add(projectEntity);
        }
        return projectEntities;
    }

    private static List<ElectorControlEntity> electors(RealmResults<RealmElectorControl> realmEntities) {

        ArrayList<ElectorControlEntity> entities = new ArrayList<>(realmEntities.size());

        for (RealmElectorControl control : realmEntities) {
            ElectorControlEntity entity = new ElectorControlEntity.Builder()
                    .uid(control.getUid())
                    .timestamp(control.getTimestamp())
                    .latitude(control.getLatitude())
                    .longitude(control.getLongitude())
                    .build();
            entities.add(entity);
        }

        return entities;
    }


    private static List<ElectorControlEntity> electorControls(RealmResults<RealmElectorControl> realmEntities) {

        ArrayList<ElectorControlEntity> entities = new ArrayList<>(realmEntities.size());

        for (RealmElectorControl control : realmEntities) {

            ElectorControlEntity entity = new ElectorControlEntity.Builder()
                    .uid(control.getUid())
                    .timestamp(control.getTimestamp())
                    .latitude(control.getLatitude())
                    .longitude(control.getLongitude())
                    .build();

            entities.add(entity);
        }

        return entities;
    }

    private static List<AgitatorControlEntity> agitatorControls(RealmResults<RealmAgitatorControl> realmEntities) {

        ArrayList<AgitatorControlEntity> entities = new ArrayList<>(realmEntities.size());

        for (RealmAgitatorControl control : realmEntities) {

            RealmList<RealmCodeValue> realmCodeValues = control.getCodeValues();
            ArrayList<CodeValue> codeValues = new ArrayList<>(realmCodeValues.size());

            for(RealmCodeValue realmCodeValue : realmCodeValues){
                CodeValue codeValue = new CodeValue(realmCodeValue.getCode(), realmCodeValue.getValue());
                codeValues.add(codeValue);
            }

            AgitatorControlEntity entity = new AgitatorControlEntity.Builder()
                    .uid(control.getUid())
                    .codeValues(codeValues)
                    .timestamp(control.getTimestamp())
                    .latitude(control.getLatitude())
                    .longitude(control.getLongitude())
                    .build();

            entities.add(entity);
        }

        return entities;
    }


    private static List<ElectorProjectControlSendEntity> electorProjectControls(RealmResults<RealmElectorProjectControl> realmEntities) {

        ArrayList<ElectorProjectControlSendEntity> entities = new ArrayList<>(realmEntities.size());

        for (RealmElectorProjectControl control : realmEntities) {

            ElectorProjectControlSendEntity entity = new ElectorProjectControlSendEntity.Builder()
                    .projectGui(control.getProjectGui())
                    .supporterGuid(control.getSupporterGuid())
                    .latitude(control.getLatitude())
                    .longitude(control.getLongitude())
                    .scanDateTime(control.getScanDateTime())
                    .build();

            entities.add(entity);
        }

        return entities;
    }

    private static List<AgitatorProjectControlSendEntity> agitatorProjectControls(RealmResults<RealmAgitatorProjectControl> realmEntities) {

        ArrayList<AgitatorProjectControlSendEntity> entities = new ArrayList<>(realmEntities.size());

        for (RealmAgitatorProjectControl control : realmEntities) {

            AgitatorProjectControlSendEntity entity = new AgitatorProjectControlSendEntity.Builder()
                    .projectGui(control.getProjectGui())
                    .agitatorGuid(control.getAgitatorGuid())
                    .latitude(control.getLatitude())
                    .longitude(control.getLongitude())
                    .scanDateTime(control.getScanDateTime())
                    .build();

            entities.add(entity);
        }

        return entities;
    }

    private static List<AgitatorControlEntity> agitators(RealmResults<RealmAgitatorControl> realmEntities) {

        ArrayList<AgitatorControlEntity> entities = new ArrayList<>(realmEntities.size());

        for (RealmAgitatorControl control : realmEntities) {
            RealmList<RealmCodeValue> realmCodeValues = control.getCodeValues();
            List<CodeValue> codeValues = new ArrayList<>(realmCodeValues.size());
            for (RealmCodeValue realmCodeValue : realmCodeValues) {
                codeValues.add(new CodeValue(realmCodeValue.getCode(), realmCodeValue.getValue()));
            }
            AgitatorControlEntity entity = new AgitatorControlEntity.Builder()
                    .uid(control.getUid())
                    .timestamp(control.getTimestamp())
                    .comment(control.getComment())
                    .codeValues(codeValues)
                    .latitude(control.getLatitude())
                    .longitude(control.getLongitude())
                    .build();

            entities.add(entity);
        }
        return entities;
    }


}
