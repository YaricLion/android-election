package com.itdoors.elections.data.oauth;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.functions.Func1;

public final class RepeatWithDelayOperator implements Func1<Observable<? extends Void>, Observable<?>> {

    private final int maxRetries;
    private final int retryDelay;
    private final TimeUnit timeUnit;
    private int retryCount;

    public RepeatWithDelayOperator(int maxRetries, int retryDelay, TimeUnit timeUnit) {
        this.maxRetries = maxRetries;
        this.retryDelay = retryDelay;
        this.timeUnit = timeUnit;
        this.retryCount = 0;
    }

    @Override
    public Observable<?> call(Observable<? extends Void> attempts) {
        return attempts
                .flatMap((Void vVoid) -> {
                    if (++retryCount < maxRetries) {
                        return Observable.timer(retryDelay, timeUnit);
                    }
                    return Observable.empty();
                });
    }

}
