package com.itdoors.elections.data.db.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by yariclion on 25.06.16.
 */
public class RealmProject extends RealmObject {

    @PrimaryKey
    @Required
    private String guid;
    private String name;

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGuid() {
        return guid;
    }

    public String getName() {
        return name;
    }

}
