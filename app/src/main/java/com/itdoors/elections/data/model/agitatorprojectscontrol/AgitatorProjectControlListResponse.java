package com.itdoors.elections.data.model.agitatorprojectscontrol;

import com.itdoors.elections.data.model.ProjectControlResponseListItem;

public interface AgitatorProjectControlListResponse extends ProjectControlResponseListItem {
}
