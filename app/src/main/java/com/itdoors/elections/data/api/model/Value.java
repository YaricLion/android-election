package com.itdoors.elections.data.api.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * Created by v014nd on 22.09.2015.
 */
public final class Value implements Parcelable {

    private final Integer value;

    public Value(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.value);
    }

    protected Value(Parcel in) {
        this.value = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Parcelable.Creator<Value> CREATOR = new Parcelable.Creator<Value>() {
        public Value createFromParcel(Parcel source) {
            return new Value(source);
        }

        public Value[] newArray(int size) {
            return new Value[size];
        }
    };

    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Value value = (Value) o;

        return !(this.value != null ? this.value.equals(value.value) : value.value != null);

    }


    @Override public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }

    @Override public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("value", value)
                .toString();
    }
}