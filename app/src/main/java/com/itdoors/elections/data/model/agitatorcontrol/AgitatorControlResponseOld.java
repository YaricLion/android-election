package com.itdoors.elections.data.model.agitatorcontrol;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.common.base.MoreObjects;
import com.google.gson.annotations.SerializedName;

public final class AgitatorControlResponseOld implements Parcelable {

    @NonNull @SerializedName("control_id")
    private final int id;
    @NonNull @SerializedName("agitator_id")
    private final int agitator_id;

    public AgitatorControlResponseOld(@NonNull int id, @NonNull int agitator_id) {
        this.id = id;
        this.agitator_id = agitator_id;
    }

    @NonNull public int getId() {
        return id;
    }

    @NonNull public int getAgitatorId() {
        return agitator_id;
    }

    @Override public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("agitator_id", agitator_id)
                .toString();
    }


    @Override public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.agitator_id);
    }

    protected AgitatorControlResponseOld(Parcel in) {
        this.id = in.readInt();
        this.agitator_id = in.readInt();
    }

    public static final Parcelable.Creator<AgitatorControlResponseOld> CREATOR = new Parcelable.Creator<AgitatorControlResponseOld>() {
        public AgitatorControlResponseOld createFromParcel(Parcel source) {
            return new AgitatorControlResponseOld(source);
        }

        public AgitatorControlResponseOld[] newArray(int size) {
            return new AgitatorControlResponseOld[size];
        }
    };
}
