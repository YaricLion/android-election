package com.itdoors.elections.data.model.agitatorprojectscontrol;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.itdoors.elections.data.model.ProjectControlErrorResponseListItem;

public final class AgitatorProjectControlListErrorResponse implements AgitatorProjectControlListResponse, ProjectControlErrorResponseListItem, Parcelable {

    @SerializedName("code")
    private final int code;
    @SerializedName("message")
    private final String msg;
    @SerializedName("project_guid")
    private final String projectGui;
    @SerializedName("agitator_guid")
    private final String agitatorGuid;
    @SerializedName("scan_date_time")
    private final String scanDateTime;

    public AgitatorProjectControlListErrorResponse(int code, String msg, String projectGui, String agitatorGuid, String scanDateTime) {
        this.code = code;
        this.msg = msg;
        this.projectGui = projectGui;
        this.agitatorGuid = agitatorGuid;
        this.scanDateTime = scanDateTime;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return msg;
    }

    public String getAgitatorGuid() {
        return agitatorGuid;
    }

    public String getProjectGui() {
        return projectGui;
    }

    public String getScanDateTime() {
        return scanDateTime;
    }

    @Override
    public String toString() {
        return "AgitatorProjectControlListErrorResponse{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", projectGui='" + projectGui + '\'' +
                ", agitatorGuid='" + agitatorGuid + '\'' +
                ", scanDateTime='" + scanDateTime + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.code);
        dest.writeString(this.msg);
        dest.writeString(this.projectGui);
        dest.writeString(this.agitatorGuid);
        dest.writeString(this.scanDateTime);
    }

    protected AgitatorProjectControlListErrorResponse(Parcel in) {
        this.code = in.readInt();
        this.msg = in.readString();
        this.projectGui = in.readString();
        this.agitatorGuid = in.readString();
        this.scanDateTime = in.readString();
    }

    public static final Parcelable.Creator<AgitatorProjectControlListErrorResponse> CREATOR = new Parcelable.Creator<AgitatorProjectControlListErrorResponse>() {
        public AgitatorProjectControlListErrorResponse createFromParcel(Parcel source) {
            return new AgitatorProjectControlListErrorResponse(source);
        }

        public AgitatorProjectControlListErrorResponse[] newArray(int size) {
            return new AgitatorProjectControlListErrorResponse[size];
        }
    };

}
