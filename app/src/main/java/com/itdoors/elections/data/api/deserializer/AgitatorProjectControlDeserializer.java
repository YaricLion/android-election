package com.itdoors.elections.data.api.deserializer;

import com.google.common.base.Optional;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.itdoors.elections.data.api.model.Project;
import com.itdoors.elections.data.api.model.ProjectControlState;
import com.itdoors.elections.data.model.Agitator;
import com.itdoors.elections.data.model.agitatorprojectscontrol.AgitatorProjectControlResponse;

import java.lang.reflect.Type;

public final class AgitatorProjectControlDeserializer implements JsonDeserializer<AgitatorProjectControlResponse> {

    public static AgitatorProjectControlResponse deserialize(JsonElement jsonElement) {
        JsonObject jObj = (JsonObject) jsonElement;

        Optional<JsonElement> guid = Optional.fromNullable(jObj.get("guid"));
        if (!guid.isPresent())
            throw new JsonParseException("Failed to parse control with no guid");

        Optional<JsonElement> state = Optional.fromNullable(jObj.get("state"));
        Optional<JsonElement> scanDateTime = Optional.fromNullable(jObj.get("scan_date_time"));
        Optional<JsonElement> createDateTime = Optional.fromNullable(jObj.get("create_date_time"));
        Optional<JsonObject> project = Optional.fromNullable(jObj.getAsJsonObject("project"));
        Optional<JsonObject> agitator = Optional.fromNullable(jObj.getAsJsonObject("agitator"));

        ProjectControlState controlState = ProjectControlState.fromCode(state.isPresent() ? state.get().getAsString() : null);
        if (controlState == null)
            throw new JsonParseException("Couldn't project control state : " + (state.isPresent() ? state.get().getAsString() : "null"));

        Project projectParsed = null;
        if (project.isPresent()) {
            Optional<JsonElement> projectGuid = Optional.fromNullable(project.get().get("guid"));
            if (!projectGuid.isPresent())
                throw new JsonParseException("Failed to parse project with no guid");

            Optional<JsonElement> projectName = Optional.fromNullable(project.get().get("name"));
            projectParsed = new Project(
                    projectGuid.get().getAsString(),
                    projectName.isPresent() ? projectName.get().getAsString() : null);
        }

        Agitator agitatorParsed = null;
        if (agitator.isPresent()) {
            Optional<JsonElement> agitatorGuid = Optional.fromNullable(agitator.get().get("guid"));
            if (!agitatorGuid.isPresent())
                throw new JsonParseException("Failed to parse agitator with no guid");

            Optional<JsonElement> agitatorName = Optional.fromNullable(agitator.get().get("first_name"));
            Optional<JsonElement> agitatorSurname = Optional.fromNullable(agitator.get().get("last_name"));
            Optional<JsonElement> agitatorSecondName = Optional.fromNullable(agitator.get().get("second_name"));

            agitatorParsed = new Agitator(
                    agitatorGuid.get().getAsString(),
                    agitatorName.isPresent() ? agitatorName.get().getAsString() : null,
                    agitatorSurname.isPresent() ? agitatorSurname.get().getAsString() : null,
                    agitatorSecondName.isPresent() ? agitatorSecondName.get().getAsString() : null
            );

        }

        return new AgitatorProjectControlResponse(
                guid.get().getAsString(),
                controlState,
                scanDateTime.isPresent() ? scanDateTime.get().getAsString() : null,
                createDateTime.isPresent() ? createDateTime.get().getAsString() : null,
                projectParsed,
                agitatorParsed
        );

    }

    @Override
    public AgitatorProjectControlResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        return deserialize(json);
    }
}
