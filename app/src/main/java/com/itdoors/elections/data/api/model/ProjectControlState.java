package com.itdoors.elections.data.api.model;

import com.itdoors.elections.R;

import timber.log.Timber;

/**
 * Created by yariclion on 25.06.16.
 */
public enum ProjectControlState {

    //state=(accepted|failed|unconfirmed)

    ACCEPTED("accepted", R.string.project_control_status_accepted),
    FAILED("failed", R.string.project_control_status_failed),
    UNCONFIRMED("unconfirmed", R.string.project_control_status_unconfirmed),
    OFFLINE("offline", R.string.project_control_status_offline),
    UNKNOWN("?", R.string.project_control_status_unknown);

    private final String code;
    private final int resourceNameId;

    ProjectControlState(String code, int resourceNameId) {
        this.code = code;
        this.resourceNameId = resourceNameId;
    }


    public static ProjectControlState fromCode(String code) {

        if ("accepted".equalsIgnoreCase(code)) return ACCEPTED;
        else if ("failed".equalsIgnoreCase(code)) return FAILED;
        else if ("unconfirmed".equalsIgnoreCase(code)) return UNCONFIRMED;
            else if("offline".equalsIgnoreCase(code))return OFFLINE;
        else if (code != null && !code.isEmpty()) {
            Timber.e("Unknown agitator type : " + code);
            return UNKNOWN;
        } else {
            Timber.e("Unknown agitator type : null");
            return null;
        }
    }

    @Override
    public String toString() {
        return code;
    }

    public int getResourceNameId() {
        return resourceNameId;
    }

}
