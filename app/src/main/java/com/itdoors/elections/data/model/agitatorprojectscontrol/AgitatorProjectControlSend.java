package com.itdoors.elections.data.model.agitatorprojectscontrol;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yariclion on 25.06.16.
 */
public final class AgitatorProjectControlSend implements Parcelable {

    @SerializedName("project_guid")
    private final String projectGui;
    @SerializedName("agitator_guid")
    private final String agitatorGuid;
    @SerializedName("lat")
    private final String latitude;
    @SerializedName("long")
    private final String longitude;
    @SerializedName("scan_date_time")
    private final String scanDateTime;


    public AgitatorProjectControlSend(String projectGui, String agitatorGuid, String latitude, String longitude, String scanDateTime) {
        this.projectGui = projectGui;
        this.agitatorGuid = agitatorGuid;
        this.latitude = latitude;
        this.longitude = longitude;
        this.scanDateTime = scanDateTime;
    }

    public String getAgitatorGuid() {
        return agitatorGuid;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getProjectGui() {
        return projectGui;
    }

    public String getScanDateTime() {
        return scanDateTime;
    }

    @Override
    public String toString() {
        return "AgitatorProjectControlSend{" +
                "projectGui='" + projectGui + '\'' +
                ", agitatorGuid='" + agitatorGuid + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", scanDateTime='" + scanDateTime + '\'' +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.projectGui);
        dest.writeString(this.agitatorGuid);
        dest.writeString(this.latitude);
        dest.writeString(this.longitude);
        dest.writeString(this.scanDateTime);
    }

    protected AgitatorProjectControlSend(Parcel in) {
        this.projectGui = in.readString();
        this.agitatorGuid = in.readString();
        this.latitude = in.readString();
        this.longitude = in.readString();
        this.scanDateTime = in.readString();
    }

    public static final Parcelable.Creator<AgitatorProjectControlSend> CREATOR = new Parcelable.Creator<AgitatorProjectControlSend>() {
        public AgitatorProjectControlSend createFromParcel(Parcel source) {
            return new AgitatorProjectControlSend(source);
        }

        public AgitatorProjectControlSend[] newArray(int size) {
            return new AgitatorProjectControlSend[size];
        }
    };


    private AgitatorProjectControlSend(Builder builder) {
        this.projectGui = builder.projectGui;
        this.agitatorGuid = builder.agitatorGuid;
        this.latitude = builder.latitude;
        this.longitude = builder.longitude;
        this.scanDateTime = builder.scanDateTime;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static final class Builder {

        private String projectGui;
        private String agitatorGuid;
        private String latitude;
        private String longitude;
        private String scanDateTime;

        public Builder() {
        }

        public Builder projectGui(String val) {
            projectGui = val;
            return this;
        }

        public Builder agitatorGuid(String val) {
            agitatorGuid = val;
            return this;
        }

        public Builder latitude(String val) {
            latitude = val;
            return this;
        }

        public Builder longitude(String val) {
            longitude = val;
            return this;
        }

        public Builder scanDateTime(String val) {
            scanDateTime = val;
            return this;
        }

        public AgitatorProjectControlSend build() {
            return new AgitatorProjectControlSend(this);
        }

    }
}
