package com.itdoors.elections.data.model.electorcontrol;

import com.itdoors.elections.data.model.ControlStorageViewModel;

public final class ElectorControlViewModel implements ControlStorageViewModel {

    private final String uid;
    private final String timestamp;

    public ElectorControlViewModel(String electorId, String timestamp) {
        this.uid = electorId;
        this.timestamp = timestamp;
    }

    private ElectorControlViewModel(Builder builder) {
        uid = builder.electorId;
        timestamp = builder.dateTimestamp;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(ElectorControlViewModel copy) {
        Builder builder = new Builder();
        builder.electorId = copy.uid;
        builder.dateTimestamp = copy.timestamp;
        return builder;
    }


    @Override
    public String uid() {
        return uid;
    }

    @Override
    public int timestamp() {
        return Integer.parseInt(timestamp);
    }

    @Override
    public String toString() {
        return "ElectorControlViewModel{" +
                "uid='" + uid + '\'' +
                ", timestamp='" + timestamp + '\'' +
                '}';
    }


    public static final class Builder {
        private String electorId;
        private String dateTimestamp;

        public Builder() {
        }

        public Builder uid(String val) {
            electorId = val;
            return this;
        }

        public Builder timestamp(String val) {
            dateTimestamp = val;
            return this;
        }

        public ElectorControlViewModel build() {
            return new ElectorControlViewModel(this);
        }
    }
}
