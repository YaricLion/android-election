package com.itdoors.elections.data.model.electorcontrol;

import com.google.gson.annotations.SerializedName;

/**
 * Created by v014nd on 14.06.2016.
 */
public class ElectorControlEntity {

    @SerializedName("guid")
    private final String electorId;
    @SerializedName("date")
    private final String dateTimestamp;
    @SerializedName("lat")
    private final String latitude;
    @SerializedName("long")
    private final String longitude;

    public ElectorControlEntity(String electorId, String dateTimestamp, String latitude, String longitude) {
        this.electorId = electorId;
        this.dateTimestamp = dateTimestamp;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    private ElectorControlEntity(Builder builder) {
        electorId = builder.electorId;
        dateTimestamp = builder.dateTimestamp;
        latitude = builder.latitude;
        longitude = builder.longitude;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(ElectorControlEntity copy) {
        Builder builder = new Builder();
        builder.electorId = copy.electorId;
        builder.dateTimestamp = copy.dateTimestamp;
        builder.latitude = copy.latitude;
        builder.longitude = copy.longitude;
        return builder;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getTimestamp() {
        return dateTimestamp;
    }

    public String getUid() {
        return electorId;
    }

    @Override
    public String toString() {
        return "ElectorControlEntity{" +
                "uid='" + electorId + '\'' +
                ", timestamp='" + dateTimestamp + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                '}';
    }

    public static final class Builder {
        private String electorId;
        private String dateTimestamp;
        private String latitude;
        private String longitude;

        public Builder() {
        }

        public Builder uid(String val) {
            electorId = val;
            return this;
        }

        public Builder timestamp(String val) {
            dateTimestamp = val;
            return this;
        }

        public Builder latitude(String val) {
            latitude = val;
            return this;
        }

        public Builder longitude(String val) {
            longitude = val;
            return this;
        }

        public ElectorControlEntity build() {
            return new ElectorControlEntity(this);
        }
    }


}
