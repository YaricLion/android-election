package com.itdoors.elections.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.common.base.MoreObjects;
import com.google.gson.annotations.SerializedName;

/**
 * Created by v014nd on 08.09.2015.
 */
public final class Elector implements Parcelable {

    @NonNull
    @SerializedName("id")
    private final String guid;

    @SerializedName("name")
    private final String name;
    @SerializedName("surname")
    private final String surname;
    @SerializedName("second_name")
    private final String secondName;
    @SerializedName("birth_date")
    private final String birthDate;

    public Elector(@NonNull String guid, String name, String surname, String secondName, String birthDate) {
        this.guid = guid;
        this.name = name;
        this.surname = surname;
        this.secondName = secondName;
        this.birthDate = birthDate;
    }

    public Elector(@NonNull String guid, String name, String surname, String secondName) {
        this(guid, name, surname, secondName, null);
    }

    public String getSecondName() {
        return secondName;
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    @NonNull public String getGuid() {
        return guid;
    }

    public String getBirthDate() {
        return birthDate;
    }

    @Override
    public String toString() {
        return "Elector{" +
                "guid='" + guid + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", secondName='" + secondName + '\'' +
                ", birthDate='" + birthDate + '\'' +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.guid);
        dest.writeString(this.name);
        dest.writeString(this.surname);
        dest.writeString(this.secondName);
        dest.writeString(this.birthDate);
    }

    protected Elector(Parcel in) {
        this.guid = in.readString();
        this.name = in.readString();
        this.surname = in.readString();
        this.secondName = in.readString();
        this.birthDate = in.readString();
    }

    public static final Creator<Elector> CREATOR = new Creator<Elector>() {
        public Elector createFromParcel(Parcel source) {
            return new Elector(source);
        }

        public Elector[] newArray(int size) {
            return new Elector[size];
        }
    };
}
