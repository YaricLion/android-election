package com.itdoors.elections.data.model.electorcontrol;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.itdoors.elections.data.model.ProjectControlErrorResponseListItem;

public final class ElectorProjectControlListErrorResponse implements ElectorProjectControlListResponse, ProjectControlErrorResponseListItem, Parcelable {

    @SerializedName("code")
    private final int code;
    @SerializedName("message")
    private final String msg;
    @SerializedName("project_guid")
    private final String projectGui;
    @SerializedName("supporter_guid")
    private final String supporterGuid;
    @SerializedName("scan_date_time")
    private final String scanDateTime;

    public ElectorProjectControlListErrorResponse(int code, String msg, String projectGui, String agitatorGuid, String scanDateTime) {
        this.code = code;
        this.msg = msg;
        this.projectGui = projectGui;
        this.supporterGuid = agitatorGuid;
        this.scanDateTime = scanDateTime;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return msg;
    }

    public String getSupporterGuid() {
        return supporterGuid;
    }

    public String getProjectGui() {
        return projectGui;
    }

    public String getScanDateTime() {
        return scanDateTime;
    }

    @Override
    public String toString() {
        return "AgitatorProjectControlListErrorResponse{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", projectGui='" + projectGui + '\'' +
                ", supporterGuid='" + supporterGuid + '\'' +
                ", scanDateTime='" + scanDateTime + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.code);
        dest.writeString(this.msg);
        dest.writeString(this.projectGui);
        dest.writeString(this.supporterGuid);
        dest.writeString(this.scanDateTime);
    }

    protected ElectorProjectControlListErrorResponse(Parcel in) {
        this.code = in.readInt();
        this.msg = in.readString();
        this.projectGui = in.readString();
        this.supporterGuid = in.readString();
        this.scanDateTime = in.readString();
    }

    public static final Creator<ElectorProjectControlListErrorResponse> CREATOR = new Creator<ElectorProjectControlListErrorResponse>() {
        public ElectorProjectControlListErrorResponse createFromParcel(Parcel source) {
            return new ElectorProjectControlListErrorResponse(source);
        }

        public ElectorProjectControlListErrorResponse[] newArray(int size) {
            return new ElectorProjectControlListErrorResponse[size];
        }
    };

}
