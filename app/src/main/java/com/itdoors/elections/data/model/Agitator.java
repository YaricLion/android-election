package com.itdoors.elections.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.common.base.MoreObjects;
import com.google.gson.annotations.SerializedName;
import com.itdoors.elections.data.api.model.AgitatorType;

/**
 * Created by v014nd on 08.09.2015.
 */
public final class Agitator implements Parcelable {

    @NonNull
    @SerializedName("id")
    private final String guid;

    @SerializedName("name")
    private final String name;
    @SerializedName("surname")
    private final String surname;
    @SerializedName("second_name")
    private final String secondName;

    @SerializedName("photo")
    private final String photoLink;

    @NonNull
    @SerializedName("type")
    private final AgitatorType agitatorType;

    public Agitator(@NonNull String guid, String name, String surname, String secondName, String photoLink, @NonNull AgitatorType agitatorType) {
        this.guid = guid;
        this.name = name;
        this.surname = surname;
        this.secondName = secondName;
        this.photoLink = photoLink;
        this.agitatorType = agitatorType;
    }

    public Agitator(@NonNull String guid, String name, String surname, String secondName) {
        this(guid, name, surname, secondName, null, AgitatorType.TENT);
    }

    @NonNull
    public String getGuid() {
        return guid;
    }

    public String getName() {
        return name;
    }

    public String getPhotoLink() {
        return photoLink;
    }

    public String getSurname() {
        return surname;
    }

    public String getSecondName() {
        return secondName;
    }

    @NonNull public AgitatorType getType() {
        return agitatorType;
    }

    @Override  public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("uid", guid)
                .add("name", name)
                .add("surname", surname)
                .add("secondName", secondName)
                .add("photoLink", photoLink)
                .toString();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.guid);
        dest.writeString(this.name);
        dest.writeString(this.surname);
        dest.writeString(this.secondName);
        dest.writeString(this.photoLink);
        dest.writeInt(this.agitatorType == null ? -1 : this.agitatorType.ordinal());
    }

    protected Agitator(Parcel in) {
        this.guid = in.readString();
        this.name = in.readString();
        this.surname = in.readString();
        this.secondName = in.readString();
        this.photoLink = in.readString();
        int tmpAgitatorType = in.readInt();
        this.agitatorType = tmpAgitatorType == -1 ? null : AgitatorType.values()[tmpAgitatorType];
    }

    public static final Creator<Agitator> CREATOR = new Creator<Agitator>() {
        public Agitator createFromParcel(Parcel source) {
            return new Agitator(source);
        }

        public Agitator[] newArray(int size) {
            return new Agitator[size];
        }
    };
}
