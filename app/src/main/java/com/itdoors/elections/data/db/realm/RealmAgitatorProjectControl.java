package com.itdoors.elections.data.db.realm;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class RealmAgitatorProjectControl extends RealmObject {

    private String projectGui;
    private String agitatorGuid;
    private String latitude;
    private String longitude;
    private String scanDateTime;

    public void setAgitatorGuid(String agitatorGuid) {
        this.agitatorGuid = agitatorGuid;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setProjectGui(String projectGui) {
        this.projectGui = projectGui;
    }

    public void setScanDateTime(String scanDateTime) {
        this.scanDateTime = scanDateTime;
    }

    public String getScanDateTime() {
        return scanDateTime;
    }

    public String getProjectGui() {
        return projectGui;
    }

    public String getAgitatorGuid() {
        return agitatorGuid;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }
}
