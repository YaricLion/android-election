package com.itdoors.elections.data.model.agitatorprojectscontrol;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.itdoors.elections.data.api.model.Project;
import com.itdoors.elections.data.api.model.ProjectControlState;
import com.itdoors.elections.data.model.Agitator;

public class AgitatorProjectControlResponse implements Parcelable {

    public static final int ALREADY_SCANNED_HTTP_CODE = 209;

    @SerializedName("guid")
    private final String controlGuid;
    @SerializedName("state")
    private final ProjectControlState controlState;
    @SerializedName("scan_date_time")
    private final String scanDateTime;
    @SerializedName("create_date_time")
    private final String createDateTime;
    @SerializedName("project")
    private final Project project;
    @SerializedName("agitator")
    private final Agitator agitator;

    public AgitatorProjectControlResponse(String controlGuid, ProjectControlState controlState, String scanDateTime, String createDateTime, Project project, Agitator agitator) {
        this.controlGuid = controlGuid;
        this.controlState = controlState;
        this.scanDateTime = scanDateTime;
        this.createDateTime = createDateTime;
        this.project = project;
        this.agitator = agitator;
    }

    public String getScanDateTime() {
        return scanDateTime;
    }

    public Agitator getAgitator() {
        return agitator;
    }

    public Project getProject() {
        return project;
    }

    public ProjectControlState getControlState() {
        return controlState;
    }

    public String getGuid() {
        return controlGuid;
    }

    public String getCreateDateTime() {
        return createDateTime;
    }

    @Override
    public String toString() {
        return "AgitatorProjectControlResponse{" +
                "controlGuid='" + controlGuid + '\'' +
                ", controlState=" + controlState +
                ", scanDateTime='" + scanDateTime + '\'' +
                ", createDateTime='" + createDateTime + '\'' +
                ", project=" + project +
                ", agitator=" + agitator +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.controlGuid);
        dest.writeInt(this.controlState == null ? -1 : this.controlState.ordinal());
        dest.writeString(this.scanDateTime);
        dest.writeString(this.createDateTime);
        dest.writeParcelable(this.project, 0);
        dest.writeParcelable(this.agitator, 0);
    }

    protected AgitatorProjectControlResponse(Parcel in) {
        this.controlGuid = in.readString();
        int tmpControlState = in.readInt();
        this.controlState = tmpControlState == -1 ? null : ProjectControlState.values()[tmpControlState];
        this.scanDateTime = in.readString();
        this.createDateTime = in.readString();
        this.project = in.readParcelable(Project.class.getClassLoader());
        this.agitator = in.readParcelable(Agitator.class.getClassLoader());
    }

    public static final Parcelable.Creator<AgitatorProjectControlResponse> CREATOR = new Parcelable.Creator<AgitatorProjectControlResponse>() {
        public AgitatorProjectControlResponse createFromParcel(Parcel source) {
            return new AgitatorProjectControlResponse(source);
        }

        public AgitatorProjectControlResponse[] newArray(int size) {
            return new AgitatorProjectControlResponse[size];
        }
    };
}
