package com.itdoors.elections.data.api;


import com.itdoors.elections.data.api.model.User;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class UserModule {

    private final String apiUrl;
    private final String email;

    public UserModule(String apiUrl, String email) {
        this.apiUrl = apiUrl;
        this.email = email;
    }

    @Provides
    @UserScope
    @Named("UserApiUrl")
    public String provideApiUrl() {
        return apiUrl;
    }

    @Provides
    @UserScope
    @Named("UserApiEmail")
    public String provideUserEmail() {
        return email;
    }

}
