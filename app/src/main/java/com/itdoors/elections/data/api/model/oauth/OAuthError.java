package com.itdoors.elections.data.api.model.oauth;

import com.google.common.base.Objects;
import com.google.gson.annotations.SerializedName;

public final class OAuthError {

    @SerializedName("error")
    private final String error;
    @SerializedName("error_description")
    private final String description;
    @SerializedName("error_uri")
    private final String uri;

    public OAuthError(String error, String description, String uri) {
        this.error = error;
        this.description = description;
        this.uri = uri;
    }

    public OAuthErrorType getType() {
        return OAuthErrorType.fromString(error);
    }

    public String getError() {
        return error;
    }

    public String getUri() {
        return uri;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("error", error)
                .add("description", description)
                .add("uri", uri)
                .toString();
    }
}

