package com.itdoors.elections.ui.mvp.view;


public interface OfflineElectionDayControlView <VM, RESPONSE> extends OfflineControlView<VM>, LoadingView, GPSView, View {

    void showControlResponse(RESPONSE controlInfo);
    void showFailedControlRequestMsg(int code);
    void showFailedControlRequestUnknownMsg();

}