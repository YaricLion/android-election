package com.itdoors.elections.ui.mvp.base;

public interface ComponentCache {
    long generateId();
    <C> C getComponent(long index);
    <C> void setComponent(long index, C component);

    void removeComponent(long index);
}

