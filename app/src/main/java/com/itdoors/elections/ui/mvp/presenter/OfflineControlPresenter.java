package com.itdoors.elections.ui.mvp.presenter;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.itdoors.elections.ui.fragment.ControlFragment;
import com.itdoors.elections.ui.mvp.model.StorageModel;
import com.itdoors.elections.ui.mvp.view.OfflineControlView;
import com.itdoors.elections.ui.mvp.view.OfflineProjectControlView;

import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Observable;

/**
 * Created by yariclion on 25.06.16.
 */
public abstract class OfflineControlPresenter<MODEL extends StorageModel, VIEW extends OfflineControlView<VIEW_MODEL>, VIEW_MODEL, CONTROL_INFO> extends BasePresenter<VIEW> {

    private LocationPresenter<VIEW> locationPresenterDelegate;
    private StoragePresenter<MODEL, VIEW> storagePresenterDelegate;

    protected OfflineControlPresenter(MODEL storageModel) {
        init(storageModel);
    }


    private void init(MODEL storageModel) {

        this.locationPresenterDelegate = new LocationPresenter<VIEW>() {
            @Override
            protected ReactiveLocationProvider getProvider() {
                return OfflineControlPresenter.this.getProvider();
            }
        };

        this.storagePresenterDelegate = new StoragePresenter<MODEL, VIEW>(storageModel) {

            @Override
            public void onDestroy() {

            }
        };

    }

    protected Observable<Location> getLocation() {
        return locationPresenterDelegate.getLocation();
    }

    protected PresenterCallback getPresenterCallback() {
        return storagePresenterDelegate.getPresenterCallback();
    }

    protected MODEL getStorageModel() {
        return storagePresenterDelegate.getStorageModel();
    }

    @Override
    public void onCreate(@Nullable Bundle bundle) {
        locationPresenterDelegate.onCreate(bundle);
        storagePresenterDelegate.onCreate(bundle);

    }

    @Override
    public void bindView(VIEW view) {
        super.bindView(view);
        locationPresenterDelegate.bindView(view);
        storagePresenterDelegate.bindView(view);

    }

    @Override
    public void unbindView() {
        super.unbindView();
        locationPresenterDelegate.unbindView();
        storagePresenterDelegate.unbindView();

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle bundle) {
        locationPresenterDelegate.onSaveInstanceState(bundle);
        storagePresenterDelegate.onSaveInstanceState(bundle);
    }

    @Override
    public VIEW getView() {

        return super.getView();
    }


    @Override
    public void onDestroy() {
        locationPresenterDelegate.onDestroy();
        storagePresenterDelegate.onDestroy();
    }

    @Override
    public void onStart() {
        locationPresenterDelegate.onStart();
        storagePresenterDelegate.onStart();
    }

    @Override
    public void onStop() {
        locationPresenterDelegate.onStop();
        storagePresenterDelegate.onStop();
    }


    protected abstract ReactiveLocationProvider getProvider();

    protected static class FailedToShowInfoDataOnViewException extends RuntimeException {
        public FailedToShowInfoDataOnViewException(Throwable throwable) {
            super(throwable);
        }
    }

    protected static class FailedToShowControlDataOnViewException extends RuntimeException {
        public FailedToShowControlDataOnViewException(Throwable throwable) {
            super(throwable);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        locationPresenterDelegate.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ControlFragment.REQUEST_CHECK_SETTINGS && resultCode == Activity.RESULT_CANCELED) {
            getView().hideControlLoading();
        }
        storagePresenterDelegate.onActivityResult(requestCode, resultCode, data);

    }
}
