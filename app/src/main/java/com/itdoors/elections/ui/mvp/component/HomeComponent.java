package com.itdoors.elections.ui.mvp.component;

import com.itdoors.elections.AppComponent;
import com.itdoors.elections.data.api.UserComponent;
import com.itdoors.elections.ui.fragment.HomeFragment;
import com.itdoors.elections.ui.mvp.base.HasPresenter;
import com.itdoors.elections.ui.mvp.presenter.HomePresenter;
import com.itdoors.elections.ui.mvp.scope.HomeScope;

import dagger.Component;

@Component(
        dependencies = UserComponent.class
)
@HomeScope
public interface HomeComponent extends HasPresenter<HomePresenter> {
    void inject(HomeFragment fragment);

}