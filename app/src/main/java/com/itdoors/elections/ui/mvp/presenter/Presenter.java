package com.itdoors.elections.ui.mvp.presenter;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public interface Presenter<V> {
    void onCreate(@Nullable Bundle bundle);
    void onSaveInstanceState(@NonNull Bundle bundle);

    void onStart();
    void onStop();

    void onActivityResult(int requestCode, int resultCode, Intent data);

    void onDestroy();
    void bindView(V view);
    void unbindView();
    V getView();
}

