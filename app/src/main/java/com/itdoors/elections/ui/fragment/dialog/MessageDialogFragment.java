package com.itdoors.elections.ui.fragment.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.afollestad.materialdialogs.MaterialDialog;
import com.itdoors.elections.R;

public class MessageDialogFragment extends DialogFragment {

    public static void showMessage(AppCompatActivity activity,String title, String message) {

        FragmentManager fm = activity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag("msg_dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        newInstance(title, message).show(ft, "msg_dialog");

    }

    public static MessageDialogFragment newInstance(String title, String msg){

        MessageDialogFragment fragment = new MessageDialogFragment();

        Bundle args = new Bundle();
        args.putString("args_msg", msg);
        args.putString("args_title", title);

        fragment.setArguments(args);

        return fragment;
    }

    public String getMessage(){
        return getParams("args_msg");
    }

    public String getTitle(){
        return getParams("args_title");
    }

    private String getParams(String key){
        if(getArguments() != null){
            return getArguments().getString(key);
        }
        return null;
    }

    @NonNull
    @Override public Dialog onCreateDialog(Bundle savedInstanceState) {

        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title(getTitle())
                .content(getMessage())
                .positiveText(R.string.ok)
                .build();

        return dialog;

    }

}
