package com.itdoors.elections.ui.mvp.component;

import com.itdoors.elections.AppComponent;
import com.itdoors.elections.data.api.UserComponent;
import com.itdoors.elections.ui.fragment.ProjectsFragment;
import com.itdoors.elections.ui.fragment.StorageControlFragment;
import com.itdoors.elections.ui.mvp.base.HasPresenter;
import com.itdoors.elections.ui.mvp.presenter.ProjectsPresenter;
import com.itdoors.elections.ui.mvp.presenter.StorageControlPresenter;
import com.itdoors.elections.ui.mvp.scope.ProjectsScope;
import com.itdoors.elections.ui.mvp.scope.StorageControlScope;

import dagger.Component;

@Component(dependencies = UserComponent.class)
@ProjectsScope
public interface ProjectsComponent extends HasPresenter<ProjectsPresenter> {
    void inject(ProjectsFragment fragment);
}
