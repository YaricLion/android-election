package com.itdoors.elections.ui.activity;

import android.support.v4.app.Fragment;

import com.itdoors.elections.ui.fragment.StorageControlFragment;

public class StorageActivity extends SingleFragmentActivity {

    @Override
    protected Fragment setupFragment() {
        return new StorageControlFragment();
    }

}
