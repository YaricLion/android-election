package com.itdoors.elections.ui.view.storage;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.itdoors.elections.R;
import com.itdoors.elections.data.model.agitatorcontrol.AgitatorControlViewModel;
import com.itdoors.elections.data.model.ControlStorageViewModel;
import com.itdoors.elections.data.model.electorcontrol.ElectorControlViewModel;
import com.itdoors.elections.ui.view.ContentListener;
import com.itdoors.elections.ui.view.recycler.BindableViewHolder;
import com.itdoors.elections.ui.view.recycler.item.AgitatorStorageControlItemView;
import com.itdoors.elections.ui.view.recycler.item.ElectorStorageControlItemView;

import java.util.Collections;
import java.util.List;

public final class StorageControlAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ContentListener<List<ControlStorageViewModel>> {

    private List<ControlStorageViewModel> models = Collections.emptyList();

    private final int ELECTOR_ITEM_TYPE = 0;
    private final int AGITATOR_ITEM_TYPE = 1;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case ELECTOR_ITEM_TYPE: {
                ElectorStorageControlItemView view = (ElectorStorageControlItemView) LayoutInflater.from(parent.getContext()).inflate(R.layout.view_storage_elector_item, parent, false);
                ElectorControlViewHolder holder = new ElectorControlViewHolder(view);
                return holder;
            }
            case AGITATOR_ITEM_TYPE: {
                AgitatorStorageControlItemView view = (AgitatorStorageControlItemView) LayoutInflater.from(parent.getContext()).inflate(R.layout.view_storage_agitator_item, parent, false);
                AgitatorControlViewHolder holder = new AgitatorControlViewHolder(view);
                return holder;
            }
        }
        throw undefinedViewType(viewType);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ControlStorageViewModel model = models.get(position);
        switch (holder.getItemViewType()) {
            case ELECTOR_ITEM_TYPE:
                ((ElectorControlViewHolder) holder).bind((ElectorControlViewModel) model);
                break;
            case AGITATOR_ITEM_TYPE:
                ((AgitatorControlViewHolder) holder).bind((AgitatorControlViewModel) model);
                break;
            default:
                throw undefinedViewType(holder.getItemViewType());
        }
    }

    private RuntimeException undefinedViewType(int viewType) {
        return new IllegalStateException("Unknown view model :" + viewType);
    }

    private RuntimeException undefinedModelType(ControlStorageViewModel mode) {
        return new IllegalStateException("Unknown view model :" + mode.getClass().toString());
    }

    @Override
    public int getItemViewType(int position) {
        ControlStorageViewModel model = models.get(position);
        if (model.getClass().equals(ElectorControlViewModel.class))
            return ELECTOR_ITEM_TYPE;
        else if (model.getClass().equals(AgitatorControlViewModel.class))
            return AGITATOR_ITEM_TYPE;
        else throw undefinedModelType(model);
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    @Override
    public void onLoaded(List<ControlStorageViewModel> content) {
        this.models = content == null ? Collections.emptyList() : content;
        notifyDataSetChanged();
    }

    @Override
    public void onError(Throwable error) {
    }

    public static final class ElectorControlViewHolder extends BindableViewHolder<ElectorControlViewModel> {
        public ElectorControlViewHolder(ElectorStorageControlItemView view) {
            super(view);
        }
    }

    public static final class AgitatorControlViewHolder extends BindableViewHolder<AgitatorControlViewModel> {
        public AgitatorControlViewHolder(AgitatorStorageControlItemView itemView) {
            super(itemView);
        }
    }

}
