package com.itdoors.elections.ui.mvp.model;

import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.itdoors.elections.data.api.AppService;
import com.itdoors.elections.data.db.DataService;
import com.itdoors.elections.data.model.agitatorprojectscontrol.AgitatorProjectControlResponse;
import com.itdoors.elections.data.model.agitatorprojectscontrol.AgitatorProjectControlSend;
import com.itdoors.elections.data.model.agitatorprojectscontrol.AgitatorProjectControlSendEntity;
import com.itdoors.elections.data.model.agitatorprojectscontrol.AgitatorProjectControlViewModel;
import com.itdoors.elections.util.AsyncSubjectWrapper;
import com.itdoors.elections.ui.mvp.scope.AgitatorProjectControlScope;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by yariclion on 25.06.16.
 */
@AgitatorProjectControlScope
public class AgitatorProjectControlModel extends StorageModel {

    private final int SCAN_CODE = 1;
    private final int CHANGE_STATUS = 2;

    private final AppService appService;

    private AsyncSubjectWrapper<AgitatorProjectControlViewModel> asyncScan = new AsyncSubjectWrapper<>();
    private AsyncSubjectWrapper<AgitatorProjectControlResponse> asyncChangeStatus = new AsyncSubjectWrapper<>();

    @Inject
    public AgitatorProjectControlModel(AppService appService, DataService dataService) {
        super(dataService);
        this.appService = appService;
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    public Observable<AgitatorProjectControlResponse> changeStatus(String controlGuid, String state) {
        return asyncChangeStatus.createIfNeedAndSubscribe(CHANGE_STATUS, appService.changeAgitatorStatus(controlGuid, state));
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    public Observable<AgitatorProjectControlViewModel> scanAgitator(AgitatorProjectControlSend sendItem) {
        return asyncScan.createIfNeedAndSubscribe(SCAN_CODE,
                appService.controlAgitatorProject(sendItem)
                        .map(AgitatorProjectControlModel::viewModel)
        );
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    public Observable<Void> saveControl(AgitatorProjectControlSend sendItem) {
 /*       return Observable.just(sendItem)
                .map(AgitatorProjectControlModel::entity)
                .flatMap(getDataService()::addAgitatorProjectControl);
 */
        return getDataService().addAgitatorProjectControl(entity(sendItem));
    }

    private static AgitatorProjectControlViewModel viewModel(AgitatorProjectControlResponse response) {
        return AgitatorProjectControlViewModel.newBuilder()
                .controlGuid(response.getGuid())
                .controlState(response.getControlState())
                .createDateTime(response.getCreateDateTime())
                .scanDateTime(response.getScanDateTime())
                .project(response.getProject())
                .agitator(response.getAgitator())
                .build();
    }

    private static AgitatorProjectControlSendEntity entity(AgitatorProjectControlSend send) {
        return AgitatorProjectControlSendEntity.newBuilder()
                .agitatorGuid(send.getAgitatorGuid())
                .projectGui(send.getProjectGui())
                .scanDateTime(send.getScanDateTime())
                .latitude(send.getLatitude())
                .longitude(send.getLongitude())
                .build();
    }

    public void resetScan() {
        asyncScan = new AsyncSubjectWrapper<>();
    }

    public void resetChangeStatus() {
        asyncChangeStatus = new AsyncSubjectWrapper<>();
    }
}
