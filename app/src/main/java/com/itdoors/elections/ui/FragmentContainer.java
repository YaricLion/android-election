package com.itdoors.elections.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.itdoors.elections.R;


public interface FragmentContainer {

  View create(LayoutInflater inflater, ViewGroup container);
  int container();
  int layout();

  FragmentContainer DEFAULT = new FragmentContainer() {

      @Override public View create(LayoutInflater inflater, ViewGroup container) {
          return inflater.inflate(layout(), container, false);
      }

      @Override public int container() {
          return R.id.fr_container;
      }

      @Override public int layout() {
          return R.layout.fragment_def_container_layout;
      }

  };
}
