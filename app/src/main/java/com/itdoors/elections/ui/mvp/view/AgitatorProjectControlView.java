package com.itdoors.elections.ui.mvp.view;

import com.itdoors.elections.data.model.agitatorprojectscontrol.AgitatorProjectControlViewModel;
import com.itdoors.elections.data.model.agitatorprojectscontrol.AgitatorProjectControlResponse;

/**
 * Created by yariclion on 25.06.16.
 */
public interface AgitatorProjectControlView extends OfflineProjectControlView<AgitatorProjectControlViewModel, AgitatorProjectControlResponse> {

}
