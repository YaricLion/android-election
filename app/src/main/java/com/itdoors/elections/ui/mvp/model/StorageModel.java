package com.itdoors.elections.ui.mvp.model;

import com.itdoors.elections.data.db.DataService;
import com.itdoors.elections.ui.mvp.presenter.BasePresenterCallback;
import com.itdoors.elections.ui.mvp.presenter.PresenterCallback;

import java.io.IOException;

import timber.log.Timber;

public abstract class StorageModel implements Model {

    private final DataService dataService;

    private boolean isDataServiceAvailable = false;

    protected StorageModel(DataService dataService) {
        this.dataService = dataService;
    }

    protected DataService getDataService() {
        return this.dataService;
    }

    protected boolean isDataServiceAvailable() {
        return isDataServiceAvailable;
    }

    protected void setDataServiceAvailable(boolean is) {
        this.isDataServiceAvailable = is;
    }

    public PresenterCallback createPresenterCallback() {
        return new BasePresenterCallback() {
            @Override
            public void onCreate() {
                try {
                    getDataService().open();
                    setDataServiceAvailable(true);
                } catch (IOException e) {
                    Timber.tag("DB").e(e, "Fail to open DataService!");
                }
            }

            @Override
            public void onDestroy() {
                try {
                    getDataService().close();
                } catch (IOException e) {
                    Timber.tag("DB").e(e, "Fail to close DataService!");
                }
            }

        };

    }
}
