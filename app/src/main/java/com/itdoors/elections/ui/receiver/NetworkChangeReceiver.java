package com.itdoors.elections.ui.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.itdoors.elections.App;
import com.itdoors.elections.AppComponent;
import com.itdoors.elections.ui.event.NetworkEstablishedEvent;
import com.itdoors.elections.ui.service.SendService;
import com.itdoors.elections.util.Device;
import com.squareup.otto.Bus;

import javax.inject.Inject;

public class NetworkChangeReceiver extends BroadcastReceiver {

    @Inject Device device;
    @Inject Bus bus;

    @Override
    public void onReceive(Context context, Intent intent) {
        AppComponent component = App.getAppComponent(context);
        component.inject(this);

        App app = (App) context.getApplicationContext();
        if (device.isNetworkAvailable() && app.isUserLoggedIn()) {
            Intent newIntent = new Intent(context, SendService.class);
            context.startService(newIntent);

            bus.post(new NetworkEstablishedEvent());

        }
    }

}
