package com.itdoors.elections.ui.mvp.component;


import com.itdoors.elections.data.api.UserComponent;
import com.itdoors.elections.ui.fragment.AgitatorProjectControlFragment;
import com.itdoors.elections.ui.mvp.base.HasPresenter;
import com.itdoors.elections.ui.mvp.presenter.AgitatorProjectControlPresenter;
import com.itdoors.elections.ui.mvp.scope.AgitatorProjectControlScope;

import dagger.Component;

@Component(
        dependencies = UserComponent.class
)
@AgitatorProjectControlScope
public interface AgitatorProjectControlComponent extends HasPresenter<AgitatorProjectControlPresenter> {
    void inject(AgitatorProjectControlFragment fragment);
}
