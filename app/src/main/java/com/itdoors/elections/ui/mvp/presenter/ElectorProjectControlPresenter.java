package com.itdoors.elections.ui.mvp.presenter;

import android.location.Location;

import com.itdoors.elections.data.api.model.ProjectControlState;
import com.itdoors.elections.data.model.electorprojectscontrol.ElectorProjectControlResponse;
import com.itdoors.elections.data.model.electorprojectscontrol.ElectorProjectControlSend;
import com.itdoors.elections.data.model.electorprojectscontrol.ElectorProjectControlViewModel;
import com.itdoors.elections.ui.mvp.model.ElectorProjectControlModel;
import com.itdoors.elections.ui.mvp.scope.ElectorProjectControlScope;
import com.itdoors.elections.ui.mvp.view.ElectorProjectControlView;
import com.itdoors.elections.util.Device;
import com.itdoors.elections.util.SchedulerProvider;
import com.itdoors.elections.util.TimeUtils;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Observable;
import rx.subscriptions.CompositeSubscription;

import static com.itdoors.elections.util.MVPUtils.onError;

@ElectorProjectControlScope
public class ElectorProjectControlPresenter extends OfflineControlPresenter<ElectorProjectControlModel, ElectorProjectControlView, ElectorProjectControlViewModel, ElectorProjectControlResponse> {

    private final int CONTROL_REQUEST_TIMEOUT_IN_MINUTES = 5;

    private final SchedulerProvider schedulerProvider;
    private final ReactiveLocationProvider locationProvider;
    private final Device device;

    private CompositeSubscription subscription = new CompositeSubscription();

    @Inject
    public ElectorProjectControlPresenter(ElectorProjectControlModel staorageModel, SchedulerProvider schedulerProvider, ReactiveLocationProvider locationProvider, Device device) {
        super(staorageModel);
        this.schedulerProvider = schedulerProvider;
        this.locationProvider = locationProvider;
        this.device = device;
    }

    @Override
    protected ReactiveLocationProvider getProvider() {
        return locationProvider;
    }

    public void sendControl(final String controlGuid, final ProjectControlState state) {

        if (getView() != null)
            getView().showControlLoading();

        if (state != ProjectControlState.ACCEPTED && state != ProjectControlState.FAILED)
            throw new IllegalArgumentException("Failed state :" + state.toString());

        if (device.isNetworkAvailable()) {
            getStorageModel().resetChangeStatus();
            getStorageModel()
                    .changeStatus(controlGuid, state.toString())
                    .compose(schedulerProvider.applySchedulers())
                    .subscribe(
                            response -> {
                                if (getView() != null) {
                                    getView().hideControlLoading();
                                    getView().showControlResponse(response, state);
                                }
                            },
                            throwable -> {
                                if (getView() != null) {
                                    getView().hideControlLoading();
                                    onError(
                                            throwable,
                                            httpCode -> {
                                                getView().showFailedControlRequestMsg(httpCode, state);
                                            },
                                            () -> {
                                                getView().showFailedControlRequestUnknownMsg(state);
                                            }
                                    );
                                }
                            });

        } else {
            if (getView() != null) {
                getView().hideControlLoading();
                getView().showNoInternetConnection();
            }
        }
    }

    public void scanElector(String projectGuid, String ElectorGuid) {

        if (getView() != null)
            getView().showLoading();

        Observable<Location> locationObservable = this.getLocation()
                .first()
                .timeout(CONTROL_REQUEST_TIMEOUT_IN_MINUTES, TimeUnit.MINUTES);

        Observable<ElectorProjectControlSend> sendEntityObservable = locationObservable.map(location -> {
            long gpsTime = location.getTime();
            String lat = Double.toString(location.getLatitude());
            String lon = Double.toString(location.getLongitude());
            String time = TimeUtils.formatGps(gpsTime);
            ElectorProjectControlSend send = new ElectorProjectControlSend(projectGuid, ElectorGuid, lat, lon, time);
            return send;
        });

        if (device.isNetworkAvailable()) {

            subscription.add(
                    sendEntityObservable
                            .flatMap(getStorageModel()::scanElector)
                            .compose(schedulerProvider.applySchedulers())
                            .subscribe(vm -> {
                                        if (getView() != null) {
                                            getView().hideLoading();
                                            getView().showInfo(vm);
                                        }
                                    },
                                    throwable -> {
                                        if (getView() != null)
                                            getView().hideLoading();
                                        onError(
                                                throwable,
                                                httpCode -> {
                                                    getView().showFailedInfoRequestMsg(httpCode);
                                                    if (httpCode != 404) {
                                                        getView().showRetry();
                                                    }
                                                },
                                                () -> {
                                                    getView().showFailedInfoRequestUnknownMsg();
                                                    getView().showRetry();
                                                }
                                        );
                                    }
                            )
            );
        } else {

            subscription.add(
                    sendEntityObservable
                            .flatMap(sendItem -> getStorageModel().saveControl(sendItem))
                            .compose(schedulerProvider.applySchedulers())
                            .subscribe(
                                    aVoid -> {
                                        if (getView() != null) {
                                            getView().showControlWillBeSentLater();
                                        }
                                    },
                                    throwable -> {
                                        if (getView() != null) {
                                            getView().showDatabaseError();
                                        }
                                    }
                            )
            );
          /*  if(getView() != null) {
                getView().hideLoading();
                getView().showNoInternetConnection();
                getView().showRetry();
            }*/
        }
    }

    private static ElectorProjectControlViewModel viewModel(ElectorProjectControlResponse response) {
        return ElectorProjectControlViewModel.newBuilder()
                .controlGuid(response.getGuid())
                .project(response.getProject())
                .elector(response.getElector())
                .controlState(response.getControlState())
                .scanDateTime(response.getScanDateTime())
                .createDateTime(response.getScanDateTime())
                .build();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (subscription != null) {
            subscription.unsubscribe();
            subscription = null;
        }
        getPresenterCallback().onDestroy();
    }
}
