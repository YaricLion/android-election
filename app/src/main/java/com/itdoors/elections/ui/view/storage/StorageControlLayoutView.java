package com.itdoors.elections.ui.view.storage;

import android.content.Context;
import android.content.res.Configuration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.LinearLayout;

import com.itdoors.elections.App;
import com.itdoors.elections.R;
import com.itdoors.elections.data.model.ControlStorageViewModel;
import com.itdoors.elections.ui.BetterViewAnimator;
import com.itdoors.elections.ui.event.RetryStorageEvent;
import com.itdoors.elections.ui.mvp.view.StorageControlView;
import com.itdoors.elections.util.Device;
import com.squareup.otto.Bus;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import jp.wasabeef.recyclerview.animators.adapters.SlideInBottomAnimationAdapter;

public class StorageControlLayoutView extends LinearLayout implements StorageControlView<List<ControlStorageViewModel>> {

    private static final int ANIMATION_DURATION = 500;

    @Bind(R.id.storage_control_animator)
    BetterViewAnimator animatorView;
    @Bind(R.id.storage_control_list)
    RecyclerView recyclerView;

    @Bind(R.id.storage_control_retry)
    Button retryBtn;

    @Inject
    Device device;
    @Inject
    Bus bus;

    private StorageControlAdapter adapter;

    public StorageControlLayoutView(Context context, AttributeSet attrs) {
        super(context, attrs);
        App.getUserComponent(context).inject(this);
        adapter = new StorageControlAdapter();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {

            }
        });

        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), getDisplayColumns(device), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(animate(adapter));
        retryBtn.setOnClickListener(view -> retry());

    }

    private void retry() {
        bus.post(new RetryStorageEvent());
    }

    private RecyclerView.Adapter animate(RecyclerView.Adapter adapter) {

        SlideInBottomAnimationAdapter wrappingAdapter = new SlideInBottomAnimationAdapter(adapter);
        wrappingAdapter.setFirstOnly(true);
        wrappingAdapter.setDuration(ANIMATION_DURATION);
        return wrappingAdapter;
    }

    public static int getDisplayColumns(Device device) {

        boolean isTablet = device.isTablet();
        int orientation = device.orientation();
        if (isTablet) {
            if (orientation == Configuration.ORIENTATION_PORTRAIT)
                return 2;
            return 3;
        }
        if (orientation == Configuration.ORIENTATION_PORTRAIT)
            return 1;
        return 2;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    @Override
    public void showLoading() {
        animatorView.setDisplayedChildId(R.id.storage_control_progress);
    }

    @Override
    public void showInfo(List<ControlStorageViewModel> info) {
        adapter.onLoaded(info);
        animatorView.setDisplayedChildId(R.id.storage_control_list);
    }

    @Override
    public void showError(Throwable throwable) {
        adapter.onError(throwable);
        animatorView.setDisplayedChildId(R.id.storage_control_error);
    }

    @Override
    public void showEmpty() {
        animatorView.setDisplayedChildId(R.id.storage_control_empty);
    }

    @Override
    public void hideLoading() {
    }

}
