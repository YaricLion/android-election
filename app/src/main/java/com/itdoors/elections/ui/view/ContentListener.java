package com.itdoors.elections.ui.view;

/**
 * Created by v014nd on 30.04.2015.
 */
public interface ContentListener<T> {
    void onLoaded(T content);

    void onError(Throwable error);
}
