package com.itdoors.elections.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.itdoors.elections.App;
import com.itdoors.elections.R;
import com.itdoors.elections.ui.event.ToScanEvent;
import com.itdoors.elections.ui.mvp.presenter.HomePresenter;
import com.itdoors.elections.ui.mvp.component.DaggerHomeComponent;
import com.itdoors.elections.ui.mvp.component.HomeComponent;
import com.itdoors.elections.ui.mvp.view.HomeView;
import com.squareup.otto.Bus;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by v014nd on 10.09.2015.
 */
public class HomeFragment extends BaseFragment<HomeComponent, HomePresenter> implements HomeView {

    @Inject HomePresenter presenter;
    @Inject Bus bus;

    @Bind(R.id.home_scan_btn) ImageView scanBtn;

    @Override protected HomeComponent onCreateNonConfigurationComponent() {
        return DaggerHomeComponent.builder().userComponent(App.getUserComponent(getActivity())).build();
    }

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getComponent().inject(this);
    }

    @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                                       @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override public void navigateToScanView() {
        bus.post(new ToScanEvent());
    }


    @OnClick(R.id.home_scan_btn) public void onScanClicked(ImageView scanBtn){
        presenter.scan();
    }

}
