package com.itdoors.elections.ui.view.recycler;

import com.itdoors.elections.data.model.ViewModel;

/**
 * Created by v014nd on 14.06.2016.
 */
public interface Bindable<VM extends ViewModel> {
    void bind(VM vm);
}
