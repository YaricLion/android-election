package com.itdoors.elections.ui.mvp.model;

import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.itdoors.elections.data.api.AppService;
import com.itdoors.elections.data.db.DataService;
import com.itdoors.elections.data.model.Elector;
import com.itdoors.elections.data.model.electorcontrol.ElectorControlEntity;
import com.itdoors.elections.data.model.electorcontrol.ElectorControlResponseOld;
import com.itdoors.elections.data.model.electorcontrol.ElectorControlSend;
import com.itdoors.elections.data.model.electorprojectscontrol.ElectorProjectControlSend;
import com.itdoors.elections.data.model.electorprojectscontrol.ElectorProjectControlViewModel;
import com.itdoors.elections.ui.mvp.scope.ElectorControlScope;
import com.itdoors.elections.util.AsyncSubjectWrapper;

import javax.inject.Inject;

import rx.Observable;
import rx.subjects.AsyncSubject;

import static com.itdoors.elections.util.MVPUtils.code;
import static com.itdoors.elections.util.MVPUtils.INIT_CODE;

@ElectorControlScope
public class ElectorControlModel extends StorageModel {

    private AsyncSubjectWrapper<Elector> electorAsyncSubject = new AsyncSubjectWrapper<>();
    private AsyncSubjectWrapper<ElectorControlResponseOld> controlAsyncSubject = new AsyncSubjectWrapper<>();

    private final AppService appService;

    @Inject
    public ElectorControlModel(AppService appService,DataService dataService) {
        super(dataService);
        this.appService = appService;
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    public Observable<Elector> verifyElector(String guid){
        int code = code(guid);
        return electorAsyncSubject.createIfNeedAndSubscribe(code, appService.verifyElector(guid));
    }

    public void resetElectorRequest() {
        electorAsyncSubject = new AsyncSubjectWrapper<>();
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    public Observable<ElectorControlResponseOld> controlElector(String guid, String dateTimeStamp, String latitude, String longitude) {
        int code = code(guid, dateTimeStamp, latitude, longitude);
        return controlAsyncSubject.createIfNeedAndSubscribe(code, appService.controlElectors(guid, dateTimeStamp, latitude, longitude));
    }

    public void resetControltRequest(){
        controlAsyncSubject = new AsyncSubjectWrapper<>();
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    public Observable<Void> saveControl(ElectorControlSend sendItem) {
        return getDataService().addElectorControl(entity(sendItem));
    }

    private static ElectorControlEntity entity(ElectorControlSend sendItem){
        return ElectorControlEntity.newBuilder()
                .uid(sendItem.getElectorId())
                .timestamp(sendItem.getDateTimestamp())
                .latitude(sendItem.getLatitude())
                .longitude(sendItem.getLongitude())
                .build();
    }


}
