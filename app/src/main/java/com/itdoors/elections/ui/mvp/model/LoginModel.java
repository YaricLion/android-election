package com.itdoors.elections.ui.mvp.model;

import com.itdoors.elections.BuildConfig;
import com.itdoors.elections.data.api.AppService;
import com.itdoors.elections.data.api.OauthService;
import com.itdoors.elections.data.api.model.User;
import com.itdoors.elections.data.api.model.oauth.AccessToken;

import com.itdoors.elections.ui.mvp.model.prefs.ApiUrlRxPrefs;
import com.itdoors.elections.ui.mvp.model.prefs.TokenRxPrefs;
import com.itdoors.elections.ui.mvp.model.prefs.UserRxPrefs;
import com.itdoors.elections.ui.mvp.scope.LoginScope;

import javax.inject.Inject;

import rx.Observable;
import rx.subjects.AsyncSubject;

import static com.itdoors.elections.util.MVPUtils.code;
import static com.itdoors.elections.util.MVPUtils.INIT_CODE;

@LoginScope
public class LoginModel implements Model {



    private final OauthService oauthService;
    private final AppService appService;

    // Implement cache using An AsyncSubject which emits the last value
    // (and only the last value) emitted by the source Observable,
    // and only after that source Observable completes.

    private AsyncSubject<AccessToken> accessTokenAsyncSubject;
    private AsyncSubject<User> userAsyncSubject;

    private int tokenRequestCode = INIT_CODE;
    private int userRequestCode = INIT_CODE;

    private final TokenRxPrefs tokenPrefs;
    private final UserRxPrefs userPrefs;
    private final ApiUrlRxPrefs apiUrlPrefs;


    @Inject
    public LoginModel(OauthService oauthService, AppService appService, TokenRxPrefs tokenRxPrefs, UserRxPrefs userRxPrefs, ApiUrlRxPrefs apiUrlPrefs) {

        this.oauthService = oauthService;
        this.appService = appService;
        this.tokenPrefs = tokenRxPrefs;
        this.userPrefs = userRxPrefs;
        this.apiUrlPrefs = apiUrlPrefs;
    }


    public Observable<AccessToken> getToken(String username, String password) {

        int code = code(username, password);

        if (tokenRequestCode == INIT_CODE) {

            tokenRequestCode = code;

        } else if (tokenRequestCode != code) {
            // can't use cached data
            accessTokenAsyncSubject = null;
            tokenRequestCode = code;
        }
        if (accessTokenAsyncSubject == null) {

            accessTokenAsyncSubject = AsyncSubject.create();
            oauthService.getAccessToken(

                    BuildConfig.CLIENT_ID,
                    BuildConfig.CLIENT_SECRET,
                    OauthService.GrandType.PASSWORD,
                    username,
                    password

            ).subscribe(accessTokenAsyncSubject);

        }
        return accessTokenAsyncSubject;
    }

    public Observable<User> getUser(AccessToken token){

        int code = code(token);

        if (userRequestCode == INIT_CODE) {
            userRequestCode = code;
        } else if (userRequestCode != code) {
            // can't use cached data
            userAsyncSubject = null;
            userRequestCode = code;
        }
        if (userAsyncSubject == null) {
            userAsyncSubject = AsyncSubject.create();
            appService.getUser(token.getToken()).subscribe(userAsyncSubject);
        }
        return userAsyncSubject;

    }

    public void reset(){


        accessTokenAsyncSubject = null;
        tokenRequestCode = INIT_CODE;

        userAsyncSubject = null;
        userRequestCode = INIT_CODE;

        //cleanToken().subscribe();
        //clearUser().subscribe();

    }

    public void clearInfo(){
        cleanToken().subscribe();
        clearUser().subscribe();
    }

    public Observable<User> getCurrentUser(){
        return userPrefs.get();
    }

    public Observable<Boolean> saveToken(AccessToken token){
        return tokenPrefs.set(token);
    }

    public Observable<Boolean> saveUser(User user){
        return userPrefs.set(user);
    }

    public Observable<Boolean> saveApiUrl(String apiUrl) {
        return apiUrlPrefs.set(apiUrl);
    }

    public Observable<Boolean> clearUser(){
        return userPrefs.delete();
    }

    public Observable<Boolean> cleanToken(){
        return tokenPrefs.delete();
    }

   /* private static int code (String username, String password){
        return username.hashCode() * password.hashCode();
    }*/
/*
    private static int code(AccessToken token){
        return  token.hashCode();

    }*/
}