package com.itdoors.elections.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.base.Optional;
import com.itdoors.elections.App;
import com.itdoors.elections.R;
import com.itdoors.elections.data.api.model.ProjectControlState;
import com.itdoors.elections.data.model.Elector;
import com.itdoors.elections.data.model.electorprojectscontrol.ElectorProjectControlResponse;
import com.itdoors.elections.data.model.electorprojectscontrol.ElectorProjectControlViewModel;
import com.itdoors.elections.ui.BetterViewAnimator;
import com.itdoors.elections.ui.event.RejectProjectCanceledEvent;
import com.itdoors.elections.ui.event.RejectProjectControlConfirmedEvent;
import com.itdoors.elections.ui.event.ResponseOkClickedEvent;
import com.itdoors.elections.ui.fragment.dialog.AreYouSureDialog;
import com.itdoors.elections.ui.fragment.dialog.EnableGpsDialogFragment;
import com.itdoors.elections.ui.fragment.dialog.GetCoordinatesDialogFragment;
import com.itdoors.elections.ui.fragment.dialog.ProjectControlResponseDialog;
import com.itdoors.elections.ui.mvp.component.DaggerElectorProjectControlComponent;
import com.itdoors.elections.ui.mvp.component.ElectorProjectControlComponent;
import com.itdoors.elections.ui.mvp.presenter.ElectorProjectControlPresenter;
import com.itdoors.elections.ui.mvp.view.ElectorProjectControlView;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import icepick.Icepick;
import icepick.State;
import rx.subjects.PublishSubject;
import timber.log.Timber;

public class ElectorProjectControlFragment extends OfflineControlFragment<ElectorProjectControlComponent, ElectorProjectControlPresenter, ElectorProjectControlViewModel, ElectorProjectControlResponse> implements ElectorProjectControlView {

    private static final String EL_GUID_TAG = "EL_GUID_TAG";
    private static final String PR_GUID_TAG = "PR_GUID_TAG";

    @Inject
    Bus bus;
    @Inject
    ElectorProjectControlPresenter presenter;

    @Bind(R.id.elector_project_control_animator)
    BetterViewAnimator animatorView;
    @Bind(R.id.btns_holder)
    View btnsHolderView;
    @Bind(R.id.elector_project_control_submit_btn)
    Button submitBtn;
    @Bind(R.id.elector_project_control_reject_btn)
    Button rejectBtn;

    @BindString(R.string.error_def_msg)
    String failedMsg;

    @Bind(R.id.elector_project_control_name)
    TextView nameView;
    @Bind(R.id.elector_project_control_surname)
    TextView surnameView;
    @Bind(R.id.elector_project_control_second_name)
    TextView secondNameView;
    @Bind(R.id.elector_project_control_birth_day)
    TextView birthdayView;


    @Bind(R.id.ok_btn_holder)
    View okHolderView;
    @Bind(R.id.elector_project_control_ok_btn)
    Button okBtn;

    @Bind(R.id.elector_project_control_msg_holder)
    View msgHolderView;
    @Bind(R.id.elector_project_control_response_msg)
    TextView msgView;
    @Bind(R.id.elector_project_control_response_time)
    TextView timeView;

    private final PublishSubject<Boolean> controlSending = PublishSubject.create();

    @State
    boolean controlRequestActive = false;
    @State
    ElectorProjectControlViewModel controlVM = null;

    public static ElectorProjectControlFragment newInstance(String electorGuid, String projectGuid) {

        ElectorProjectControlFragment fragment = new ElectorProjectControlFragment();
        if (electorGuid == null || projectGuid == null)
            throw new IllegalArgumentException("elector guid and project guid should be defined");

        Bundle args = new Bundle();
        args.putString(EL_GUID_TAG, electorGuid);
        args.putString(PR_GUID_TAG, projectGuid);
        fragment.setArguments(args);

        return fragment;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getComponent().inject(this);
        Icepick.restoreInstanceState(this, savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_elector_project_control, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        load();

        controlSending.subscribe(loading -> {
            controlRequestActive = loading;
            submitBtn.setEnabled(!controlRequestActive);
            rejectBtn.setEnabled(!controlRequestActive);
        });

        controlSending.onNext(controlRequestActive);

        submitBtn.setOnClickListener(v -> {
            presenter.sendControl(controlVM.getGuid(), ProjectControlState.ACCEPTED);
        });

        rejectBtn.setOnClickListener(v -> {
            new AreYouSureDialog().show(getFragmentManager(), "confirm_dg");
        });

        okBtn.setOnClickListener(v -> {
            getActivity().finish();
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private void load() {

        String electorGuid = getAgGuid();
        String projectGuid = getPrGuid();
        presenter.scanElector(projectGuid, electorGuid);

    }

    private String getAgGuid() {
        return getArguments() != null ? getArguments().getString(EL_GUID_TAG) : null;
    }

    private String getPrGuid() {
        return getArguments() != null ? getArguments().getString(PR_GUID_TAG) : null;
    }

    @Override
    public void onStart() {
        super.onStart();
        bus.register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        bus.unregister(this);
    }

    @Subscribe
    public void onRejectConfirmed(RejectProjectControlConfirmedEvent event) {

        Timber.tag("CONTROL").d("Reject status confirmed");
        String guid = controlVM.getGuid();
        presenter.sendControl(guid, ProjectControlState.FAILED);
    }

    @Subscribe
    public void onRejectCanceled(RejectProjectCanceledEvent event) {
        Timber.tag("CONTROL").d("Reject status canceled");
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    @Override
    protected ElectorProjectControlComponent onCreateNonConfigurationComponent() {
        return DaggerElectorProjectControlComponent.builder()
                .userComponent(App.getUserComponent(getActivity()))
                .build();
    }


    @Override
    public void showRetry() {
        animatorView.setDisplayedChildId(R.id.elector_project_control_error);
    }


    @Override
    public void showControlLoading() {
        controlSending.onNext(true);
    }

    @Override
    public void hideControlLoading() {
        controlSending.onNext(false);
    }

    @Override
    public void showInfo(ElectorProjectControlViewModel model) {

        Optional<Elector> elector = Optional.fromNullable(model.getElector());
        if (elector.isPresent()) {

            Optional<String> name = Optional.fromNullable(elector.get().getName());
            Optional<String> surname = Optional.fromNullable(elector.get().getSurname());
            Optional<String> secondName = Optional.fromNullable(elector.get().getSecondName());
            Optional<String> birthDay = Optional.fromNullable(elector.get().getBirthDate());
            Optional<String> time = Optional.fromNullable(model.getScanDateTime());

            nameView.setText(name.or("-"));
            surnameView.setText(surname.or("-"));
            secondNameView.setText(secondName.or("-"));
            birthdayView.setText(birthDay.or("-"));


            ProjectControlState state = model.getControlState();
            if (state == ProjectControlState.UNCONFIRMED) {

                okHolderView.setVisibility(View.GONE);
                okBtn.setEnabled(false);

                msgHolderView.setVisibility(View.GONE);

                btnsHolderView.setVisibility(View.VISIBLE);
                submitBtn.setEnabled(true);
                rejectBtn.setEnabled(true);

            } else {

                btnsHolderView.setVisibility(View.GONE);
                submitBtn.setEnabled(false);
                rejectBtn.setEnabled(false);

                okHolderView.setVisibility(View.VISIBLE);
                okBtn.setEnabled(true);

                msgHolderView.setVisibility(View.VISIBLE);
                timeView.setText(time.or("-"));

                if (state == ProjectControlState.ACCEPTED)
                    msgView.setText(R.string.elector_project_control_submit_already_done_response);
                else if (state == ProjectControlState.OFFLINE)
                    msgView.setText(R.string.agitator_project_control_reject_already_done_offline);
                else msgView.setText(R.string.elector_project_control_reject_already_done_response);

            }

            controlVM = model;
        }

    }

    @Override
    public void showFailedInfoRequestMsg(int code) {
        if (code == 404) {
            animatorView.setDisplayedChildId(R.id.elector_project_control_not_found_error);
        } else {
            showFailedInfoRequestUnknownMsg();
        }
    }

    @Override
    public void showFailedInfoRequestUnknownMsg() {
        Toast.makeText(getActivity().getApplicationContext(), failedMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showControlResponse(ElectorProjectControlResponse controlInfo, ProjectControlState state) {
        int action = (state == ProjectControlState.ACCEPTED) ?
                ProjectControlResponseDialog.SUBMIT : ProjectControlResponseDialog.REJECT;

        submitBtn.setEnabled(false);
        rejectBtn.setEnabled(false);

        ProjectControlResponseDialog.newInstance(controlInfo, action, controlVM)
                .show(getFragmentManager(), "project_control_response");
    }

    @Override
    public void showFailedControlRequestUnknownMsg(ProjectControlState state) {
        Toast.makeText(getActivity().getApplicationContext(), failedMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showFailedControlRequestMsg(int code, ProjectControlState state) {

        Timber.d("Elector control response fail : " + code);
        if (code == 400) {
            int msg = (state == ProjectControlState.ACCEPTED) ?
                    R.string.project_control_already_accepted : R.string.project_control_already_rejected;
            Toast.makeText(getActivity().getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity().getApplicationContext(), failedMsg, Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void showControlWillBeSentLater() {
        Toast.makeText(getActivity().getApplicationContext(), R.string.project_control_added_to_db, Toast.LENGTH_SHORT).show();
        getActivity().finish();

    }

    @Override
    public void showDatabaseError() {
        animatorView.setDisplayedChildId(R.id.elector_project_control_db_error);
    }

    @Override
    public void showNoInternetConnection() {
        Toast.makeText(getActivity().getApplicationContext(), R.string.no_network_msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showEnableGps() {
        new EnableGpsDialogFragment().show(getActivity().getSupportFragmentManager(), "enable_gps");
    }

    @Override
    public void blockActionViewUntilGetGPSLocation() {
        submitBtn.setEnabled(false);
        rejectBtn.setEnabled(false);
    }

    @Override
    public void unblockActionViewWhenGPSLocationAvailable() {
        submitBtn.setEnabled(true);
        rejectBtn.setEnabled(true);
    }

    @Override
    public void showGetCoordinatesView() {
        new GetCoordinatesDialogFragment().show(getActivity().getSupportFragmentManager(), "coordinates_tag");
    }

    @Override
    public void hideGetCoordinatesView() {
        DialogFragment fragment = (DialogFragment) getActivity().getSupportFragmentManager().findFragmentByTag("coordinates_tag");
        if (fragment != null) {
            //TODO fail to dismiss
            try {
                fragment.dismiss();
            } catch (Throwable th) {
                Timber.e(th, "Fail to dismiss");
            }
        }
    }


    @Override
    public void showLoading() {
        animatorView.setDisplayedChildId(R.id.elector_project_control_progress);
    }

    @Override
    public void hideLoading() {
        animatorView.setDisplayedChildId(R.id.elector_project_control_content);
    }

    @Subscribe
    public void onResponseOk(ResponseOkClickedEvent event) {
        getActivity().finish();
    }

}
