package com.itdoors.elections.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.common.base.Optional;
import com.itdoors.elections.R;
import com.itdoors.elections.data.api.model.Code;
import com.itdoors.elections.ui.mvp.model.AgitatorControlModel;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CodeView  extends RelativeLayout{

    @Bind(R.id.code_name)  TextView codeNameTextView;
    @Bind(R.id.code_value) PlusMinusView codeValueView;

    public CodeView(Context context){
        this(context, null);
    }

    public CodeView(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_code, this, true);
        ButterKnife.bind(this, view);

    }

    public void setName(String name){
        codeNameTextView.setText(name);
    }

    public void plus() {
        codeValueView.plus();
    }

    public void minus(){
        codeValueView.minus();
    }

    public int getValue(){
        switch (codeValueView.getCheckedId()){
            case R.id.minus_btn: return AgitatorControlModel.MINUS_VALUE;
            case R.id.plus_btn:  return AgitatorControlModel.PLUS_VALUE;
            default:
                return -1;
        }
    }

    public void bindTo(Code code) {
        setName(Optional.fromNullable(code.getName()).or("-"));
    }

    public boolean isChecked(){
      return codeValueView.getCheckedId() != -1;
    }

}
