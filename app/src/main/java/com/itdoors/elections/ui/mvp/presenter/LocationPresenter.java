package com.itdoors.elections.ui.mvp.presenter;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.os.Looper;

import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsStates;
import com.itdoors.elections.ui.fragment.ControlFragment;
import com.itdoors.elections.ui.mvp.view.GPSView;

import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Observable;
import timber.log.Timber;

/**
 * Created by v014nd on 07.06.2016.
 */
public abstract class LocationPresenter<VIEW extends GPSView> extends BasePresenter<VIEW> {

    protected final Handler mainHandler = new Handler(Looper.getMainLooper());

    @Override public void onActivityResult(int requestCode, int resultCode, Intent data){

        Timber.d("onActivityResult");

        final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);//intent);
        switch (requestCode) {
            case ControlFragment.REQUEST_CHECK_SETTINGS:
                //Refrence: https://developers.google.com/android/reference/com/google/android/gms/location/SettingsApi
                switch (resultCode) {
                    case Activity.RESULT_OK:        getView().userEnableGPS();     break;
                    case Activity.RESULT_CANCELED:  {

                        getView().userDisabledGPS();
                        getView().hideGetCoordinatesView();
                        //                  getView().hideControlLoading();

                    }   break;
                    default:
                        break;
                }
                break;
        }

    }

    protected abstract ReactiveLocationProvider getProvider();

    protected Observable<Location> getLocation(){

        final LocationRequest locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setNumUpdates(1)
                .setInterval(100);

        return getProvider().checkLocationSettings(
                new LocationSettingsRequest.Builder()
                        .addLocationRequest(locationRequest)
                        .setAlwaysShow(true)  //Refrence: http://stackoverflow.com/questions/29824408/google-play-services-locationservices-api-new-option-never
                        .build())
                .doOnSubscribe(() -> {
                    mainHandler.post(() -> {
                        if (getView() != null) {
                            getView().showGetCoordinatesView();
                            getView().blockActionViewUntilGetGPSLocation();
                        }
                    });
                })
                .doOnNext(locationSettingsResult -> mainHandler.post(() -> {
                    getView().startGPSResolution(locationSettingsResult.getStatus());
                    Timber.d("Location : start location resolution ");
                }))
                .flatMap(locationSettingsResult -> {
                    Timber.d("Location : start getting location ");
                    return getProvider().getUpdatedLocation(locationRequest);
                })
                //.timeout(15, TimeUnit.SECONDS)
                .doOnNext(loc -> mainHandler.post(() -> {
                    if (getView() != null) {
                        getView().hideGetCoordinatesView();
                        getView().unblockActionViewWhenGPSLocationAvailable();
                    }
                    if (loc != null) {
                        String lat = Double.toString(loc.getLatitude());
                        String lon = Double.toString(loc.getLongitude());
                        Timber.d("Location : " + "lat:" + lat + "," + "lon" + lon);
                    } else {
                        Timber.d("Location : null");
                    }
                }));
    }
}
