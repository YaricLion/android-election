package com.itdoors.elections.ui.mvp.presenter;

import com.itdoors.elections.ui.mvp.scope.HomeScope;
import com.itdoors.elections.ui.mvp.view.HomeView;

import javax.inject.Inject;

/**
 * Created by v014nd on 10.09.2015.
 */
@HomeScope
public class HomePresenter extends BasePresenter<HomeView> {

    @Inject
    public HomePresenter(){

    }
    public void scan(){
        if(getView() != null){
            getView().navigateToScanView();
        }
    }

}
