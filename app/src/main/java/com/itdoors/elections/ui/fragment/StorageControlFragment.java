package com.itdoors.elections.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.itdoors.elections.App;
import com.itdoors.elections.R;
import com.itdoors.elections.ui.event.RetryStorageEvent;
import com.itdoors.elections.ui.mvp.component.DaggerStorageControlComponent;
import com.itdoors.elections.ui.mvp.component.StorageControlComponent;
import com.itdoors.elections.ui.mvp.presenter.StorageControlPresenter;
import com.itdoors.elections.ui.view.storage.StorageControlLayoutView;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import icepick.Icepick;

public class StorageControlFragment extends BaseFragment<StorageControlComponent, StorageControlPresenter> {

    @Inject
    StorageControlPresenter storageControlPresenter;
    @Inject
    Bus bus;

    @Bind(R.id.storage_control_view)
    StorageControlLayoutView storageControlView;

    @Override
    protected StorageControlComponent onCreateNonConfigurationComponent() {
        return DaggerStorageControlComponent.builder().userComponent(App.getUserComponent(getActivity())).build();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getComponent().inject(this);
        Icepick.restoreInstanceState(this, savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_storage_control, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        load();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private void load() {
        storageControlPresenter.loadStorageControlInfo();
    }

    @Override
    public void onStart() {
        super.onStart();
        bus.register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        bus.unregister(this);
    }

    @Subscribe
    public void onRetryStorage(RetryStorageEvent event) {
        load();
    }

}
