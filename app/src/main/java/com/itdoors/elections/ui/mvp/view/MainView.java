package com.itdoors.elections.ui.mvp.view;

/**
 * Created by v014nd on 22.06.2016.
 */
public interface MainView extends GPSView, View {
    //void showUnableToFigureOutIsElectionTime(Throwable th);

    void showIsElectionDay(Boolean isElectionDay);

    void showStartRefreshingCodes();

    void showCodesRefreshed();

    void showCodesRefreshFailed(Throwable th);

    void showControlCountInfo(long prAgitator, long prElectors, long edElectors);
/*  void showAgitatorProjectControl(long agitator);
    void showElectorProjectControl(long electors);*/

    void showProjectControlDBError(Throwable throwable);
    void showCodesAvailability(boolean available);
    void showCodesAvailabilityError(Throwable throwable);

    void showFailedTimeRequestMsg(int code);
    void showFailedTimeRequestUnknownMsg();
    void showNoTimeLocallySaved();
}
