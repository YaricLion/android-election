package com.itdoors.elections.ui.mvp.presenter;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.itdoors.elections.ui.mvp.presenter.Presenter;

public abstract class BasePresenter<V> implements Presenter<V> {
    private V view;
    @Override public void onCreate(@Nullable Bundle bundle) {}
    @Override public void bindView(V view) { this.view = view;}
    @Override public void unbindView() {
        this.view = null;
    }
    @Override public void onSaveInstanceState(@NonNull Bundle bundle) {}
    @Override public V getView() {
        return view;
    }
    @Override public void onDestroy() {}

    @Override public void onActivityResult(int requestCode, int resultCode, Intent data){}

    @Override public void onStart(){}
    @Override public void onStop(){}


}