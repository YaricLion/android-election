package com.itdoors.elections.ui.mvp.component;

import com.itdoors.elections.data.api.UserComponent;
import com.itdoors.elections.ui.fragment.MainFragment;
import com.itdoors.elections.ui.mvp.base.HasPresenter;
import com.itdoors.elections.ui.mvp.presenter.MainPresenter;
import com.itdoors.elections.ui.mvp.scope.MainScope;

import dagger.Component;

@Component(dependencies = UserComponent.class)
@MainScope
public interface MainComponent extends HasPresenter<MainPresenter> {
    void inject(MainFragment fragment);
}
