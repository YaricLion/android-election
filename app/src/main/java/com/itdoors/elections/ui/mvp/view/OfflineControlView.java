package com.itdoors.elections.ui.mvp.view;

/**
 * Created by yariclion on 12.07.16.
 */
public interface OfflineControlView<VM> extends GPSView{

    void showRetry();
    void showControlLoading();
    void hideControlLoading();

    void showInfo(VM model);
    void showFailedInfoRequestMsg(int code);
    void showFailedInfoRequestUnknownMsg();
    void showControlWillBeSentLater();

    void showDatabaseError();
    void showNoInternetConnection();
}
