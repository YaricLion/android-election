package com.itdoors.elections.ui.mvp.base;

import android.os.Bundle;

public class ComponentControllerDelegate<C> {

    private static final String PRESENTER_INDEX_KEY = "presenter-index";

    private C component;
    private ComponentCache cache;
    private long componentId;
    private boolean isDestroyedBySystem;

    public void onCreate(ComponentCache cache, Bundle savedInstanceState,
                         ComponentFactory<C> componentFactory) {
        this.cache = cache;
        if (savedInstanceState == null) {
            componentId = cache.generateId();
        } else {
            componentId = savedInstanceState.getLong(PRESENTER_INDEX_KEY);
        }
        component = cache.getComponent(componentId);
        if (component == null && componentFactory != null) {
            component = componentFactory.createComponent();
            cache.setComponent(componentId, component);
        }
    }

    public void onResume() {
        isDestroyedBySystem = false;
    }

    /** Quite interesting method to understand if system is gonna kill activity and fragments or it's just a configuration changes issue.
     * If onSaveInstanceState is calling that mean that instance is destroyed by system.
     * Also we can save component id in that memento-bundle-state.*/
    public void onSaveInstanceState(Bundle outState) {
        isDestroyedBySystem = true;
        outState.putLong(PRESENTER_INDEX_KEY, componentId);
    }

    /**
     * When is time to destroy instance, if not by the system, then clear instance of component in component cache */
    public void onDestroy() {
        if (!isDestroyedBySystem) {
            // User is exiting this view, remove component from the cache
            //cache.setComponent(componentId, null);
            cache.removeComponent(componentId);
        }
    }

    public C getComponent() {
        return component;
    }

    public long getComponentId() {
        return componentId;
    }
}