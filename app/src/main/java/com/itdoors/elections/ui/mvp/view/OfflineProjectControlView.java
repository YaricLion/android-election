package com.itdoors.elections.ui.mvp.view;

import com.itdoors.elections.data.api.model.ProjectControlState;

public interface OfflineProjectControlView<VM, RESPONSE> extends OfflineControlView<VM>, LoadingView, GPSView, View {

    void showControlResponse(RESPONSE controlInfo, ProjectControlState state);
    void showFailedControlRequestMsg(int code, ProjectControlState state);
    void showFailedControlRequestUnknownMsg(ProjectControlState state);

}
