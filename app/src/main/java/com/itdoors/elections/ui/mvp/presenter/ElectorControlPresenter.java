package com.itdoors.elections.ui.mvp.presenter;

import android.location.Location;

import com.itdoors.elections.data.model.Elector;
import com.itdoors.elections.data.model.electorcontrol.ElectorControlResponseOld;
import com.itdoors.elections.data.model.electorcontrol.ElectorControlSend;
import com.itdoors.elections.ui.mvp.scope.ElectorControlScope;
import com.itdoors.elections.ui.mvp.model.ElectorControlModel;
import com.itdoors.elections.ui.mvp.view.ElectorControlView;
import com.itdoors.elections.util.Device;
import com.itdoors.elections.util.SchedulerProvider;
import com.itdoors.elections.util.TimeUtils;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Observable;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

import static com.itdoors.elections.util.MVPUtils.onError;

@ElectorControlScope
public class ElectorControlPresenter extends OfflineControlPresenter<ElectorControlModel, ElectorControlView, Elector,ElectorControlResponseOld> {

    private final int CONTROL_REQUEST_TIMEOUT_IN_MINUTES = 5;

    private final ElectorControlModel electorControlModel;

    private final SchedulerProvider schedulerProvider;
    private final ReactiveLocationProvider locationProvider;
    private final Device device;

    private CompositeSubscription subscription = new CompositeSubscription();

    @Inject
    public ElectorControlPresenter(ElectorControlModel electorControlModel, SchedulerProvider schedulerProvider, ReactiveLocationProvider locationProvider, Device device) {
        super(electorControlModel);
        this.electorControlModel = electorControlModel;
        this.schedulerProvider = schedulerProvider;
        this.locationProvider = locationProvider;
        this.device = device;
    }

    public void scanElector(String guid){
        getView().showLoading();

        Observable<Location> locationObservable = this.getLocation()
                .first()
                .timeout(CONTROL_REQUEST_TIMEOUT_IN_MINUTES, TimeUnit.MINUTES);

        Observable<ElectorControlSend> sendEntityObservable = locationObservable.map(location -> {
            long gpsTime = location.getTime();
            String lat = Double.toString(location.getLatitude());
            String lon = Double.toString(location.getLongitude());
            String time = TimeUtils.formatGps(gpsTime);
            ElectorControlSend electorControlSend = new ElectorControlSend(guid, time, lat, lon);
            return electorControlSend;
        });

        if(device.isNetworkAvailable()) {
            subscription.add(
                    electorControlModel.verifyElector(guid)
                            .compose(schedulerProvider.applySchedulers())
                            .subscribe(
                                    elector -> {
                                        try {
                                            if (getView() != null) {
                                                getView().hideLoading();
                                                getView().showInfo(elector);
                                            }
                                        } catch (Throwable th) {
                                            Timber.e(th, "Fail to show elector with uid :" + guid + ". " + "Problem on View.");
                                            throw new FailedToShowInfoDataOnViewException(th);
                                        }
                                    },
                                    throwable -> {
                                        if (!(throwable instanceof FailedToShowInfoDataOnViewException)) {
                                            Timber.e(throwable, "Fail to show elector with uid :" + guid);
                                        }
                                        if (getView() != null) {
                                            onError(
                                                    throwable,
                                                    httpCode -> {
                                                        getView().showFailedInfoRequestMsg(httpCode);
                                                        if (httpCode != 404) {
                                                            getView().showRetry();
                                                        }
                                                    },
                                                    () -> {
                                                        getView().showFailedInfoRequestUnknownMsg();
                                                        getView().showRetry();
                                                    }
                                            );
                                        }
                                    }
                            ));
        }
        else {
            subscription.add(
                    sendEntityObservable
                            .flatMap(sendItem -> getStorageModel().saveControl(sendItem))
                            .compose(schedulerProvider.applySchedulers())
                            .subscribe(
                                    aVoid -> {
                                        if (getView() != null) {
                                            getView().showControlWillBeSentLater();
                                        }
                                    },
                                    throwable -> {
                                        if (getView() != null) {
                                            getView().showDatabaseError();
                                        }
                                    }
                            )
            );
        }
    }

    public void sendControl(String guid){

        getView().showControlLoading();
        electorControlModel.resetControltRequest();
        final String dateTimestamp = String.valueOf(Device.getCurrentDate());

        if(device.isNetworkAvailable()) {
            subscription.add(
                    this.getLocation()
                            .timeout(CONTROL_REQUEST_TIMEOUT_IN_MINUTES, TimeUnit.MINUTES)
                            .flatMap(location -> {
                                long gpsTime = location.getTime();
                                String lat = Double.toString(location.getLatitude());
                                String lon = Double.toString(location.getLongitude());
                                String time = TimeUtils.formatGps(gpsTime);
                                return electorControlModel.controlElector(guid, time, lat, lon);
                            })
                            .compose(schedulerProvider.applySchedulers())
                            .subscribe(
                                    electorControl -> {
                                        try {

                                            if (getView() != null) {
                                                getView().hideControlLoading();
                                                getView().showControlResponse(electorControl);
                                            }

                                        } catch (Throwable throwable) {
                                            Timber.e(throwable,
                                                    "Fail to send control elector. " + ". " +
                                                            "Data not on the view. " + ". " +
                                                            "electorControl response : " + electorControl.toString() + ". " +
                                                            "Request : " +
                                                            "uid=" + guid +
                                                            "date=" + dateTimestamp);
                                            throw new FailedToShowControlDataOnViewException(throwable);
                                        }
                                    },
                                    throwable -> {

                                        if (!(throwable instanceof FailedToShowControlDataOnViewException)) {
                                            Timber.e(throwable,
                                                    "Fail to send control elector. " + ". " +
                                                            "Request : " +
                                                            "uid=" + guid +
                                                            "date=" + dateTimestamp);
                                        }

                                        if (getView() != null) {
                                            getView().hideControlLoading();
                                            onError(throwable, httpCode -> getView().showFailedControlRequestMsg(httpCode),
                                                    () -> getView().showFailedControlRequestUnknownMsg()
                                            );
                                        }
                                    })
            );
        }else {
            if (getView() != null) {
                getView().hideControlLoading();
                getView().showNoInternetConnection();
            }
        }
    }

    public void reloadElector(String gui){
        electorControlModel.resetElectorRequest();
        scanElector(gui);
    }

    @Override public void onDestroy() {
        super.onDestroy();
        if(subscription != null) {
            subscription.unsubscribe();
            subscription = null;
        }
    }

    @Override protected ReactiveLocationProvider getProvider() {
        return locationProvider;
    }
}
