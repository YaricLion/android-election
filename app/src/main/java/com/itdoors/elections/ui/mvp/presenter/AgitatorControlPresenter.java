package com.itdoors.elections.ui.mvp.presenter;

import android.util.Pair;

import com.google.gson.Gson;
import com.itdoors.elections.data.model.Agitator;
import com.itdoors.elections.data.model.agitatorcontrol.AgitatorControlResponseOld;
import com.itdoors.elections.data.api.model.AgitatorType;
import com.itdoors.elections.data.api.model.Code;
import com.itdoors.elections.data.api.model.CodeValue;
import com.itdoors.elections.ui.mvp.model.AgitatorControlModel;
import com.itdoors.elections.ui.mvp.scope.AgitatorControlScope;
import com.itdoors.elections.ui.mvp.view.AgitatorControlView;
import com.itdoors.elections.util.Device;
import com.itdoors.elections.util.SchedulerProvider;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Observable;
import rx.functions.Func2;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

import static com.itdoors.elections.util.MVPUtils.onError;

@AgitatorControlScope
public class AgitatorControlPresenter extends ControlPresenter<Agitator, AgitatorControlView, AgitatorControlResponseOld> {

    private final int CONTROL_REQUEST_TIMEOUT_IN_MINUTES = 5;

    private final AgitatorControlModel agitatorControlModel;

    private final SchedulerProvider schedulerProvider;
    private final ReactiveLocationProvider locationProvider;

    private final Gson gson;

    private CompositeSubscription subscription = new CompositeSubscription();

    @Inject
    public AgitatorControlPresenter(AgitatorControlModel agitatorControlModel, SchedulerProvider schedulerProvider, ReactiveLocationProvider locationProvider, Gson gson){
        this.agitatorControlModel = agitatorControlModel;
        this.schedulerProvider = schedulerProvider;
        this.locationProvider = locationProvider;
        this.gson = gson;
    }

    public void loadAgitator(String guid){
        getView().showLoading();
        subscription.add(
                Observable.zip(
                            agitatorControlModel.getAgitator(guid),
                            agitatorControlModel.getCodes(),
                            (Func2<Agitator, List<Code>, Pair<Agitator, List<Code>>>) Pair::new
                        )
                        .compose(schedulerProvider.applySchedulers())
                        .subscribe(
                                pair -> {
                                    try {
                                        if (getView() != null) {
                                            Agitator agitator = pair.first;
                                            List<Code> codes = pair.second;
                                            if(isSupported(agitator)) {
                                                getView().showInfo(agitator);
                                                getView().showCodes(codes);
                                                getView().hideLoading();
                                            }
                                            else{
                                                getView().showUnsupportedAgitatorType(agitator);
                                            }
                                        }
                                    }
                                    catch (Throwable throwable){
                                        Timber.e(throwable, "Fail to show agitator with uid :" + guid + ". " + "Problem on View.");
                                        throw new FailedToShowInfoDataOnViewException(throwable);
                                    }
                                },
                                throwable -> {
                                    if(!(throwable instanceof FailedToShowInfoDataOnViewException))
                                        Timber.e(throwable, "Fail to show agitator with uid :" + guid);

                                    if (getView() != null) {
                                        onError(
                                                throwable,
                                                httpCode -> {
                                                    getView().showFailedInfoRequestMsg(httpCode);
                                                    if (httpCode != 404) {
                                                        getView().showRetry();
                                                    }
                                                },
                                                () -> {
                                                    getView().showFailedInfoRequestUnknownMsg();
                                                    getView().showRetry();
                                                }
                                        );
                                    }
                                }
                        ));
    }

    public void reloadAgitator(String gui){
        agitatorControlModel.resetAgitator();
        agitatorControlModel.resetCodes();
        loadAgitator(gui);
    }

   public void sendControl(String guid, List<CodeValue> codeValues, String comment){

       getView().showControlLoading();
       agitatorControlModel.resetControl();

       final String dateTimestamp = String.valueOf(Device.getCurrentDate());
       final String jsonCodeValues = gson.toJson(codeValues);

       subscription.add(
               this.getLocation()
                       .timeout(CONTROL_REQUEST_TIMEOUT_IN_MINUTES, TimeUnit.MINUTES)
                       .flatMap(location -> {
                           String lat = Double.toString(location.getLatitude());
                           String lon = Double.toString(location.getLongitude());
                           return agitatorControlModel.controlAgitator(guid, jsonCodeValues, comment, dateTimestamp, lat, lon);
                       })
                       .compose(schedulerProvider.applySchedulers())
                       .subscribe(
                               agitatorControl -> {
                                   try {
                                       if (getView() != null) {
                                           getView().hideControlLoading();
                                           getView().showControlResponse(agitatorControl);
                                       }
                                   } catch (Throwable throwable) {

                                       Timber.e(throwable,
                                               "Fail to send control agitator. " + ". " +
                                                       "Data not on the view. " + ". " +
                                                       "Request : " +
                                                       "uid=" + guid +
                                                       "codes=" + jsonCodeValues +
                                                       "comment=" + comment +
                                                       "date=" + dateTimestamp);

                                       throw new FailedToShowControlDataOnViewException(throwable);
                                   }
                               },
                               throwable -> {

                                   if (!(throwable instanceof FailedToShowControlDataOnViewException))
                                       Timber.e(throwable,
                                               "Fail to send control agitator. " + ". " +
                                                       "Request : " +
                                                       "uid=" + guid +
                                                       "codes=" + jsonCodeValues +
                                                       "comment=" + comment +
                                                       "date=" + dateTimestamp);

                                   if (getView() != null) {
                                       getView().hideControlLoading();
                                       onError(throwable,
                                               httpCode -> getView().showFailedControlRequestMsg(httpCode),
                                               () -> getView().showFailedControlRequestUnknownMsg()
                                       );
                                   }

                               })
       );
   }

    private boolean isSupported(Agitator agitator){
        return agitator.getType() == AgitatorType.TENT;
    }

    @Override public void onDestroy() {
        super.onDestroy();
        if(subscription != null) {
            subscription.unsubscribe();
            subscription = null;
        }
    }

    @Override protected ReactiveLocationProvider getProvider() {
        return locationProvider;
    }
}
