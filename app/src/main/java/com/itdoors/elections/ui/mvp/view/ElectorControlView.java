package com.itdoors.elections.ui.mvp.view;

import com.itdoors.elections.data.model.Elector;
import com.itdoors.elections.data.model.electorcontrol.ElectorControlResponseOld;

public interface ElectorControlView extends OfflineElectionDayControlView<Elector, ElectorControlResponseOld> {

}
