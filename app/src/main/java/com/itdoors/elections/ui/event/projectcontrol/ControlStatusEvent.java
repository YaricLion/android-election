package com.itdoors.elections.ui.event.projectcontrol;

/**
 * Created by yariclion on 03.07.16.
 */
public final class ControlStatusEvent {

    private final boolean success;

    public ControlStatusEvent(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

}
