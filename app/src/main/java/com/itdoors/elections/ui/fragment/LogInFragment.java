package com.itdoors.elections.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.itdoors.elections.App;
import com.itdoors.elections.AppComponent;
import com.itdoors.elections.BuildConfig;
import com.itdoors.elections.R;
import com.itdoors.elections.ui.event.LoginToMainActivityEvent;
import com.itdoors.elections.ui.event.LoginToPreviousActivityEvent;
import com.itdoors.elections.ui.mvp.component.DaggerLoginComponent;
import com.itdoors.elections.ui.mvp.presenter.LoginPresenter;
import com.itdoors.elections.ui.mvp.component.LoginComponent;
import com.itdoors.elections.ui.mvp.view.LoginView;
import com.itdoors.elections.util.Device;
import com.itdoors.elections.util.NotEmptyValidator;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.otto.Bus;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;
import icepick.Icepick;
import icepick.State;
import rx.subjects.PublishSubject;
import timber.log.Timber;

public class LogInFragment extends BaseDynamicPresenterFragment<LoginComponent, LoginPresenter> implements LoginView {

    private static final String BLOCKING_ARGS_TAG = "is_blocking_mode";

    //@Inject Bus bus;

    private Bus bus;

    @Inject LoginPresenter presenter;
    @Inject
    Device device;

    @Bind(R.id.login_ip_edit)
    MaterialEditText urlView;
    @Bind(R.id.login_email_edit)
    MaterialEditText emailView;
    @Bind(R.id.login_password_edit)
    MaterialEditText passView;

    @Bind(R.id.login_signin_btn)      Button loginBtn;

    @State boolean isLogging = false;

    @BindString(R.string.email_empty_msg)
    String emailEmptyMsgString;
    @BindString(R.string.ip_empty_msg)
    String ipEmptyMsgString;
    @BindString(R.string.password_empty_msg)
    String passwordEmptyMsgString;

    @BindString(R.string.wrong_email_or_password_msg) String wrongEmailOrPassString;
    @BindString(R.string.error_def_msg)          String defErrorMsg;

    @BindString(R.string.client_side_error)
    String clientSideErrorMsg;
    @BindString(R.string.server_side_error)
    String serverSideErrorMsg;


    private PublishSubject<Boolean> loggingSubject = PublishSubject.create();

    public static LogInFragment newInstance(boolean blocking){

        LogInFragment fragment = new LogInFragment();

        Bundle bundle = new Bundle();
        bundle.putBoolean(BLOCKING_ARGS_TAG, blocking);
        fragment.setArguments(bundle);

        return fragment;

    }

    protected boolean isBlocking(){
        return getArguments() != null && getArguments().getBoolean(BLOCKING_ARGS_TAG);
    }

   /* @Override    protected LoginComponent onCreateNonConfigurationComponent() {
        return DaggerLoginComponent.builder().appComponent(App.getAppComponent(getActivity())).build();
    }*/

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppComponent appComponent = App.getAppComponent(getActivity());
        bus = appComponent.bus();
        device = appComponent.device();

        //getComponent().inject(this);

        Icepick.restoreInstanceState(this, savedInstanceState);
    }

    @Override public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login_with_ip, container, false);
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        urlView.setVisibility(View.GONE);

        //urlView.addValidator( new NotEmptyValidator(ipEmptyMsgString));
        emailView.addValidator(new NotEmptyValidator(emailEmptyMsgString));
        passView.addValidator(new NotEmptyValidator(passwordEmptyMsgString));

        loggingSubject.subscribe(loading -> {

            isLogging = loading;

            //urlView  .setEnabled(!isLogging);
            loginBtn.setEnabled(!isLogging);
            emailView.setEnabled(!isLogging);
            passView.setEnabled(!isLogging);

        });

        loggingSubject.onNext(isLogging);

        if (BuildConfig.DEBUG) {

            String email = "election_admin";
            String pass = "siT421";

            emailView.setText(email);
            passView.setText(pass);
        }
    }


    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick(R.id.login_signin_btn) public void onLogin(Button button){

        if (!validateBeforeLogin()) return;
        if (!device.isNetworkAvailable()) {
            Toast.makeText(getActivity().getApplicationContext(), R.string.no_network_msg, Toast.LENGTH_SHORT).show();
            return;
        }

        String email = emailView.getText().toString();
        String password = passView.getText().toString();
        //String url      = urlView.getText().toString();
        String url = BuildConfig.API_URL;

        if (getComponent() == null) {
            createComponentAndInject(url, email);
            createPresenter();
        } else {

            LoginComponent loginComponent = getComponent();

            String currentApiUrl = loginComponent.apiUrl();
            String currentEmail = loginComponent.email();

            if (!currentApiUrl.equals(url)) {

                releasePresenter();
                App.releaseUserComponent(getActivity());
                createComponentAndInject(url, email);
                createPresenter();
            }

        }


        presenter.login(email, password, url);

    }

    private void createComponentAndInject(String ip, String email) {
        createComponent(
                DaggerLoginComponent.builder()
                        .userComponent(App.createUserComponent(getActivity(), ip, email))
                        .build()
        );
        getComponent().inject(this);
    }


    private boolean validateBeforeLogin() {

        boolean filedCorrectly = true;

        if (!emailView.validate())
            filedCorrectly = false;
        if (!passView.validate())
            filedCorrectly = false;
       /* if(!urlView.validate())
            filedCorrectly = false;
*/
        return filedCorrectly;

    }

    @Override  public void navigateToMainActivity() {
        bus.post(new LoginToMainActivityEvent());
    }

    @Override public void navigateBackToCurrentActivity() {
        bus.post(new LoginToPreviousActivityEvent());
    }

    @Override  public void showFailedTokenRequestMsg(int code) {
        Timber.d("Fail to get token : " + code);

        String msg;
        if (code == 400 || code == 401)
            msg = wrongEmailOrPassString;
        else if (code > 400 && code < 500)
            msg = clientSideErrorMsg;
        else
            msg = serverSideErrorMsg;

        Toast.makeText(getActivity().getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override  public void showFailedTokenRequestUnknownMsg() {
        Timber.d("Fail to get token : Unknown");
        Toast.makeText(getActivity().getApplicationContext(), defErrorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override public void showFailedUserRequestMsg(int code) {
        Timber.d("Fail to get token : " + code);
        Toast.makeText(getActivity().getApplicationContext(), defErrorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override public void showFailedUserRequestUnknownMsg() {
        Timber.d("Fail to get user : Unknown");
        Toast.makeText(getActivity().getApplicationContext(), defErrorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override public void showLoading() {
        Timber.d("Show loading");
        loggingSubject.onNext(true);
    }

    @Override public void hideLoading() {
        Timber.d("Hide loading");
        loggingSubject.onNext(false);
    }


}
