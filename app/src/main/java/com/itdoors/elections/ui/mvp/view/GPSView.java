package com.itdoors.elections.ui.mvp.view;

import com.google.android.gms.common.api.Status;

/**
 * Created by v014nd on 07.06.2016.
 */
public interface GPSView extends View {

    void showEnableGps();

    void startGPSResolution(Status status);

    void blockActionViewUntilGetGPSLocation();
    void unblockActionViewWhenGPSLocationAvailable();
    void errorToOpenSetting(Throwable throwable);

    void userEnableGPS();
    void userDisabledGPS();

    void showGetCoordinatesView();

    void hideGetCoordinatesView();
}
