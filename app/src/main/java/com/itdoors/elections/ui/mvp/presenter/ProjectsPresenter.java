package com.itdoors.elections.ui.mvp.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.itdoors.elections.data.model.project.ProjectViewModel;
import com.itdoors.elections.ui.mvp.model.ProjectsModel;
import com.itdoors.elections.ui.mvp.scope.ProjectsScope;
import com.itdoors.elections.ui.mvp.view.ProjectsView;
import com.itdoors.elections.util.Device;

import java.util.List;

import javax.inject.Inject;

import rx.subscriptions.CompositeSubscription;

@ProjectsScope
public class ProjectsPresenter extends BasePresenter<ProjectsView<List<ProjectViewModel>>> {

    private final ProjectsModel projectsModel;
    private final Device device;

    private CompositeSubscription subscription = new CompositeSubscription();


    private PresenterCallback presenterCallback;

    @Inject
    public ProjectsPresenter(ProjectsModel model, Device device) {
        this.projectsModel = model;
        this.device = device;
    }

    public void load() {
        if (getView() != null) {
            getView().showLoading();
        }

        boolean internet = device.isNetworkAvailable();
        if (!internet) {
            if (getView() != null)
                getView().showNoInternetConnection();

            subscription.add(
                    projectsModel.load(false)
                            .subscribe(
                                    projectViewModels -> {
                                        if (getView() != null) {
                                            if (projectViewModels == null || projectViewModels.isEmpty())
                                                getView().showDatabaseIsEmpty();
                                            else
                                                getView().showDatabaseInfo(projectViewModels, null);
                                        }
                                    },
                                    throwable -> {
                                        if (getView() != null)
                                            getView().showDatabaseError(throwable);
                                    }
                            ));
        } else {
            subscription.add(
                    projectsModel.load(true)
                            .subscribe(
                                    projectViewModels -> {
                                        if (getView() != null) {
                                            if (projectViewModels == null || projectViewModels.isEmpty())
                                                getView().showResponseIsEmpty();
                                            else
                                                getView().showResponseInfo(projectViewModels);
                                        }
                                    },
                                    throwable -> {
                                        if (getView() != null)
                                            getView().showResponseError(throwable);
                                    }
                            )
            );
        }

    }

    public void refresh(int mode) {
        projectsModel.reset();
        load();
    }

    public void retry(int mode) {
        projectsModel.reset();
        load();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (subscription != null) {
            subscription.unsubscribe();
            subscription = null;
        }
        presenterCallback.onDestroy();
    }

    private void setUpCallback() {
        if (presenterCallback == null) {
            //presenterCallback = new BasePresenterCallback();
            presenterCallback = projectsModel.createPresenterCallback();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        setUpCallback();
        presenterCallback.onCreate();
    }

    @Override
    public void unbindView() {
        super.unbindView();
        presenterCallback.onUnbindView();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle bundle) {
        super.onSaveInstanceState(bundle);
        presenterCallback.onSaveInstanceState();
    }

    @Override
    public void onStart() {
        super.onStart();
        presenterCallback.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        presenterCallback.onStop();
    }

}
