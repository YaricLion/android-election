package com.itdoors.elections.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.itdoors.elections.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by v014nd on 21.09.2015.
 */
public class PlusMinusView extends RelativeLayout {

    private static final int CHECKED_PLUS_VALUE  = 1;
    private static final int CHECKED_MINUS_VALUE = 0;

    private final String plusText;
    private final String minusText;

    private final int checkedValue;

    @Bind(R.id.plus_minus_radio_btns_group)    RadioGroup plusMinusRadioGroup;
    @Bind(R.id.plus_btn)   RadioButton plusRadioButton;

    @Bind(R.id.minus_btn)  RadioButton minusRadioButton;



    public PlusMinusView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.PlusMinusOptions, 0, 0);
        try {
            plusText = a.getString(R.styleable.PlusMinusOptions_plusKey);
            minusText = a.getString(R.styleable.PlusMinusOptions_minusKey);
            checkedValue = a.getInt(R.styleable.PlusMinusOptions_defaultCheck, CHECKED_PLUS_VALUE);
        } finally {
            a.recycle();
        }

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_plus_minus, this, true);

        ButterKnife.bind(this, view);
        initViews();
    }

    private void initViews() {

        plusRadioButton.setText(plusText);
        minusRadioButton.setText(minusText);

    }

    public void plus() {
        plusMinusRadioGroup.check(R.id.plus_btn);
    }

    public void minus() {
        plusMinusRadioGroup.check(R.id.minus_btn);
    }

    public int getCheckedId() {
        return plusMinusRadioGroup.getCheckedRadioButtonId();
    }


}
