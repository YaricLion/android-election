package com.itdoors.elections.ui.view.recycler;

import android.view.View;

import com.itdoors.elections.data.model.ViewModel;

/**
 * Created by v014nd on 14.06.2016.
 */
public interface BindableView<VM extends ViewModel> extends Bindable<VM> {
    View getView();
}
