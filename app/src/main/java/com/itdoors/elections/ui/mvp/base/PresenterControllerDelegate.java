package com.itdoors.elections.ui.mvp.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.itdoors.elections.ui.mvp.presenter.Presenter;

import timber.log.Timber;

public class PresenterControllerDelegate<PRESENTER extends Presenter> {
    private boolean isDestroyedBySystem;
    private PRESENTER presenter;

    public void onCreate(PRESENTER presenter, Bundle savedInstanceState) {
        this.presenter = presenter;
        presenter.onCreate(savedInstanceState);
    }
/*
    @SuppressWarnings("unchecked")
    public void onCreateView(Fragment fragment, View view) {
        try {
            presenter.bindView(fragment);
        } catch (ClassCastException e1) {
            try {
                presenter.bindView(view);
            } catch (ClassCastException e2) {
                throw new RuntimeException("Either your view or fragment needs to implement the " +
                        "view interface expected by " + presenter.getClass().getSimpleName() + ".");
            }
        }
    }*/

    @SuppressWarnings("unchecked")
    public void onViewCreated(Fragment fragment, View view) {
        bind(fragment, view);
    }

    public void bind(Fragment fragment, View view) {

        Timber.d("bind : " + true);
        if (fragment != null && fragment instanceof com.itdoors.elections.ui.mvp.view.View) {
            presenter.bindView((com.itdoors.elections.ui.mvp.view.View) fragment);
        } else if (view != null && view instanceof com.itdoors.elections.ui.mvp.view.View) {
            presenter.bindView((com.itdoors.elections.ui.mvp.view.View) view);
        } else {
            throw new RuntimeException("Either your view or fragment needs to implement the " +
                    "view interface expected by " + presenter.getClass().getSimpleName() + ".");
        }

    }
    public void onResume() {
        isDestroyedBySystem = false;
    }

    public void onSaveInstanceState(Bundle outState) {
        isDestroyedBySystem = true;
        presenter.onSaveInstanceState(outState);
    }
    public void onStart(){
        presenter.onStart();
    }
    public void onStop(){
        presenter.onStop();
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        presenter.onActivityResult(requestCode, resultCode, data);
    }


    public void onDestroyView() {
        presenter.unbindView();
    }

    public void onDestroy() {
        if (!isDestroyedBySystem) {
            presenter.onDestroy();
        }
    }
}