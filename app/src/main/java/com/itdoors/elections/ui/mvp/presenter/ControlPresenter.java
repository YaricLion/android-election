package com.itdoors.elections.ui.mvp.presenter;

import android.app.Activity;
import android.content.Intent;

import com.itdoors.elections.ui.fragment.ControlFragment;
import com.itdoors.elections.ui.mvp.view.ControlView;

/**
 * Created by v014nd on 18.09.2015.
 */
public abstract class ControlPresenter<MODEL, VIEW extends ControlView<MODEL, CONTROL_INFO>, CONTROL_INFO> extends LocationPresenter<VIEW> {

    private PresenterCallback presenterCallback;

    protected static class FailedToShowInfoDataOnViewException extends RuntimeException {
        public FailedToShowInfoDataOnViewException(Throwable throwable) {
            super(throwable);
        }
    }

    protected static class FailedToShowControlDataOnViewException extends RuntimeException {
        public FailedToShowControlDataOnViewException(Throwable throwable) {
            super(throwable);
        }
    }

    @Override public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ControlFragment.REQUEST_CHECK_SETTINGS && resultCode == Activity.RESULT_CANCELED) {
            getView().hideControlLoading();
        }
    }

}
