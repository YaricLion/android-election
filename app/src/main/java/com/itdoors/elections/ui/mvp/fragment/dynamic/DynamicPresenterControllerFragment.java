package com.itdoors.elections.ui.mvp.fragment.dynamic;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.itdoors.elections.ui.mvp.base.HasPresenter;
import com.itdoors.elections.ui.mvp.base.PresenterControllerDelegate;
import com.itdoors.elections.ui.mvp.presenter.Presenter;

public abstract class DynamicPresenterControllerFragment<COMPONENT extends HasPresenter<PRESENTER>, PRESENTER extends Presenter> extends DynamicComponentControllerFragment<COMPONENT> {

    private final String IS_PRESENTER_ALREADY_CREATED = "isPresenterCreated";

    private PresenterControllerDelegate<PRESENTER> presenterDelegate = new PresenterControllerDelegate<>();

    private Bundle fragmentSavedInstance = null;
    private boolean isPresenterCreated = false;

    protected void createPresenter() {

        presenterDelegate.onCreate(getPresenter(), fragmentSavedInstance);
        presenterDelegate.bind(this, getView());
        isPresenterCreated = true;

    }

    protected void releasePresenter() {

        releaseComponent();

        presenterDelegate = new PresenterControllerDelegate<>();
        fragmentSavedInstance = null;
        isPresenterCreated = false;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentSavedInstance = savedInstanceState;
        if (savedInstanceState != null) {
            isPresenterCreated = savedInstanceState.getBoolean(IS_PRESENTER_ALREADY_CREATED);
            if (isPresenterCreated) {
                if (getComponent() != null && getPresenter() != null) {
                    presenterDelegate.onCreate(getPresenter(), savedInstanceState);
                    isPresenterCreated = true;
                } else {
                    isPresenterCreated = false;
                    fragmentSavedInstance = null;
                }
            }
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (isPresenterCreated) {
            presenterDelegate.onViewCreated(this, view);
            //presenterDelegate.onCreateView(this, view);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isPresenterCreated) {
            presenterDelegate.onResume();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(IS_PRESENTER_ALREADY_CREATED, isPresenterCreated);
        if (isPresenterCreated) {
            presenterDelegate.onSaveInstanceState(outState);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (isPresenterCreated) {
            presenterDelegate.onStart();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (isPresenterCreated) {
            presenterDelegate.onStop();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (isPresenterCreated) {
            presenterDelegate.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (isPresenterCreated) {
            presenterDelegate.onDestroyView();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (isPresenterCreated) {
            presenterDelegate.onDestroy();
        }
    }

    public PRESENTER getPresenter() {
        if (getComponent() == null)
            throw new NullPointerException("Component (container for Presenter) isn't created yet!");
        return getComponent().getPresenter();
    }
}