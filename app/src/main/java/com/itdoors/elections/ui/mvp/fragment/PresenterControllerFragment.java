package com.itdoors.elections.ui.mvp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.itdoors.elections.ui.mvp.base.HasPresenter;
import com.itdoors.elections.ui.mvp.presenter.Presenter;
import com.itdoors.elections.ui.mvp.base.PresenterControllerDelegate;

import timber.log.Timber;

public abstract class PresenterControllerFragment<COMPONENT extends HasPresenter<PRESENTER>, PRESENTER extends Presenter>
        extends ComponentControllerFragment<COMPONENT> {
    private PresenterControllerDelegate<PRESENTER> presenterDelegate = new PresenterControllerDelegate<>();

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenterDelegate.onCreate(getPresenter(), savedInstanceState);
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenterDelegate.onViewCreated(this, view);
        //presenterDelegate.onCreateView(this, view);
    }

    @Override public void onResume() {
        super.onResume();
        presenterDelegate.onResume();
    }

    @Override public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        presenterDelegate.onSaveInstanceState(outState);
    }

    @Override public void onStart() {
        super.onStart();
        presenterDelegate.onStart();
    }

    @Override public void onStop() {
        super.onStop();
        presenterDelegate.onStop();
    }


    @Override  public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        presenterDelegate.onActivityResult(requestCode, resultCode, data);
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        presenterDelegate.onDestroyView();
    }

    @Override public void onDestroy() {
        super.onDestroy();
        presenterDelegate.onDestroy();
    }

    public PRESENTER getPresenter() {
        return getComponent().getPresenter();
    }


}