package com.itdoors.elections.ui.mvp.presenter;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.itdoors.elections.data.api.model.TimeRange;
import com.itdoors.elections.ui.event.NetworkEstablishedEvent;
import com.itdoors.elections.ui.event.projectcontrol.ControlStatusEvent;
import com.itdoors.elections.ui.mvp.model.MainModel;
import com.itdoors.elections.ui.mvp.scope.MainScope;
import com.itdoors.elections.ui.mvp.view.MainView;
import com.itdoors.elections.util.Device;
import com.itdoors.elections.util.SchedulerProvider;
import com.itdoors.elections.util.TimeUtils;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.text.ParseException;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Observable;
import rx.subscriptions.CompositeSubscription;

import static com.itdoors.elections.util.MVPUtils.onError;

@MainScope
public class MainPresenter extends LocationPresenter<MainView> {

    private static final int TIME_REQUEST_TIMEOUT_IN_MINUTES = 5;

    private final MainModel mainModel;
    private final SchedulerProvider schedulerProvider;
    private final ReactiveLocationProvider locationProvider;
    private final Device device;

    private CompositeSubscription subscription = new CompositeSubscription();

    private PresenterCallback presenterCallback;

    private final Bus bus;

    private Handler handler = new Handler();

    @Inject
    public MainPresenter(MainModel mainModel, SchedulerProvider schedulerProvider, ReactiveLocationProvider locationProvider, Device device, Bus bus) {
        this.mainModel = mainModel;
        this.schedulerProvider = schedulerProvider;
        this.locationProvider = locationProvider;
        this.device = device;
        this.bus = bus;
    }

    public void isElectionDay() {

            if (getView() != null)
                getView().blockActionViewUntilGetGPSLocation();

            final Observable<Long> timeObservable = getLocation()
                .first()
                .timeout(TIME_REQUEST_TIMEOUT_IN_MINUTES, TimeUnit.MINUTES)
                .map(Location::getTime);

            if(device.isNetworkAvailable()) {
                    subscription.add(
                            timeObservable
                            .flatMap(mainModel::isElectionDayTime)
                            .compose(schedulerProvider.applySchedulers())
                            .subscribe(
                                    isElectionDayTime -> {
                                        if (getView() != null) {
                                            getView().unblockActionViewWhenGPSLocationAvailable();
                                            getView().showIsElectionDay(isElectionDayTime);
                                        }
                                    },
                                    throwable -> {
                                        if (getView() != null) {
                                            getView().unblockActionViewWhenGPSLocationAvailable();
                                            onError(throwable,
                                                    getView()::showFailedTimeRequestMsg,
                                                    getView()::showFailedTimeRequestUnknownMsg);
                                            //getView().showUnableToFigureOutIsElectionTime(throwable);
                                        }
                                    }
                            )
                    );
            }
            else {
                    subscription.add(
                        mainModel.isElectionDaySaved()
                            .doOnNext(saved -> {
                                if (!saved) {
                                    handler.post(() -> {
                                        if (getView() != null) {
                                            getView().unblockActionViewWhenGPSLocationAvailable();
                                            getView().showNoTimeLocallySaved();
                                        }
                                    });
                                }
                            })
                            .filter(saved -> saved)
                            .flatMap(saved -> mainModel.getElectionDay())
                            .flatMap(electionDay ->
                                            timeObservable.flatMap(timeStamp -> {
                                                        TimeRange timeRange = new TimeRange(electionDay.getStart(),
                                                                electionDay.getFinish());
                                                        try {
                                                            Boolean inTimeRange = TimeUtils.inTimeRange(timeStamp, timeRange);
                                                            return Observable.just(inTimeRange);
                                                        } catch (ParseException e) {
                                                            return Observable.error(e);
                                                        }
                                                    }
                                            )
                            )
                            .subscribe(
                                    isElectionDayTime -> {
                                        if (getView() != null) {
                                            getView().unblockActionViewWhenGPSLocationAvailable();
                                            getView().showIsElectionDay(isElectionDayTime);
                                        }
                                    },
                                    throwable -> {
                                        if (getView() != null) {
                                            getView().unblockActionViewWhenGPSLocationAvailable();
                                            onError(throwable,
                                                    getView()::showFailedTimeRequestMsg,
                                                    getView()::showFailedTimeRequestUnknownMsg);
                                        }
                                    }));

            }

    }

    @Subscribe public void onProjectControlDataSendEvent(ControlStatusEvent event) {
        checkDatabaseProjectsState();
    }

    @Subscribe public void onNetworkEstablishedEvent(NetworkEstablishedEvent event) {
        isElectionDay();
    }

    /*public void refreshCodesIfNeed() {

        if (device.isNetworkAvailable()) {
            if (getView() != null) {
                getView().showStartRefreshingCodes();
            }

            subscription.add(
                    mainModel.refreshCodes()
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    aVoid -> {
                                        if (getView() != null)
                                            getView().showCodesRefreshed();
                                    },
                                    throwable -> {
                                        if (getView() != null)
                                            getView().showCodesRefreshFailed(throwable);
                                    }
                            )
            );
        }else{

            subscription.add(
                    mainModel.isCodesAvailable()
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    available -> {
                                        if(getView() != null)
                                            getView().showCodesAvailability(available);
                                    },
                                    throwable -> {
                                        if(getView() != null)
                                            getView().showCodesAvailabilityError(throwable);
                                    }));

        }
    }*/

    public void checkDatabaseProjectsState() {

        Long agCount   = mainModel.agitatorProjectControlCount().toBlocking().first();
        Long elCount   = mainModel.electorProjectControlCount().toBlocking().first();
        Long elEDCount = mainModel.electorControlCount().toBlocking().first();

        if (getView() != null) {
            getView().showControlCountInfo(agCount, elCount, elEDCount);
        }

    }

    @Override
    protected ReactiveLocationProvider getProvider() {
        return locationProvider;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (subscription != null) {
            subscription.unsubscribe();
            subscription = null;
        }
    }


    public void setPresenterCallback(PresenterCallback presenterCallback) {
        if (presenterCallback == null)
            throw new NullPointerException("PresenterCallback shouldn't be null!");
        this.presenterCallback = presenterCallback;

    }


    protected PresenterCallback getPresenterCallback() {
        return this.presenterCallback;
    }

    private void setUpCallback() {
        if (presenterCallback == null) {
            //presenterCallback = new BasePresenterCallback();
            presenterCallback = mainModel.createPresenterCallback();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        setUpCallback();
        presenterCallback.onCreate();
    }


    @Override
    public void bindView(MainView view) {
        super.bindView(view);
    }

    @Override
    public void unbindView() {
        super.unbindView();
        presenterCallback.onUnbindView();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle bundle) {
        super.onSaveInstanceState(bundle);
        presenterCallback.onSaveInstanceState();
    }

    @Override
    public void onStart() {
        super.onStart();
        presenterCallback.onStart();
        bus.register(this);
        checkDatabaseProjectsState();
    }

    @Override
    public void onStop() {
        super.onStop();
        presenterCallback.onStop();
        bus.unregister(this);
    }
}
