package com.itdoors.elections.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.itdoors.elections.App;
import com.itdoors.elections.R;
import com.itdoors.elections.data.model.project.ProjectViewModel;
import com.itdoors.elections.ui.activity.ProjectsActivity;
import com.itdoors.elections.ui.activity.ScanActivity;
import com.itdoors.elections.ui.fragment.dialog.EnableGpsDialogFragment;
import com.itdoors.elections.ui.fragment.dialog.GetCoordinatesDialogFragment;
import com.itdoors.elections.ui.mvp.component.DaggerMainComponent;
import com.itdoors.elections.ui.mvp.component.MainComponent;
import com.itdoors.elections.ui.mvp.presenter.MainPresenter;
import com.itdoors.elections.ui.mvp.view.MainView;
import com.itdoors.elections.ui.service.SendService;
import com.itdoors.elections.util.Device;
import com.squareup.otto.Bus;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import icepick.Icepick;
import icepick.State;
import rx.subjects.PublishSubject;
import timber.log.Timber;

public class MainFragment extends LocationFragment<MainComponent, MainPresenter> implements MainView {

    @Inject MainPresenter presenter;
    @Inject Bus bus;
    @Inject Device device;

    @Bind(R.id.main_control_btn)      Button agitatorsControlBtn;
    @Bind(R.id.main_projects_btn)     Button projectsBtn;
    @Bind(R.id.main_election_day_btn) Button electionDayBtn;

    @Bind(R.id.main_election_day_msg_holder)  View electionDayMsgHolderView;
    @Bind(R.id.main_gps_retry_btn)            Button gpsBtn;
    @Bind(R.id.main_project_control_holder)   View projectControlHolderView;
    @Bind(R.id.main_project_control_send_btn) View sendBtnView;
    @Bind(R.id.main_project_control_status)   TextView dbProjectControlStatusView;

    private final PublishSubject<Boolean> checkingIsElectionDay = PublishSubject.create();

    @State boolean checkingRequestIsActive = false;

    @Override protected MainComponent onCreateNonConfigurationComponent() {
        return DaggerMainComponent.builder().userComponent(App.getUserComponent(getActivity())).build();
    }

    @OnClick(R.id.main_projects_btn) public void onProjectsClicked(Button btn) {
        Intent intent = new Intent(getActivity(), ProjectsActivity.class);
        startActivity(intent);
    }

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getComponent().inject(this);
        Icepick.restoreInstanceState(this, savedInstanceState);
    }

    @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        presenter.isElectionDay();
        //presenter.refreshCodesIfNeed();
        //presenter.checkDatabaseProjectsState();

        electionDayBtn.setEnabled(false);
        agitatorsControlBtn.setEnabled(false);

        checkingIsElectionDay.subscribe(loading -> {
            checkingRequestIsActive = loading;
            if (loading) {
                electionDayBtn.setEnabled(false);
                electionDayMsgHolderView.setVisibility(View.GONE);
                gpsBtn.setEnabled(false);
            }
        });
        checkingIsElectionDay.onNext(checkingRequestIsActive);

        sendBtnView.setOnClickListener(v -> {
            if (device.isNetworkAvailable()) {
                Intent newIntent = new Intent(getActivity(), SendService.class);
                getActivity().startService(newIntent);
            } else {
                Toast.makeText(getActivity().getApplicationContext(), R.string.no_network_msg, Toast.LENGTH_SHORT).show();
            }
        });
        electionDayBtn.setOnClickListener(v -> {

            Intent newIntent = ScanActivity.newElectionDayInstance(getActivity());
            startActivity(newIntent);

        });

    }

    private void toScanActivity(ProjectViewModel viewModel) {
        String projectGuid = viewModel.getGuid();
        String projectName = viewModel.getName();
        Intent intent = ScanActivity.newProjectControlInstance(getActivity(), projectGuid, projectName);
        startActivity(intent);
    }

    @OnClick(R.id.main_gps_retry_btn)
    void onGPSRetry(Button btn) {
        //presenter.isElectionDay();
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

 /*   @Override public void showUnableToFigureOutIsElectionTime(Throwable th) {
        Timber.tag("TIME").e(th, "Unable to figure out is today an election day!");
        //electionDayMsgHolderView.setVisibility(View.VISIBLE);
        //gpsBtn.setEnabled(true);
    }*/

    @Override public void showIsElectionDay(Boolean isElectionDay) {
        electionDayBtn.setEnabled(isElectionDay);
        //electionDayBtn.setEnabled(true);
        if (!isElectionDay) {
            Toast.makeText(getActivity().getApplicationContext(), R.string.main_msg_today_is_not_election_day, Toast.LENGTH_SHORT).show();
        }
        electionDayMsgHolderView.setVisibility(View.GONE);
        gpsBtn.setEnabled(false);
    }

    @Override public void showStartRefreshingCodes() {
        Timber.tag("CODES").d("START refreshing");

    }

    @Override
    public void showCodesRefreshed() {
        Timber.tag("CODES").d("END refreshing");
        Toast.makeText(getActivity().getApplicationContext(), R.string.main_codes_refreshed, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showCodesRefreshFailed(Throwable th) {
        Timber.tag("CODES").e(th, "END refreshing.");
    }


    @Override public void showControlCountInfo(long prAgitator, long prElectors, long edElectors) {

        projectControlHolderView.setVisibility(View.VISIBLE);

        String status = String.format(
                getString(R.string.main_control_status),
                String.valueOf(prAgitator),
                String.valueOf(prElectors),
                String.valueOf(edElectors)
        );

        boolean enableSend = (prAgitator > 0 || prElectors > 0 || edElectors > 0);
        sendBtnView.setEnabled(enableSend);
        dbProjectControlStatusView.setText(status);
    }

    @Override
    public void showProjectControlDBError(Throwable throwable) {
        projectControlHolderView.setVisibility(View.INVISIBLE);
        Timber.tag("STORAGE").e(throwable, "FAIL to show project control status");
    }

    @Override public void showCodesAvailability(boolean available) {
        Timber.tag("CODES").d("Codes available : " + available);
        if(!available)
            Toast.makeText(getActivity().getApplicationContext(), R.string.main_codes_not_available, Toast.LENGTH_SHORT).show();
    }

    @Override public void showCodesAvailabilityError(Throwable throwable) {
        Timber.tag("CODES").e(throwable, "Codes availability check error !");
    }

    @Override public void showFailedTimeRequestMsg(int code) {

        int msg = R.string.unable_to_figure_out_time_error;
        if(code >= 500)
            msg = R.string.unable_to_figure_out_time_server_error;
        Toast.makeText(getActivity().getApplicationContext(), msg, Toast.LENGTH_SHORT).show();

    }

    @Override public void showFailedTimeRequestUnknownMsg() {
        Timber.d("Fail to get time : Unknown");
        Toast.makeText(getActivity().getApplicationContext(), R.string.unable_to_figure_out_time_error, Toast.LENGTH_SHORT).show();
    }

    @Override public void showNoTimeLocallySaved() {
        Timber.d("Fail to get time : Already not set");
        Toast.makeText(getActivity().getApplicationContext(), R.string.unable_to_figure_out_time_no_internet, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showEnableGps() {
        new EnableGpsDialogFragment().show(getActivity().getSupportFragmentManager(), "enable_gps");
    }

    @Override
    public void blockActionViewUntilGetGPSLocation() {
        electionDayBtn.setEnabled(false);
    }

    @Override
    public void unblockActionViewWhenGPSLocationAvailable() {
    }

    @Override
    public void errorToOpenSetting(Throwable throwable) {
        Timber.e("Error opening settings activity.", throwable);
        Toast.makeText(getActivity().getApplicationContext(), R.string.error_to_get_location, Toast.LENGTH_LONG).show();
    }

    @Override
    public void userEnableGPS() {
        Timber.d("User enabled location");
    }

    @Override
    public void userDisabledGPS() {
        // The user was asked to change settings, but chose not to
        Timber.d("User Cancelled enabling location");
        Toast.makeText(getActivity().getApplicationContext(), R.string.election_day_is_not_defined_without_location, Toast.LENGTH_LONG).show();
        blockActionViewUntilGetGPSLocation();
        //showUnableToFigureOutIsElectionTime(null);
    }

    @Override
    public void showGetCoordinatesView() {
        new GetCoordinatesDialogFragment().show(getActivity().getSupportFragmentManager(), "coordinates_tag");
    }

    @Override
    public void hideGetCoordinatesView() {
        DialogFragment fragment = (DialogFragment) getActivity().getSupportFragmentManager().findFragmentByTag("coordinates_tag");
        if (fragment != null) {
            //TODO fail to dismiss
            try {
                fragment.dismiss();
            } catch (Throwable th) {
                Timber.e(th, "Fail to dismiss");
            }
        }
    }
}