package com.itdoors.elections.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.itdoors.elections.App;
import com.itdoors.elections.AppComponent;
import com.itdoors.elections.ui.ActivityContainer;
import com.itdoors.elections.ui.Intents;
import com.itdoors.elections.ui.event.LoginToMainActivityEvent;
import com.itdoors.elections.ui.event.LoginToPreviousActivityEvent;
import com.itdoors.elections.ui.fragment.LogInFragment;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;

import timber.log.Timber;

public class LoginActivity extends BaseActivity {

    public static final String FLAG_NO_FRAGMENT = "noFragment";
    @Inject ActivityContainer appContainer;

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppComponent appComponent = App.getAppComponent(this);
        if (appComponent != null) {
            appComponent.inject(this);
            appContainer.setContainer(this);
            Intent intent = getIntent();
            boolean noFragment = intent.getBooleanExtra(FLAG_NO_FRAGMENT, false);
            if (savedInstanceState == null && !noFragment) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(appContainer.container(), setupFragment())
                        .commit();
            }
        }
    }

    private Fragment setupFragment(){
        return LogInFragment.newInstance(isBlockingMode());
    }

    @Subscribe
    public void toScanActivity(LoginToMainActivityEvent event) {
       /* Timber.d("To home activity");
        Intent intent = new Intent(this, HomeActivity.class);
        //TODO single on top
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();*/

        Intent intent = new Intent(this, MainActivity.class);
        //TODO single on top
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();

    }

    @Subscribe public void toPreviousActivity(LoginToPreviousActivityEvent event){
        finish();
    }

    @Override
    protected boolean isUserLoggedIn() {
        //Stub
        return true;
    }

    public boolean isBlockingMode(){
        return getIntent() != null && getIntent().getBooleanExtra(Intents.Login.BLOCKING, false);
    }

    @Override public void onBackPressed() {
        if(!isBlockingMode())
            super.onBackPressed();
    }
}
