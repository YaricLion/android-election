package com.itdoors.elections.ui.mvp.presenter;

/**
 * Created by v014nd on 08.06.2016.
 */
public class BasePresenterCallback implements PresenterCallback {

    @Override
    public void onCreate() {
    }

    @Override
    public void onDestroy() {
    }

    @Override
    public void onSaveInstanceState() {
    }

    @Override
    public void onStart() {
    }

    @Override
    public void onStop() {
    }

    @Override
    public void onBindView() {
    }

    @Override
    public void onUnbindView() {
    }

}
