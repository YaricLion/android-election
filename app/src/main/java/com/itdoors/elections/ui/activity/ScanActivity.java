package com.itdoors.elections.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.zxing.ResultPoint;
import com.itdoors.elections.App;
import com.itdoors.elections.R;
import com.itdoors.elections.data.api.UserComponent;
import com.itdoors.elections.ui.Intents;
import com.itdoors.elections.util.Device;
import com.itdoors.elections.util.GuidQrParser;
import com.itdoors.elections.util.SchedulerProvider;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CompoundBarcodeView;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;
import rx.subjects.PublishSubject;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

public class ScanActivity extends BaseActivity{

    private static final String PR_GUID = "PR_GUID";
    private static final String PR_NAME = "PR_NAME";

    public static final int ELECTION_DAY_MODE = 0;
    public static final int CONTROL_MODE = 1;
    public static final int PROJECT_CONTROL_MODE = 2;

    private static final String MODE_KEY = "mode";

    @Inject Device device;
    @Inject GuidQrParser guidQrParser;
    @Inject SchedulerProvider schedulerProvider;

    @Bind(R.id.barcode_scanner)  CompoundBarcodeView barcodeView;
    @Bind(R.id.barcodePreview)   ImageView barcodePreview;
    @Bind(R.id.scan_toolbar)     Toolbar mToolbar;

    @BindString(R.string.agitator_found) String agitatorFound;
    @BindString(R.string.elector_found)  String electorFound;

    private PublishSubject<BarcodeResult> barcodeResultPublishSubject = PublishSubject.create();
    private CompositeSubscription onPauseSubscription = new CompositeSubscription();

    private BarcodeCallback callback = new BarcodeCallback() {
        @Override public void barcodeResult(BarcodeResult result) {
            Timber.d(" Camera callback " + result.getText());
            barcodeResultPublishSubject.onNext(result);
        }
        @Override public void possibleResultPoints(List<ResultPoint> resultPoints) {}
    };

    public static Intent newElectionDayInstance(Context context){
        Intent intent = new Intent(context, ScanActivity.class);
        intent.putExtra(MODE_KEY, ELECTION_DAY_MODE);
        return intent;
    }

    public static Intent newProjectControlInstance(Context context, String projectGuid, String projectName) {
        Intent intent = new Intent(context, ScanActivity.class);
        intent.putExtra(MODE_KEY, PROJECT_CONTROL_MODE);
        intent.putExtra(PR_GUID, projectGuid);
        intent.putExtra(PR_NAME, projectName);
        return intent;
    }


    private int getMode() {

        Bundle extras = getIntent().getExtras();
        if (extras != null)
            if (extras.containsKey(MODE_KEY))
                return extras.getInt(MODE_KEY);

        throw new IllegalStateException("Scanning process should be in specific mode!");
    }

    private String getProjectName() {

        if (getMode() == PROJECT_CONTROL_MODE) {
            Bundle extras = getIntent().getExtras();
            if (extras.containsKey(PR_NAME))
                return extras.getString(PR_NAME);
        }

        return null;
    }

    private String getProjectGuid() {
        if (getMode() == PROJECT_CONTROL_MODE) {
            Bundle extras = getIntent().getExtras();
            if (extras.containsKey(PR_GUID))
                return extras.getString(PR_GUID);
        }
        throw new IllegalStateException("Not in a project conrolt mode!");
    }

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UserComponent userComponent = App.getUserComponent(this);
        if (userComponent != null) {
            userComponent.inject(this);
            setContentView(R.layout.continuous_scan);
            ButterKnife.bind(this);

            setSupportActionBar(mToolbar);

            ActionBar actionBar;
            if ((actionBar = getSupportActionBar()) != null) {

                actionBar.setDisplayShowHomeEnabled(true);
                actionBar.setHomeButtonEnabled(true);
                actionBar.setDisplayHomeAsUpEnabled(true);
                setupTitleIfNeed(actionBar);

            }
            barcodeView.decodeContinuous(callback);

        }
    }

    private void setupTitleIfNeed(ActionBar actionBar) {
        int mode = getMode();
        if(mode == PROJECT_CONTROL_MODE) {
            String prName = getProjectName();
            if (prName != null)
                actionBar.setTitle(prName);
        }else if(mode == ELECTION_DAY_MODE){
            actionBar.setTitle(R.string.election_day);
        }
    }

    @Override protected void onResume() {
        super.onResume();
        if (barcodeView != null) {

            barcodeView.resume();

            if (onPauseSubscription != null) {

                Subscription barCodeFindSub = barcodeResultPublishSubject
                        .doOnNext(barcodeResult -> {
                            Timber.d(" Camera callback onNext" + barcodeResult.getText());
                        })
                        .filter(barcodeResult -> barcodeResult != null)
                        .filter(barcodeResult -> GuidQrParser.matches(barcodeResult.getText()))
                        .throttleFirst(5, TimeUnit.SECONDS)
                        .subscribe(barcodeResult -> {

                            String qrText = barcodeResult.getText();
                            Bitmap qrBitmap = barcodeResult.getBitmapWithResultPoints(Color.YELLOW);

                            GuidQrParser.ParseResult result = guidQrParser.parse(qrText);

                            barcodeView.setStatusText(getStatusMsg(result.getType()));
                            barcodePreview.setImageBitmap(qrBitmap);
                            barcodeView.pause();

                            int mode = getMode();

                            if (mode == PROJECT_CONTROL_MODE) {

                                String projectGuid = getProjectGuid();
                                if (result.getType() == GuidQrParser.AGITATOR_TYPE) {
                                    toAgitatorProjectControlActivity(projectGuid, result.getGuid());
                                } else if (result.getType() == GuidQrParser.ELECTOR_TYPE) {
                                    toElectorProjectControlActivity(projectGuid, result.getGuid());
                                }

                            } else if (mode == CONTROL_MODE) {

                                if (result.getType() == GuidQrParser.AGITATOR_TYPE) {
                                    toAgitatorActivity(result.getGuid());
                                } else if (result.getType() == GuidQrParser.ELECTOR_TYPE) {
                                    toElectorActivity(result.getGuid());
                                }

                            } else if(mode == ELECTION_DAY_MODE) {

                                if (result.getType() == GuidQrParser.AGITATOR_TYPE) {
                                    //toAgitatorActivity(result.getGuid());
                                    Toast.makeText(getApplicationContext(), R.string.only_elector_in_election_day, Toast.LENGTH_SHORT).show();
                                } else if (result.getType() == GuidQrParser.ELECTOR_TYPE) {
                                    toElectorActivity(result.getGuid());
                                }
                            }

                        });

                onPauseSubscription.add(barCodeFindSub);

            }
        }
    }

    @Override protected void onPause() {
        super.onPause();
        if (barcodeView != null) {
            barcodeView.pause();

            if (onPauseSubscription != null) {
                onPauseSubscription.unsubscribe();
                onPauseSubscription = new CompositeSubscription();
            }
        }
    }

    private String getStatusMsg(int type){

        String msg = null;

        if(type == GuidQrParser.AGITATOR_TYPE){
            msg = agitatorFound;
        }
        else if(type == GuidQrParser.ELECTOR_TYPE){
            msg = electorFound;
        }

        return msg;
    }

    @OnClick(R.id.scan_pause) public void pause(View view) {
        if (barcodeView != null)
            barcodeView.pause();
    }

    @OnClick(R.id.scan_resume) public void resume(View view) {
        if (barcodeView != null)
            barcodeView.resume();
    }

    @Override public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (barcodeView != null)
            return barcodeView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
        return super.onKeyDown(keyCode, event);
    }

    private void toAgitatorActivity(String guid){
        Intent intent = new Intent(this, AgitatorControlActivity.class);
        intent.putExtra(Intents.Agitator.GUID, guid);
        startActivity(intent);
    }

    private void toElectorActivity(String guid){
        Intent intent = new Intent(this, ElectorControlActivity.class);
        intent.putExtra(Intents.Elector.GUID, guid);
        startActivity(intent);
    }

    private void toAgitatorProjectControlActivity(String projectGuid, String agitatorGuid) {
        Intent intent = new Intent(this, AgitatorProjectControlActivity.class);
        intent.putExtra(Intents.Agitator.GUID, agitatorGuid);
        intent.putExtra(Intents.Project.GUID, projectGuid);
        startActivity(intent);
    }

    private void toElectorProjectControlActivity(String projectGuid, String electorGuid) {
        Intent intent = new Intent(this, ElectorProjectControlActivity.class);
        intent.putExtra(Intents.Elector.GUID, electorGuid);
        intent.putExtra(Intents.Project.GUID, projectGuid);
        startActivity(intent);
    }

}
