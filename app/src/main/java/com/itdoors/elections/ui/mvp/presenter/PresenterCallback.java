package com.itdoors.elections.ui.mvp.presenter;

public interface PresenterCallback {

    void onCreate();

    void onDestroy();

    void onSaveInstanceState();

    void onStart();

    void onStop();

    void onBindView();

    void onUnbindView();

}