package com.itdoors.elections.ui.activity;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.view.LayoutInflaterFactory;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.itdoors.elections.App;
import com.itdoors.elections.AppComponent;
import com.itdoors.elections.R;
import com.itdoors.elections.ui.Intents;
import com.itdoors.elections.ui.mvp.model.prefs.TokenRxPrefs;
import com.itdoors.elections.ui.mvp.model.prefs.UserRxPrefs;
import com.nispok.snackbar.Snackbar;
import com.squareup.otto.Bus;

import timber.log.Timber;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public abstract class BaseActivity extends AppCompatActivity {

    protected Bus bus;
    protected TokenRxPrefs tokenHelper;
    protected UserRxPrefs userHelper;
    protected Application app;

    private LayoutInflaterFactory layoutInflaterHook;

    @Override protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppComponent component = App.getAppComponent(this);
        app = component.app();
        bus         = component.bus();
        tokenHelper = component.tokenHelper();
        userHelper  = component.userHelper();

        if (!isUserLoggedIn()) {
            login();
            finish();
        }

    }

    @Override protected void onStart() {
        super.onStart();
        bus.register(this);
    }

    @Override protected void onStop() {
        super.onStop();
        bus.unregister(this);
    }

    @Override public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {

        View view = null;
        if (layoutInflaterHook != null) {
            view = layoutInflaterHook.onCreateView(parent, name, context, attrs);
        }
        return view != null ? view : super.onCreateView(parent, name, context, attrs);

    }

    public void setLayoutInflaterHook(LayoutInflaterFactory layoutInflaterHook) {
        this.layoutInflaterHook = layoutInflaterHook;
    }

    protected boolean isUserLoggedIn() {

        boolean hasToken = tokenHelper.isSet().toBlocking().first();
        boolean hasUser = userHelper.isSet().toBlocking().first();

        if (!hasToken || !hasUser) {
            Timber.d("Need to login");
            return false;
        }

        return true;

    }

    protected void login() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    protected void logout() {

        cleanUserSessionComponent();

        Boolean deleteToken = tokenHelper.delete().toBlocking().first();
        Boolean deleteUser = userHelper.delete().toBlocking().first();

        Timber.d("Token and user info is deleted");
        login();

    }

    protected void cleanUserSessionComponent() {
        ((App) app).releaseUserComponent();
    }

    protected void onNoNetwork(){

        Snackbar.with(getApplicationContext())
                .text(R.string.no_network_msg)
                .actionLabel(getResources().getString(R.string.snackbar_settings_action)) // action button label
                .actionColor(getResources().getColor(R.color.theme_accent))
                .actionListener(snackbar -> {
                    Intent intent = new Intent(Settings.ACTION_SETTINGS);
                    startActivity(intent);
                })
                .show(this);
    }


    protected void onNeedToLogin(){

        Toast.makeText(getApplicationContext(), R.string.need_to_login, Toast.LENGTH_LONG).show();
        Timber.d("Need to login");

        Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        loginIntent.putExtra(Intents.Login.BLOCKING, true);
        startActivity(loginIntent);

    }

    protected void onNeedToLoginAgain(){

        Toast.makeText(getApplicationContext(), R.string.need_to_login_again, Toast.LENGTH_LONG).show();
        Timber.d("Need to login again");

        Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        loginIntent.putExtra(Intents.Login.BLOCKING, true);
        startActivity(loginIntent);

    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}