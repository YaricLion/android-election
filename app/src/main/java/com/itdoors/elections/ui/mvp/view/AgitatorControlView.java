package com.itdoors.elections.ui.mvp.view;

import com.itdoors.elections.data.model.Agitator;
import com.itdoors.elections.data.model.agitatorcontrol.AgitatorControlResponseOld;
import com.itdoors.elections.data.api.model.Code;

import java.util.List;

/**
 * Created by v014nd on 12.09.2015.
 */
public interface AgitatorControlView extends ControlView<Agitator, AgitatorControlResponseOld> {
    void showCodes(List<Code> codes);
    void showUnsupportedAgitatorType(Agitator agitator);
}
