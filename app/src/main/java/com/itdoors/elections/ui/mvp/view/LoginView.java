package com.itdoors.elections.ui.mvp.view;

import com.itdoors.elections.ui.mvp.view.LoadingView;

/**
 * Created by v014nd on 09.09.2015.
 */
public interface LoginView extends LoadingView{

     void navigateToMainActivity();

     void showFailedTokenRequestMsg(int code);
     void showFailedTokenRequestUnknownMsg();

     void showFailedUserRequestMsg(int code);
     void showFailedUserRequestUnknownMsg();

     void navigateBackToCurrentActivity();

}
