package com.itdoors.elections.ui.mvp.model;

import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.itdoors.elections.data.db.DataService;
import com.itdoors.elections.data.model.ControlStorageViewModel;
import com.itdoors.elections.data.model.agitatorcontrol.AgitatorControlEntity;
import com.itdoors.elections.data.model.agitatorcontrol.AgitatorControlViewModel;
import com.itdoors.elections.data.model.electorcontrol.ElectorControlEntity;
import com.itdoors.elections.data.model.electorcontrol.ElectorControlViewModel;
import com.itdoors.elections.ui.mvp.scope.StorageControlScope;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;

@StorageControlScope
public class StorageControlModel extends StorageModel {

    @Inject
    public StorageControlModel(DataService dataService) {
        super(dataService);
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    public Observable<List<ControlStorageViewModel>> loadStorageControlInfo() {
        if (!isDataServiceAvailable())
            return Observable.error(new IOException("Data storage is Unavailable!"));
        //Realm not working properly with rx Observables, so this time Guava libraries Utils will be good enough to use
        Observable<List<ControlStorageViewModel>> storageInfo =
                Observable.zip(
                        getDataService().getAgitatorControlInfo().map(StorageControlModel::copyAgitators),
                        getDataService().getElectorControlInfo().map(StorageControlModel::copyElectors),
                        (agitatorVMs, electorVMs) -> ImmutableList.copyOf(Iterables.concat(agitatorVMs, electorVMs))
                );
        return storageInfo;
    }

    private static List<ControlStorageViewModel> copyElectors(List<ElectorControlEntity> entities) {
        List<ControlStorageViewModel> viewModels = new ArrayList<>(entities.size());
        for (ElectorControlEntity controlEntity : entities) {
            ElectorControlViewModel entity = new ElectorControlViewModel.Builder()
                    .uid(controlEntity.getUid())
                    .timestamp(controlEntity.getTimestamp())
                    .build();
            viewModels.add(entity);
        }
        return viewModels;
    }

    private static List<ControlStorageViewModel> copyAgitators(List<AgitatorControlEntity> entities) {
        List<ControlStorageViewModel> viewModels = new ArrayList<>(entities.size());
        for (AgitatorControlEntity entity : entities) {
            AgitatorControlViewModel viewModel = new AgitatorControlViewModel.Builder()
                    .uid(entity.getUid())
                    .codeValues(entity.getCodeValues())
                    .comment(entity.getComment())
                    .timestamp(entity.getTimestamp())
                    .build();
            viewModels.add(viewModel);
        }
        return viewModels;
    }

}
