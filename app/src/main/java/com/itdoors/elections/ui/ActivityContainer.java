package com.itdoors.elections.ui;

import android.app.Activity;
import android.view.ViewGroup;


import com.itdoors.elections.R;

import static butterknife.ButterKnife.findById;

/** An indirection which allows controlling the root container used for each activity or fragment. */

public interface ActivityContainer {

  void setContainer(Activity activity);
  ViewGroup viewGroup(Activity activity);
  int container();
  int layout();

  ActivityContainer DEFAULT = new ActivityContainer() {

      @Override  public void setContainer(Activity activity) {
        activity.setContentView(layout());
      }

      @Override public ViewGroup viewGroup(Activity activity) {
        return findById(activity, container());
      }

      @Override public int container() {
        return R.id.act_container;
      }

      @Override public int layout() {
        return R.layout.activity_def_container_layout;
      }

  };



}
