package com.itdoors.elections.ui.view.projects;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.itdoors.elections.R;
import com.itdoors.elections.data.model.project.ProjectViewModel;
import com.itdoors.elections.ui.event.projects.ProjectClickedEvent;
import com.itdoors.elections.ui.view.ContentListener;
import com.itdoors.elections.ui.view.recycler.BindableViewHolder;
import com.itdoors.elections.ui.view.recycler.item.ProjectItemView;
import com.squareup.otto.Bus;

import java.util.Collections;
import java.util.List;

import timber.log.Timber;

public final class ProjectsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ContentListener<List<ProjectViewModel>> {

    private final Bus bus;

    private List<ProjectViewModel> models = Collections.emptyList();

    public ProjectsAdapter(Bus bus) {
        this.bus = bus;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ProjectItemView view = (ProjectItemView) LayoutInflater.from(parent.getContext()).inflate(R.layout.view_project_item, parent, false);
        return new ProjectControlViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ProjectViewModel viewModel = models.get(position);

        ProjectControlViewHolder projectViewHolder = ((ProjectControlViewHolder) holder);
        projectViewHolder.bind(viewModel);

        ProjectItemView view = (ProjectItemView) projectViewHolder.getView();
        view.setOnProjectClickedListener(vm -> {
            Timber.tag("PROJECT").d("on project clicked!");
            bus.post(new ProjectClickedEvent(vm));
        });

    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    @Override
    public void onLoaded(List<ProjectViewModel> content) {
        if (content == null)
            models = Collections.emptyList();
        models = content;
    }

    @Override
    public void onError(Throwable error) {
    }

    public static final class ProjectControlViewHolder extends BindableViewHolder<ProjectViewModel> {
        public ProjectControlViewHolder(ProjectItemView view) {
            super(view);
        }
    }

}
