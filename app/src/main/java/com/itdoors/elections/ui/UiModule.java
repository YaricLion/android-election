package com.itdoors.elections.ui;

import android.app.Application;

import com.itdoors.elections.PerApp;
import com.itdoors.elections.util.Device;
import com.itdoors.elections.util.MainThreadBus;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@Module
public final class UiModule {

    @Provides @PerApp  ActivityHierarchyServer provideActivityHierarchyServer() {
        return ActivityHierarchyServer.NONE;
    }

    @Provides @PerApp @Named("io") Scheduler provideSubscribeOnScheduler() {
        return Schedulers.io();
    }

    @Provides @PerApp @Named("main") Scheduler provideObserveOnScheduler() {
        return AndroidSchedulers.mainThread();
    }

    @Provides @PerApp Bus provideEventBus(){

        return new MainThreadBus(ThreadEnforcer.ANY);

    }


    @Provides @PerApp Device provideDeviceInfo(Application app){
        return new Device(app);
    }


    @Provides @PerApp @Named("linkValidation") String provideLinkRegEx(){
        return "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
    }

    @Provides @PerApp @Named("emailValidation") String provideEmailRegEx(){
        return "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    }

    @Provides @PerApp @Named("phoneValidation") String providePhoneRegEx(){
        return "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    }


    @Provides @PerApp ActivityContainer provideAppContainer(){
        return ActivityContainer.DEFAULT;
    }

    @Provides @PerApp FragmentContainer provideFragmentContainer(){
        return FragmentContainer.DEFAULT;
    }
}
