package com.itdoors.elections.ui.mvp.component;

import com.itdoors.elections.AppComponent;
import com.itdoors.elections.data.api.UserComponent;
import com.itdoors.elections.ui.fragment.AgitatorControlFragment;
import com.itdoors.elections.ui.mvp.base.HasPresenter;
import com.itdoors.elections.ui.mvp.presenter.AgitatorControlPresenter;
import com.itdoors.elections.ui.mvp.scope.AgitatorControlScope;

import dagger.Component;

@Component(
        dependencies = UserComponent.class
)
@AgitatorControlScope
public interface AgitatorControlComponent extends HasPresenter<AgitatorControlPresenter> {
    void inject(AgitatorControlFragment fragment);
}
