package com.itdoors.elections.ui.fragment.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.itdoors.elections.App;
import com.itdoors.elections.R;
import com.itdoors.elections.ui.event.RejectProjectCanceledEvent;
import com.itdoors.elections.ui.event.RejectProjectControlConfirmedEvent;
import com.squareup.otto.Bus;

import javax.inject.Inject;

/**
 * Created by yariclion on 28.06.16.
 */
public final class AreYouSureDialog extends DialogFragment {

    @Inject
    Bus eventBus;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        App.getUserComponent(getActivity()).inject(this);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.agitator_reject_dialog_title)
                .content(R.string.agitator_reject_dialog_msg)
                .positiveText(R.string.agitator_reject_dialog_ok)
                .negativeText(R.string.agitator_reject_dialog_cancel)
                .build();

        View positiveAction = dialog.getActionButton(DialogAction.POSITIVE);
        View negativeAction = dialog.getActionButton(DialogAction.NEGATIVE);

        positiveAction.setOnClickListener(view -> {
                    eventBus.post(new RejectProjectControlConfirmedEvent());
                    dialog.dismiss();
                }
        );
        negativeAction.setOnClickListener(view -> {
                    eventBus.post(new RejectProjectCanceledEvent());
                    dialog.dismiss();
                }
        );

        return dialog;

    }
}
