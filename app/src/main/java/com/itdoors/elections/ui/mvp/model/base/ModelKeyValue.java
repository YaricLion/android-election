package com.itdoors.elections.ui.mvp.model.base;

import rx.Observer;
import rx.Subscription;

/**
 * Created by v014nd on 09.09.2015.
 */
public interface ModelKeyValue<K,V> {
    public Subscription loadGallery(final K key, Observer<V> observer);
}

