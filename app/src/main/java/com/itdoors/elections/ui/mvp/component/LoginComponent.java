package com.itdoors.elections.ui.mvp.component;

import com.itdoors.elections.data.api.UserComponent;
import com.itdoors.elections.ui.fragment.LogInFragment;
import com.itdoors.elections.ui.mvp.base.HasPresenter;
import com.itdoors.elections.ui.mvp.presenter.LoginPresenter;
import com.itdoors.elections.ui.mvp.scope.LoginScope;

import javax.inject.Named;

import dagger.Component;


@Component(
        dependencies = UserComponent.class
)
@LoginScope
public interface LoginComponent extends HasPresenter<LoginPresenter> {
    void inject(LogInFragment fragment);

    @Named("UserApiUrl")
    String apiUrl();

    @Named("UserApiEmail")
    String email();
}
