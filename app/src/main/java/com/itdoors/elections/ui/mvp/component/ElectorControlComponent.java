package com.itdoors.elections.ui.mvp.component;

import com.itdoors.elections.AppComponent;
import com.itdoors.elections.data.api.UserComponent;
import com.itdoors.elections.ui.fragment.ElectorControlFragment;
import com.itdoors.elections.ui.mvp.base.HasPresenter;
import com.itdoors.elections.ui.mvp.presenter.ElectorControlPresenter;
import com.itdoors.elections.ui.mvp.scope.ElectorControlScope;

import dagger.Component;

@Component(
        dependencies = UserComponent.class
)
@ElectorControlScope
public interface ElectorControlComponent extends HasPresenter<ElectorControlPresenter> {
    void inject(ElectorControlFragment fragment);
}
