package com.itdoors.elections.ui.mvp.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.itdoors.elections.data.model.ControlStorageViewModel;
import com.itdoors.elections.util.AsyncSubjectWrapper;
import com.itdoors.elections.ui.mvp.model.StorageControlModel;
import com.itdoors.elections.ui.mvp.scope.StorageControlScope;
import com.itdoors.elections.ui.mvp.view.StorageControlView;
import com.itdoors.elections.util.SchedulerProvider;

import java.util.List;

import javax.inject.Inject;

import rx.Subscription;
import timber.log.Timber;

@StorageControlScope
public class StorageControlPresenter extends StoragePresenter<StorageControlModel, StorageControlView<List<ControlStorageViewModel>>> {

    private final SchedulerProvider schedulerProvider;

    private Subscription subscription;

    @Inject
    public StorageControlPresenter(StorageControlModel storageControlModel, SchedulerProvider schedulerProvider) {
        super(storageControlModel);
        this.schedulerProvider = schedulerProvider;
    }

    public void loadStorageControlInfo() {

        if (getView() != null)
            getView().showLoading();

        if (subscription != null)
            subscription.unsubscribe();

        subscription = getStorageModel().loadStorageControlInfo()
                .doOnNext(entities -> Timber.tag("STORAGE").d("Presenter . View Models : " + entities.toString()))
                .subscribe(
                        controlStorageViewModels -> {
                            Timber.tag("STORAGE").d("controlStorage : " + controlStorageViewModels.toString());
                            try {
                                if (getView() != null) {
                                    getView().hideLoading();
                                    if (controlStorageViewModels != null && !controlStorageViewModels.isEmpty()) {
                                        getView().showInfo(controlStorageViewModels);
                                    } else {
                                        getView().showEmpty();
                                    }
                                }
                            } catch (Throwable throwable) {
                                Timber.tag("STORAGE").e("Fail to show storage control info! ERROR when displaying view models.");
                                throw throwable;
                            }
                        },
                        throwable -> {
                            if (getView() != null)
                                getView().showError(throwable);
                            Timber.tag("STORAGE").e("Fail to show storage control info! ERROR when try to get view models.");
                        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (subscription != null) {
            subscription.unsubscribe();
            subscription = null;
        }
        getPresenterCallback().onDestroy();
    }

}
