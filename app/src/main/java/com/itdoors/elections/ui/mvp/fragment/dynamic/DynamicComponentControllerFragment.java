package com.itdoors.elections.ui.mvp.fragment.dynamic;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.itdoors.elections.ui.mvp.base.ComponentCache;
import com.itdoors.elections.ui.mvp.base.ComponentControllerDelegate;

public abstract class DynamicComponentControllerFragment<COMPONENT> extends Fragment {

    private final String IS_COMPONENT_ALREADY_CREATED = "isComponentCreated";

    private ComponentControllerDelegate<COMPONENT> componentDelegate = new ComponentControllerDelegate<>();

    private boolean isComponentCreated = false;
    private Bundle fragmentSavedStateInstance = null;

    protected void createComponent(COMPONENT component) {

        ComponentCache componentCache = (ComponentCache) getActivity().getApplication();
        componentDelegate.onCreate(componentCache, fragmentSavedStateInstance, () -> component);
        isComponentCreated = true;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentSavedStateInstance = savedInstanceState;
        if (savedInstanceState != null) {
            isComponentCreated = savedInstanceState.getBoolean(IS_COMPONENT_ALREADY_CREATED);
            if (isComponentCreated) {
                ComponentCache componentCache = (ComponentCache) getActivity().getApplication();
                componentDelegate.onCreate(componentCache, savedInstanceState, null);
                if (getComponent() == null) {
                    isComponentCreated = false;
                    fragmentSavedStateInstance = null;
                }
            }
        }
    }

    protected void releaseComponent() {

        ComponentCache componentCache = (ComponentCache) getActivity().getApplication();
        componentCache.removeComponent(componentDelegate.getComponentId());

        componentDelegate = new ComponentControllerDelegate<>();
        fragmentSavedStateInstance = null;
        isComponentCreated = false;

    }

    @Override
    public void onResume() {
        super.onResume();
        if (isComponentCreated) {
            componentDelegate.onResume();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(IS_COMPONENT_ALREADY_CREATED, isComponentCreated);
        if (isComponentCreated) {
            componentDelegate.onSaveInstanceState(outState);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (isComponentCreated) {
            componentDelegate.onDestroy();
        }
    }

    public COMPONENT getComponent() {
        return componentDelegate.getComponent();
    }

}
