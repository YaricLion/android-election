package com.itdoors.elections.ui.mvp.view;

/**
 * Created by v014nd on 10.09.2015.
 */
public interface HomeView extends View {
    public void navigateToScanView();
}
