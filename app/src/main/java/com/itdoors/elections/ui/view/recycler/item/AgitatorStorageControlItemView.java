package com.itdoors.elections.ui.view.recycler.item;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.common.base.Optional;
import com.itdoors.elections.R;
import com.itdoors.elections.data.model.agitatorcontrol.AgitatorControlViewModel;
import com.itdoors.elections.ui.view.recycler.BindableView;
import com.itdoors.elections.util.TimeUtils;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by v014nd on 14.06.2016.
 */
public class AgitatorStorageControlItemView extends RelativeLayout implements BindableView<AgitatorControlViewModel> {

    @Bind(R.id.agitator_item_card_time)
    TextView timeView;
    @Bind(R.id.agitator_item_card_comment)
    TextView commentView;

    public AgitatorStorageControlItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public View getView() {
        return this;
    }

    @Override
    public void bind(AgitatorControlViewModel viewModel) {
        String comment = Optional.fromNullable(viewModel.getComment()).or("");
        String timeStamp = Integer.toString(viewModel.timestamp());
        String time = Optional.fromNullable(timeStamp).isPresent() ? TimeUtils.format(getContext(), timeStamp) : "";
        timeView.setText(time);
        commentView.setText(comment);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

}
