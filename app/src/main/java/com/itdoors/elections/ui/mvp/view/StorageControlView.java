package com.itdoors.elections.ui.mvp.view;

public interface StorageControlView<MODEL> extends LoadingView {

    void showInfo(MODEL info);

    void showError(Throwable throwable);

    void showEmpty();
}
