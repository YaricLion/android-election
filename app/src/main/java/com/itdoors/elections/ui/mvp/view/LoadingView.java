package com.itdoors.elections.ui.mvp.view;

/**
 * Created by v014nd on 09.09.2015.
 */
public interface LoadingView extends View {
    void showLoading();
    void hideLoading();
}
