package com.itdoors.elections.ui.view.recycler.item;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.itdoors.elections.R;
import com.itdoors.elections.data.model.electorcontrol.ElectorControlViewModel;
import com.itdoors.elections.ui.view.recycler.BindableView;
import com.itdoors.elections.util.TimeUtils;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ElectorStorageControlItemView extends RelativeLayout implements BindableView<ElectorControlViewModel> {

    @Bind(R.id.elector_item_card_time)
    TextView timeView;

    public ElectorStorageControlItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public View getView() {
        return this;
    }

    @Override
    public void bind(ElectorControlViewModel viewModel) {
        String time = TimeUtils.format(getContext(), Integer.toString(viewModel.timestamp()));
        timeView.setText(time);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

}
