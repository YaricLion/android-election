package com.itdoors.elections.ui.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;

import com.itdoors.elections.App;
import com.itdoors.elections.data.api.UserComponent;
import com.itdoors.elections.data.model.Elector;
import com.itdoors.elections.ui.ActivityContainer;
import com.itdoors.elections.ui.Intents;
import com.itdoors.elections.ui.event.NoNetworkEvent;
import com.itdoors.elections.ui.event.login.NeedToLoginAgainEvent;
import com.itdoors.elections.ui.event.login.NeedToLoginEvent;
import com.itdoors.elections.ui.fragment.AgitatorProjectControlFragment;
import com.itdoors.elections.ui.fragment.ElectorControlFragment;
import com.itdoors.elections.ui.fragment.ElectorProjectControlFragment;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;

import timber.log.Timber;

public class ElectorProjectControlActivity extends ControlActivity {

    @Inject
    ActivityContainer appContainer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UserComponent userComponent = App.getUserComponent(this);
        if (userComponent != null) {
            userComponent.inject(this);
            appContainer.setContainer(this);
            if (savedInstanceState == null) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(appContainer.container(), setupFragment())
                        .commit();
            }

            ActionBar actionBar;
            if ((actionBar = getSupportActionBar()) != null) {
                actionBar.setDisplayShowHomeEnabled(true);
                actionBar.setHomeButtonEnabled(true);
                actionBar.setDisplayHomeAsUpEnabled(true);
            }
        }
    }

    private Fragment setupFragment() {

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {

            String elGuid = bundle.getString(Intents.Elector.GUID);
            String prGuid = bundle.getString(Intents.Project.GUID);

            if (elGuid != null && prGuid != null) {
                return ElectorProjectControlFragment.newInstance(elGuid, prGuid);
            }
        }

        throw new IllegalStateException("No agitator or project guid!");

    }


    @Subscribe
    public void onNeedToLoginAgain(NeedToLoginAgainEvent event) {
        onNeedToLoginAgain();
    }

    @Subscribe
    public void onNeedToLogin(NeedToLoginEvent event) {
        onNeedToLogin();
    }

    @Subscribe
    public void onNoNetworkEvent(NoNetworkEvent event) {
        onNoNetwork();
    }
}
