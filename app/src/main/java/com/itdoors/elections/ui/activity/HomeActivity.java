package com.itdoors.elections.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;

import com.itdoors.elections.App;
import com.itdoors.elections.R;
import com.itdoors.elections.data.api.UserComponent;
import com.itdoors.elections.ui.ActivityContainer;
import com.itdoors.elections.ui.event.NoNetworkEvent;
import com.itdoors.elections.ui.event.ToScanEvent;
import com.itdoors.elections.ui.event.login.NeedToLoginAgainEvent;
import com.itdoors.elections.ui.event.login.NeedToLoginEvent;
import com.itdoors.elections.ui.fragment.HomeFragment;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by v014nd on 10.09.2015.
 */
public class HomeActivity extends BaseActivity {

    @Inject  ActivityContainer appContainer;

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UserComponent userComponent = App.getUserComponent(this);
        if (userComponent != null) {
            userComponent.inject(this);
            appContainer.setContainer(this);
            if (savedInstanceState == null) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(appContainer.container(), setupFragment())
                        .commit();
            }
        }

    }

    private Fragment setupFragment(){
        return new HomeFragment();
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.home_action_logout:
                Timber.d("Logout");
                logout();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }



    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Subscribe public void toScanActivity(ToScanEvent event){
        Intent intent = new Intent(this, ScanActivity.class);
        startActivity(intent);
    }

    @Subscribe public void onNeedToLoginAgain(NeedToLoginAgainEvent event){
        onNeedToLoginAgain();
    }
    @Subscribe public void onNeedToLogin(NeedToLoginEvent event){
        onNeedToLogin();
    }
    @Subscribe public void onNoNetworkEvent(NoNetworkEvent event){
        onNoNetwork();
    }

}
