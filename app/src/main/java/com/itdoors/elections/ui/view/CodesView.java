package com.itdoors.elections.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.widget.LinearLayout;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableMap;
import com.itdoors.elections.data.api.model.Code;
import com.itdoors.elections.data.api.model.Value;
import com.itdoors.elections.ui.mvp.model.AgitatorControlModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import timber.log.Timber;

public class CodesView extends LinearLayout {

    public static final int INIT_CAPACITY = 6;

    private BiMap<CodeView,Code> codeBiMap = HashBiMap.create(INIT_CAPACITY);

    public CodesView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(VERTICAL);
    }

    public void onLoaded(List<Code> codes){
        for(Code code : codes) {
            CodeView codeView = new CodeView(getContext());
            codeView.bindTo(code);
            addView(codeView);
            codeBiMap.put(codeView, code);
        }
    }

    public Map<Code, Value> getCodeValueMap(){
        int len = codeBiMap.size();
        HashMap<Code, Value> values = new HashMap<>(len);
        Set<Map.Entry<CodeView, Code>> set = codeBiMap.entrySet();
        for(Map.Entry<CodeView, Code> entry : set){
            CodeView view = entry.getKey();
            Code code = entry.getValue();
            values.put(code, new Value(view.getValue()));
        }
        return ImmutableMap.copyOf(values);

    }

    public void restoreState(Map<Code, Value> map){
        
        Timber.d("State map - restoreState : " + (( map != null ) ? map.toString() : "null" ));
        
        if(map != null && !map.isEmpty()) {
            Set<Code> keySet = map.keySet();
            for (Code code : keySet) {

                CodeView view = codeBiMap.inverse().get(code);
                if(view != null){
                    Value value = map.get(code);
                    int v = value.getValue();
                    if(v == AgitatorControlModel.MINUS_VALUE){
                        Timber.d("minus");
                        view.minus();
                    }
                    else if(v == AgitatorControlModel.PLUS_VALUE){
                        Timber.d("plus");
                        view.plus();
                    }
                    else{
                        Timber.d("nothing");
                    }

                }
            }

        }

    }

    public boolean isAllChecked(){
        Set<CodeView> set = codeBiMap.keySet();
        for(CodeView view : set) {
            if (!view.isChecked())
                return false;
        }
        return true;
    }

}
