package com.itdoors.elections.ui.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;

/**
 * Created by v014nd on 18.09.2015.
 */
public class ControlActivity extends BaseActivity {

    @Override  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

}
