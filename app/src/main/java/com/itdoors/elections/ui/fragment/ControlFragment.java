package com.itdoors.elections.ui.fragment;

import android.content.IntentSender;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.itdoors.elections.R;
import com.itdoors.elections.ui.mvp.base.HasPresenter;
import com.itdoors.elections.ui.mvp.presenter.Presenter;
import com.itdoors.elections.ui.mvp.view.ControlView;

import timber.log.Timber;

public abstract class ControlFragment<COMPONENT extends HasPresenter<PRESENTER>, PRESENTER extends Presenter, MODEL, CONTROL_INFO>
        extends LocationFragment<COMPONENT, PRESENTER>
        implements ControlView<MODEL, CONTROL_INFO> {

    @Override public void errorToOpenSetting(Throwable th) {
        Timber.e("Error opening settings activity.", th);
        Toast.makeText(getActivity().getApplicationContext(), R.string.error_to_get_location, Toast.LENGTH_LONG).show();
    }

    @Override public void userEnableGPS() {
        // All required changes were successfully made
        Timber.d("User enabled location");
    }

    @Override public void userDisabledGPS() {
        // The user was asked to change settings, but chose not to
        Timber.d("User Cancelled enabling location");
        Toast.makeText(getActivity().getApplicationContext(), R.string.data_will_not_send_without_location, Toast.LENGTH_LONG).show();
    }

}
