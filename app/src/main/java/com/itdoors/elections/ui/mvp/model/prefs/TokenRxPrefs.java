package com.itdoors.elections.ui.mvp.model.prefs;


import com.itdoors.elections.data.api.model.oauth.AccessToken;
import com.itdoors.elections.util.Prefs;
import com.itdoors.elections.util.RxPrefs;

public final class TokenRxPrefs extends RxPrefs.RxPrefsImpl<AccessToken> {

    public TokenRxPrefs(Prefs<AccessToken> prefs) {
        super(prefs);
    }
}
