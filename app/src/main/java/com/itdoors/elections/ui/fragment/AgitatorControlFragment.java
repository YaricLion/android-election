package com.itdoors.elections.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.itdoors.elections.App;
import com.itdoors.elections.R;
import com.itdoors.elections.data.model.Agitator;
import com.itdoors.elections.data.model.agitatorcontrol.AgitatorControlResponseOld;
import com.itdoors.elections.data.api.model.AgitatorType;
import com.itdoors.elections.data.api.model.Code;
import com.itdoors.elections.data.api.model.CodeValue;
import com.itdoors.elections.data.api.model.Value;
import com.itdoors.elections.ui.BetterViewAnimator;
import com.itdoors.elections.ui.event.ControlAgitatorOkClickedEvent;
import com.itdoors.elections.ui.event.SettingsEvent;
import com.itdoors.elections.ui.fragment.dialog.ControlAgitatorResponseDialogFragment;
import com.itdoors.elections.ui.fragment.dialog.EnableGpsDialogFragment;
import com.itdoors.elections.ui.fragment.dialog.GetCoordinatesDialogFragment;
import com.itdoors.elections.ui.fragment.dialog.MessageDialogFragment;
import com.itdoors.elections.ui.mvp.component.DaggerAgitatorControlComponent;
import com.itdoors.elections.ui.mvp.presenter.AgitatorControlPresenter;
import com.itdoors.elections.ui.mvp.component.AgitatorControlComponent;
import com.itdoors.elections.ui.mvp.view.AgitatorControlView;
import com.itdoors.elections.ui.transform.CircleStrokeTransformation;
import com.itdoors.elections.ui.view.CodesView;
import com.itdoors.elections.util.ParcelableUtils;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;
import icepick.Icepick;
import icepick.State;
import rx.subjects.PublishSubject;
import timber.log.Timber;

public class AgitatorControlFragment extends ControlFragment<AgitatorControlComponent, AgitatorControlPresenter, Agitator, AgitatorControlResponseOld> implements AgitatorControlView {

    private static final String AG_GUID_TAG = "AG_GUID_TAG";

    @Inject Bus bus;
    @Inject Picasso picasso;
    @Inject AgitatorControlPresenter presenter;

    @Bind(R.id.agitator_animator)      BetterViewAnimator animatorView;
    @Bind(R.id.agitator_control_edit)  MaterialEditText commentEditText;

    @Bind(R.id.agitator_codes_view)                     CodesView codesView;
    @Bind(R.id.agitator_send_btn)                       Button sendControlBtn;

    @Bind(R.id.agitator_name)        TextView nameView;
    @Bind(R.id.agitator_surname)     TextView surnameView;
    @Bind(R.id.agitator_second_name) TextView secondNameView;

    @Bind(R.id.agitator_not_supported_msg) TextView notSupportedAgitatotTypeMsgView;

    @Bind(R.id.agitator_photo)       ImageView photoView;

    @BindString(R.string.error_def_msg) String failedMsg;
    @BindString(R.string.sent)          String sent;
    @BindString(R.string.data_is_sent)  String dataIsSent;

    @BindString(R.string.you_should_check_all_points_title) String checkAllPointsTitle;
    @BindString(R.string.you_should_check_all_points_msg)   String checkAllPointsMsg;

    @BindString(R.string.not_supported_agitator_type_currently_msg)   String notSupportedCurrentlyMsg;
    @BindString(R.string.not_supported_agitator_type_msg)             String notSupportedMsg;
    @BindString(R.string.not_supported_unknown_agitator_type_msg)   String notSupportedUnknownMsg;

    private final PublishSubject<Boolean> controlLoading = PublishSubject.create();

    private CircleStrokeTransformation transformation;

    @State boolean controlRequestActive = false;

    private Map<Code, Value> savedStateMap = null;

    public static AgitatorControlFragment newInstance(String agitatorGuid){

        AgitatorControlFragment fragment = new AgitatorControlFragment();
        if(agitatorGuid == null)
            return fragment;

        Bundle args = new Bundle();
        args.putString(AG_GUID_TAG, agitatorGuid);
        fragment.setArguments(args);

        return fragment;

    }

    @Override public void onStart() {
        super.onStart();
        bus.register(this);
    }

    @Override public void onStop() {
        super.onStop();
        bus.unregister(this);
    }

    @OnClick(R.id.agitator_send_btn)
    public void onSend(Button button){
        if(codesView.isAllChecked())
            onSend();
        else
            MessageDialogFragment.showMessage((AppCompatActivity)getActivity(), checkAllPointsTitle, checkAllPointsMsg );
    }

    @OnClick(R.id.agitator_retry)
    public void onRetry(Button button){
       showLoading();
       retry();
}

    @Override protected AgitatorControlComponent onCreateNonConfigurationComponent() {
        return DaggerAgitatorControlComponent.builder().userComponent(App.getUserComponent(getActivity())).build();
        //.appComponent(App.getAppComponent(getActivity())).build();
    }

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getComponent().inject(this);
        Icepick.restoreInstanceState(this, savedInstanceState);


        Map<Code, Value> restoredMap = ParcelableUtils.readMap(savedInstanceState);
        Timber.d("State map - onCreate : " + ((restoredMap != null) ? restoredMap.toString() : "null"));
        if(restoredMap != null)
            savedStateMap = ImmutableMap.copyOf(restoredMap);
    }

    @Override public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);

        Map<Code, Value> mapToSave = codesView.getCodeValueMap();

        Timber.d("State map - onSaveInstanceState : " + mapToSave.toString());
        ParcelableUtils.writeMap(mapToSave, outState);

    }

    @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                                       @Nullable Bundle savedInstanceState) {
        transformation =  new CircleStrokeTransformation(getActivity(), getResources().getColor(android.R.color.transparent), 0);
        return inflater.inflate(R.layout.fragment_agitator_control, container, false);
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        load();

        controlLoading.subscribe(loading -> {
            controlRequestActive = loading;
            sendControlBtn.setEnabled(!controlRequestActive);
            commentEditText.setEnabled(!controlRequestActive);
        });

        controlLoading.onNext(controlRequestActive);


    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Subscribe public void onSettingsClicked(SettingsEvent event) {
        Timber.d("On settings clicked");
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }

    @Subscribe public void onDataSentOkClicked(ControlAgitatorOkClickedEvent event){
        Timber.d("On ok clicked");
        getActivity().finish();
    }

    private void onSend(){

        final String guid = getAgGuid();
        final List<CodeValue> codeValues = getCodeValues();
        final String comment = getComment();
        if (guid != null && codeValues != null){
            presenter.sendControl(guid, codeValues, comment);
        }

    }

    private void load() {
       String gui = getAgGuid();
       if (gui != null) {
           presenter.loadAgitator(gui);
       }

    }

    private void retry(){
        String gui = getAgGuid();
        if (gui != null) {
            presenter.reloadAgitator(gui);
        }
    }


    private String getAgGuid(){
        return getArguments() != null ? getArguments().getString(AG_GUID_TAG) : null;
    }

    private List<CodeValue> getCodeValues(){

        Map<Code, Value> map = codesView.getCodeValueMap();

        Set<Map.Entry<Code, Value>> set = map.entrySet();
        List<CodeValue> list = new ArrayList<>(set.size());

        for(Map.Entry<Code, Value> entry : set){
            Code code = entry.getKey();
            Value value = entry.getValue();
            list.add(new CodeValue(code.getCode(), value.getValue().toString()));
        }

        return ImmutableList.copyOf(list);

    }

    private String getComment(){
        return commentEditText.getText().toString();
    }

    @Override public void showLoading() {
        animatorView.setDisplayedChildId(R.id.agitator_progress);
    }

    @Override public void hideLoading() {
        animatorView.setDisplayedChildId(R.id.agitator_content);
    }

    @Override public void showRetry() {
        animatorView.setDisplayedChildId(R.id.agitator_error);
    }

    @Override public void showInfo(Agitator agitator) {


        String link = agitator.getPhotoLink();

        photoView.setVisibility(View.VISIBLE);
        picasso.load(link)
                .centerCrop()
                .fit()
                .transform(transformation)
                .placeholder(R.drawable.ic_unknown_person)
                .error(R.drawable.ic_unknown_person)
                .into(photoView);

        Optional<String> name = Optional.fromNullable(agitator.getName());
        Optional<String> surname = Optional.fromNullable(agitator.getSurname());
        Optional<String> secondName = Optional.fromNullable(agitator.getSecondName());

        nameView.setText(name.or("-"));
        surnameView.setText(surname.or("-"));
        secondNameView.setText(secondName.or("-"));

    }

    @Override public void showFailedInfoRequestMsg(int code) {
        if(code == 404){
            animatorView.setDisplayedChildId(R.id.agitator_not_found_error);
        }
        else {
            Toast.makeText(getActivity().getApplicationContext(), failedMsg, Toast.LENGTH_SHORT).show();
        }
    }

    @Override public void showFailedInfoRequestUnknownMsg() {
        Toast.makeText(getActivity().getApplicationContext(), failedMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showControlResponse(AgitatorControlResponseOld controlInfo) {
        Timber.d("Agitator control response : " + controlInfo.toString());
        //TODO fail to show response dialog after onSaveInstanceState
        try {
            new ControlAgitatorResponseDialogFragment().show(getActivity().getSupportFragmentManager(), "control_tag");
        }
        catch (Throwable th){
            Toast.makeText(getActivity().getApplicationContext(), dataIsSent , Toast.LENGTH_SHORT).show();
            getActivity().finish();
        }
    }

    @Override public void showFailedControlRequestMsg(int code) {
        Timber.d("Agitator control response fail : " + code);
        Toast.makeText(getActivity().getApplicationContext(), failedMsg, Toast.LENGTH_SHORT).show();
    }

    @Override public void showFailedControlRequestUnknownMsg() {
        Timber.d("Agitator control response fail");
        Toast.makeText(getActivity().getApplicationContext(), failedMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showControlWillBeSentLater() {

    }

    @Override
    public void showDatabaseError() {

    }

    @Override
    public void showNoInternetConnection() {

    }

    @Override  public void showGetCoordinatesView() {
        new GetCoordinatesDialogFragment().show(getActivity().getSupportFragmentManager(), "coordinates_tag");
    }

    @Override public void hideGetCoordinatesView() {
        DialogFragment fragment = (DialogFragment) getActivity().getSupportFragmentManager().findFragmentByTag("coordinates_tag");
        if(fragment != null){
            //TODO fail to dismiss
            try {
                fragment.dismiss();
            }
            catch (Throwable th){
                Timber.e(th, "Fail to dismiss");
            }
        }
    }

    @Override public void blockActionViewUntilGetGPSLocation() {
        sendControlBtn.setEnabled(false);
        commentEditText.setEnabled(false);
    }

    @Override public void unblockActionViewWhenGPSLocationAvailable() {
        sendControlBtn.setEnabled(true);
        commentEditText.setEnabled(true);
    }

    @Override public void showEnableGps() {
        new EnableGpsDialogFragment().show(getActivity().getSupportFragmentManager(), "enable_gps");
    }

    @Override public void showControlLoading() {
        controlLoading.onNext(true);
    }

    @Override public void hideControlLoading() {
        controlLoading.onNext(false);
    }

    @Override public void showCodes(List<Code> codes) {
        Timber.d("Codes : " + codes.toString());
        codesView.onLoaded(codes);
        if(savedStateMap != null){
            codesView.restoreState(savedStateMap);
        }
    }

    @Override  public void showUnsupportedAgitatorType(Agitator agitator) {

        AgitatorType type = agitator.getType();
        String msg = (type != AgitatorType.UNKNOWN)?
            String.format((type == AgitatorType.JUNIOR ) ? notSupportedCurrentlyMsg : notSupportedMsg ,
                getString(type.getResourceNameId())): notSupportedUnknownMsg;

        notSupportedAgitatotTypeMsgView.setText(msg);
        animatorView.setDisplayedChildId(R.id.agitator_not_supported_error);

    }

}
