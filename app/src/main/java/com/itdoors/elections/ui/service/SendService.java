package com.itdoors.elections.ui.service;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.google.gson.Gson;
import com.itdoors.elections.App;
import com.itdoors.elections.R;
import com.itdoors.elections.data.api.AppService;
import com.itdoors.elections.data.api.UserComponent;
import com.itdoors.elections.data.db.DataService;
import com.itdoors.elections.data.model.agitatorcontrol.AgitatorControlEntity;
import com.itdoors.elections.data.model.agitatorcontrol.AgitatorControlSend;
import com.itdoors.elections.data.model.agitatorprojectscontrol.AgitatorProjectControlSend;
import com.itdoors.elections.data.model.agitatorprojectscontrol.AgitatorProjectControlSendEntity;
import com.itdoors.elections.data.model.electorcontrol.ElectorControlEntity;
import com.itdoors.elections.data.model.electorcontrol.ElectorControlSend;
import com.itdoors.elections.data.model.electorprojectscontrol.ElectorProjectControlSend;
import com.itdoors.elections.data.model.electorprojectscontrol.ElectorProjectControlSendEntity;
import com.itdoors.elections.ui.event.projectcontrol.ControlStatusEvent;
import com.itdoors.elections.util.Device;
import com.itdoors.elections.util.SchedulerProvider;
import com.squareup.otto.Bus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

public class SendService extends Service {

    private final int PROJECT_CONTROL_NOTIFICATION_ID = 100;

    @Inject Device device;
    @Inject DataService dataService;
    @Inject Bus bus;
    @Inject SchedulerProvider sheSchedulerProvider;
    @Inject Gson gson;

    AppService appService;

    private boolean isDataServiceAvailable = false;
    private boolean isUserLoggedIn = false;

    private CompositeSubscription sendControlSub = new CompositeSubscription();

    @Override public void onCreate() {

        isUserLoggedIn = ((App) getApplicationContext()).isUserLoggedIn();

        if (isUserLoggedIn) {

            UserComponent component = App.getUserComponent(getApplicationContext());
            component.inject(this);
            appService = component.appService();

            try {
                getDataService().open();
                setDataServiceAvailable(true);
            } catch (IOException e) {
                Timber.tag("DB").e(e, "Fail to open DataService!");
            }
        }

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (isUserLoggedIn && device.isNetworkAvailable()) {
            Timber.tag("STORAGE").d("onStartCommand");
            startSending(startId);
            return START_STICKY;
        }
        return super.onStartCommand(intent, flags, startId);
    }


    private void startSending(final int startId) {

        if (device.isNetworkAvailable() && isDataServiceAvailable() &&
                sendControlSub != null && !sendControlSub.hasSubscriptions()) {
                sendControlSub.add(
                                Observable.concat(
                                        sendElectorsControl(),
                                        sendElectorsProjectControl(),
                                        sendAgitatorsProjectControl()
                                        )
                                .compose(sheSchedulerProvider.applySchedulers())
                                .subscribe(new rx.Observer<Void>() {
                                    @Override public void onCompleted() {
                                        Timber.tag("STORAGE").d("DATA IS SENT");
                                        Toast.makeText(getApplicationContext(), R.string.data_is_sent, Toast.LENGTH_SHORT).show();
                                        stopSelf(startId);
                                        notifyProjectControlStatus(true);
                                        sendControlSub.clear();
                                    }
                                    @Override public void onError(Throwable e) {
                                        Timber.tag("STORAGE").d("DATA ERROR");
                                        Toast.makeText(getApplicationContext(), R.string.error_def_msg, Toast.LENGTH_SHORT).show();
                                        stopSelf(startId);
                                        notifyProjectControlStatus(false);
                                        sendControlSub.clear();
                                    }
                                    @Override public void onNext(Void aVoid) {}
                                })
                );
        } else {
            stopSelf(startId);
        }
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    private Observable<Void> sendAgitatorsProjectControl() {
          return dataService.getAgitatorProjectControlInfo()
                .filter(control -> !control.isEmpty())
                .map(SendService::agitatorProjectControls)
                .flatMap(appService::controlAgitatorProject)
                .flatMap(response -> dataService.clearAgitatorProjectControlInfo());
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    private Observable<Void> sendElectorsProjectControl() {
        return dataService.getElectorProjectControlInfo()
                .filter(control -> !control.isEmpty())
                .map(SendService::electorProjectControls)
                .flatMap(appService::controlElectorProject)
                .flatMap(response -> dataService.clearElectorProjectControlInfo());
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    private Observable<Void> sendElectorsControl() {
        return dataService.getElectorControlInfo()
                .filter(control -> !control.isEmpty())
                .map(SendService::electorControls)
                .flatMap(appService::controlElectors)
                .flatMap(response -> dataService.clearElectorControlInfo());
    }

    private void notifyProjectControlStatus(boolean success) {

        int msg = success ? R.string.notify_cntrl_data_sent : R.string.notify_cntrl_data_error;
        Context context = getApplicationContext();
        Notification.Builder builder = new Notification.Builder(getApplicationContext());
        builder.setSmallIcon(R.mipmap.ic_launcher)
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(false)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(msg));

        Notification notification = getNotification(builder);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(PROJECT_CONTROL_NOTIFICATION_ID, notification);

        bus.post(new ControlStatusEvent(success));

        Timber.tag("STORAGE").d("notifyProjectControlStatus : " + success);

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private Notification getNotification(Notification.Builder builder) {
        int sdkInt = android.os.Build.VERSION.SDK_INT;
        if (sdkInt < Build.VERSION_CODES.JELLY_BEAN) {
            return builder.getNotification();
        }
        return builder.build();
    }

    @Override
    public void onDestroy() {

        if (sendControlSub != null && !sendControlSub.isUnsubscribed()) {
            sendControlSub.unsubscribe();
            sendControlSub = null;
        }

        if (isUserLoggedIn) {
            try {
                getDataService().close();
            } catch (IOException e) {
                Timber.tag("DB").e(e, "Fail to close DataService!");
            }
        }

    }

    protected void setDataServiceAvailable(boolean is) {
        this.isDataServiceAvailable = is;
    }

    protected boolean isDataServiceAvailable() {
        return isDataServiceAvailable;
    }

    protected DataService getDataService() {
        return this.dataService;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////

    private static AgitatorProjectControlSend agitatorProjectControl(AgitatorProjectControlSendEntity send) {
        return AgitatorProjectControlSend.newBuilder()
                .agitatorGuid(send.getAgitatorGuid())
                .projectGui(send.getProjectGui())
                .scanDateTime(send.getScanDateTime())
                .latitude(send.getLatitude())
                .longitude(send.getLongitude())
                .build();
    }

    private static ElectorProjectControlSend electorProjectControl(ElectorProjectControlSendEntity send) {
        return ElectorProjectControlSend.newBuilder()
                .supporterGuid(send.getSupporterGuid())
                .projectGui(send.getProjectGui())
                .scanDateTime(send.getScanDateTime())
                .latitude(send.getLatitude())
                .longitude(send.getLongitude())
                .build();
    }

    private static List<AgitatorProjectControlSend> agitatorProjectControls(List<AgitatorProjectControlSendEntity> items) {
        List<AgitatorProjectControlSend> sendItems = new ArrayList<>(items.size());
        for (AgitatorProjectControlSendEntity entity : items)
            sendItems.add(agitatorProjectControl(entity));
        return sendItems;
    }

    private static List<ElectorProjectControlSend> electorProjectControls(List<ElectorProjectControlSendEntity> items) {
        List<ElectorProjectControlSend> sendItems = new ArrayList<>(items.size());
        for (ElectorProjectControlSendEntity entity : items)
            sendItems.add(electorProjectControl(entity));
        return sendItems;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////

    private static AgitatorControlSend agitatorControl(AgitatorControlEntity send, Gson gson) {
        return AgitatorControlSend.newBuilder()
                .uid(send.getUid())
                .dateTimestamp(send.getTimestamp())
                .comment(send.getComment())
                .jsonCodeValues(gson.toJson(send.getCodeValues()))
                .latitude(send.getLatitude())
                .longitude(send.getLongitude())
                .build();
    }

    private static ElectorControlSend electorControl(ElectorControlEntity send) {
        return ElectorControlSend.newBuilder()
                .uid(send.getUid())
                .dateTimestamp(send.getTimestamp())
                .latitude(send.getLatitude())
                .longitude(send.getLongitude())
                .build();
    }

    private static List<AgitatorControlSend> agitatorControls(List<AgitatorControlEntity> items, Gson gson) {
        List<AgitatorControlSend> sendItems = new ArrayList<>(items.size());
        for (AgitatorControlEntity entity : items)
            sendItems.add(agitatorControl(entity, gson));
        return sendItems;
    }

    private static List<ElectorControlSend> electorControls(List<ElectorControlEntity> items) {
        List<ElectorControlSend> sendItems = new ArrayList<>(items.size());
        for (ElectorControlEntity entity : items)
            sendItems.add(electorControl(entity));
        return sendItems;
    }
}
