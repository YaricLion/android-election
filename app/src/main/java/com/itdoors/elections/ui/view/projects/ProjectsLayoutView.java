package com.itdoors.elections.ui.view.projects;

import android.content.Context;
import android.content.res.Configuration;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.itdoors.elections.App;
import com.itdoors.elections.R;
import com.itdoors.elections.data.model.project.ProjectViewModel;
import com.itdoors.elections.ui.BetterViewAnimator;
import com.itdoors.elections.ui.event.projects.RefreshProjectsEvent;
import com.itdoors.elections.ui.event.projects.RetryProjectsEvent;
import com.itdoors.elections.ui.mvp.view.ProjectsView;
import com.itdoors.elections.util.Device;
import com.squareup.otto.Bus;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.recyclerview.animators.adapters.SlideInBottomAnimationAdapter;


public class ProjectsLayoutView extends LinearLayout implements ProjectsView<List<ProjectViewModel>> {

    private static final int ANIMATION_DURATION = 500;

    public static final int API = 0;
    public static final int DB = 1;

    @Bind(R.id.projects_animator)
    BetterViewAnimator animatorView;
    @Bind(R.id.projects_list)
    RecyclerView recyclerView;

    @Bind(R.id.projects_db_retry)
    Button retryDatabaseBtn;
    @Bind(R.id.projects_api_retry)
    Button retryApiBtn;

    @Bind(R.id.projects_refresh_layout)
    SwipeRefreshLayout refreshLayout;
    @Bind(R.id.projects_list_msg)
    TextView databaseMsgView;

    @BindString(R.string.project_data_msg)
    String dataFromDatabaseMsg;

    @Inject
    Device device;
    @Inject
    Bus bus;

    private int mode = API;

    private ProjectsAdapter adapter;

    public ProjectsLayoutView(Context context, AttributeSet attrs) {
        super(context, attrs);
        App.getUserComponent(context).inject(this);
        adapter = new ProjectsAdapter(bus);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);

        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), getDisplayColumns(device), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(animate(adapter));

        refreshLayout.setOnRefreshListener(this::refresh);
        databaseMsgView.setText(dataFromDatabaseMsg);


    }

    private void refresh() {
        showLoading();
        bus.post(new RefreshProjectsEvent(mode));
    }

    private void switchMode() {
        mode = (mode == API) ? DB : API;
    }

    private void retry() {
        showLoading();
        bus.post(new RetryProjectsEvent(mode));
    }


    @Nullable
    @OnClick(R.id.projects_db_retry)
    void onDatabaseRetryClicked(Button btn) {
        retry();
    }

    @Nullable
    @OnClick(R.id.projects_api_retry)
    void onApiRetryClicked(Button btn) {
        retry();
    }

    private RecyclerView.Adapter animate(RecyclerView.Adapter adapter) {

        SlideInBottomAnimationAdapter wrappingAdapter = new SlideInBottomAnimationAdapter(adapter);
        wrappingAdapter.setFirstOnly(true);
        wrappingAdapter.setDuration(ANIMATION_DURATION);
        return wrappingAdapter;
    }

    public static int getDisplayColumns(Device device) {

        boolean isTablet = device.isTablet();
        int orientation = device.orientation();
        if (isTablet) {
            if (orientation == Configuration.ORIENTATION_PORTRAIT)
                return 2;
            return 3;
        }
        if (orientation == Configuration.ORIENTATION_PORTRAIT)
            return 1;
        return 2;
    }

    @Override
    public void showLoading() {
        animatorView.setDisplayedChildId(R.id.projects_progress);
    }

    @Override
    public void showDatabaseInfo(List<ProjectViewModel> info, String lastTimeUpdated) {
        animatorView.setDisplayedChildId(R.id.projects_refresh_layout);
        databaseMsgView.setVisibility(View.VISIBLE);
        mode = DB;
        refreshLayout.setRefreshing(false);
        adapter.onLoaded(info);
    }

    @Override
    public void showResponseInfo(List<ProjectViewModel> info) {
        animatorView.setDisplayedChildId(R.id.projects_refresh_layout);
        databaseMsgView.setVisibility(View.GONE);
        mode = API;
        refreshLayout.setRefreshing(false);
        adapter.onLoaded(info);
    }

    @Override
    public void showDatabaseError(Throwable th) {
        animatorView.setDisplayedChildId(R.id.projects_db_error);
        refreshLayout.setRefreshing(false);
        adapter.onError(th);
    }

    @Override
    public void showResponseError(Throwable th) {
        animatorView.setDisplayedChildId(R.id.projects_api_error);
        adapter.onError(th);
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void showDatabaseIsEmpty() {
        animatorView.setDisplayedChildId(R.id.projects_db_empty);
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void showResponseIsEmpty() {
        animatorView.setDisplayedChildId(R.id.projects_api_empty);
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void showNoInternetConnection() {
        Toast.makeText(getContext().getApplicationContext(), R.string.no_network_msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showDbError(Throwable th) {
        Toast.makeText(getContext().getApplicationContext(), R.string.projects_storage_error, Toast.LENGTH_SHORT).show();
    }
}
