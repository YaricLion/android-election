package com.itdoors.elections.ui.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import com.itdoors.elections.App;
import com.itdoors.elections.data.api.UserComponent;
import com.itdoors.elections.ui.ActivityContainer;
import com.itdoors.elections.ui.Intents;
import com.itdoors.elections.ui.event.NoNetworkEvent;
import com.itdoors.elections.ui.event.login.NeedToLoginAgainEvent;
import com.itdoors.elections.ui.event.login.NeedToLoginEvent;
import com.itdoors.elections.ui.fragment.ElectorControlFragment;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;

import timber.log.Timber;

public class ElectorControlActivity extends ControlActivity {

    @Inject ActivityContainer appContainer;

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UserComponent userComponent = App.getUserComponent(this);
        if (userComponent != null) {
            userComponent.inject(this);
            appContainer.setContainer(this);
            if (savedInstanceState == null) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(appContainer.container(), setupFragment())
                        .commit();
            }

            ActionBar actionBar;
            if ((actionBar = getSupportActionBar()) != null) {
                actionBar.setDisplayShowHomeEnabled(true);
                actionBar.setHomeButtonEnabled(true);
                actionBar.setDisplayHomeAsUpEnabled(true);
            }
        }
    }

    private Fragment setupFragment(){

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            String guid = bundle.getString(Intents.Elector.GUID);
            if(guid != null){
                Timber.d("Create Elector Control Fragment with gui:" + guid);
                return ElectorControlFragment.newInstance(guid);
            }
        }

        return new ElectorControlFragment();

    }

    @Subscribe public void onNeedToLoginAgain(NeedToLoginAgainEvent event){
        onNeedToLoginAgain();
    }
    @Subscribe public void onNeedToLogin(NeedToLoginEvent event){
        onNeedToLogin();
    }
    @Subscribe public void onNoNetworkEvent(NoNetworkEvent event){
        onNoNetwork();
    }

}
