package com.itdoors.elections.ui.event.projects;

import com.itdoors.elections.data.model.project.ProjectViewModel;

public final class ProjectClickedEvent {

    private final ProjectViewModel projectViewModel;

    public ProjectClickedEvent(ProjectViewModel projectViewModel) {
        this.projectViewModel = projectViewModel;
    }

    public ProjectViewModel getProjectViewModel() {
        return projectViewModel;
    }

}
