package com.itdoors.elections.ui.event.projects;

public final class RetryProjectsEvent {

    private final int mode;

    public RetryProjectsEvent(int mode) {
        this.mode = mode;
    }

    public int getMode() {
        return mode;
    }
}
