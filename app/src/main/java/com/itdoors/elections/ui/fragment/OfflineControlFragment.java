package com.itdoors.elections.ui.fragment;

import android.widget.Toast;

import com.itdoors.elections.R;
import com.itdoors.elections.ui.mvp.base.HasPresenter;
import com.itdoors.elections.ui.mvp.presenter.Presenter;
import com.itdoors.elections.ui.mvp.view.OfflineControlView;
import com.itdoors.elections.ui.mvp.view.OfflineProjectControlView;

import timber.log.Timber;

public abstract class OfflineControlFragment<COMPONENT extends HasPresenter<PRESENTER>, PRESENTER extends Presenter, VM, CONTROL_INFO>
        extends LocationFragment<COMPONENT, PRESENTER>
        implements OfflineControlView<VM> {

    @Override
    public void errorToOpenSetting(Throwable th) {
        Timber.e("Error opening settings activity.", th);
        Toast.makeText(getActivity().getApplicationContext(), R.string.error_to_get_location, Toast.LENGTH_LONG).show();
    }

    @Override
    public void userEnableGPS() {
        // All required changes were successfully made
        Timber.d("User enabled location");
    }

    @Override
    public void userDisabledGPS() {
        // The user was asked to change settings, but chose not to
        Timber.d("User Cancelled enabling location");
        Toast.makeText(getActivity().getApplicationContext(), R.string.data_will_not_send_without_location, Toast.LENGTH_LONG).show();
    }

}
