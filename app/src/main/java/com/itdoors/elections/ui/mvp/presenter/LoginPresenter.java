package com.itdoors.elections.ui.mvp.presenter;

import android.os.Handler;
import android.os.Looper;
import android.util.Pair;

import com.itdoors.elections.data.api.model.User;
import com.itdoors.elections.data.api.model.oauth.AccessToken;
import com.itdoors.elections.ui.mvp.model.LoginModel;
import com.itdoors.elections.ui.mvp.scope.LoginScope;
import com.itdoors.elections.ui.mvp.view.LoginView;
import com.itdoors.elections.util.CrashlyticsUtils;
import com.itdoors.elections.util.SchedulerProvider;

import static com.itdoors.elections.util.MVPUtils.onError;


import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.functions.Func2;
import timber.log.Timber;

@LoginScope
public class LoginPresenter extends BasePresenter<LoginView> {

    private final LoginModel loginModel;
    private final SchedulerProvider schedulerProvider;

    private final Handler mainHandler = new Handler(Looper.getMainLooper());

    @Inject LoginPresenter(LoginModel loginModel, SchedulerProvider schedulerProvider) {
        this.loginModel = loginModel;
        this.schedulerProvider = schedulerProvider;
    }

    private Subscription subscription;

    public void login(String username, String password, String apiUrl) {

        getView().showLoading();
        loginModel.reset();
        subscription =
                loginModel.getToken(username, password)
                        .doOnError(
                                throwable -> {
                                    Timber.e(throwable, "Fail to get token");
                                    mainHandler.post(() -> {
                                        if (getView() != null) {
                                            getView().hideLoading();
                                            onError(throwable,
                                                    getView()::showFailedTokenRequestMsg,
                                                    getView()::showFailedTokenRequestUnknownMsg);
                                        }
                                    });
                                })
                        .flatMap(
                                token ->
                                Observable.zip(
                                        Observable.just(token),
                                        loginModel.getUser(token),
                                            (Func2<AccessToken, User, Pair<AccessToken, User>>) Pair::new
                                )
                        )
                        .map(pair -> {

                            AccessToken newToken = pair.first;
                            User newUser = pair.second;
                            User currentUser = loginModel.getCurrentUser().toBlocking().first();

                            if(currentUser == null){

                                Timber.d("This is the first login or User is not saved in preferences !");
                                loginModel.saveToken(newToken).toBlocking().first();
                                loginModel.saveUser(newUser).toBlocking().first();
                                loginModel.saveApiUrl(apiUrl).toBlocking().first();

                                mainHandler.post(() -> CrashlyticsUtils.setUserInfo(newUser));

                                return false;
                            }
                            else {

                                boolean isUserTheSame;
                                if (currentUser.getId() == newUser.getId()) {
                                    Timber.d("Got same user : " + newUser.toString() + "!");
                                    isUserTheSame = true;
                                } else {
                                    loginModel.saveUser(newUser).toBlocking().first();
                                    Timber.d("User has been changed : " + newToken.toString() + "!");
                                    isUserTheSame = false;
                                }

                                loginModel.saveToken(newToken).toBlocking().first();
                                loginModel.saveUser(newUser).toBlocking().first();
                                loginModel.saveApiUrl(apiUrl).toBlocking().first();

                                mainHandler.post(() -> CrashlyticsUtils.setUserInfo(newUser));

                                return isUserTheSame;

                            }
                        })
                        .delay(1, TimeUnit.SECONDS)
                        .compose(schedulerProvider.applySchedulers())
                        .subscribe(
                                isUserTheSame ->{
                                      mainHandler.post(() -> {
                                          if (getView() != null) {
                                              getView().hideLoading();
                                              if(isUserTheSame){
                                                  getView().navigateBackToCurrentActivity();
                                              }
                                              else {
                                                  getView().navigateToMainActivity();
                                              }
                                          }
                                      });
                                },
                                throwable -> mainHandler.post(() -> {
                                    if (getView() != null) {
                                        getView().hideLoading();
                                        onError(throwable,
                                                getView()::showFailedUserRequestMsg,
                                                getView()::showFailedUserRequestUnknownMsg);
                                    }
                                }));
    }

    @Override public void onDestroy() {
        super.onDestroy();
        mainHandler.removeCallbacksAndMessages(null);
        if(subscription != null)
            subscription.unsubscribe();
    }


}
