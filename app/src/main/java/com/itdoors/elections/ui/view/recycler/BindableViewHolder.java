package com.itdoors.elections.ui.view.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.itdoors.elections.data.model.ViewModel;

public class BindableViewHolder<VM extends ViewModel> extends RecyclerView.ViewHolder implements Bindable<VM> {

    private final BindableView<VM> bindableView;

    public BindableViewHolder(BindableView<VM> bindableView) {
        super(bindableView.getView());
        this.bindableView = bindableView;
    }

    public View getView() {
        return bindableView.getView();
    }

    @Override
    public void bind(VM vm) {
        bindableView.bind(vm);
    }

}
