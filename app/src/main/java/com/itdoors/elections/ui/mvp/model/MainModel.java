package com.itdoors.elections.ui.mvp.model;

import com.google.common.collect.ImmutableList;
import com.itdoors.elections.data.api.AppService;
import com.itdoors.elections.data.api.model.Code;
import com.itdoors.elections.data.api.model.TimeRange;
import com.itdoors.elections.data.db.DataService;
import com.itdoors.elections.data.model.ElectionDay;
import com.itdoors.elections.util.AsyncSubjectWrapper;
import com.itdoors.elections.ui.mvp.scope.MainScope;
import com.itdoors.elections.util.TimeUtils;

import java.text.ParseException;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

@MainScope
public class MainModel extends StorageModel {

    private final int REFRESH_CODES_CODE = 1;
    private final int TIMES_CODE = 2;

    private final AppService appService;

    private final List<TimeRange> electionDayTimeList = ImmutableList.of(
            TimeRange.from("2016-07-17 08:00:00", "2016-07-18 10:00:00")
    );

    private AsyncSubjectWrapper<List<Code>> asyncSubjectWrapper = new AsyncSubjectWrapper<>();

    private AsyncSubjectWrapper<ElectionDay> electDayAsyncSub = new AsyncSubjectWrapper<>();

    @Inject
    public MainModel(AppService appService, DataService dataService) {
        super(dataService);
        this.appService = appService;
    }
/*
    public Observable<Boolean> isProjectsAvailable() {
        return getDataService().isProjectsAvailable();
    }*/

    public Observable<Boolean> isCodesAvailable(){
        return getDataService().getCodesCount().map(count -> count > 0);
    }

    public Observable<Void> refreshCodes() {
        return asyncSubjectWrapper.createIfNeedAndSubscribe(REFRESH_CODES_CODE, appService.getCodes())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(getDataService()::rewriteCodes);
    }

    public Observable<Boolean> isElectionDayTime(long timeStamp) {
        //Boolean inTime = TimeUtils.inTimeRanges(timeStampUTC, electionDayTimeList);
        //return Observable.just(inTime);
        return electDayAsyncSub.createIfNeedAndSubscribe(TIMES_CODE, appService.getElectionDayTimes())
            .flatMap(
                    electionDay -> getDataService()
                            .rewriteElectionDay(electionDay)
                            .map(__ -> electionDay))
            .flatMap(electionDay -> {
                TimeRange timeRange = new TimeRange(electionDay.getStart(), electionDay.getFinish());
                try {
                    boolean inTimeRange = TimeUtils.inTimeRange(timeStamp, timeRange);
                    return Observable.just(inTimeRange);
                } catch (ParseException e) {
                    return Observable.error(e);
                }
            });

    }

    public Observable<Boolean> isElectionDaySaved(){
        return getDataService().getElectionDayCount().map(count -> count > 0);
    }

    public Observable<Long> electorProjectControlCount() {
        return getDataService().getElectorProjectControlCount();
    }

    public Observable<Long> agitatorProjectControlCount() {
        return getDataService().getAgitatorProjectControlCount();
    }

    public Observable<Long> electorControlCount() {
        return getDataService().getElectorControlCount();
    }

    public Observable<ElectionDay> getElectionDay() {
        return getDataService().getElectionDay();
    }
}
