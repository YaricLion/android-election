package com.itdoors.elections.ui.mvp.model;

import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.itdoors.elections.data.api.AppService;
import com.itdoors.elections.data.db.DataService;
import com.itdoors.elections.data.model.electorprojectscontrol.ElectorProjectControlResponse;
import com.itdoors.elections.data.model.electorprojectscontrol.ElectorProjectControlSend;
import com.itdoors.elections.data.model.electorprojectscontrol.ElectorProjectControlSendEntity;
import com.itdoors.elections.data.model.electorprojectscontrol.ElectorProjectControlViewModel;
import com.itdoors.elections.ui.mvp.scope.ElectorProjectControlScope;
import com.itdoors.elections.util.AsyncSubjectWrapper;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by yariclion on 25.06.16.
 */
@ElectorProjectControlScope
public class ElectorProjectControlModel extends StorageModel {

    private final int SCAN_CODE = 1;
    private final int CHANGE_STATUS = 2;

    private final AppService appService;

    private AsyncSubjectWrapper<ElectorProjectControlViewModel> asyncScan = new AsyncSubjectWrapper<>();
    private AsyncSubjectWrapper<ElectorProjectControlResponse> asyncChangeStatus = new AsyncSubjectWrapper<>();

    @Inject
    public ElectorProjectControlModel(AppService appService, DataService dataService) {
        super(dataService);
        this.appService = appService;
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    public Observable<ElectorProjectControlResponse> changeStatus(String controlGuid, String state) {
        return asyncChangeStatus.createIfNeedAndSubscribe(CHANGE_STATUS, appService.changeElectorStatus(controlGuid, state));
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    public Observable<ElectorProjectControlViewModel> scanElector(ElectorProjectControlSend sendItem) {
        return asyncScan.createIfNeedAndSubscribe(SCAN_CODE,
                appService.controlElectorProject(sendItem)
                        .map(ElectorProjectControlModel::viewModel)
        );
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    public Observable<Void> saveControl(ElectorProjectControlSend sendItem) {
 /*       return Observable.just(sendItem)
                .map(ElectorProjectControlModel::entity)
                .flatMap(getDataService()::addElectorProjectControl);
 */
        return getDataService().addElectorProjectControl(entity(sendItem));
    }

    private static ElectorProjectControlViewModel viewModel(ElectorProjectControlResponse response) {
        return ElectorProjectControlViewModel.newBuilder()
                .controlGuid(response.getGuid())
                .controlState(response.getControlState())
                .createDateTime(response.getCreateDateTime())
                .scanDateTime(response.getScanDateTime())
                .project(response.getProject())
                .elector(response.getElector())
                .build();
    }

    private static ElectorProjectControlSendEntity entity(ElectorProjectControlSend send) {
        return ElectorProjectControlSendEntity.newBuilder()
                .supporterGuid(send.getSupporterGuid())
                .projectGui(send.getProjectGui())
                .scanDateTime(send.getScanDateTime())
                .latitude(send.getLatitude())
                .longitude(send.getLongitude())
                .build();
    }

    public void resetScan() {
        asyncScan = new AsyncSubjectWrapper<>();
    }

    public void resetChangeStatus() {
        asyncChangeStatus = new AsyncSubjectWrapper<>();
    }
}
