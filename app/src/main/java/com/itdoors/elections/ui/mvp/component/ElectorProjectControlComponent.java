package com.itdoors.elections.ui.mvp.component;

import com.itdoors.elections.data.api.UserComponent;
import com.itdoors.elections.ui.fragment.ElectorProjectControlFragment;
import com.itdoors.elections.ui.mvp.base.HasPresenter;
import com.itdoors.elections.ui.mvp.presenter.ElectorProjectControlPresenter;
import com.itdoors.elections.ui.mvp.scope.ElectorProjectControlScope;

import dagger.Component;

@Component(
        dependencies = UserComponent.class
)
@ElectorProjectControlScope
public interface ElectorProjectControlComponent extends HasPresenter<ElectorProjectControlPresenter> {
    void inject(ElectorProjectControlFragment fragment);
}
