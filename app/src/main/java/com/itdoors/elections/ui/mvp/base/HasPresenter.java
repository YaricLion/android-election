package com.itdoors.elections.ui.mvp.base;

import com.itdoors.elections.ui.mvp.presenter.Presenter;

/**
 * Created by v014nd on 09.09.2015.
 */
public interface HasPresenter<P extends Presenter> {
    P getPresenter();
}

