package com.itdoors.elections.ui.mvp.base;

import android.app.Application;
import android.support.multidex.MultiDexApplication;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class ComponentCacheApplication extends MultiDexApplication implements ComponentCache {
    private final AtomicLong nextId = new AtomicLong();
    private Map<Long, Object> components = new HashMap<>();

    @Override public long generateId() {
        return nextId.getAndIncrement();
    }

    @SuppressWarnings("unchecked")
    @Override public <C> C getComponent(long index) {
        return (C) components.get(index);
    }

    @Override public <C> void setComponent(long index, C component) {
        components.put(index, component);
    }

    @Override
    public void removeComponent(long index) {
        components.remove(index);
    }
}