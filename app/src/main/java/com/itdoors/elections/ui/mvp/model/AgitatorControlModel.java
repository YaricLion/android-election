package com.itdoors.elections.ui.mvp.model;

import com.itdoors.elections.data.api.AppService;
import com.itdoors.elections.data.model.Agitator;
import com.itdoors.elections.data.model.agitatorcontrol.AgitatorControlResponseOld;
import com.itdoors.elections.data.api.model.Code;
import com.itdoors.elections.ui.mvp.scope.AgitatorControlScope;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.subjects.AsyncSubject;

import static com.itdoors.elections.util.MVPUtils.code;
import static com.itdoors.elections.util.MVPUtils.INIT_CODE;

@AgitatorControlScope
public class AgitatorControlModel implements Model {

    public final static int PLUS_VALUE  = 1;
    public final static int MINUS_VALUE = 0;

    private final AppService appService;

    private int agitatorRequestCode = INIT_CODE;
    private int controlRequestCode = INIT_CODE;
    private int codesRequestCode = INIT_CODE;

    private AsyncSubject<Agitator> agitatorAsyncSubject;
    private AsyncSubject<AgitatorControlResponseOld> controlAsyncSubject;
    private AsyncSubject<List<Code>> controlCodesSubject;


    @Inject
    public AgitatorControlModel(AppService appService){

        this.appService = appService;
    }

    public Observable<Agitator> getAgitator(String guid){

            int code = code(guid);

            if (agitatorRequestCode == INIT_CODE) {
                agitatorRequestCode = code;
            } else if (agitatorRequestCode != code) {
                // can't use cached data
                agitatorAsyncSubject = null;
                agitatorRequestCode = code;
            }
            if (agitatorAsyncSubject == null) {
                agitatorAsyncSubject = AsyncSubject.create();
                appService.getAgitator(guid).subscribe(agitatorAsyncSubject);
            }
            return agitatorAsyncSubject;
    }

    public void resetAgitator() {

        agitatorAsyncSubject = null;
        agitatorRequestCode = INIT_CODE;

    }

/*

    public Observable<AgitatorControlOld> controlAgitators(String uid, int likeCode, String comment, String dateTimeStamp,  String latitude, String longitude){

        int code = code(uid, likeCode, comment, dateTimeStamp, latitude, longitude);

        if (controlRequestCode == INIT) {
            controlRequestCode = code;
        } else if (controlRequestCode != code) {
            // can't use cached data
            controlAsyncSubject = null;
            controlRequestCode = code;
        }
        if (controlAsyncSubject == null) {
            controlAsyncSubject = AsyncSubject.create();
            appService.controlAgitators(uid, likeCode, comment, dateTimeStamp, latitude, longitude).subscribe(controlAsyncSubject);
        }
        return controlAsyncSubject;
    }
*/


    public Observable<AgitatorControlResponseOld> controlAgitator(String guid, String jsonCodeValues, String comment, String dateTimeStamp, String latitude, String longitude) {

        int code = code(guid, jsonCodeValues, comment, dateTimeStamp, latitude, longitude);

        if (controlRequestCode == INIT_CODE) {
            controlRequestCode = code;
        } else if (controlRequestCode != code) {
            // can't use cached data
            controlAsyncSubject = null;
            controlRequestCode = code;
        }
        if (controlAsyncSubject == null) {
            controlAsyncSubject = AsyncSubject.create();
            appService.controlAgitators(guid, jsonCodeValues, comment, dateTimeStamp, latitude, longitude).subscribe(controlAsyncSubject);
        }
        return controlAsyncSubject;
    }


    public void resetControl(){
        controlAsyncSubject = null;
        controlRequestCode = INIT_CODE;
    }

    public Observable<List<Code>> getCodes(){

        int code = code();

        if (codesRequestCode == INIT_CODE) {
            codesRequestCode = code;
        } else if (codesRequestCode != code) {
            // can't use cached data
            controlCodesSubject = null;
            codesRequestCode = code;
        }
        if (controlCodesSubject == null) {
            controlCodesSubject = AsyncSubject.create();
            appService.getCodes().subscribe(controlCodesSubject);
        }
        return controlCodesSubject;

    }

    public void resetCodes() {
        controlCodesSubject = null;
        codesRequestCode = INIT_CODE;
    }

}
