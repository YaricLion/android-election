package com.itdoors.elections.ui.mvp.view;

public interface ControlView<VM, CONTROL_RESPONSE> extends LoadingView, GPSView, View {

    void showControlLoading();
    void hideControlLoading();

    void showRetry();

    void showInfo(VM model);
    void showFailedInfoRequestMsg(int code);
    void showFailedInfoRequestUnknownMsg();

    void showControlResponse(CONTROL_RESPONSE controlInfo);
    void showFailedControlRequestMsg(int code);
    void showFailedControlRequestUnknownMsg();

    void showControlWillBeSentLater();
    void showDatabaseError();
    void showNoInternetConnection();

}
