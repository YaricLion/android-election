package com.itdoors.elections.ui.mvp.model;

import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.itdoors.elections.data.api.AppService;
import com.itdoors.elections.data.api.model.Project;
import com.itdoors.elections.data.db.DataService;
import com.itdoors.elections.data.model.project.ProjectEntity;
import com.itdoors.elections.data.model.project.ProjectViewModel;
import com.itdoors.elections.util.AsyncSubjectWrapper;
import com.itdoors.elections.ui.mvp.presenter.BasePresenterCallback;
import com.itdoors.elections.ui.mvp.presenter.PresenterCallback;
import com.itdoors.elections.ui.mvp.scope.ProjectsScope;
import com.itdoors.elections.util.Device;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

@ProjectsScope
public class ProjectsModel extends StorageModel {

    private static final int GET_PROJECTS_REQUEST = 1;

    private final AppService appService;
    private final Device device;

    private AsyncSubjectWrapper<List<Project>> asyncSubjectWrapper = new AsyncSubjectWrapper<>();

    @Inject
    public ProjectsModel(DataService dataService, AppService appService, Device device) {
        super(dataService);
        this.appService = appService;
        this.device = device;
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    public Observable<List<ProjectViewModel>> load(boolean loadFromNetwork) {

        if (!isDataServiceAvailable())
            return Observable.error(new IOException("Data storage is Unavailable!"));

        if (loadFromNetwork) {
            return
                    //resubscribe if request in process or previous result already in memory
                    asyncSubjectWrapper.createIfNeedAndSubscribe(GET_PROJECTS_REQUEST, appService.getProjects())
                            .map(ProjectsModel::entities)
                            .observeOn(AndroidSchedulers.mainThread())
                                    //save to db
                            .doOnNext(
                                    entities -> getDataService().writeProjects(entities)
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribe()
                            )
                                    //entities -> projects
                            .map(ProjectsModel::viewModels); // realm don't like "blocking" or "timing" functions from rx

        } else {
            return getDataService().getProjects().map(ProjectsModel::viewModels);
        }

    }

    public void reset() {
        asyncSubjectWrapper = new AsyncSubjectWrapper<>();
    }


    private static ProjectEntity entity(Project project) {
        ProjectEntity projectEntity = new ProjectEntity.Builder()
                .uid(project.getUid())
                .name(project.getName())
                .build();
        return projectEntity;
    }


    private static Project project(ProjectEntity projectEntity) {
        return new Project(projectEntity.getUid(), projectEntity.getName());
    }

    private static ProjectViewModel viewModel(Project project) {
        ProjectViewModel viewModel = new ProjectViewModel.Builder()
                .uid(project.getUid())
                .name(project.getName())
                .build();
        return viewModel;
    }

    private static ProjectViewModel viewModel(ProjectEntity entity) {
        ProjectViewModel viewModel = new ProjectViewModel.Builder()
                .uid(entity.getUid())
                .name(entity.getName())
                .build();
        return viewModel;
    }

    private static List<ProjectViewModel> viewModels(List<ProjectEntity> entities) {
        List<ProjectViewModel> viewModels = new ArrayList<>(entities.size());
        for (ProjectEntity projectEntity : entities) {
            ProjectViewModel viewModel = new ProjectViewModel.Builder()
                    .uid(projectEntity.getUid())
                    .name(projectEntity.getName())
                    .build();
            viewModels.add(viewModel);
        }
        return viewModels;
    }

    private static List<ProjectEntity> entities(List<Project> projects) {
        List<ProjectEntity> projectEntities = new ArrayList<>(projects.size());
        for (Project project : projects) {
            ProjectEntity projectEntity = new ProjectEntity.Builder()
                    .uid(project.getUid())
                    .name(project.getName())
                    .build();
            projectEntities.add(projectEntity);
        }
        return projectEntities;
    }

}
