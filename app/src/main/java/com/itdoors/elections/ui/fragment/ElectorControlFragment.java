package com.itdoors.elections.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.base.Optional;
import com.itdoors.elections.App;
import com.itdoors.elections.R;
import com.itdoors.elections.data.model.Elector;
import com.itdoors.elections.data.model.electorcontrol.ElectorControlResponseOld;
import com.itdoors.elections.ui.BetterViewAnimator;
import com.itdoors.elections.ui.event.SettingsEvent;
import com.itdoors.elections.ui.fragment.dialog.EnableGpsDialogFragment;
import com.itdoors.elections.ui.fragment.dialog.GetCoordinatesDialogFragment;
import com.itdoors.elections.ui.mvp.component.DaggerElectorControlComponent;
import com.itdoors.elections.ui.mvp.component.ElectorControlComponent;
import com.itdoors.elections.ui.mvp.presenter.ElectorControlPresenter;
import com.itdoors.elections.ui.mvp.view.ElectorControlView;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;
import icepick.Icepick;
import icepick.State;
import rx.subjects.PublishSubject;
import timber.log.Timber;

public class ElectorControlFragment extends ControlFragment<ElectorControlComponent, ElectorControlPresenter, Elector, ElectorControlResponseOld> implements ElectorControlView {


    private static final String ELECTOR_GUID_TAG = "ELECTOR_GUID_TAG";

    @Inject  Bus bus;
    @Inject  ElectorControlPresenter presenter;

    @BindString(R.string.error_def_msg)    String failedMsg;
    @BindString(R.string.sent)               String sent;
    @BindString(R.string.data_is_sent)  String dataIsSent;

    @Bind(R.id.elector_animator)   BetterViewAnimator animatorView;
    @Bind(R.id.elector_send_btn)   Button sendControlBtn;

    @Bind(R.id.elector_name)        TextView nameView;
    @Bind(R.id.elector_surname)     TextView surnameView;
    @Bind(R.id.elector_second_name) TextView secondNameView;

    private final PublishSubject<Boolean> controlLoading = PublishSubject.create();

    @State boolean controlRequestActive = false;

    public static ElectorControlFragment newInstance(String electorGUID){

        ElectorControlFragment fragment = new ElectorControlFragment();
        if(electorGUID == null)
            return fragment;

        Bundle args = new Bundle();
        args.putString(ELECTOR_GUID_TAG, electorGUID);
        fragment.setArguments(args);
        return fragment;
    }

    private String getElectorGuid(){
        return getArguments() != null ? getArguments().getString(ELECTOR_GUID_TAG) : null;
    }

    @Override public void onStart() {
        super.onStart();
        bus.register(this);
    }

    @Override public void onStop() {
        super.onStop();
        bus.unregister(this);
    }

    @OnClick(R.id.elector_send_btn)
    public void onSend(Button button){
        onSend();
    }

    @OnClick(R.id.elector_retry)
    public void onRetry(Button button){
        showLoading();
        retry();
    }

    private void onSend(){
        final String guid = getElectorGuid();
        if (guid != null) {
            presenter.sendControl(guid);
        }
    }

    private void retry(){
        String gui = getElectorGuid();
        if (gui != null) {
            presenter.reloadElector(gui);
        }
    }

    private void load() {
        String gui = getElectorGuid();
        if (gui != null) {
            presenter.scanElector(gui);
        }
    }

    @Override protected ElectorControlComponent onCreateNonConfigurationComponent() {
        return DaggerElectorControlComponent.builder().userComponent(App.getUserComponent(getActivity())).build();
    }

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getComponent().inject(this);
        Icepick.restoreInstanceState(this, savedInstanceState);
    }

    @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                                       @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_elector_control, container, false);
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        load();

        controlLoading.subscribe(loading -> {
            controlRequestActive = loading;
            sendControlBtn.setEnabled(!controlRequestActive);
        });

        controlLoading.onNext(controlRequestActive);
    }

    @Override public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Subscribe public void onSettingsClicked(SettingsEvent event){
        Timber.d("On settings clicked");
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }


    @Override public void showEnableGps() {
        new EnableGpsDialogFragment().show(getActivity().getSupportFragmentManager(), "enable_gps");
    }

    @Override public void showControlLoading() {
        controlLoading.onNext(true);
    }

    @Override public void hideControlLoading() {
        controlLoading.onNext(false);
    }

    @Override public void showInfo(Elector elector) {

        Optional<String> name = Optional.fromNullable(elector.getName());
        Optional<String> surname = Optional.fromNullable(elector.getSurname());
        Optional<String> secondName = Optional.fromNullable(elector.getSecondName());

        nameView.setText(name.or("-"));
        surnameView.setText(surname.or("-"));
        secondNameView.setText(secondName.or("-"));

    }

    @Override public void showFailedInfoRequestMsg(int code) {
        Timber.d("Failed control : " + code);
        if(code == 404){
            animatorView.setDisplayedChildId(R.id.elector_not_found_error);
        }
        else {
            Toast.makeText(getActivity().getApplicationContext(), failedMsg, Toast.LENGTH_SHORT).show();
        }
    }

    @Override public void showFailedInfoRequestUnknownMsg() {
        Timber.d("Failed control : unknown ");
        Toast.makeText(getActivity().getApplicationContext(), failedMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showControlResponse(ElectorControlResponseOld controlInfo) {
        Timber.d("Retrofit : control response : " + controlInfo.toString());
        Toast.makeText(getActivity().getApplicationContext(), dataIsSent , Toast.LENGTH_SHORT).show();
        getActivity().finish();
    }

    @Override public void showFailedControlRequestMsg(int code) {
        Toast.makeText(getActivity().getApplicationContext(), failedMsg, Toast.LENGTH_SHORT).show();
    }

    @Override public void showFailedControlRequestUnknownMsg() {
        Toast.makeText(getActivity().getApplicationContext(), failedMsg, Toast.LENGTH_SHORT).show();
    }

    @Override public void showControlWillBeSentLater() {
        Toast.makeText(getActivity().getApplicationContext(), R.string.project_control_added_to_db, Toast.LENGTH_SHORT).show();
        getActivity().finish();
    }

    @Override public void showDatabaseError() {
        animatorView.setDisplayedChildId(R.id.elector_control_db_error);
    }

    @Override public void showNoInternetConnection() {
        Toast.makeText(getActivity().getApplicationContext(), R.string.no_network_msg, Toast.LENGTH_SHORT).show();
    }

    @Override  public void showGetCoordinatesView() {
        new GetCoordinatesDialogFragment().show(getActivity().getSupportFragmentManager(), "coordinates_tag");
    }

    @Override public void hideGetCoordinatesView() {
        DialogFragment fragment = (DialogFragment) getActivity().getSupportFragmentManager().findFragmentByTag("coordinates_tag");
        if(fragment != null){
            //TODO fail to dismiss
            try {
                fragment.dismiss();
            }catch (Throwable th){
                Timber.e(th, "Fail to dismiss");
            }
        }
    }

    @Override public void blockActionViewUntilGetGPSLocation() {
        sendControlBtn.setEnabled(false);
    }

    @Override public void unblockActionViewWhenGPSLocationAvailable() {
        sendControlBtn.setEnabled(true);
    }

    @Override  public void showLoading() {
        animatorView.setDisplayedChildId(R.id.elector_progress);
    }

    @Override public void hideLoading() {
        animatorView.setDisplayedChildId(R.id.elector_content);
    }

    @Override public void showRetry() {
        animatorView.setDisplayedChildId(R.id.elector_error);
    }


}
