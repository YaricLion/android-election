package com.itdoors.elections.ui.event.projects;

/**
 * Created by yariclion on 25.06.16.
 */
public final class RefreshProjectsEvent {

    private final int mode;

    public RefreshProjectsEvent(int mode) {
        this.mode = mode;
    }

    public int getMode() {
        return mode;
    }
}
