package com.itdoors.elections.ui.fragment.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.itdoors.elections.App;
import com.itdoors.elections.R;
import com.itdoors.elections.ui.event.SettingsEvent;
import com.squareup.otto.Bus;

import javax.inject.Inject;

import timber.log.Timber;

public class EnableGpsDialogFragment extends MessageDialogFragment {

    @Inject  Bus eventBus;

    @Nullable
    @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        App.getUserComponent(getActivity()).inject(this);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @NonNull
    @Override public Dialog onCreateDialog(Bundle savedInstanceState) {

        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.need_gps)
                .content(R.string.enable_gps_msg)
                .positiveText(R.string.setting)
                .negativeText(R.string.ok)
                .btnStackedGravity(GravityEnum.END)
                .build();

        View positiveAction = dialog.getActionButton(DialogAction.POSITIVE);
        View negativeAction = dialog.getActionButton(DialogAction.NEGATIVE);

        positiveAction.setOnClickListener(view -> {
                    Timber.d("On settings clicked");
                   eventBus.post(new SettingsEvent());
                   dialog.dismiss();
               }
        );

        negativeAction.setOnClickListener( view -> {
            dialog.dismiss();
        });

        return dialog;

    }

}
