package com.itdoors.elections.ui.mvp.model.prefs;

import com.itdoors.elections.util.Prefs;
import com.itdoors.elections.util.RxPrefs;

public class ApiUrlRxPrefs extends RxPrefs.RxPrefsImpl<String> {

    public ApiUrlRxPrefs(Prefs<String> prefs) {
        super(prefs);
    }
}
