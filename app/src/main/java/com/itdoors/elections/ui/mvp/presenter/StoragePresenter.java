package com.itdoors.elections.ui.mvp.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.itdoors.elections.ui.mvp.model.StorageControlModel;
import com.itdoors.elections.ui.mvp.model.StorageModel;

/**
 * Created by yariclion on 27.06.16.
 */
public abstract class StoragePresenter<MODEL extends StorageModel, VIEW> extends BasePresenter<VIEW> {

    private final MODEL storageModel;
    private PresenterCallback presenterCallback;

    public StoragePresenter(MODEL storageModel) {
        this.storageModel = storageModel;
    }

    public void setPresenterCallback(PresenterCallback presenterCallback) {
        if (presenterCallback == null)
            throw new NullPointerException("PresenterCallback shouldn't be null!");
        this.presenterCallback = presenterCallback;

    }

    protected MODEL getStorageModel() {
        return this.storageModel;
    }

    protected PresenterCallback getPresenterCallback() {
        return this.presenterCallback;
    }

    private void setUpCallback() {
        if (presenterCallback == null) {
            //presenterCallback = new BasePresenterCallback();
            presenterCallback = getStorageModel().createPresenterCallback();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        setUpCallback();
        presenterCallback.onCreate();
    }

    @Override
    public void unbindView() {
        super.unbindView();
        presenterCallback.onUnbindView();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle bundle) {
        super.onSaveInstanceState(bundle);
        presenterCallback.onSaveInstanceState();
    }

    @Override
    public void onDestroy() {
        throw new IllegalStateException("Should always be overriden. To unsubscribe and call presenter.onDestroy() callback!");
    }

    @Override
    public void onStart() {
        super.onStart();
        presenterCallback.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        presenterCallback.onStop();
    }

}
