package com.itdoors.elections.ui.mvp.view;

import com.itdoors.elections.data.model.electorprojectscontrol.ElectorProjectControlResponse;
import com.itdoors.elections.data.model.electorprojectscontrol.ElectorProjectControlViewModel;

/**
 * Created by yariclion on 25.06.16.
 */
public interface ElectorProjectControlView extends OfflineProjectControlView<ElectorProjectControlViewModel, ElectorProjectControlResponse> {


}
