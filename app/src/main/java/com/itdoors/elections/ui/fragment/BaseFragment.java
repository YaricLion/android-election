package com.itdoors.elections.ui.fragment;

import com.itdoors.elections.App;
import com.itdoors.elections.ui.mvp.base.HasPresenter;
import com.itdoors.elections.ui.mvp.presenter.Presenter;
import com.itdoors.elections.ui.mvp.fragment.PresenterControllerFragment;

/**
 * Created by v014nd on 09.09.2015.
 */
public abstract class BaseFragment<COMPONENT extends HasPresenter<PRESENTER>, PRESENTER extends Presenter> extends PresenterControllerFragment<COMPONENT, PRESENTER>{

    @Override public void onDestroy() {
        super.onDestroy();
        App.getRefWatcher(getActivity()).watch(this);
    }


}
