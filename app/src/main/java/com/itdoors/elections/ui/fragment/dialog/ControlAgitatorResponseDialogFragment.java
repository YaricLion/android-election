package com.itdoors.elections.ui.fragment.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.itdoors.elections.App;
import com.itdoors.elections.R;
import com.itdoors.elections.ui.event.ControlAgitatorOkClickedEvent;
import com.squareup.otto.Bus;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by v014nd on 16.09.2015.
 */
public class ControlAgitatorResponseDialogFragment extends MessageDialogFragment{

    @Inject Bus eventBus;

    @Nullable
    @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        App.getUserComponent(getActivity()).inject(this);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @NonNull
    @Override public Dialog onCreateDialog(Bundle savedInstanceState) {

        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.sent)
                .content(R.string.data_is_sent)
                .positiveText(R.string.ok)
                .build();

        View positiveAction = dialog.getActionButton(DialogAction.POSITIVE);

        positiveAction.setOnClickListener(view -> {
                    Timber.d("On settings clicked");
                    eventBus.post(new ControlAgitatorOkClickedEvent());
                    dialog.dismiss();
                }
        );

        return dialog;

    }

}
