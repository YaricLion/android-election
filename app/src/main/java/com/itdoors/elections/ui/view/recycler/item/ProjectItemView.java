package com.itdoors.elections.ui.view.recycler.item;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.common.base.Optional;
import com.itdoors.elections.R;
import com.itdoors.elections.data.model.project.ProjectViewModel;
import com.itdoors.elections.ui.view.recycler.BindableView;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;

/**
 * Created by yariclion on 25.06.16.
 */
public class ProjectItemView extends RelativeLayout implements BindableView<ProjectViewModel> {

    public interface OnProjectClickedListener {
        void onProjectClicked(ProjectViewModel vm);
    }

    @Bind(R.id.project_name)
    TextView nameView;
    @Bind(R.id.project_card_view)
    CardView cardView;
    @BindString(R.string.project_undefined_name)
    String undefined;

    private OnProjectClickedListener onProjectClickedListener;

    public ProjectItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public View getView() {
        return this;
    }

    @Override
    public void bind(ProjectViewModel projectViewModel) {

        String name = Optional.of(projectViewModel.getName()).or(undefined);
        nameView.setText(name);
        cardView.setOnClickListener(v -> {
            if (onProjectClickedListener != null)
                onProjectClickedListener.onProjectClicked(projectViewModel);
        });
    }

    public void setOnProjectClickedListener(OnProjectClickedListener onProjectClickedListener) {
        this.onProjectClickedListener = onProjectClickedListener;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }


}
