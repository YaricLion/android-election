package com.itdoors.elections.ui.fragment;

import android.content.IntentSender;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.itdoors.elections.R;
import com.itdoors.elections.ui.mvp.base.HasPresenter;
import com.itdoors.elections.ui.mvp.presenter.Presenter;
import com.itdoors.elections.ui.mvp.view.ControlView;
import com.itdoors.elections.ui.mvp.view.GPSView;

import timber.log.Timber;

public abstract class LocationFragment<COMPONENT extends HasPresenter<PRESENTER>, PRESENTER extends Presenter> extends BaseFragment<COMPONENT, PRESENTER> implements GPSView {

    public static final int REQUEST_CHECK_SETTINGS = 0;

    @Override
    public void startGPSResolution(Status status) {
        if (status.getStatusCode() == LocationSettingsStatusCodes.RESOLUTION_REQUIRED) {
            try {
                status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
            } catch (IntentSender.SendIntentException th) {
                errorToOpenSetting(th);
            }
        }
    }


}
