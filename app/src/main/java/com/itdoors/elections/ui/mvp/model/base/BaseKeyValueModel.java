package com.itdoors.elections.ui.mvp.model.base;


import com.itdoors.elections.util.EndObserver;

import java.util.LinkedHashMap;
import java.util.Map;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.subjects.PublishSubject;


public abstract class BaseKeyValueModel<K,V> implements ModelKeyValue<K,V> {

    private final Map<K, V> cache = new LinkedHashMap<>();
    private final Map<K, PublishSubject<V>> requests = new LinkedHashMap<>();

    // TODO pull underlying logic into a re-usable component for debouncing and caching last value.
    public Subscription load(final K key, Observer<V> observer) {
        V value = cache.get(key);
        if (value != null) {
            // We have a cached value. Emit it immediately.
            observer.onNext(value);
        }

        PublishSubject<V> request = requests.get(key);
        if (request != null) {
            // There's an in-flight network request for this section already. Join it.
            return request.subscribe(observer);
        }

        request = PublishSubject.create();
        requests.put(key, request);

        Subscription subscription = request.subscribe(observer);

        request.subscribe(new EndObserver<V>() {
            @Override
            public void onEnd() {
                requests.remove(key);
            }

            @Override
            public void onNext(V value) {
                cache.put(key, value);
            }
        });

        subscribe(request);

        return subscription;

    }

    abstract Subscription subscribe(Observable<V> request);



}
