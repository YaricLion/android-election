package com.itdoors.elections.ui.fragment.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.itdoors.elections.App;
import com.itdoors.elections.R;

public class GetCoordinatesDialogFragment extends DialogFragment {

    @Nullable
    @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        App.getUserComponent(getActivity()).inject(this);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @NonNull
    @Override public Dialog onCreateDialog(Bundle savedInstanceState) {

        return new MaterialDialog.Builder(getActivity())
                .title(R.string.gps)
                .content(R.string.get_coordinates)
                .progress(true, 0)
                .show();

    }

}
