package com.itdoors.elections.ui.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;

/**
 * Created by yariclion on 27.06.16.
 */
public class ProjectControlActivity extends BaseActivity {

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
