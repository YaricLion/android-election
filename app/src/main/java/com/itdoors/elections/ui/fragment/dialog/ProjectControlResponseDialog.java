package com.itdoors.elections.ui.fragment.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.common.base.Optional;
import com.itdoors.elections.App;
import com.itdoors.elections.R;
import com.itdoors.elections.data.api.model.ProjectControlState;
import com.itdoors.elections.data.model.Agitator;
import com.itdoors.elections.data.model.Elector;
import com.itdoors.elections.data.model.agitatorprojectscontrol.AgitatorProjectControlResponse;
import com.itdoors.elections.data.model.agitatorprojectscontrol.AgitatorProjectControlViewModel;
import com.itdoors.elections.data.model.electorprojectscontrol.ElectorProjectControlResponse;
import com.itdoors.elections.data.model.electorprojectscontrol.ElectorProjectControlViewModel;
import com.itdoors.elections.ui.event.ResponseOkClickedEvent;
import com.squareup.otto.Bus;

import javax.inject.Inject;

import butterknife.ButterKnife;

/**
 * Created by yariclion on 28.06.16.
 */
public class ProjectControlResponseDialog extends DialogFragment {

    private static final String WHO_TAG = "who";
    private static final String ACTION_TAG = "action";

    private static final String RESPONSE_TAG = "response";
    private static final String VM_TAG = "vm";

    private static final int ELECTOR = 0;
    private static final int AGITATOR = 1;

    public static final int SUBMIT = 0;
    public static final int REJECT = 1;

    @Inject
    Bus bus;

    public static ProjectControlResponseDialog newInstance(AgitatorProjectControlResponse response, int action, AgitatorProjectControlViewModel vm) {

        ProjectControlResponseDialog dialog = new ProjectControlResponseDialog();

        Bundle args = new Bundle();
        args.putInt(WHO_TAG, AGITATOR);
        args.putInt(ACTION_TAG, action);
        args.putParcelable(RESPONSE_TAG, response);
        args.putParcelable(VM_TAG, vm);
        dialog.setArguments(args);

        return dialog;
    }


    public static ProjectControlResponseDialog newInstance(ElectorProjectControlResponse response, int action, ElectorProjectControlViewModel vm) {

        ProjectControlResponseDialog dialog = new ProjectControlResponseDialog();

        Bundle args = new Bundle();
        args.putInt(WHO_TAG, ELECTOR);
        args.putInt(ACTION_TAG, action);
        args.putParcelable(RESPONSE_TAG, response);
        args.putParcelable(VM_TAG, vm);
        dialog.setArguments(args);

        return dialog;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        App.getUserComponent(getActivity()).inject(this);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().setCancelable(false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(R.layout.fragment_control_response, null);
        initDialogView(view);

        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.project_control_response_dialog_title)
                .customView(view, false)
                .positiveText(R.string.ok)
                .build();

        View positiveAction = dialog.getActionButton(DialogAction.POSITIVE);

        positiveAction.setOnClickListener(v -> {
                    bus.post(new ResponseOkClickedEvent());
                    dialog.dismiss();
                }
        );

        return dialog;

    }

    private void initDialogView(View view) {

        TextView whoView = ButterKnife.findById(view, R.id.project_control_response_who);
        TextView surnameView = ButterKnife.findById(view, R.id.project_control_response_surname);
        TextView nameView = ButterKnife.findById(view, R.id.project_control_response_name);
        TextView secondNameView = ButterKnife.findById(view, R.id.project_control_response_second_name);
        TextView msgView = ButterKnife.findById(view, R.id.project_control_response_msg);
        TextView timeView = ButterKnife.findById(view, R.id.project_control_response_time);

        int who = getArguments().getInt(WHO_TAG);
        int action = getArguments().getInt(ACTION_TAG);

        if (who == AGITATOR) {

            AgitatorProjectControlResponse response = getArguments().getParcelable(RESPONSE_TAG);
            AgitatorProjectControlViewModel viewModel = getArguments().getParcelable(VM_TAG);

            Optional<Agitator> agitator = Optional.fromNullable(response.getAgitator());

            String time = response.getScanDateTime();

            String agitatorString = getResources().getString(R.string.agitator);
            whoView.setText(agitatorString);

            if (agitator.isPresent()) {
                surnameView.setText(Optional.fromNullable(agitator.get().getSurname()).or("-"));
                nameView.setText(Optional.fromNullable(agitator.get().getName()).or("-"));
                secondNameView.setText(Optional.fromNullable(agitator.get().getSecondName()).or("-"));
            }
            timeView.setText(Optional.fromNullable(time).or("-"));

            ProjectControlState responseState = response.getControlState();
            ProjectControlState sendState = viewModel.getControlState();

            if (action == SUBMIT) {
                if (sendState == ProjectControlState.UNCONFIRMED) {
                    if (responseState == ProjectControlState.ACCEPTED) {
                        msgView.setText(R.string.agitator_project_control_submit_success_response);
                    }
                } else if (sendState == ProjectControlState.ACCEPTED) {
                    if (responseState == ProjectControlState.ACCEPTED) {
                        msgView.setText(R.string.agitator_project_control_submit_already_done_response);
                    }
                }

            } else if (action == REJECT) {
                if (sendState == ProjectControlState.UNCONFIRMED || sendState == ProjectControlState.ACCEPTED) {
                    if (responseState == ProjectControlState.FAILED) {
                        msgView.setText(R.string.agitator_project_control_reject_success_response);
                    } else {
                        if (responseState == ProjectControlState.FAILED) {
                            msgView.setText(R.string.agitator_project_control_reject_already_done_response);
                        }
                    }
                }
            }
        } else if (who == ELECTOR) {

            ElectorProjectControlResponse response = getArguments().getParcelable(RESPONSE_TAG);
            ElectorProjectControlViewModel viewModel = getArguments().getParcelable(VM_TAG);

            Optional<Elector> elector = Optional.fromNullable(response.getElector());

            String time = response.getScanDateTime();

            String electorString = getResources().getString(R.string.elector);
            whoView.setText(electorString);

            if (elector.isPresent()) {
                surnameView.setText(Optional.fromNullable(elector.get().getSurname()).or("-"));
                nameView.setText(Optional.fromNullable(elector.get().getName()).or("-"));
                secondNameView.setText(Optional.fromNullable(elector.get().getSecondName()).or("-"));
            }
            timeView.setText(Optional.fromNullable(time).or("-"));

            ProjectControlState responseState = response.getControlState();
            ProjectControlState sendState = viewModel.getControlState();

            if (action == SUBMIT) {
                if (sendState == ProjectControlState.UNCONFIRMED) {
                    if (responseState == ProjectControlState.ACCEPTED) {
                        msgView.setText(R.string.elector_project_control_submit_success_response);
                    }
                } else if (sendState == ProjectControlState.ACCEPTED) {
                    if (responseState == ProjectControlState.ACCEPTED) {
                        msgView.setText(R.string.elector_project_control_submit_already_done_response);
                    }
                }

            } else if (action == REJECT) {
                if (sendState == ProjectControlState.UNCONFIRMED || sendState == ProjectControlState.ACCEPTED) {
                    if (responseState == ProjectControlState.FAILED) {
                        msgView.setText(R.string.elector_project_control_reject_success_response);
                    } else {
                        if (responseState == ProjectControlState.FAILED) {
                            msgView.setText(R.string.elector_project_control_reject_already_done_response);
                        }
                    }
                }
            }
        }

    }


}
