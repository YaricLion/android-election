package com.itdoors.elections.ui;

/**
 * Created by v014nd on 07.05.2015.
 */
public final class Intents {

    private static final String PACKAGE_ID = "com.itdoors.elections";

    private Intents(){
    }

    public static final class Agitator{
        public static final String GUID = PACKAGE_ID + ".intents.agitator.id";
    }

    public static final class Project {
        public static final String GUID = PACKAGE_ID + ".intents.project.id";
    }

    public static final class Elector{
        public static final String GUID = PACKAGE_ID + ".intents.elector.id";
    }

    public static final class Login{
        public static final String BLOCKING = PACKAGE_ID + ".intents.blocking_mode";
    }

}
