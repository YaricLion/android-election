package com.itdoors.elections.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.itdoors.elections.App;
import com.itdoors.elections.R;
import com.itdoors.elections.data.api.model.Project;
import com.itdoors.elections.data.model.project.ProjectViewModel;
import com.itdoors.elections.ui.activity.ScanActivity;
import com.itdoors.elections.ui.event.projects.ProjectClickedEvent;
import com.itdoors.elections.ui.event.projects.RefreshProjectsEvent;
import com.itdoors.elections.ui.event.projects.RetryProjectsEvent;
import com.itdoors.elections.ui.mvp.component.DaggerProjectsComponent;
import com.itdoors.elections.ui.mvp.component.ProjectsComponent;
import com.itdoors.elections.ui.mvp.presenter.ProjectsPresenter;
import com.itdoors.elections.ui.view.projects.ProjectsLayoutView;
import com.itdoors.elections.ui.view.storage.StorageControlLayoutView;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import icepick.Icepick;
import timber.log.Timber;

/**
 * Created by yariclion on 24.06.16.
 */
public class ProjectsFragment extends BaseFragment<ProjectsComponent, ProjectsPresenter> {

    @Inject ProjectsPresenter projectsPresenter;
    @Inject Bus bus;

    @Bind(R.id.projects_view) ProjectsLayoutView projectsLayoutView;

    @Override protected ProjectsComponent onCreateNonConfigurationComponent() {
        return DaggerProjectsComponent.builder()
                .userComponent(App.getUserComponent(getActivity()))
                .build();
    }

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getComponent().inject(this);
        Icepick.restoreInstanceState(this, savedInstanceState);
    }

    @Override public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_projects, container, false);
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        load();
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private void load() {
        projectsPresenter.load();
    }

    @Override public void onStart() {
        super.onStart();
        bus.register(this);
    }

    @Override public void onStop() {
        super.onStop();
        bus.unregister(this);
    }

    @Subscribe public void onRetry(RetryProjectsEvent event) {
        int mode = event.getMode();
        projectsPresenter.retry(mode);
    }

    @Subscribe public void onRefresh(RefreshProjectsEvent event) {
        int mode = event.getMode();
        projectsPresenter.refresh(mode);
    }

    @Subscribe public void onProjectClickedEvent(ProjectClickedEvent event) {
        Timber.tag("PROJECT").d("on project clicked received!");
        ProjectViewModel viewModel = event.getProjectViewModel();
        toScanActivity(viewModel);
    }

    private void toScanActivity(ProjectViewModel viewModel) {
        String projectGuid = viewModel.getGuid();
        String projectName = viewModel.getName();
        Intent intent = ScanActivity.newProjectControlInstance(getActivity(), projectGuid, projectName);
        startActivity(intent);
    }
}

