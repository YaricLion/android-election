package com.itdoors.elections.ui.mvp.model.prefs;

import com.itdoors.elections.data.api.model.User;
import com.itdoors.elections.util.Prefs;
import com.itdoors.elections.util.RxPrefs;

public final class UserRxPrefs extends RxPrefs.RxPrefsImpl<User> {
    public UserRxPrefs(Prefs<User> prefs) {
        super(prefs);
    }
}
