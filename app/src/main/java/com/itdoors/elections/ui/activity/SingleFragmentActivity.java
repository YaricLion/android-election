package com.itdoors.elections.ui.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.itdoors.elections.App;
import com.itdoors.elections.AppComponent;
import com.itdoors.elections.ui.ActivityContainer;

import javax.inject.Inject;

public abstract class SingleFragmentActivity extends AppCompatActivity {

    @Inject
    ActivityContainer appContainer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppComponent component = App.getAppComponent(this);
        appContainer = component.activityContainer();
        appContainer.setContainer(this);
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(appContainer.container(), setupFragment())
                    .commit();
        }

    }

    protected abstract Fragment setupFragment();


}
