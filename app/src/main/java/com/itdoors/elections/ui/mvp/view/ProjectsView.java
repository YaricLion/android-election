package com.itdoors.elections.ui.mvp.view;

import java.util.List;

/**
 * Created by yariclion on 25.06.16.
 */
public interface ProjectsView<MODEL> extends View {

    void showLoading();

    void showDatabaseInfo(MODEL info, String lastTimeUpdated);

    void showResponseInfo(MODEL info);

    void showDatabaseError(Throwable th);

    void showResponseError(Throwable th);

    void showDatabaseIsEmpty();

    void showResponseIsEmpty();

    void showNoInternetConnection();

    void showDbError(Throwable th);

}
