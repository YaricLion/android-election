package com.itdoors.elections.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;

import com.itdoors.elections.App;
import com.itdoors.elections.R;
import com.itdoors.elections.data.api.UserComponent;
import com.itdoors.elections.ui.ActivityContainer;
import com.itdoors.elections.ui.event.NoNetworkEvent;
import com.itdoors.elections.ui.event.ToScanEvent;
import com.itdoors.elections.ui.event.login.NeedToLoginAgainEvent;
import com.itdoors.elections.ui.event.login.NeedToLoginEvent;
import com.itdoors.elections.ui.fragment.MainFragment;
import com.itdoors.elections.ui.service.SendService;
import com.squareup.otto.Subscribe;

import java.lang.ref.WeakReference;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by v014nd on 22.06.2016.
 */
public class MainActivity extends BaseActivity {

    @Inject
    ActivityContainer appContainer;

    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    private final Handler handler = new Handler();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UserComponent userComponent = App.getUserComponent(this);
        if (userComponent != null) {
            userComponent.inject(this);
            appContainer.setContainer(this);
            if (savedInstanceState == null) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(appContainer.container(), setupFragment())
                        .commit();
            }
        } else {
            Timber.tag("COMPONENT").d("user component = null");
        }

        //executorService.scheduleAtFixedRate(new SendServiceRunnable(handler, this), 1L , 5L, TimeUnit.SECONDS);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //executorService.shutdownNow();
    }

    private static class SendServiceRunnable implements Runnable {

        private final Handler handler;
        private final WeakReference<Context> contextWeakReference;

        private SendServiceRunnable(Handler handler, Context context) {
            this.handler = handler;
            this.contextWeakReference = new WeakReference<Context>(context);
        }

        @Override
        public void run() {
            Context context = contextWeakReference.get();
            if (context != null) {
                if (!Thread.currentThread().isInterrupted()) {
                    Intent newIntent = new Intent(context, SendService.class);
                    context.startService(newIntent);
                }
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if(fragment != null)
                fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    private Fragment setupFragment() {
        return new MainFragment();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_action_logout:
                Timber.d("Logout");
                logout();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Subscribe
    public void toScanActivity(ToScanEvent event) {
        Intent intent = new Intent(this, ScanActivity.class);
        startActivity(intent);
    }

    @Subscribe
    public void onNeedToLoginAgain(NeedToLoginAgainEvent event) {
        onNeedToLoginAgain();
    }

    @Subscribe
    public void onNeedToLogin(NeedToLoginEvent event) {
        onNeedToLogin();
    }

    @Subscribe
    public void onNoNetworkEvent(NoNetworkEvent event) {
        onNoNetwork();
    }

}

