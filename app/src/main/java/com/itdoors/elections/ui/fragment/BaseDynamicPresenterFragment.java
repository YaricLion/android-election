package com.itdoors.elections.ui.fragment;

import com.itdoors.elections.App;
import com.itdoors.elections.ui.mvp.base.HasPresenter;
import com.itdoors.elections.ui.mvp.fragment.PresenterControllerFragment;
import com.itdoors.elections.ui.mvp.fragment.dynamic.DynamicPresenterControllerFragment;
import com.itdoors.elections.ui.mvp.presenter.Presenter;

/**
 * Created by v014nd on 17.06.2016.
 */
public abstract class BaseDynamicPresenterFragment<COMPONENT extends HasPresenter<PRESENTER>, PRESENTER extends Presenter> extends DynamicPresenterControllerFragment<COMPONENT, PRESENTER> {

    @Override
    public void onDestroy() {
        super.onDestroy();
        App.getRefWatcher(getActivity()).watch(this);
    }

}
