package com.itdoors.elections.util;


import android.support.annotation.NonNull;

import com.rengwuxian.materialedittext.validation.METValidator;

public final class NotEmptyValidator extends METValidator {

    public NotEmptyValidator(@NonNull String errorMessage) {
        super(errorMessage);
    }

    @Override public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
        return !isEmpty && !text.toString().trim().isEmpty();
    }

}