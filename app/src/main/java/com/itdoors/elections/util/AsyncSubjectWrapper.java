package com.itdoors.elections.util;

import com.fernandocejas.frodo.annotation.RxLogObservable;

import rx.Observable;
import rx.subjects.AsyncSubject;

public final class AsyncSubjectWrapper<MODEL> {

    private static final int INIT = -1;

    private AsyncSubject<MODEL> asyncSubject = null;
    private int code = INIT;

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    public AsyncSubject<MODEL> createIfNeedAndSubscribe(int requestCode, Observable<MODEL> observable) {

        if (this.code == INIT) {
            this.code = requestCode;
        } else if (this.code != requestCode) {
            asyncSubject = null;
            this.code = requestCode;
        }

        if (asyncSubject == null) {
            asyncSubject = AsyncSubject.create();
            observable.subscribe(asyncSubject);
        }

        return asyncSubject;
    }


}
