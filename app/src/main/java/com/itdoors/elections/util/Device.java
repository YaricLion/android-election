package com.itdoors.elections.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.IBinder;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import java.io.File;
import java.util.Calendar;

/**
 * Created by v014nd on 27.04.2015.
 */
public final class Device {

    private static final String VERSION_UNAVAILABLE = "N/A";

    private final Context context;

    public Device(Context context){
        this.context = context;
    }

    public boolean isTablet() {

        boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
        boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
        return (xlarge || large);

    }

    public int orientation(){
        return context.getResources().getConfiguration().orientation;
    }

    public DisplayMetrics getMetrics() {

        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(metrics);
        return metrics;

    }

    public int getWidth(){
        return getMetrics().widthPixels;
    }

    public int getHeight(){
        return getMetrics().heightPixels;
    }

    public void hideKeyboardIfShowing(IBinder windowToken){
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0);
    }

    public int dpToPx( int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    public int mmToPx( int mm) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_MM, mm, context.getResources().getDisplayMetrics());
    }

    public int pxToDp(int px){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return (int)dp;
    }

    public String getVersion(){

        String versionName;

        PackageManager pm = context.getPackageManager();
        String packageName = context.getPackageName();

        try {
            PackageInfo info = pm.getPackageInfo(packageName, 0);
            versionName = info.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            versionName = VERSION_UNAVAILABLE;
        }

        return versionName;
    }

    public static void startBrowserLink(Activity activity, String url){

        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;

        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        activity.startActivity(i);

    }

    public boolean isPackageInstalled(String packageName) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
    public void installAppFromMarket(String packageName){

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("market://details?id=" + packageName));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

    }

    public File getCacheDir(){
        File localFile = context.getCacheDir();
        if (localFile == null) {
            localFile = new File(getDefaultCacheDir());
        }
        return localFile;
    }

    protected String getDefaultCacheDir(){
        return "/data/data/com.itdoors.tobividmene/cache/";
    }


    public boolean hasCameraHardware() {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    public boolean hasFlashLight(){
        return context.getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }

    public static long getCurrentDate(){
        return Calendar.getInstance().getTime().getTime() / 1000;
    }

    public Boolean canGetLocation() {

        LocationManager mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        return isGPSEnabled && isNetworkEnabled;
    }

    public static void throwIfOnMainThread() {
        if (isMainThread()) {
            throw new IllegalStateException("Must not be invoked from the main thread.");
        }
    }

    public static void throwIfNotOnMainThread() {
        if (!isMainThread()) {
            throw new IllegalStateException("Must be invoked from the main thread.");
        }
    }

    public static boolean isMainThread() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    public boolean isNetworkAvailable() {
        return isNetworkAvailable(context);
    }

    public static boolean isNetworkAvailable(Context context) {
        final ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivityManager == null) {
            return false;
        }

        final NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo(); // could be null in airplane mode
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }



}
