package com.itdoors.elections.util;

import android.support.annotation.Nullable;

/**
 * Created by v014nd on 11.09.2015.
 */
public final class GuidQrParser {

    private static String TYPE_AND_GUID_REGEX = "(0|1)_[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}";

    public static final int AGITATOR_TYPE = 1;
    public static final int ELECTOR_TYPE = 0;

    private static volatile GuidQrParser singleton = null;


    private GuidQrParser(){

    }

    public static GuidQrParser getInstance(){
        if (singleton == null) {
            synchronized (GuidQrParser.class) {
                if (singleton == null) {
                    singleton = new GuidQrParser();
                }
            }
        }
        return singleton;
    }

    public static boolean matches(String text){
        return text != null && text.matches(TYPE_AND_GUID_REGEX);
    }

    private int getType(String qr){
        if(!matches(qr))
            throw new IllegalArgumentException("Failed qr code : "  + qr);

        String firstLetter = qr.substring(0,1);
        int type = Integer.parseInt(firstLetter);

        return type;
    }

    private String getGuid(String qr){
        if(!matches(qr))
            throw new IllegalArgumentException("Failed qr code : "  + qr);

        String guid = qr.substring(2);
        return guid;

    }

    @Nullable public ParseResult parse(String qr){
        if(!matches(qr))
            return null;
        return new ParseResult(getType(qr), getGuid(qr));
    }


    public static class ParseResult {

        private final int type;
        private final String guid;

        public ParseResult(int type, String gui) {
            this.type = type;
            this.guid = gui;
        }

        public int getType() {
            return type;
        }

        public String getGuid() {
            return guid;
        }

    }
}
