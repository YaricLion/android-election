package com.itdoors.elections.util;

import android.content.Context;

import com.itdoors.elections.data.api.model.TimeRange;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by v014nd on 16.06.2016.
 */
public final class TimeUtils {


    public static final String DEF_DATE_TIME_FORMAT = "HH:mm dd.MM.yyyy ";
    //public static final String DEF_DATE_TIME_FORMAT = "HH:mm dd.MM.yyyy ";
    //public static final String SERVER_TIME_ZONE = "UTC+3";
    //public static final String UTC = "UTC";
    //2016-07-17 08.00.

    public static final String SERVER_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final int GPS_ACCURACY = 10 * 60 * 1000; // 10 min in millis


    private TimeUtils() {
        throw new AssertionError("No instances!");
    }

    public static String formatGps(long gpsTimeUTC) {
        DateFormat dateFormat = new SimpleDateFormat(SERVER_DATE_TIME_FORMAT);
        return dateFormat.format(new Date(gpsTimeUTC));
    }

    public static String format(String unixTimeStamp, String format, Locale locale) {
        long timeMillis = Long.parseLong(unixTimeStamp) * 1000L;
        DateFormat dateFormat = new SimpleDateFormat(format, locale);
        return dateFormat.format(new Date(timeMillis));
    }

    public static String format(Context context, String unixTimeStamp) {
        return format(unixTimeStamp, DEF_DATE_TIME_FORMAT, context.getResources().getConfiguration().locale);
    }


/*
    public static Date toTimeZone(String timeFormat, String timeZone, String time) throws ParseException {
        SimpleDateFormat sourceFormat = new SimpleDateFormat(timeFormat);
        sourceFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
        return sourceFormat.parse(time);
    }
*/


    public static int compare(long gpsTimeStamp, String serverTime) throws ParseException {

        Date gpsDateUTC = new Date(gpsTimeStamp); //UTC
        SimpleDateFormat sourceFormat =  new SimpleDateFormat(SERVER_DATE_TIME_FORMAT);
        Date serverDateUTC = sourceFormat.parse(serverTime); //in UTC
        return gpsDateUTC.compareTo(serverDateUTC);

    }

    //2016-07-13 14:00:33
    //  {"startedAt":"2016-07-13 09:00:00","finishedAt":"2016-07-16 18:00:00"}

    public static boolean inTimeRange(long gpsTime, TimeRange serverTimeRange) throws ParseException {
        return inTimeRange(gpsTime, serverTimeRange, GPS_ACCURACY);
    }

    public static boolean inTimeRange(long gpsTime, TimeRange serverTime, int accuracy) throws ParseException {

        return compare(gpsTime - accuracy, serverTime.getStartTime()) >= 0 && compare(gpsTime + accuracy, serverTime.getEndTime()) <= 0;
    }

    /*public static boolean inTimeRanges(long gpsTime, List<TimeRange> rangeList) throws ParseException {
        for (TimeRange range : rangeList) {
            if (inTimeRange(gpsTime, range))
                return true;
        }
        return false;
    }*/


}
