package com.itdoors.elections.util;

import retrofit.RetrofitError;
import rx.functions.Action0;
import rx.functions.Action1;

/**
 * Created by v014nd on 14.09.2015.
 */
public final class MVPUtils {

    public static final int INIT_CODE  = -1;
    public static final int EMPTY_CODE = +1;


    private MVPUtils(){

    }

    public static boolean isUnauthorizedError(Throwable throwable){
        if(( throwable instanceof RetrofitError )) {
            RetrofitError error = (RetrofitError) throwable;
            if (error.getKind() == RetrofitError.Kind.HTTP &&
                    (error.getResponse().getStatus() == 401 ||
                            error.getResponse().getStatus() == 400)) {
                return true;
            }
        }
        return false;
    }

    public static void onError(Throwable throwable, Action1<Integer> statusAction, Action0 actionUnknown) {

        if (throwable instanceof RetrofitError) {
            RetrofitError error = (RetrofitError) throwable;
            if (error.getKind() == RetrofitError.Kind.HTTP) {
                int status = error.getResponse().getStatus();
                statusAction.call(status);
            } else {
                actionUnknown.call();
            }
        } else {
            actionUnknown.call();
        }
    }

    public static String getStatus(Throwable throwable){
        if (throwable instanceof RetrofitError) {
            RetrofitError error = (RetrofitError) throwable;
            if (error.getKind() == RetrofitError.Kind.HTTP) {
                int status = error.getResponse().getStatus();
                return Integer.toString(status);
            }
        }
        return "UNKNOWN";
    }

    public static boolean isHttp(Throwable throwable) {
        if (throwable instanceof RetrofitError) {
            RetrofitError error = (RetrofitError) throwable;
            if (error.getKind() == RetrofitError.Kind.HTTP) {
               return true;
            }
        }
        return false;
    }

    public static  int code(Object... objects){

        int res = 1;
        for(Object object : objects){
            if(object != null) {
                res = res * object.hashCode();
            }
        }
        return  res;
    }

    public static  int code() {
        return EMPTY_CODE;
    }

}
