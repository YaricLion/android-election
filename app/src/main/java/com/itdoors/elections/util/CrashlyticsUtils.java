package com.itdoors.elections.util;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.google.common.base.Optional;
import com.itdoors.elections.BuildConfig;
import com.itdoors.elections.data.api.model.User;

public final class CrashlyticsUtils {

    private CrashlyticsUtils(){}

    public static void setUserInfo(User user){

        if(BuildConfig.USE_CRASHLYTICS) {

            Optional<Integer> id = Optional.fromNullable(user.getId());
            Optional<String> name = Optional.fromNullable(user.getName());
            Optional<String> email = Optional.fromNullable(user.getEmail());

            CrashlyticsCore core = Crashlytics.getInstance().core;
            core.setUserIdentifier(id.or(-1).toString());
            core.setUserName(name.or("?"));
            core.setUserEmail(email.or("?"));

        }
    }

}
