package com.itdoors.elections.util;

import android.content.Context;
import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.itdoors.elections.BuildConfig;
import com.itdoors.elections.R;
import com.itdoors.elections.data.model.agitatorcontrol.AgitatorControlEntity;
import com.itdoors.elections.data.model.electorcontrol.ElectorControlEntity;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public final class StubRestResourcesLoader {

    private final Context context;
    private final Gson gson;

    public StubRestResourcesLoader(Context context, Gson gson) {
        this.context = context;
        this.gson = gson;
    }

    public <MODEL> List<MODEL> loadList(int resourceId) {
        if (!BuildConfig.DEBUG)
            Device.throwIfOnMainThread();

        Timber.d("STUB REST : loading List:" + resourceId);
        Resources res = context.getResources();
        Reader reader = null;
        try {
            reader = new InputStreamReader(res.openRawResource(resourceId));
            Type listType = new TypeToken<ArrayList<MODEL>>() {
            }.getType();
            List<MODEL> panoramas = gson.fromJson(reader, listType);
            return panoramas;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignore) {
                }
            }
        }

    }

    public static List<ElectorControlEntity> loadElectorControlEntities(Context context, Gson gson) {

        Timber.tag("STORAGE").d("Loading resource entities: electors");
        Resources res = context.getResources();
        Reader reader = null;
        try {
            reader = new InputStreamReader(res.openRawResource(R.raw.stub_elector_control_entity));
            Type listType = new TypeToken<ArrayList<ElectorControlEntity>>() {
            }.getType();
            List<ElectorControlEntity> entities = gson.fromJson(reader, listType);
            return entities;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignore) {
                }
            }
        }

    }

    public static <E> List<E> loadEntities(Context context, Gson gson, Type listType) {

        Timber.tag("STORAGE").d("Loading resource entities:");
        Resources res = context.getResources();
        Reader reader = null;
        try {
            reader = new InputStreamReader(res.openRawResource(R.raw.stub_agitator_control_entity));
            List<E> entities = gson.fromJson(reader, listType);
            return entities;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignore) {
                }
            }
        }

    }

    public static List<AgitatorControlEntity> loadAgitatorControlEntities(Context context, Gson gson) {

        Timber.tag("STORAGE").d("Loading resource entities: agitators");
        Resources res = context.getResources();
        Reader reader = null;
        try {
            reader = new InputStreamReader(res.openRawResource(R.raw.stub_agitator_control_entity));
            Type listType = new TypeToken<ArrayList<AgitatorControlEntity>>() {
            }.getType();
            List<AgitatorControlEntity> entities = gson.fromJson(reader, listType);
            return entities;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignore) {
                }
            }
        }

    }
}
