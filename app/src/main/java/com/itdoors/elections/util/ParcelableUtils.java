package com.itdoors.elections.util;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.common.base.Optional;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by v014nd on 22.09.2015.
 */
public final class ParcelableUtils {

    private static String LEN_CODE = "len";

    private ParcelableUtils(){}

    public static <K extends Parcelable, V extends Parcelable>  void writeMap(Map<K,V> map, Bundle bundle) {

        if(map != null && map.size() > 0) {

            Set<Map.Entry<K, V>> set = map.entrySet();

            int len = set.size();
            bundle.putInt(LEN_CODE, len);

            int index = 0;
            for(Map.Entry<K,V> entry : set){
                K key   = entry.getKey();
                V value = entry.getValue();
                bundle.putParcelableArray(Integer.toString(index), new Parcelable[]{key, value});
                index ++;
            }
        }

    }

    public static <K extends Parcelable, V extends Parcelable> Map<K,V> readMap(Bundle bundle) {

        Map<K,V> map = null;

        if(bundle != null) {

            map = new HashMap<>();
            int len = bundle.getInt(LEN_CODE);

            if(len > 0){

                for(int index = 0; index < len; index ++){

                    Parcelable[] keyValue = bundle.getParcelableArray(Integer.toString(index));
                    if(keyValue == null || keyValue.length != 2)
                        throw new IllegalArgumentException(" Failed parcel keyValue pair ");

                    K key   = (K) keyValue[0];
                    V value = (V) keyValue[1];

                    map.put(key, value);

                }

            }
        }


        return map;
    }

}
