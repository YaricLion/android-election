package com.itdoors.elections.util;

import rx.Observable;

public interface RxPrefs<T> {

    Observable<Boolean> set(T value);
    Observable<T> get();
    Observable<Boolean> isSet();
    Observable<Boolean> delete();

    class RxPrefsImpl<T> implements RxPrefs<T> {

        private final Prefs<T> prefs;

        public RxPrefsImpl(Prefs<T> prefs) {
            this.prefs = prefs;
        }

        @Override public Observable<Boolean> set(T value) {
            return Observable.defer(() -> {
                prefs.set(value);
                return Observable.just(true);
            });
        }

        @Override public Observable<T> get(){
            return Observable.defer(() -> Observable.just(prefs.isSet() ? prefs.get() : null));
        }

        @Override public Observable<Boolean> isSet() {
            return Observable.defer(() -> Observable.just(prefs.isSet()));
        }

        @Override public Observable<Boolean> delete() {
            return Observable.defer(() -> {
                prefs.delete();
                return Observable.just(true);
            });
        }
    }


}
