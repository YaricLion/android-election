package com.itdoors.elections.util;

/**
 * Created by v014nd on 08.06.2016.
 */
public class Compare {
    public static int compare(int lhs, int rhs) {
        return lhs < rhs ? -1 : (lhs == rhs ? 0 : 1);
    }
}
