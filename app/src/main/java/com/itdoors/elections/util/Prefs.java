package com.itdoors.elections.util;

import com.google.gson.Gson;

/**
 * Created by v014nd on 09.09.2015.
 */
public interface Prefs<T> {

    void set(T value);
    T get();
    boolean isSet();
    void delete();


    class PrefsImpl<T> implements Prefs<T> {

        private final StringPreference prefs;
        private final Gson gson;
        private final Class<T> clazz;

        public PrefsImpl(StringPreference prefs, Gson gson, Class<T> clazz) {
            this.prefs = prefs;
            this.gson = gson;
            this.clazz = clazz;
        }

        public void set(T value) {
            prefs.set(gson.toJson(value));
        }

        public T get() {
            return gson.fromJson(prefs.get(), clazz);
        }

        public boolean isSet() {
            return prefs.isSet();
        }

        public void delete() {
            prefs.delete();
        }
    }


}
