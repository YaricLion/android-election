package com.itdoors.elections.util;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.google.common.base.Optional;

import timber.log.Timber;

public class CrashReportingTree extends Timber.HollowTree {

    @Override public void i(String message, Object... args) {
    }

    @Override public void i(Throwable t, String message, Object... args) {
        i(message, args);
    }

    @Override public void e(String message, Object... args) {
        i("ERROR: " + message, args);
    }

    @Override public void e(Throwable t, String message, Object... args) {
        e(message, args);
        CrashlyticsCore core = Crashlytics.getInstance().core;
        Optional<String> msg = Optional.fromNullable(message);
        core.log(String.format(msg.or("?"),args));
        core.logException(t);
    }

}