package com.itdoors.elections;

import android.app.Application;

import com.itdoors.elections.util.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public final class AppModule {

    private final App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Provides @PerApp Application provideApplication() {
        return app;
    }

    @Provides SchedulerProvider provideSchedulerProvider() {
        return SchedulerProvider.DEFAULT;
    }

}