package com.itdoors.elections;

import android.content.ComponentCallbacks2;
import android.content.Context;
import android.os.StrictMode;

import com.crashlytics.android.Crashlytics;
import com.itdoors.elections.data.api.ApiModule;
import com.itdoors.elections.data.api.DaggerUserComponent;
import com.itdoors.elections.data.api.UserComponent;
import com.itdoors.elections.data.api.UserModule;
import com.itdoors.elections.data.api.model.User;
import com.itdoors.elections.data.db.DbModule;
import com.itdoors.elections.data.db.realm.RealmAgitatorControl;
import com.itdoors.elections.data.db.realm.RealmAgitatorProjectControl;
import com.itdoors.elections.data.db.realm.RealmCodeValue;
import com.itdoors.elections.data.db.realm.RealmElectionDay;
import com.itdoors.elections.data.db.realm.RealmElectorControl;
import com.itdoors.elections.data.db.realm.RealmElectorProjectControl;
import com.itdoors.elections.ui.mvp.base.ComponentCacheApplication;
import com.itdoors.elections.ui.mvp.model.prefs.ApiUrlRxPrefs;
import com.itdoors.elections.ui.mvp.model.prefs.UserRxPrefs;
import com.itdoors.elections.util.CrashReportingTree;
import com.itdoors.elections.util.CrashlyticsUtils;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import com.squareup.picasso.LruCache;

import net.danlew.android.joda.JodaTimeAndroid;

import javax.inject.Inject;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
//import io.realm.RealmConfiguration;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmResults;
import timber.log.Timber;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class App extends ComponentCacheApplication {

    private AppComponent appComponent;
    private UserComponent userComponent;

    private RefWatcher refWatcher;

    @Inject LruCache picassoMemCache;
    @Inject
    UserRxPrefs userRxPrefs;
    @Inject
    ApiUrlRxPrefs apiUrlRxPrefs;

    //@Inject RealmConfiguration realmConfiguration;

    @Override public void onCreate() {
        super.onCreate();

        JodaTimeAndroid.init(this);
        refWatcher = LeakCanary.install(this);

        buildComponentAndInject();

        RealmConfiguration config = new RealmConfiguration.Builder(this).build();
        Realm.setDefaultConfiguration(config);

        if (BuildConfig.DEBUG) {

            Timber.plant(new Timber.DebugTree());
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectDiskReads()
                    .detectDiskWrites()
                    .detectNetwork()
                    .penaltyLog()
                    .build());

            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectLeakedSqlLiteObjects()
                    .detectLeakedClosableObjects()
                    .penaltyLog()
                    .build());
        } else {

            if (BuildConfig.USE_CRASHLYTICS) {

                Fabric.with(this, new Crashlytics());
                Timber.plant(new CrashReportingTree());

                if (userRxPrefs.isSet().toBlocking().first()) {
                    User user = userRxPrefs.get().toBlocking().first();
                    CrashlyticsUtils.setUserInfo(user);
                }
            }

        }

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/Roboto-Regular.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );

        createUserComponentIfLoggedIn();

        dbProjectsCheck();
        dbControlCheck();
        dbTimeCheck();

    }


    @Override public void onLowMemory() {
        clearPicassoMemCache();
        super.onLowMemory();
    }

    @Override public void onTrimMemory(int level) {
        if(level >= ComponentCallbacks2.TRIM_MEMORY_MODERATE){
            clearPicassoMemCache();
        }
        super.onTrimMemory(level);
    }

    private void clearPicassoMemCache(){
        if(picassoMemCache != null)
            picassoMemCache.evictAll();
    }

    public static RefWatcher getRefWatcher(Context context) {
        App application = (App) context.getApplicationContext();
        return application.refWatcher;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    /*

    private RealmConfiguration getRealmConfig(){

        if(BuildConfig.DEBUG) {

            return new RealmConfiguration.Builder(this)
                    .name("test.realm")
                    .inMemory()
                    .build();


        }

        return null;
    }
*/

    public void buildComponentAndInject() {

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                        //.dbModule(new DbModule(getRealmConfig()))
                .dbModule(new DbModule())
                .build();
        appComponent.inject(this);

    }

    public boolean isUserLoggedIn() {
        return userRxPrefs.isSet().toBlocking().first() && apiUrlRxPrefs.isSet().toBlocking().first();

    }

    private void createUserComponentIfLoggedIn() {

        if (userRxPrefs.isSet().toBlocking().first() && apiUrlRxPrefs.isSet().toBlocking().first()) {
            User user = userRxPrefs.get().toBlocking().first();
            String apiUrl = apiUrlRxPrefs.get().toBlocking().first();
            String email = user.getEmail();

            createUserComponent(apiUrl, email);
        } else {
            Timber.tag("COMPONENT").d("Creating user component : Not logged");
        }
    }

    public UserComponent createUserComponent(String apiUrl, String email) {

        userComponent = DaggerUserComponent.builder()
                .appComponent(appComponent)
                .userModule(new UserModule(apiUrl, email))
                .apiModule(new ApiModule())
                .build();

        return userComponent;

    }

    public void releaseUserComponent() {
        userComponent = null;
    }

    public UserComponent getUserComponent() {
        return userComponent;
    }


    public static AppComponent getAppComponent(Context context) {
        return ((App) context.getApplicationContext()).appComponent;
    }

    public static UserComponent getUserComponent(Context context) {
        return ((App) context.getApplicationContext()).userComponent;
    }

    public static UserComponent createUserComponent(Context context, String url, String email) {
        return ((App) context.getApplicationContext()).createUserComponent(url, email);
    }

    public static void releaseUserComponent(Context context) {
        ((App) context.getApplicationContext()).releaseUserComponent();
    }


    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }


    private void dbProjectsCheck() {

        if (BuildConfig.DEBUG) {

            Realm realm = Realm.getDefaultInstance();

            RealmResults<RealmElectorProjectControl> elPrResults = realm.where(RealmElectorProjectControl.class).findAll();
            RealmResults<RealmAgitatorProjectControl> agPrResults = realm.where(RealmAgitatorProjectControl.class).findAll();

            StringBuilder elPrSb = new StringBuilder();
            elPrSb.append("[");
            for (RealmElectorProjectControl control : elPrResults) {
                elPrSb.append("{");
                elPrSb.append("sp :").append(control.getSupporterGuid()).append(";");
                elPrSb.append("pr :").append(control.getProjectGui()).append(";");
                elPrSb.append("st :").append(control.getScanDateTime()).append(";");
                elPrSb.append("lt :").append(control.getLatitude()).append(";");
                elPrSb.append("ln :").append(control.getLongitude()).append(";");
                elPrSb.append("}");
            }
            elPrSb.append("]");

            StringBuilder arPrSb = new StringBuilder();
            arPrSb.append("[");
            for (RealmAgitatorProjectControl control : agPrResults) {
                arPrSb.append("{");
                arPrSb.append("ag :").append(control.getAgitatorGuid()).append(";");
                arPrSb.append("pr :").append(control.getProjectGui()).append(";");
                arPrSb.append("st :").append(control.getScanDateTime()).append(";");
                arPrSb.append("lt :").append(control.getLatitude()).append(";");
                arPrSb.append("ln :").append(control.getLongitude()).append(";");
                arPrSb.append("}");
            }
            arPrSb.append("]");

            realm.close();

            Timber.tag("STORAGE_TEST").d("AgitatorPrCont:" + arPrSb.toString());
            Timber.tag("STORAGE_TEST").d("ElectorPrCont:" + elPrSb.toString());
        }
    }

    private void dbControlCheck() {

        if (BuildConfig.DEBUG) {

            Realm realm = Realm.getDefaultInstance();

            RealmResults<RealmElectorControl> elPrResults = realm.where(RealmElectorControl.class).findAll();
            RealmResults<RealmAgitatorControl> agPrResults = realm.where(RealmAgitatorControl.class).findAll();

            StringBuilder elSb = new StringBuilder();
            elSb.append("[");
            for (RealmElectorControl control : elPrResults) {
                elSb.append("{");
                elSb.append("uid :").append(control.getUid()).append(";");
                elSb.append("tm :").append(control.getTimestamp()).append(";");
                elSb.append("lt :").append(control.getLatitude()).append(";");
                elSb.append("ln :").append(control.getLongitude()).append(";");
                elSb.append("}");
            }
            elSb.append("]");

            StringBuilder arSb = new StringBuilder();
            arSb.append("[");
            for (RealmAgitatorControl control : agPrResults) {
                arSb.append("{");
                arSb.append("uid :").append(control.getUid()).append(";");
                arSb.append("cm :").append(control.getComment()).append(";");

                RealmList<RealmCodeValue> codeValues = control.getCodeValues();
                arSb.append("cv :");
                for(RealmCodeValue codeValue : codeValues){
                    arSb.append("{" +codeValue.getCode()  + ", "  + codeValue.getValue() +"}");
                }
                arSb.append(";");
                arSb.append("st :").append(control.getTimestamp()).append(";");
                arSb.append("lt :").append(control.getLatitude()).append(";");
                arSb.append("ln :").append(control.getLongitude()).append(";");
                arSb.append("}");
            }
            arSb.append("]");

            realm.close();

            Timber.tag("STORAGE_TEST").d("AgitatorControlCont:" + arSb.toString());
            Timber.tag("STORAGE_TEST").d("ElectorControlCont:" + elSb.toString());
        }
    }

    private void dbTimeCheck(){

        if (BuildConfig.DEBUG) {

            Realm realm = Realm.getDefaultInstance();
            RealmResults<RealmElectionDay> edResults = realm.where(RealmElectionDay.class).findAll();
            StringBuilder edSb = new StringBuilder();
            edSb.append("[");
            for (RealmElectionDay electionDay : edResults) {
                edSb.append("{");
                edSb.append("s :").append(electionDay.getStart()).append(";");
                edSb.append("f :").append(electionDay.getFinish()).append(";");
                edSb.append("}");
            }
            edSb.append("]");
            realm.close();

            Timber.tag("STORAGE_TEST").d("Election Day:" + edSb.toString());
        }
    }
}
