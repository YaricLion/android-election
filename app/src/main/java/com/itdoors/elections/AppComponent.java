package com.itdoors.elections;

import android.app.Application;

import com.google.gson.Gson;
import com.itdoors.elections.data.DataModule;
import com.itdoors.elections.data.api.AppService;
import com.itdoors.elections.data.api.model.oauth.AccessToken;
import com.itdoors.elections.data.db.DataService;
import com.itdoors.elections.data.db.DbModule;
import com.itdoors.elections.ui.ActivityContainer;
import com.itdoors.elections.ui.UiModule;
import com.itdoors.elections.ui.activity.LoginActivity;
import com.itdoors.elections.ui.mvp.model.prefs.ApiUrlRxPrefs;
import com.itdoors.elections.ui.mvp.model.prefs.TokenRxPrefs;
import com.itdoors.elections.ui.mvp.model.prefs.UserRxPrefs;
import com.itdoors.elections.ui.receiver.NetworkChangeReceiver;
import com.itdoors.elections.ui.service.SendService;
import com.itdoors.elections.util.Device;
import com.itdoors.elections.util.GuidQrParser;
import com.itdoors.elections.util.Prefs;
import com.itdoors.elections.util.SchedulerProvider;
import com.squareup.otto.Bus;
import com.squareup.picasso.Picasso;

import javax.inject.Named;

import dagger.Component;
import okhttp3.OkHttpClient;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;

@PerApp
@Component(
        modules = {
                AppModule.class,
                DataModule.class,
                UiModule.class,
                DbModule.class
                //ApiModule.class,
        }

)
public interface AppComponent {

    void inject(App app);
    void inject(LoginActivity activity);

    void inject(NetworkChangeReceiver receiver);

    void inject(SendService service);

    Application app();

    TokenRxPrefs tokenHelper();

    UserRxPrefs userHelper();

    ApiUrlRxPrefs apiUrlRxPrefs();

    Bus bus();
    Picasso picasso();
    Gson gson();
    Device device();
    SchedulerProvider sheSchedulerProvider();
    ReactiveLocationProvider locationProvider();

    ActivityContainer activityContainer();

    DataService dataService();

    GuidQrParser guidQrParser();

    @Named("tokenPrefs")
    Prefs<AccessToken> tokenPrefsHelper();

    OkHttpClient okHttpClient();


}